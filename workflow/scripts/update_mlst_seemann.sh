#!/usr/bin/env bash
#date: November, 2024

# modified from https://github.com/tseemann/mlst/tree/master/scripts 



mkdir -p ${snakemake_output[db_dir]}
mkdir -p ${snakemake_params[blast_dir]}

echo -ne "" > ${snakemake_output[blast_file]}
MLST_PATH=$(dirname `which mlst`)


timeout 5m perl "$MLST_PATH"/../scripts/mlst-download_pub_mlst -d ${snakemake_output[db_dir]} -j ${snakemake[threads]} &> ${snakemake_log[0]}


MLSTDIR="${snakemake_output[db_dir]}"
BLASTDIR="${snakemake_params[blast_dir]}"
BLASTFILE="${snakemake_output[blast_file]}"

for N in $(find $MLSTDIR -mindepth 1 -maxdepth 1 -type d); do
  SCHEME=$(basename $N)
  cat "$MLSTDIR"/$SCHEME/*.tfa | grep -v 'not a locus' | sed -e "s/^>/>$SCHEME./" >> "$BLASTFILE"
done

makeblastdb -hash_index -in ${snakemake_output[blast_file]} -dbtype nucl -title "PubMLST" -parse_seqids
