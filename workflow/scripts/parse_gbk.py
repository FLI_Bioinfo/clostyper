import argparse
import pandas as pd
from Bio import SeqIO
from collections import Counter
import os


def parse_gbk(file_path, sample_name):
    """Parses a GenBank file and returns a dictionary of sequence statistics."""
    stats = {
        "Sample": sample_name,
        "CDS": 0,
        "tRNA": 0,
        "tmRNA": 0,
        "rRNA": 0,
        "ncRNA": 0,
        "Coding_Density": 0.0,
    }

    total_length = 0
    coding_length = 0
    for record in SeqIO.parse(file_path, "genbank"):
        total_length += len(record.seq)

        feature_counts = Counter(f.type.lower() for f in record.features)

        stats["CDS"] += feature_counts.get("cds", 0)
        stats["tRNA"] += feature_counts.get("trna", 0)
        stats["tmRNA"] += feature_counts.get("tmrna", 0)
        stats["rRNA"] += feature_counts.get("rrna", 0)
        stats["ncRNA"] += feature_counts.get("ncrna", 0)

        # Sum up coding length for coding density calculation
        for feature in record.features:
            if feature.type == "CDS":
                coding_length += len(feature.extract(record.seq))

    # Compute the coding density as the percentage of coding length over total length
    stats["Coding_Density"] = round((coding_length / total_length) * 100, 2) if total_length > 0 else 0
    # Average GC content over all contigs

    return stats

def main():
    parser = argparse.ArgumentParser(description="Parse GenBank (.gbk) files and extract statistics.")
    parser.add_argument("-o", "--output", required=True, help="Output TSV file")
    parser.add_argument("-i", "--input", nargs="+", required=True, help="Input GBK files (space-separated)")

    args = parser.parse_args()

    all_stats = []
    
    for gbk_file in args.input:
        sample_name = os.path.basename(gbk_file).replace(".gbk", "")  # Extract sample name
        stats = parse_gbk(gbk_file, sample_name)
        all_stats.append(stats)

    # Convert to DataFrame and save as TSV
    df = pd.DataFrame(all_stats)
    df.to_csv(args.output, sep="\t", index=False)
    print(f"Results saved to {args.output}")

if __name__ == "__main__":
    main()
