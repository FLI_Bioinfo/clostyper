#!/usr/bin/env python
# Copyright (C) <2015> EMBL-European Bioinformatics Institute

"""
This script was modified by Mostafa Abdel-Glil on 03.02.2025 and named pangenome_plots.py. The original script is roary_plots.py. 
The distribution of this script should not violate the license of the original author and instiution.
"""

# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# Neither the institution name nor the name roary_plots
# can be used to endorse or promote products derived from
# this software without prior written permission.
# For written permission, please contact <marco@ebi.ac.uk>.

# Products derived from this software may not be called roary_plots
# nor may roary_plots appear in their names without prior written
# permission of the developers. You should have received a copy
# of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

__Original_author__ = "Marco Galardini" 
__version__ = '0.1.0'

def get_options():
    import argparse

    # create the top-level parser
    description = "Create plots from roary outputs"
    parser = argparse.ArgumentParser(description = description,
                                     prog = 'roary_plots.py')

    parser.add_argument('tree', action='store',
                        help='Newick Tree file', default='accessory_binary_genes.fa.newick')
    parser.add_argument('spreadsheet', action='store',
                        help='Roary gene presence/absence spreadsheet', default='gene_presence_absence.csv')

    parser.add_argument('--labels', action='store_true',
                        default=False,
                        help='Add node labels to the tree (up to 10 chars)')
    parser.add_argument('--format',
                        choices=('png',
                                 'tiff',
                                 'pdf',
                                 'svg'),
                        default='png',
                        help='Output format [Default: png]')
    parser.add_argument('-N', '--skipped-columns', action='store',
                        type=int,
                        default=14,
                        help='First N columns of Roary\'s output to exclude [Default: 14]')
    
    parser.add_argument('--version', action='version',
                         version='%(prog)s '+__version__)

    return parser.parse_args()

if __name__ == "__main__":
    options = get_options()

    import matplotlib
    matplotlib.use('Agg')

    import matplotlib.pyplot as plt
    import seaborn as sns

    sns.set_style('white')

    import os
    import pandas as pd
    import numpy as np
    from Bio import Phylo

    t = Phylo.read(options.tree, 'newick')

    # Max distance to create better plots
    mdist = max([t.distance(t.root, x) for x in t.get_terminals()])

    # Load roary
    roary = pd.read_csv(options.spreadsheet, low_memory=False)
    # Set index (group name)
    roary.set_index('Gene', inplace=True)
    # Drop the other info columns
    roary.drop(list(roary.columns[:options.skipped_columns-1]), axis=1, inplace=True)

    # Transform it in a presence/absence matrix (1/0)
    roary.replace('.{2,100}', 1, regex=True, inplace=True)
    roary.replace(np.nan, 0, regex=True, inplace=True)

    # Sort the matrix by the sum of strains presence
    idx = roary.sum(axis=1).sort_values(ascending=False).index
    roary_sorted = roary.loc[idx]

    # Pangenome frequency plot
    plt.figure(figsize=(7, 5))

    plt.hist(roary.sum(axis=1), roary.shape[1],
             histtype="stepfilled", alpha=.7)

    plt.xlabel('No. of genomes')
    plt.ylabel('No. of genes')

    sns.despine(left=True,
                bottom=True)
    plt.savefig('pangenome_frequency.%s'%options.format, dpi=300)
    plt.clf()

    # Sort the matrix according to tip labels in the tree
    roary_sorted = roary_sorted[[x.name for x in t.get_terminals()]]
    
    # Ensure that all values in the matrix are numeric (integers or floats)
    roary_sorted = roary_sorted.apply(pd.to_numeric, errors='coerce')  # Convert non-numeric to NaN
    roary_sorted.fillna(0, inplace=True)  # Replace NaN values with 0 (absence)
    
    # Plot presence/absence matrix against the tree
    with sns.axes_style('whitegrid'):
        fig = plt.figure(figsize=(17, 10))
    
        ax1 = plt.subplot2grid((1, 40), (0, 10), colspan=30)
        ax1.matshow(roary_sorted.T, cmap=plt.cm.Blues,
                    vmin=0, vmax=1,
                    aspect='auto',
                    interpolation='none')
        ax1.set_yticks([])
        ax1.set_xticks([])
        ax1.axis('off')
    
        ax = fig.add_subplot(1, 2, 1)
        try:
            ax = plt.subplot2grid((1, 40), (0, 0), colspan=10, facecolor='white')
        except AttributeError:
            ax = plt.subplot2grid((1, 40), (0, 0), colspan=10, axisbg='white')
    
        fig.subplots_adjust(wspace=0, hspace=0)
    
        ax1.set_title('Roary matrix\n(%d gene clusters)' % roary.shape[0])
        
        buffer = 0.1  # Small buffer to prevent identical limits
        xlim_left = -mdist * 0.1
        xlim_right = mdist + mdist * 0.45 - mdist * roary.shape[1] * 0.001

        # If the difference is too small, expand the limits a bit
        if abs(xlim_right - xlim_left) < buffer:
            xlim_right = xlim_left + buffer

        ax.set_xlim(xlim_left, xlim_right)
    
        if options.labels:
            fsize = 12 - 0.1 * roary.shape[1]
            if fsize < 7:
                fsize = 7
            with plt.rc_context({'font.size': fsize}):
                Phylo.draw(t, axes=ax,
                        show_confidence=False,
                        label_func=lambda x: str(x)[:10],
                        xticks=([],), yticks=([],),
                        ylabel=('',), xlabel=('',),
                        xlim=(xlim_left, xlim_right),  # adjusted xlim
                        #xlim=(-mdist * 0.1, mdist + mdist * 0.45 - mdist * roary.shape[1] * 0.001),
                        axis=('off',),
                        title=('Tree\n(%d strains)' % roary.shape[1],),
                        do_show=False,
                        )
        else:
            Phylo.draw(t, axes=ax,
                    show_confidence=False,
                    label_func=lambda x: None,
                    xticks=([],), yticks=([],),
                    ylabel=('',), xlabel=('',),
                    xlim=(xlim_left, xlim_right),
                    #xlim=(-mdist * 0.1, mdist + mdist * 0.1),
                    axis=('off',),
                    title=('Tree\n(%d strains)' % roary.shape[1],),
                    do_show=False,
                    )    
        plt.savefig('pangenome_matrix.%s' % options.format, dpi=300)
        plt.clf()

    # Plot the pangenome pie chart
    plt.figure(figsize=(10, 10))

    # Calculate the number of genes in each category
    core = roary[(roary.sum(axis=1) >= roary.shape[1]*0.95)].shape[0]  # Genes present in >=95% of the strains
    accessory = roary[(roary.sum(axis=1) > 1) & (roary.sum(axis=1) < roary.shape[1]*0.95)].shape[0]  # Genes in more than one strain but <95%
    singleton = roary[(roary.sum(axis=1) == 1)].shape[0]  # Genes present in only one strain

    total = roary.shape[0]
    num_genomes = roary.shape[1]

    # Prepare labels and data, removing categories with zero genes
    labels = []
    sizes = []
    # colors = []
    colors = ["#3498db", "#9b59b6", "#2ecc71"]  # Bright Blue, Purple, Vibrant Green
    explode = []

    if core > 0:
        labels.append(f'Core genome\n\n 95 -100 % of genomes,\n n = {core} genes')
        sizes.append(core)
        colors.append((0, 0, 1, float(core) / total))
        explode.append(0.05)

    if accessory > 0:
        labels.append(f'Accessory genome\n\nNon-singletons in < 95% of genomes,\n n = {accessory} genes')
        sizes.append(accessory)
        colors.append((0.1, 0.6, 1, float(accessory) / total))
        explode.append(0.05)

    if singleton > 0:
        labels.append(f'Singleton\n\nGenes exclusive in one genome,\n n = {singleton} genes')
        sizes.append(singleton)
        colors.append((0.6, 0.8, 1, float(singleton) / total))
        explode.append(0.05)

    # Define function to display actual numbers on the pie chart
    def my_autopct(pct):
        val = int(round(pct * total / 100.0))
        return f'{val} genes' if val > 0 else ''

    # Plot the pie chart
    plt.pie(sizes, 
        labels=labels, 
        explode=explode, 
        radius=0.85, 
        colors=colors, 
        autopct=my_autopct, 
        wedgeprops={'edgecolor': 'black'},  # Adds a border to slices
        textprops={'fontsize': 12},
        labeldistance=1.15,  # Moves labels outward for clarity
        pctdistance=0.75,  # Keeps percentages within the pie
    )
    

    plt.tight_layout()  # Ensures nothing is cropped

    # Save the pie chart
    plt.savefig('pangenome_pie.%s' % options.format, dpi=300)
    plt.clf()