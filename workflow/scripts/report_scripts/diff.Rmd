<!-- 

C. difficile

-->

```{r, echo=FALSE, results="asis"}

if (show_cdifficile_results) {
  cat('
### 9. *C. difficile*

<div class = "blue">

#### Contents
* 9.1 Summary results of *C. difficile* strains

</div>
***

##### 9.1 Summary results of *C. difficile* strains
')
}
```

```{r, echo=FALSE, message=FALSE, out.width = "100%"}

if (show_cdifficile_results) {

  results_cdifficile <- file.path(results_dir, "3_results_all_samples/C_difficile_results/summary_results_cdifficile.tsv")

  if (file.exists(results_cdifficile)) {
    res_cdifficile <- generate_table(results_cdifficile)
    res_cdifficile
  } 
}

```









