<!-- 

C. chauvoei

-->


```{r chau_vars, echo=FALSE, message=FALSE, warning=FALSE}

show_c_chauvoei_results <- ifelse(!is.null(snakemake@config[["C_CHAUVOEI_MODULE"]]), 
                                    snakemake@config[["C_CHAUVOEI_MODULE"]], FALSE)

c_chauvoei_virulence_genes <- ifelse(!is.null(snakemake@config[["c_chauvoei_virulence_genes"]]), 
                          snakemake@config[["c_chauvoei_virulence_genes"]], FALSE)

c_chauvoei_insertion_sequences <- ifelse(!is.null(snakemake@config[["c_chauvoei_insertion_sequences"]]), 
                              snakemake@config[["c_chauvoei_insertion_sequences"]], FALSE)


```

```{r, echo=FALSE, results="asis"}

if (show_c_chauvoei_results) {
  cat('
### 9. *C. chauvoei*

<div class = "blue">

#### Contents
* 9.1 Summary results of *C. chauvoei* strains

</div>
***

##### 9.1 Summary results of *C. chauvoei* strains
')
}
```

```{r, echo=FALSE, message=FALSE, out.width = "100%"}

if (show_c_chauvoei_results) {

  results_chauvoei <- file.path(results_dir, "3_results_all_samples/C_chauvoei_results/summary_results_cchauvoei.tsv")

  if (file.exists(results_chauvoei)) {
    res_chauvoei <- generate_table(results_chauvoei)
    res_chauvoei
  } 
}

```


```{r, echo=FALSE, results="asis"}

if (show_c_chauvoei_results) {
  cat('

***

#### 3.1 Plot summaries of _C. chauvoei_ results {.tabset .tabset-fade .tabset-pills}


\n')
}

```


```{r, echo=FALSE, results="asis"}

if (c_chauvoei_virulence_genes && show_c_chauvoei_results) {

    cat('

##### Virulence Genes

\n')
}

```

```{r, echo=FALSE, message=FALSE, error=TRUE}

if (c_chauvoei_virulence_genes && show_c_chauvoei_results) {


c_chauvoei_virulence_df <- read.delim("3_results_all_samples/C_chauvoei_results/Virulence_genes.tsv", 
                  header = TRUE, sep = "\t", stringsAsFactors = FALSE, check.names = FALSE)

c_chauvoei_virulence <- generate_heatmap_val(c_chauvoei_virulence_df)

c_chauvoei_virulence

}
```


```{r, echo=FALSE, results="asis"}

if (c_chauvoei_insertion_sequences && show_c_chauvoei_results) {

    cat('

##### Insertion Sequences

\n')
}

```

```{r, echo=FALSE, message=FALSE, error=TRUE}

if (c_chauvoei_insertion_sequences && show_c_chauvoei_results) {

c_chauvoei_insertion_sequences <- read.delim("3_results_all_samples/C_chauvoei_results/Insertion_sequences.tsv", 
                                  header = TRUE, sep = "\t", stringsAsFactors = FALSE, check.names = FALSE)

# Rename column headers
# colnames(cperf_insertion_seq) <- gsub("^([^_]*)_.*", "\\1", colnames(cperf_insertion_seq))
colnames(c_chauvoei_insertion_sequences) <- sub("_.*", "", colnames(c_chauvoei_insertion_sequences))


c_chauvoei_insertion_sequences_heatmap <- generate_heatmap_val(c_chauvoei_insertion_sequences)
c_chauvoei_insertion_sequences_heatmap

}
```
