# Load required library
library(rmarkdown)

# Get Snakemake inputs
rmd_files <- snakemake@input[["scripts"]]
output_files <- snakemake@output[["html_reports"]]
working_dir <- snakemake@config[["working_dir"]]

# Render each Rmd file
for (i in seq_along(rmd_files)) {
  rmarkdown::render(rmd_files[i], knit_root_dir = working_dir,  quiet = TRUE,  output_file = output_files[i])
}

# Print success message
print("All reports successfully rendered!")
