<!-- 

Virulence

-->
```{r, echo=FALSE, message=FALSE, warning=FALSE}

# Virulence
show_virulence <- snakemake@config[["VIRULENCE"]]
vir_databases <- snakemake@config[["abricate_virulence"]][["params"]][["databases"]]

abricate_vfdb <- FALSE

if (show_virulence  == TRUE) {
    abricate_vfdb <- "all" %in% vir_databases || "vfdb" %in% vir_databases
}
```

```{r, echo=FALSE, results="asis"}

if (show_virulence) {
  cat('
### 4. Virulence

<div class = "blue">

#### Contents
* 4.1 Results of Virulence factor databases

</div>
***

#### 4.1 Results of Virulence factor databases {.tabset .tabset-fade .tabset-pills}


\n')
}

```

```{r, echo=FALSE, results="asis"}

if (abricate_vfdb && show_virulence) {
  cat('
##### ABRicate + VFDB

\n')
}

```

```{r, echo=FALSE, message=FALSE, out.width = "100%"}

if (abricate_vfdb && show_virulence) {

  vfdb_tbl <- generate_table("3_results_all_samples/4_virulence/abricate_vfdb_summary.tsv")
  vfdb_tbl
}

```

```{r, echo=FALSE, results="asis"}

if (abricate_vfdb && show_virulence) {
  cat('

***

**ABRicate + VFDB database detailed summary**

\n')
}

```

```{r, echo=FALSE, message=FALSE, out.width = "100%"}

if (abricate_vfdb && show_virulence) {
  vfdb_detailed <- generate_table("3_results_all_samples/4_virulence/abricate_vfdb_detailed.tsv")
  vfdb_detailed
}

```

```{r, echo=FALSE, results="asis"}

if (abricate_vfdb && show_virulence) {
  cat('

***

**Summary plot visualization of virulence genes detected based on ABRicate + VFDB database**

\n')
}

```

```{r, echo=FALSE, message=FALSE, error=TRUE}

if (abricate_vfdb && show_virulence) {

vfdb_df <- read.delim("3_results_all_samples/4_virulence/abricate_vfdb_summary.tsv", 
                  header = TRUE, sep = "\t", stringsAsFactors = FALSE, check.names = FALSE)
vfdb_df <- subset(vfdb_df, select = -`No. of genes`)  # Remove Virulence_genes column

vfdb_plot <- generate_heatmap_val(vfdb_df)

vfdb_plot


}
```
