<!-- 

MLST

-->

```{r, echo=FALSE, results="asis"}

if (show_mlst) {
  cat('
### 5. MLST

<div class = "blue">

#### Contents
* 5.1 Results of multilocus sequence typing

</div>
***

##### 5.1 MLST analysis workflow


<div class = "blue">

\n')

cat(sprintf("&#x2714; You used  `%s` for multilocus sequence typing <br>\n", mlst_tool))

  cat('
</div>

***

##### 5.2 Results of multilocus sequence typing 

')
}

```


```{r, echo=FALSE, message=FALSE, out.width = "100%"}

if (show_mlst) {

  vfdb_tbl <- generate_table("3_results_all_samples/5_MLST/mlst.tsv")
  vfdb_tbl

}

```

