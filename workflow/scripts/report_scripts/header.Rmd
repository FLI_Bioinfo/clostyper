---
title: "Clostyper Analysis Report"
date: "This report was created on `r format(Sys.time(), ' %A, %d %B %Y at %H:%M %Z')`"
output: 
  html_document: 
    theme: 
      bootswatch: journal # flatly, cerulean, lumen, etc.
    toc: true
    # self_contained: FALSE #- split for faster performance. pics are not displayed
    toc_float: false 
    highlight: tango  # highlighting
---

<style type="text/css">
/* Customize the title font */
    h1.title {
      font-family: "Lato", sans-serif; 
      font-size: 70px; 
      font-weight: bold; 
      letter-spacing: 3px; 
      text-align: center; /* Center the text */
      margin-bottom: 20px; /* Add space below */

      /* Centered background larger than content */
      background-color: #f2f2f2; 
      padding: 30px 0; 
      width: 120%; /* Responsive width */
      max-width: 1400px; /* Prevents excessive stretch */

      /* Centering the background */
      position: relative;
      left: -10%; /* Align it to the center */
      transform: translateX(0%); /* Centering it */

      /* Centering text inside the box */
      display: flex;
      align-items: center;
      justify-content: center;
      min-height: 10rem; /* Ensure proper height */
    }

    /* Adjustments for smaller screens */
    @media (max-width: 1000px) {
      h1.title {
        font-size: 5vw; /* Smaller font for smaller screens */
        padding: 20px 10px; /* Less padding */
        width: 130%; /* Wider background on mobile */
        left: -15%; /* Center better */
      }

</style>



<style type="text/css">
/* Set maximum width of content */
.main-container {
  max-width: 1200px;
  margin-left: auto;
  margin-right: auto;
}

/* Style links */
a {
  color: #2E80B2;
  text-decoration: none;
}

a:hover {
  text-decoration: underline;
}

/* General Font Settings */
body {
  font-size: 95%;
}

h1, h2, h3 {
  font-weight: bold;
}

/* Adjusting the width  */
.container {
  width: 95%;
  max-width: 1200px;
  margin: auto;
}

/* Ensure tables are displayed without wrapping */
table {
  white-space: nowrap;
  width: 100%;
  border-collapse: collapse;
}

/* Styling for table cells and borders */
table th, table td {
  padding: 8px 12px;
  text-align: left;
  border: 1px solid #ddd;
}

/*  blue gradient for headers */
table.dataTable th {
  background: linear-gradient(to bottom, #4a74a8, #2c5176);
  color: white;
  padding: 12px;
  text-align: left;
  border-bottom: 2px solid #26476a; /* slight border for contrast */
}

/* Fixed first column header with the same gradient */
table.dataTable th.fixedColumns-1 {
  background: linear-gradient(to bottom, #4a74a8, #2c5176); 
  color: white;
  position: sticky;
  left: 0;
  z-index: 3; /* Ensure it's above other headers */
  border-right: 2px solid #26476a; /* Clear separation from next column */
}

/* Fixed first column cells matching the gradient header */
table.dataTable td.fixedColumns-1 {
  background: linear-gradient(to bottom, #4a74a8, #2c5176);
  color: white;
  position: sticky;
  left: 0;
  z-index: 2; /* Ensure it stays above other cells */
  border-right: 2px solid #26476a; /* Border separating fixed column */
}

/* Ensure non-fixed column headers retain the blue gradient */
table.dataTable th:not(.fixedColumns-1) {
  background: linear-gradient(to bottom, #4a74a8, #2c5176);
}

/* Table general styling */
table.dataTable {
  width: 100%;
  border-collapse: collapse;
  background-color: #e6f0ff; /* Light background to complement dark headers */
}

table.dataTable td {
  padding: 10px;
  border: 1px solid #2c5176; /* Slightly darker border for consistency */
}

/* For ensuring fixed column is visible */
table.dataTable td, table.dataTable th {
  box-sizing: border-box; /* Prevents overflow */
}
</style>

<style>
  .blue {
    font-size: 12px;
    color: black;
    background-color: #e6f0ff; /* Light Blue */
    padding: 20px;
    border-radius: 5px;
  }
</style>

<style>
  .plotly {
      width: 100% !important;
      height: auto !important; 
      max-width: 1200px;
      margin: auto;
      padding: 0 !important;

    }
</style>

<style>
    /* Ensure the footer stays at the bottom with a fixed width */
    .custom-footer {
        width: 100%;
        max-width: 1400px;
        background-color: #f2f2f2; /* Color theme */
        color: rgb(99, 97, 97);
        text-align: left;
        padding: 15px;
        position: fixed;
        bottom: 0;
        left: 50%; /* Align it to the center */
        transform: translateX(-50%); /* Centering it */
        z-index: 1000;
        font-size: 14px; /* Adjust font size */
        height: 6rem; /* Footer height */
        display: none; /* Initially hidden */
    }

    /* Reduce space between footer lines */
    .custom-footer p {
        margin: 2px 0; /* Reduce vertical spacing between lines */
        line-height: 1.2; /* Adjust line spacing */
    }
    /* Ensure content above footer is not hidden */
    body {
        padding-bottom: 100px; /* Adjust based on footer height */
    }

    /* Show footer when at the bottom of the page */
    body.scroll-footer .custom-footer {
        display: block;
    }
</style>

<footer class="custom-footer">
    <p>Clostyper's source code is available on [GitLab](https://gitlab.com/FLI_Bioinfo/clostyper).</p>
    <p>Check also the [Clostyper's wiki page](https://gitlab.com/FLI_Bioinfo/clostyper/-/wikis/home).</p>
    <p>Clostyper was written by Mostafa Abdel-Glil at [Friedrich-Loeffler-Institut](https://www.fli.de/en/startpage/), Germany</p>
</footer> 

<script>
    // Show the footer when scrolling to the bottom
    window.onscroll = function() {
        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
            document.body.classList.add("scroll-footer");
        } else {
            document.body.classList.remove("scroll-footer");
        }
    };
</script>


```{r, echo=FALSE, message=FALSE, warning=FALSE}

# Load configuration parameters from Snakemake
show_raw_reads <- snakemake@config[["RAWREADS_QUALITY"]]
fastp <- snakemake@config[["use_fastp"]]
kraken <- snakemake@config[["use_kraken2"]]
confindr <- snakemake@config[["use_confindr"]]

# Genomes
show_genomes <- snakemake@config[["GENOME_ASSESSMENT"]]
assembly <- snakemake@config[["ASSEMBLY"]]
assembly_tool <- snakemake@config[["Assembly_software"]]
annotation <- snakemake@config[["ANNOTATION"]]
annotation_tool <- snakemake@config[["Annotation_software"]]
kraken2_assembly <- snakemake@config[["kraken2_assemblies"]][["run_kraken2_assemblies"]]
checkM <- snakemake@config[["checkM"]][["run_checkM"]]
referenceseeker <- snakemake@config[["referenceseeker"]][["run_referenceseeker"]]
fastANI <- snakemake@config[["fastANI"]][["run_fastANI"]]

# MLST
show_mlst <- snakemake@config[["MLST"]]
mlst_tool <- snakemake@config[["MLST_software"]]

# cgMLST
show_cgmlst_analysis <- snakemake@config[["COREGENOME_MLST"]]
show_pangenome_analysis <- snakemake@config[["PANGENOME"]]

# C. difficile module
show_cdifficile_results <- snakemake@config[["C_DIFFICILE_MODULE"]]


# #reads
# show_raw_reads = TRUE
# fastp = FALSE
# kraken = TRUE
# confindr = FALSE

# # genomes
# show_genomes = TRUE
# Assemebly = TRUE
# assembly_tool = "shovill" #snakemake@config[["myparam"]]
# annotation = TRUE
# annotation_tool = "bakta"
# kraken2_assembly = FALSE
# checkM = TRUE
# referenceseeker = TRUE
# fastANI = TRUE
# # fastp <- snakemake@config[["myparam"]]

# # resistance
# show_resistance = TRUE
# amrfinder = TRUE
# abricate_ncbi = TRUE
# abricate_card = TRUE
# abricate_resfinder = TRUE
# abricate_megares = TRUE
# abricate_argannot = TRUE

# # virulence
# show_virulence = TRUE 
# abricate_vfdb = TRUE

# # MLST
# show_mlst = TRUE
# mlst_tool = "mlst"

# # cgSNP
# show_snp_analysis = TRUE
# coresnpfilter_thr = 0.98
# exclude_reference_from_alignment = TRUE
# mask_user_sites = "file"
# mask_repetitive_regions = TRUE
# minimum_alignment_length = "XXX"
# minimum_percent_identity = "XXX"
# recombination_filter = TRUE
# homoplasies_filter = TRUE 
# snpdists = "3_results_all_samples/6_core_genome_SNPs/Pairwise_SNP_distances/cgSNP_PairwiseDistances.tsv"
# final_tree = "3_results_all_samples/6_core_genome_SNPs/Phylogeny/coregenome_phylogeny.newick"
# phylogeny_input = "which_phylogeny"
# which_phylogeny = "snippy"
# phylogeny_software = "raxml"
# clustering_with_hclust = TRUE 
# clustering_with_fastbaps = TRUE
# all_snp_analysis_dir_name = "6_core_genome_SNPs"


# show_cgmlst_analysis = TRUE 
# show_pangenome_analysis = TRUE

# show_cdifficile_results = TRUE


```

```{r packages, echo=FALSE, message=FALSE, warning=FALSE, results='hide'}
if(!require(pacman)){install.packages("pacman");suppressMessages(library(pacman))}
pacman::p_load(benchmarkme, DT, knitr, ggplot2, plotly, reshape2) #RColorBrewer, dendextend, ape, BiocManager, reactable
```

```{r, echo=FALSE, message=FALSE, warning=FALSE}
knitr::opts_chunk$set(
  out.width = '80%',
  fig.asp = 0.5,
  fig.align = 'center',
  echo = FALSE,
  warning = FALSE,
  message = TRUE,
  fig.height = 20,
  fig.width = 20
)

options(markdown.HTML.header = system.file("misc", "datatables.html", package = "knitr"))

```


```{r import_data, echo=FALSE, message=FALSE, warning=FALSE}
results_dir <- snakemake@params[["working_dir"]]
reports_dir <- snakemake@params[["reports_dir"]]
version <- snakemake@params[["version"]]
samples <- file.path(paste0(results_dir , "3_results_all_samples/sample_names.txt", sep=""))
```

```{r define_functions, echo=FALSE, message=FALSE, warning=FALSE}
library(ggplot2)
library(plotly)
library(DT)
library(reshape2)

generate_heatmap <- function(file_path) {

  # Load binary data from a tab-delimited file
  binary_df <- read.delim(file_path, header = TRUE, sep = "\t", stringsAsFactors = FALSE, check.names = FALSE)

  # Convert the data to a matrix (excluding the Sample column)
  binary_matrix <- as.matrix(binary_df[,-1])
  rownames(binary_matrix) <- binary_df$Sample  # Set row names as Sample IDs

  # Convert to long format
  binary_df_long <- melt(binary_matrix, varnames = c("Sample", "Feature"), value.name = "Presence")

  # Create heatmap using plotly
  heatmap <- plot_ly(
    data = binary_df_long, 
    x = ~Feature, 
    y = ~Sample, 
    z = ~Presence,
    type = "heatmap",
    colorscale = list(c(0, 1), c("grey92", "black")),  # Define color scale
    zmin = 0, zmax = 1,  # Ensures correct color mapping
    showscale = FALSE,   # Remove legend
    hoverinfo = "none",  # Optional: removes hover text to reduce file size
    xgap = 1,  # Adds white space between cells
    ygap = 1   # Adds white space between cells
  ) %>%
    layout(
      title = "Gene Presence/Absence Heatmap",
      xaxis = list(title = "", showgrid = FALSE),
      yaxis = list(title = "", showgrid = FALSE),
      margin = list(t = 40, r = 0, b = 0, l = 0),
      height = min(800, nrow(binary_matrix) * 30)  # Dynamic height
    )

  return(heatmap)
}


generate_heatmap_val <- function(input_data) {
  # Check if input is a file path (character) or a dataframe
  if (is.character(input_data) && file.exists(input_data)) {
    df <- read.delim(input_data, header = TRUE, sep = "\t", stringsAsFactors = FALSE, check.names = FALSE)
  } else if (is.data.frame(input_data)) {
    df <- input_data  # Use the dataframe directly
  } else {
    stop("Invalid input: Must be a file path or a dataframe")
  }

  # Ensure the dataframe is not empty
  if (nrow(df) == 0 || ncol(df) < 2) {
    stop("Invalid input: The dataframe must have at least one sample and one feature column")
  }

  # Process data: Convert semicolon-separated values to the maximum value
  process_values <- function(x) {
    x <- as.character(x)  # Ensure x is a character string
    if (x %in% c("-", ".", "", "NA") || is.na(x)) {
      return(NA)  # Missing values remain NA
    } else {
      values <- suppressWarnings(as.numeric(unlist(strsplit(x, ";\\s*"))))  # Split and convert to numeric
      if (all(is.na(values))) {
        return(NA)  # If all converted values are NA, return NA
      }
      return(max(values, na.rm = TRUE))  # Take the max value for multiple values
    }
  }

  # Ensure first column is named 'Sample' for consistency
  colnames(df)[1] <- "Sample"

  # Apply processing to all columns except 'Sample'
  df_processed <- df
  for (col in names(df)[-1]) {
    df_processed[[col]] <- sapply(df[[col]], process_values)
  }

  # Sort Samples Alphabetically
  df_processed <- df_processed[order(df_processed$Sample), ]

  # Convert to matrix format
  numeric_matrix <- as.matrix(df_processed[, -1])
  rownames(numeric_matrix) <- df_processed$Sample

  # Convert to long format for plotting
  df_long <- melt(numeric_matrix, varnames = c("Sample", "Feature"), value.name = "Presence")

  # Ensure Samples Stay Sorted Alphabetically in Plot
  df_long$Sample <- factor(df_long$Sample, levels = unique(df_processed$Sample))

  # Handle Cases Where All Values Are Missing or Zero
  if (all(is.na(df_long$Presence)) || all(df_long$Presence == 0, na.rm = TRUE)) {
    df_long$Presence[is.na(df_long$Presence)] <- 0  # Replace NA with 0
  }

  # Set color scale dynamically to handle missing values
  color_scale <- list(
    c(0, 1), c("grey92", "black")
  )

  # Calculate height dynamically with a minimum of 3 samples
  min_height <- 3 * 100  # Minimum height (3 samples * 30 pixels/sample)
  calculated_height <- nrow(numeric_matrix) * 30
  height <- pmin(800, pmax(min_height, calculated_height, 100))  # Minimum 100, max 800

  # Create heatmap with plotly
  heatmap <- plot_ly(
    data = df_long,
    x = ~Feature,
    y = ~Sample,
    z = ~Presence,
    type = "heatmap",
    colorscale = color_scale,  # Dynamic color scale
    zmin = 0,
    zmax = 100,  # Ensures correct scaling based on percentage values
    showscale = FALSE,  # Hide color legend
    hoverinfo = "text",
    xgap = 1,  # Adds white space between cells
    ygap = 1,  # Adds white space between cells
    text = ~paste("Sample:", Sample, "<br>Feature:", Feature, "<br>Presence:", Presence)
  ) %>%
    layout(
      title = "Gene Presence/Absence Heatmap",
      xaxis = list(title = ""),
      yaxis = list(title = "", autorange = "reversed"),  # Ensures samples align correctly
      margin = list(t = 40, r = 50, b = 100, l = 100),
      height = height,  # Use the calculated height
      shapes = list(
        list(
          type = "rect",
          x0 = -0.5,  # Left boundary
          x1 = max(length(unique(df_long$Feature)), 1) - 0.5, # At least 1 wide
          y0 = -0.5,  # Bottom boundary
          y1 = max(length(unique(df_long$Sample)), 1) - 0.5,  # At least 1 high
          line = list(color = "black", width = 2)  # Border color & thickness
        )
      )
    )

  return(heatmap)
}



generate_heatmap_gg <- function(file_path) {

  # Load binary data from a tab-delimited file
  binary_df <- read.delim(file_path, header = TRUE, sep = "\t", stringsAsFactors = FALSE, check.names = FALSE)

  # Convert the data to a matrix (excluding the Sample column)
  binary_matrix <- as.matrix(binary_df[,-1])
  rownames(binary_matrix) <- binary_df$Sample  # Set row names as Sample IDs

  # Convert to long format
  binary_df_long <- melt(binary_matrix, varnames = c("Sample", "Feature"), value.name = "Presence")

  # Create heatmap using ggplot
  p <- ggplot(binary_df_long, 
    aes(x = Feature, y = Sample, fill = as.factor(Presence))) +
    geom_tile(color = "white") +
    scale_fill_manual(values = c("0" = "grey92", "1" = "black"), 
                    name = NULL,  # Remove legend title
                    labels = c("Gene absent", "Gene present")) + 
    theme_minimal() +
    theme(axis.text.x = element_text(angle = 90, hjust = 1),
          axis.title.x = element_blank(),
          axis.title.y = element_blank())

  # Set dynamic figure height (30px per sample, max 800px)
  num_samples <- nrow(binary_matrix)
  fig_height <- min(800, num_samples * 30)

  # Convert to interactive plotly figure
  plotly_fig <- ggplotly(p) %>%
    layout(width = NULL, height = fig_height, 
    margin = list(t = 40, r = 0, b = 0, l = 0), # NULL to adapt to container width
    showlegend = FALSE, 
    title = "Gene Presence/Absence Heatmap" )  

# Print the interactive plot
  return(plotly_fig)
}

# library(readr)
# library(reactable)

# generate_table_reactable <- function(file_path) {
#   parsed_table <- read_delim(file_path, delim = "\t", col_names = TRUE, quote = "", trim_ws = TRUE)
  
#   table <- reactable(parsed_table,
#     searchable = TRUE, pagination = TRUE, highlight = TRUE, resizable = TRUE, striped = TRUE, bordered = TRUE,
#     defaultPageSize = 10, showPageSizeOptions = TRUE, pageSizeOptions = c(10, 25, 50, 100),
#     defaultColDef = colDef(align = "center", style = list(whiteSpace = "nowrap"))
#   )
  
#   return(table)
# }


# function to display a styled table
generate_table <- function(file_path) {
  # Load the data from the provided file path
  parsed_table <- read.delim(file_path, header = TRUE, sep = "\t", dec = ".", quote = "", check.names = FALSE)
  
  # Create a DT table with the required options and styling
  table <- DT::datatable(parsed_table, caption = "",
                         rownames = FALSE,
                         fillContainer = FALSE,
                         filter = 'top',
                         escape = FALSE,
                         class = 'cell-border stripe',
                         style = 'default',
                         extensions = list("ColReorder" = NULL, "Buttons" = NULL, "FixedColumns" = list(leftColumns = 1)),
                         options = list(
                           fixedColumns = TRUE,
                           pageLength = 10,
                           columnDefs = list(list(className = 'dt-center', targets = "_all")),
                           dom = 'BRrltpi',
                           scrollX = TRUE,
                           order = list(list(0, 'asc')),
                           lengthMenu = list(c(10, 50, -1), c('10', '50', 'All')),
                           ColReorder = TRUE,
                           rowReorder = TRUE,
                           searchHighlight = TRUE,
                           scrollCollapse = TRUE,
                           buttons = list(
                             list(extend = 'copy', exportOptions = list(columns = ":visible")),
                             list(extend = 'print', exportOptions = list(columns = ":visible")),
                             list(text = 'Download',
                                  extend = 'collection',
                                  buttons = list(
                                    list(extend = 'csv', exportOptions = list(columns = ":visible")),
                                    list(extend = 'excel', exportOptions = list(columns = ":visible")),
                                    list(extend = 'pdf', exportOptions = list(columns = ":visible"))
                                  )
                             ),
                             I('colvis')
                           )
                         )
  ) %>%
    DT::formatStyle(names(parsed_table), lineHeight = '80%')  # Apply row styling
  
  # Return the table
  return(table)
}
```

* User: <span style="color:#2E80B2">`r format(ifelse(nzchar(Sys.getenv("LOGNAME")), Sys.getenv("LOGNAME"), "Guest User"))`</span>
* Linux system: <span style="color:#2E80B2">`r format(Sys.info()["nodename"])` `r format(Sys.info()["machine"])` (`r cpu <- benchmarkme::get_cpu(); cpu$no_of_cores` CPUs and `r ram <- benchmarkme::get_ram(); format(ram, scientific = TRUE) ` RAMs)</span>
* No. of samples:  <span style="color:#2E80B2">`r dat <- read.table( samples,  header =TRUE); nrow(dat) `</span>
* Results:` `r results_dir` `

This analysis was done with Clostyper version `r version`. For more information, visit the [Clostyper wiki page](https://gitlab.com/FLI_Bioinfo/clostyper/-/wikis/home).

***

## Clostyper results {.tabset }

