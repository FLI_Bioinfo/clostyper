#!/bin/bash
# date: October, 24, 2018
# check if a directory exists and move it to another directory
# the script was made for a specific use within the pangenome.Snakemake workflow. Thus no time stamp subfolder will be created for Roary

directory=$1
outdirectory=$2
time_stamp=$(date +"%d-%m-%y%T")

remove_trailing_slash() {
  local path=$1
  if [[ ${path: -1} == "/" ]]; then
    #https://stackoverflow.com/questions/4609949/what-does-1-in-sed-do
    path=$(echo $path | sed 's/\(.*\)\//\1 /')
  fi
  echo $path
  #  path="$1" ;
  #  new_path=`echo "$path" | perl -nl -e 's/\/+$//;' -e 'print $_'` ; echo $new_path ;
}

directory=$(remove_trailing_slash "${directory}")
outdirectory=$(remove_trailing_slash "${outdirectory}")

if [ -d "${directory}" ]; then
  mkdir -p "${outdirectory}/${time_stamp}"
  mv "${directory}" "${outdirectory}/${time_stamp}"
fi
