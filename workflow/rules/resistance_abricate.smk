"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


"""
Abricate resistance
"""


rule get_latest_abricate_resistance_databases:
    output:
        abricate_databases=temp(directory(temporary_todelete + "abricate_db")),
        ncbi=directory(temporary_todelete + "abricate_db/ncbi"),
        card=directory(temporary_todelete + "abricate_db/card"),
        resfinder=directory(temporary_todelete + "abricate_db/resfinder"),
        megares=directory(temporary_todelete + "abricate_db/megares"),
        argannot=directory(temporary_todelete + "abricate_db/argannot"),
        version=results_all_samples_dir + all_resistance_dir_name + "/versions/abricate_databases_version.txt",
    conda:
        envs_folder + "abricate.yaml"
    threads: 2
    log:
        results_all_samples_dir + all_resistance_dir_name + "/versions/abricate_databases.log",
    params:
        abricate_databases=config["abricate_databases"] if config["abricate_databases"] else "__no_name__",
    shell:
        r"""
        mkdir -p {output.abricate_databases}
        abricate --version > {output.version}
        date > {log}
        for db in ncbi card argannot resfinder megares; do
            if [[ -d {params.abricate_databases}/$db ]] && [[ "$(ls -A {params.abricate_databases}/$db)" ]]; then
                cp -r -L {params.abricate_databases}/"$db"/ {output.abricate_databases}/
                echo "Found ABRicate database: $db"
                echo -ne "\\n\\nWill use the database $db from the folder: {params.abricate_databases}\\n" >> {output.version}
            elif
                echo "did not find ABRicate database: $db - will try to download it"
                abricate-get_db --db $db  --force --dbdir {output.abricate_databases} --debug &>> {log}; then
                echo -ne "\\n\\nABRicate database $db update on: `date`  \\n" >> {output.version}
                sed -n "/Setting up '$db'/,+2p" {log} | grep "Downloading: \|Cloning " | sed 's/Downloading: /Download link: /g; ' >> {output.version}
            else
                abricate_path=$(dirname `which abricate`)
                cp -r "$abricate_path/../db/$db" {output.abricate_databases}
                echo -ne "\\n\\nFailed to update DB: $db - will use the database from abricate installation: $abricate_path\\n" >> {output.version}
            fi
        done
        """


# ncbi
rule abricate_resistance_ncbi:
    input:
        contig=assembly_all + "{sample}.fasta",
        db_dir=temporary_todelete + "abricate_db",
    output:
        resistance_ncbi=results_per_sample_dir + "{sample}/resistance/{sample}_abricate_ncbi.tsv",
    threads: 8
    log:
        results_per_sample_dir + "{sample}/resistance/{sample}_abricate_ncbi.log",
    params:
        abricate_database="ncbi",
        min_identity="--minid " + str(config["abricate_resistance"]["params"]["min_identity"]) if config["abricate_resistance"]["params"]["min_identity"] else "",
        min_coverage="--mincov " + str(config["abricate_resistance"]["params"]["min_coverage"]) if config["abricate_resistance"]["params"]["min_coverage"] else "",
        abricate_extraparams=config["abricate_resistance"]["params"]["abricate_extraparams"] if config["abricate_resistance"]["params"]["abricate_extraparams"] else "",
    conda:
        envs_folder + "abricate.yaml"
    shell:
        r"""
        export PERL5LIB=""
        abricate --nopath --db {params.abricate_database} --datadir {input.db_dir} --threads {threads} {params.min_identity} {params.min_coverage} {params.abricate_extraparams} {input.contig} > {output} 2> {log}
        """


rule contigs_with_resistance_taxonomy_ncbi:
    input:
        abricate=results_per_sample_dir + "{sample}/resistance/{sample}_abricate_ncbi.tsv",
        kraken=results_per_sample_dir + "{sample}/" + kraken_dir_name + "/{sample}_kraken_contigs_raw.txt",
    output:
        resistance_ncbi=results_per_sample_dir + "{sample}/resistance/{sample}_abricate_ncbi_w_taxonomy.tsv",
    threads: 4
    params:
        taxdump_database=config["taxdump_database"] if config["taxdump_database"] else "",
    conda:
        envs_folder + "kraken2.yaml"
    shell:
        r"""
         if [[ $(csvtk nrow -t {input[0]} ) -gt 1 ]]; then
            paste <( cat {input[0]} ) <( cat {input[0]} \
            | csvtk cut -f 2 -t \
            | while read contig ; do \
                csvtk grep -f 2 -p $contig  -tH  {input[1]}\
                | csvtk cut -f 3 -t \
                | taxonkit lineage --threads 4 --data-dir {params.taxdump_database} -n  \
                | csvtk -Ht cut -f 1,2 ;
            done \
            | csvtk add-header -n "Contig taxonid","Contig taxonomic group" -t )  --delimiters '\\t' >  {output}
        fi
         """


rule abricate_summary_res_ncbi:
    input:
        expand(results_per_sample_dir + "{sample}/resistance/{sample}_abricate_ncbi.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/resistance/{genome}_abricate_ncbi.tsv", genome=GENOMES),
    output:
        tsv=results_all_samples_dir + all_resistance_dir_name + "/abricate_ncbi_summary.tsv",
        # tsv_binary=results_all_samples_dir + all_resistance_dir_name + "/abricate_ncbi_summary_binary.tsv",
    conda:
        envs_folder + "abricate.yaml"
    params:
        suffix="_abricate_ncbi.tsv",
    shell:
        r"""
        abricate --nopath --summary {input} | 
            csvtk replace -t -C "$" -f 1 -p "{params.suffix}" -r "" | 
            csvtk rename  -t -C "$" -f "#FILE" -n "Sample" | 
            csvtk rename  -t -C "$" -f "NUM_FOUND" -n "No. of genes" > {output.tsv}
        """
        
# """if [[ $(csvtk ncol -t {output.tsv} ) -gt 3 ]]; then
#     cat {output.tsv} | csvtk cut -t -f -2 | csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv}) - 1 ) -p '^.$'   -r @ | csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv}) - 1 ) -p '[^@]+' -r 1 | csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv}) - 1 ) -p '^@$'   -r 0 > {output.tsv_binary}
# elif [[ $(csvtk ncol -t {output.tsv} ) = 3 ]]; then
#     cat {output.tsv} | csvtk cut -t -f -2 | csvtk replace -t -f 2 -p '^.$'   -r @ | csvtk replace -t -f 2 -p '[^@]+' -r 1 | csvtk replace -t -f 2 -p '^@$'   -r 0 > {output.tsv_binary}
# else
#     cat {output.tsv}  > {output.tsv_binary};
# fi
# """


rule detail_summary_abricate_ncbi:
    input:
        expand(results_per_sample_dir + "{sample}/resistance/{sample}_abricate_ncbi.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/resistance/{genome}_abricate_ncbi.tsv", genome=GENOMES),
    output:
        FOF=temp(results_all_samples_dir + all_resistance_dir_name + "/samples_path_ncbi.tsv"),
        tsv=results_all_samples_dir + all_resistance_dir_name + "/abricate_ncbi_detailed.tsv",
        # xlsx=results_all_samples_dir + all_resistance_dir_name + "/abricate_ncbi_detailed.xlsx",
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        echo -ne "" > {output.FOF} &&
        for path in {input}; do
            if [[ -s $path ]]; then
                echo -ne "$path\n" >> {output.FOF};
            fi
        done
        csvtk concat -t --infile-list {output.FOF} -C "$" -T -i -E  -I |  csvtk replace -C "$" -p ".fasta" -r '$1 ' | csvtk rename  -t -C "$" -f "#FILE" -n "Sample" --out-file {output.tsv}
        """
#        csvtk csv2xlsx -t {output.tsv} -o {output.xlsx}


# card
use rule abricate_resistance_ncbi as abricate_resistance_card with:
    output:
        results_per_sample_dir + "{sample}/resistance/{sample}_abricate_card.tsv",
    log:
        results_per_sample_dir + "{sample}/resistance/{sample}_abricate_card.log",
    params:
        abricate_database="card",
        min_identity="--minid " + str(config["abricate_resistance"]["params"]["min_identity"]) if config["abricate_resistance"]["params"]["min_identity"] else "",
        min_coverage="--mincov " + str(config["abricate_resistance"]["params"]["min_coverage"]) if config["abricate_resistance"]["params"]["min_coverage"] else "",
        abricate_extraparams=config["abricate_resistance"]["params"]["abricate_extraparams"] if config["abricate_resistance"]["params"]["abricate_extraparams"] else "",


use rule abricate_summary_res_ncbi as abricate_summary_res_card with:
    input:
        expand(results_per_sample_dir + "{sample}/resistance/{sample}_abricate_card.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/resistance/{genome}_abricate_card.tsv", genome=GENOMES),
    output:
        tsv=results_all_samples_dir + all_resistance_dir_name + "/abricate_card_summary.tsv",
        # tsv_binary=results_all_samples_dir + all_resistance_dir_name + "/abricate_card_summary_binary.tsv",
    params:
        suffix="_abricate_card.tsv",


use rule detail_summary_abricate_ncbi as detail_summary_abricate_card with:
    input:
        expand(results_per_sample_dir + "{sample}/resistance/{sample}_abricate_card.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/resistance/{genome}_abricate_card.tsv", genome=GENOMES),
    output:
        FOF=temp(results_all_samples_dir + all_resistance_dir_name + "/samples_path_card.tsv"),
        tsv=results_all_samples_dir + all_resistance_dir_name + "/abricate_card_detailed.tsv",
        # xlsx=results_all_samples_dir + all_resistance_dir_name + "/abricate_card_detailed.xlsx",


# resfinder
use rule abricate_resistance_ncbi as abricate_resistance_resfinder with:
    output:
        results_per_sample_dir + "{sample}/resistance/{sample}_abricate_resfinder.tsv",
    log:
        results_per_sample_dir + "{sample}/resistance/{sample}_abricate_resfinder.log",
    params:
        abricate_database="resfinder",
        min_identity="--minid " + str(config["abricate_resistance"]["params"]["min_identity"]) if config["abricate_resistance"]["params"]["min_identity"] else "",
        min_coverage="--mincov " + str(config["abricate_resistance"]["params"]["min_coverage"]) if config["abricate_resistance"]["params"]["min_coverage"] else "",
        abricate_extraparams=config["abricate_resistance"]["params"]["abricate_extraparams"] if config["abricate_resistance"]["params"]["abricate_extraparams"] else "",


use rule abricate_summary_res_ncbi as abricate_summary_res_resfinder with:
    input:
        expand(results_per_sample_dir + "{sample}/resistance/{sample}_abricate_resfinder.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/resistance/{genome}_abricate_resfinder.tsv", genome=GENOMES),
    output:
        tsv=results_all_samples_dir + all_resistance_dir_name + "/abricate_resfinder_summary.tsv",
        # tsv_binary=results_all_samples_dir + all_resistance_dir_name + "/abricate_resfinder_summary_binary.tsv",
    params:
        suffix="_abricate_resfinder.tsv",


use rule detail_summary_abricate_ncbi as detail_summary_abricate_resfinder with:
    input:
        expand(results_per_sample_dir + "{sample}/resistance/{sample}_abricate_resfinder.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/resistance/{genome}_abricate_resfinder.tsv", genome=GENOMES),
    output:
        FOF=temp(results_all_samples_dir + all_resistance_dir_name + "/samples_path_resfinder.tsv"),
        tsv=results_all_samples_dir + all_resistance_dir_name + "/abricate_resfinder_detailed.tsv",
        # xlsx=results_all_samples_dir + all_resistance_dir_name + "/abricate_resfinder_detailed.xlsx",


# argannot
use rule abricate_resistance_ncbi as abricate_resistance_argannot with:
    output:
        results_per_sample_dir + "{sample}/resistance/{sample}_abricate_argannot.tsv",
    log:
        results_per_sample_dir + "{sample}/resistance/{sample}_abricate_argannot.log",
    params:
        abricate_database="argannot",
        min_identity="--minid " + str(config["abricate_resistance"]["params"]["min_identity"]) if config["abricate_resistance"]["params"]["min_identity"] else "",
        min_coverage="--mincov " + str(config["abricate_resistance"]["params"]["min_coverage"]) if config["abricate_resistance"]["params"]["min_coverage"] else "",
        abricate_extraparams=config["abricate_resistance"]["params"]["abricate_extraparams"] if config["abricate_resistance"]["params"]["abricate_extraparams"] else "",


use rule abricate_summary_res_ncbi as abricate_summary_res_argannot with:
    input:
        expand(results_per_sample_dir + "{sample}/resistance/{sample}_abricate_argannot.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/resistance/{genome}_abricate_argannot.tsv", genome=GENOMES),
    output:
        tsv=results_all_samples_dir + all_resistance_dir_name + "/abricate_argannot_summary.tsv",
        # tsv_binary=results_all_samples_dir + all_resistance_dir_name + "/abricate_argannot_summary_binary.tsv",
    params:
        suffix="_abricate_argannot.tsv",


use rule detail_summary_abricate_ncbi as detail_summary_abricate_argannot with:
    input:
        expand(results_per_sample_dir + "{sample}/resistance/{sample}_abricate_argannot.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/resistance/{genome}_abricate_argannot.tsv", genome=GENOMES),
    output:
        FOF=temp(results_all_samples_dir + all_resistance_dir_name + "/samples_path_argannot.tsv"),
        tsv=results_all_samples_dir + all_resistance_dir_name + "/abricate_argannot_detailed.tsv",
        # xlsx=results_all_samples_dir + all_resistance_dir_name + "/abricate_argannot_detailed.xlsx",


# megares
use rule abricate_resistance_ncbi as abricate_resistance_megares with:
    output:
        results_per_sample_dir + "{sample}/resistance/{sample}_abricate_megares.tsv",
    log:
        results_per_sample_dir + "{sample}/resistance/{sample}_abricate_megares.log",
    params:
        abricate_database="megares",
        min_identity="--minid " + str(config["abricate_resistance"]["params"]["min_identity"]) if config["abricate_resistance"]["params"]["min_identity"] else "",
        min_coverage="--mincov " + str(config["abricate_resistance"]["params"]["min_coverage"]) if config["abricate_resistance"]["params"]["min_coverage"] else "",
        abricate_extraparams=config["abricate_resistance"]["params"]["abricate_extraparams"] if config["abricate_resistance"]["params"]["abricate_extraparams"] else "",


use rule abricate_summary_res_ncbi as abricate_summary_res_megares with:
    input:
        expand(results_per_sample_dir + "{sample}/resistance/{sample}_abricate_megares.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/resistance/{genome}_abricate_megares.tsv", genome=GENOMES),
    output:
        tsv=results_all_samples_dir + all_resistance_dir_name + "/abricate_megares_summary.tsv",
        # tsv_binary=results_all_samples_dir + all_resistance_dir_name + "/abricate_megares_summary_binary.tsv",
    params:
        suffix="_abricate_megares.tsv",


use rule detail_summary_abricate_ncbi as detail_summary_abricate_megares with:
    input:
        expand(results_per_sample_dir + "{sample}/resistance/{sample}_abricate_megares.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/resistance/{genome}_abricate_megares.tsv", genome=GENOMES),
    output:
        FOF=temp(results_all_samples_dir + all_resistance_dir_name + "/samples_path_megares.tsv"),
        tsv=results_all_samples_dir + all_resistance_dir_name + "/abricate_megares_detailed.tsv",
        # xlsx=results_all_samples_dir + all_resistance_dir_name + "/abricate_megares_detailed.xlsx",


# rule abricate_resistance_call:
#     input:
#         db_dir=temporary_todelete + "abricate_db",
#     output:
#         results_all_samples_dir + all_resistance_dir_name + "/abricate_resistance_command.txt",
#     conda:
#         envs_folder + "abricate.yaml"
#     threads: 8
#     params:
#         min_identity="--minid " + str(config["abricate_resistance"]["params"]["min_identity"]) if config["abricate_resistance"]["params"]["min_identity"] else "",
#         min_coverage="--mincov " + str(config["abricate_resistance"]["params"]["min_coverage"]) if config["abricate_resistance"]["params"]["min_coverage"] else "",
#         abricate_extraparams=config["abricate_resistance"]["params"]["abricate_extraparams"] if config["abricate_resistance"]["params"]["abricate_extraparams"] else "",
#     shell:
#         r"""
#         echo "Abricate Resistance command line: \n\n\`abricate --nopath --db DATABASE --datadir {input.db_dir} --threads {threads} {params.min_identity} {params.min_coverage} {params.abricate_extraparams} NUCLEOTIDE_SEQ > OUTPUT\` \n\n\`abricate --nopath --summary ALL_OUTPUTS\`" > {output}

#         """
