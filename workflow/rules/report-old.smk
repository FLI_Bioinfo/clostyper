"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


def REPORT():
    if config["REPORT"] == True:
        count_samples, count_genomes = count_items(names[0], names_genomes[0]) ## Access the list inside the tuple
        total_count = count_samples + count_genomes
        if total_count > 400: #ToDo: read the number from config
            print("The number of samples is greater than 500. " + program_name + " will create individual reports")
            REPORT = [] 
            if ((config["RAWREADS_QUALITY"] == True) and 
            (config["use_fastp"] == True or config["use_kraken2"] == True or config ["use_confindr"] == True) and 
                check_files_with_suffix(raw_data_dir)):
                REPORT += [rules.make_raw_reads_quality_report.output],
            if config["GENOME_ASSESSMENT"] == True:
                REPORT += [rules.make_genome_quality_report.output],
            if config["RESISTANCE"] == True:
                REPORT += [rules.make_resistance_report.output],
            if config["VIRULENCE"] == True:
                REPORT += [rules.make_virulence_report.output],
            if config["MLST"] == True:
                REPORT += [rules.make_mlst_report.output]
            if config["SNP_ANALYSIS"] == True:
                REPORT += [rules.make_cgsnps_report.output],
            if config["COREGENOME_MLST"] == True:
                REPORT += [rules.make_cgmlst_report.output],
            if config["PANGENOME"] == True:
                REPORT += [rules.make_pangenome_report.output],
            if ((config["C_DIFFICILE_MODULE"] == True) and (config["C_DIIFICILE_CLADES"] == True or config["C_DIFFICILE_TXOINS"] == True or config["C_DIFFICILE_MOBILE_ELEMENTS"] == True )):
                REPORT += [rules.make_cdifficile_report.output],
        else:
            REPORT = reports_dir + "clostyper_report.html"
        return REPORT
    else:
        return []


"""
create report
"""


rule collect_report_scripts:
    input:
        report_scripts + "header.Rmd",
        rules.write_report_script_raw_reads_quality.output.report if ((config["RAWREADS_QUALITY"] == True) and (config["use_fastp"] == True or config["use_kraken2"] == True or config["use_confindr"] == True)) and has_pairedend_samples(config["samples"]) else [],
        rules.write_report_script_genome_quality.output.report  if config["GENOME_ASSESSMENT"] == True else [],
        rules.write_report_script_resistance.output.report if config["RESISTANCE"] == True else [],
        rules.write_report_script_virulence.output.report if config["VIRULENCE"] == True else [],
        rules.write_report_script_mlst.output.report if config["MLST"] == True else [],
        rules.write_report_script_snp_analysis.output.report if config["SNP_ANALYSIS"] == True else [],
        rules.write_report_script_cgmlst_analysis.output.report if config["COREGENOME_MLST"] == True else [],
        rules.write_report_script_pangenome.output.report if config["PANGENOME"] == True else [],
        rules.write_report_script_cdiff_mge.output.report if (config["C_DIFFICILE_MODULE"] == True and (config["C_DIIFICILE_CLADES"] == True or config["C_DIFFICILE_TXOINS"] == True or config["C_DIFFICILE_MOBILE_ELEMENTS"] == True )) else [],
    output:
        ensure(reports_dir + "script.Rmd", non_empty=True),
    shell:
        "cat {input} > {output} 2>/dev/null"


rule make_report:
    input:
        script=reports_dir + "script.Rmd",
        # ref_size=results_all_samples_dir + reference_dir_name + "/ref_size.txt",
    output:
        reports_dir + "clostyper_report.html",
    params:
        reports_dir=reports_dir,
        working_dir=working_dir,
        version=__version__,
    #        mlst_tool = config["MLST_software"]
    threads: 16
    conda:
        envs_folder + "report_env.yaml"
    script:
        reports_dir + "script.Rmd"


################################ splits

rule raw_reads_quality_report:
    input:
        report_scripts + "header.Rmd",
        reports_dir + "raw_reads_quality_script.Rmd" if  ((config["RAWREADS_QUALITY"] == True) and (config["use_fastp"] == True or config["use_kraken2"] == True or config["use_confindr"] == True)) and has_pairedend_samples(config["samples"]) else [],
    output:
        temp(ensure(reports_dir + "raw_reads_quality_report_script.Rmd", non_empty=True)),
    shell:
        "cat {input} > {output} 2>/dev/null"


rule make_raw_reads_quality_report:
    input:
        script=rules.raw_reads_quality_report.output[0],
        ref_size=results_all_samples_dir + reference_dir_name + "/ref_size.txt",
    output:
        reports_dir + "Raw_Reads_Report.html",
    params:
        reports_dir=reports_dir,
        working_dir=working_dir,
        version=__version__,
    threads: 16
    conda:
        envs_folder + "report_env.yaml"
    script:
        "{params.reports_dir}raw_reads_quality_report_script.Rmd"


rule genome_quality_report:
    input:
        report_scripts + "header.Rmd",
        reports_dir + "genome_quality_script.Rmd" if config["GENOME_ASSESSMENT"] == True else [],
    output:
        temp(ensure(reports_dir + "genome_quality_report_script.Rmd", non_empty=True)),
    shell:
        "cat {input} > {output} 2>/dev/null"


rule make_genome_quality_report:
    input:
        script=rules.genome_quality_report.output[0],
        ref_size=results_all_samples_dir + reference_dir_name + "/ref_size.txt",
    output:
        reports_dir + "Genomes_Report.html",
    params:
        reports_dir=reports_dir,
        working_dir=working_dir,
        version=__version__,
    threads: 16
    conda:
        envs_folder + "report_env.yaml"
    script:
        "{params.reports_dir}genome_quality_report_script.Rmd"


rule resistance_report:
    input:
        report_scripts + "header.Rmd",
        reports_dir + "resistance_script.Rmd" if config["RESISTANCE"] == True else [],
    output:
        temp(ensure(reports_dir + "resistance_report_script.Rmd", non_empty=True)),
    shell:
        "cat {input} > {output} 2>/dev/null"


rule make_resistance_report:
    input:
        script=rules.resistance_report.output[0],
    output:
        reports_dir + "Resistance_Report.html",
    params:
        reports_dir=reports_dir,
        working_dir=working_dir,
        version=__version__,
    threads: 16
    conda:
        envs_folder + "report_env.yaml"
    script:
        "{params.reports_dir}resistance_report_script.Rmd"


rule virulence_report:
    input:
        report_scripts + "header.Rmd",
        reports_dir + "virulence_script.Rmd" if config["VIRULENCE"] == True else [],
    output:
        temp(ensure(reports_dir + "virulence_report_script.Rmd", non_empty=True)),
    shell:
        "cat {input} > {output} 2>/dev/null"


rule make_virulence_report:
    input:
        script=rules.virulence_report.output[0],
    output:
        reports_dir + "Virulence_Report.html",
    params:
        reports_dir=reports_dir,
        working_dir=working_dir,
        version=__version__,
    threads: 16
    conda:
        envs_folder + "report_env.yaml"
    script:
        "{params.reports_dir}virulence_report_script.Rmd"


rule mlst_report:
    input:
        report_scripts + "header.Rmd",
        reports_dir + "mlst_script.Rmd" if config["MLST"] == True else [],
    output:
        temp(ensure(reports_dir + "mlst_report_script.Rmd", non_empty=True)),
    shell:
        "cat {input} > {output} 2>/dev/null"


rule make_mlst_report:
    input:
        script=rules.mlst_report.output[0],
    output:
        reports_dir + "MLST_Report.html",
    params:
        reports_dir=reports_dir,
        working_dir=working_dir,
        version=__version__,
    threads: 16
    conda:
        envs_folder + "report_env.yaml"
    script:
        "{params.reports_dir}mlst_report_script.Rmd"


rule cgsnps_report:
    input:
        report_scripts + "header.Rmd",
        reports_dir + "snps_and_phylogeny_script.Rmd" if config["SNP_ANALYSIS"] == True else [],
    output:
        temp(ensure(reports_dir + "cgsnps_report_script.Rmd", non_empty=True)),
    shell:
        "cat {input} > {output} 2>/dev/null"


rule make_cgsnps_report:
    input:
        script=rules.cgsnps_report.output[0],
    output:
        reports_dir + "cgSNPs_Report.html",
    params:
        reports_dir=reports_dir,
        working_dir=working_dir,
        version=__version__,
    threads: 16
    conda:
        envs_folder + "report_env.yaml"
    script:
        "{params.reports_dir}cgsnps_report_script.Rmd"


rule cgmlst_report:
    input:
        report_scripts + "header.Rmd",
        reports_dir + "cgmlst_script.Rmd" if config["COREGENOME_MLST"] == True else [],
    output:
        temp(ensure(reports_dir + "cgmlst_report_script.Rmd", non_empty=True)),
    shell:
        "cat {input} > {output} 2>/dev/null"


rule make_cgmlst_report:
    input:
        script=rules.cgmlst_report.output[0],
    output:
        reports_dir + "cgMLST_Report.html",
    params:
        reports_dir=reports_dir,
        working_dir=working_dir,
        version=__version__,
    threads: 16
    conda:
        envs_folder + "report_env.yaml"
    script:
        "{params.reports_dir}cgmlst_report_script.Rmd"

rule pangenome_report:
    input:
        report_scripts + "header.Rmd",
        reports_dir + "pangenome_script.Rmd" if config["PANGENOME"] == True else [],
    output:
        temp(ensure(reports_dir + "pangenome_report_script.Rmd", non_empty=True)),
    shell:
        "cat {input} > {output} 2>/dev/null"


rule make_pangenome_report:
    input:
        script=rules.pangenome_report.output[0],
    output:
        reports_dir + "Pangenome_Report.html",
    params:
        reports_dir=reports_dir,
        working_dir=working_dir,
        version=__version__,
    threads: 16
    conda:
        envs_folder + "report_env.yaml"
    script:
        "{params.reports_dir}pangenome_report_script.Rmd"


rule cdifficile_report:
    input:
        report_scripts + "header.Rmd",
        reports_dir + "cdiff_mge_script.Rmd" if config["C_DIFFICILE_MODULE"] == True else [],
    output:
        temp(ensure(reports_dir + "cdifficile_report_script.Rmd", non_empty=True)),
    shell:
        "cat {input} > {output} 2>/dev/null"


rule make_cdifficile_report:
    input:
        script=rules.cdifficile_report.output[0],
    output:
        reports_dir + "C_difficile_Report.html",
    params:
        reports_dir=reports_dir,
        working_dir=working_dir,
        version=__version__,
    threads: 16
    conda:
        envs_folder + "report_env.yaml"
    script:
        "{params.reports_dir}cdifficile_report_script.Rmd"
