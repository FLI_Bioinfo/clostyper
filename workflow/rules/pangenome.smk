"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# TODO roary2svg if samples > 100 change the width and height per sample in the plot


def PANGENOME():
    if config["PANGENOME"] == True:
        PANGENOME = [
            results_all_samples_dir + pangenome_dir_name + "/pan_genome_reference.fa", 
            results_all_samples_dir + pangenome_dir_name + "/summary_statistics.txt", 
            results_all_samples_dir + pangenome_dir_name + "/gene_presence_absence.csv", 
            rules.add_header_summary.output[0],
            ]
        if config["plot_with_roary2svg"] == True: 
            PANGENOME.append(
                [
                    results_all_samples_dir + pangenome_dir_name + "/pan.svg",
                    results_all_samples_dir + pangenome_dir_name + "/acc.svg"
                    ]
                )
        if config["plot_with_roary_plots"] == True: 
            PANGENOME.append(
                [
                    results_all_samples_dir + pangenome_dir_name + "/pangenome_frequency.png", 
                    results_all_samples_dir + pangenome_dir_name + "/pangenome_pie.png"
                    ]
                )
        return PANGENOME
    else:
        return [] #[results_all_samples_dir + "no_pangenome_results.txt"]



rule prepare_sheet_input_panaroo_from_bakta:
    input:
        expand(rules.bakta.output[0], sample=SAMPLES),
        expand(rules.bakta.output[0], sample=GENOMES),
    output:
        temp(results_all_samples_dir + "input_panaroo_from_bakta.txt"),
    threads: 1
    shell:
        """
        echo -ne "" > {output} &&
        for path in {input}; do
            if [[ -s $path ]]; then
                echo -ne "$path\n" >> {output};
            fi
        done
        """


def pangenome_input():
    if config["Annotation_software"] == "prokka":
        return [expand(rules.prokka.output[0], sample=SAMPLES), expand(rules.prokka.output[0], sample=GENOMES)]
    elif config["Annotation_software"] == "bakta" and config["Pangenome_software"] == "panaroo":
        return rules.prepare_sheet_input_panaroo_from_bakta.output
    elif config["Annotation_software"] == "bakta" and config["Pangenome_software"] == "roary":
        return [expand(rules.bakta.output[0], sample=SAMPLES), expand(rules.bakta.output[0], sample=GENOMES)]
    else:  # default prokka
        return [expand(rules.prokka.output[0], sample=SAMPLES), expand(rules.prokka.output[0], sample=GENOMES)]


# choose pangenome_software
if config["Pangenome_software"] == "roary":

    ruleorder: roary > panaroo > create_accessory_binary_genes_newick_from_panaroo

elif config["Pangenome_software"] == "panaroo":

    ruleorder: panaroo > roary
    ruleorder: create_accessory_binary_genes_newick_from_panaroo > roary

else:  # default roary

    ruleorder: roary > panaroo > create_accessory_binary_genes_newick_from_panaroo


rule roary:  #run roary
    input:
        pangenome_input(),
    output:
        Roary_pangenome_fa=results_all_samples_dir + pangenome_dir_name + "/pan_genome_reference.fa",
        Roary_pangenome_summary=results_all_samples_dir + pangenome_dir_name + "/summary_statistics.txt",
        #Roary_aln= results_dir + "roary/core_gene_alignment.aln",
        roary_presenceabsence=results_all_samples_dir + pangenome_dir_name + "/gene_presence_absence.csv",
        roary_acc=results_all_samples_dir + pangenome_dir_name + "/accessory_binary_genes.fa.unrooted.newick",
    threads: workflow.cores * 0.5
    log:
        results_all_samples_dir + "roary.log",
    conda:
        envs_folder + "roary.yaml"
    params:
        #options=config["roary_params"],
        tmp_dir=temp(directory(temporary_todelete)),
        Roary_dir=results_all_samples_dir + pangenome_dir_name + "/",
        core_genes_alignemnt="-e " if config["roary"]["params"]["core_genes_alignemnt"] == True else "",
        mafft_alignment="-n " if config["roary"]["params"]["mafft_alignment"] == True else "",
        sequence_identity_threshold_blastp="-i " + str(config["roary"]["params"]["sequence_identity_threshold_blastp"]) if config["roary"]["params"]["sequence_identity_threshold_blastp"] else "",
        core_genome_threshold="-cd " + str(config["roary"]["params"]["core_genome_threshold"]) if config["roary"]["params"]["core_genome_threshold"] else "",
        QC_report_with_Kraken="-qc " + "-k " + config["roary"]["params"]["Kraken_database_path"] if config["roary"]["params"]["QC_report_with_Kraken"] == True and config["roary"]["params"]["Kraken_database_path"] else "",
        create_R_plots="-r " if config["roary"]["params"]["create_R_plots"] == True else "",
        dont_split_paralogs="-s " if config["roary"]["params"]["dont_split_paralogs"] == True else "",
        translation_table="-t " + str(config["roary"]["params"]["translation_table"]) if config["roary"]["params"]["translation_table"] else "",
        allow_paralogs_in_core_alignment="-ap " if config["roary"]["params"]["allow_paralogs_in_core_alignment"] == True else "",
        MCL_inflation_value="-iv " + str(config["roary"]["params"]["MCL_inflation_value"]) if config["roary"]["params"]["MCL_inflation_value"] else "",
        extra_params=config["roary"]["params"]["extra_params"],
    shell:
        """
        export PERL5LIB=""
        bash {bin_dir}fixRoaryOutDirError.sh {params.Roary_dir} {params.tmp_dir}
        roary -p {threads} -f {params.Roary_dir} \
        {params.core_genes_alignemnt}\
        {params.mafft_alignment}\
        {params.sequence_identity_threshold_blastp}\
        {params.core_genome_threshold}\
        {params.QC_report_with_Kraken}\
        {params.create_R_plots}\
        {params.dont_split_paralogs}\
        {params.translation_table}\
        {params.allow_paralogs_in_core_alignment}\
        {params.MCL_inflation_value}\
        {params.extra_params} \
        {input} 2>&1 | tee {log} >/dev/null
        mv {log} {params.Roary_dir}
        """


rule panaroo:
    input:
        pangenome_input(),
    output:
        results_all_samples_dir + pangenome_dir_name + "/pan_genome_reference.fa",
        results_all_samples_dir + pangenome_dir_name + "/summary_statistics.txt",
        results_all_samples_dir + pangenome_dir_name + "/gene_presence_absence.csv",
        results_all_samples_dir + pangenome_dir_name + "/gene_presence_absence_roary.csv",
        results_all_samples_dir + pangenome_dir_name + "/gene_presence_absence.Rtab",
        #results_all_samples_dir + pangenome_dir_name + "/accessory_binary_genes.fa.newick",
    threads: 32 #workflow.cores * 0.5
    log:
        results_all_samples_dir + pangenome_dir_name + "/panaroo.log",
    conda:
        envs_folder + "panaroo.yaml"
    params:
        tmp_dir=temp(directory(temporary_todelete)),
        dir=directory(results_all_samples_dir + pangenome_dir_name + "/"),
        clean_mode=config["panaroo"]["params"]["clean_mode"],
        remove_invalid_genes="--remove-invalid-genes " if config["panaroo"]["params"]["remove_invalid_genes"] == True else "",
        #Matching:
        sequence_identity_threshold="-c " + str(config["panaroo"]["params"]["sequence_identity_threshold"]) if config["panaroo"]["params"]["sequence_identity_threshold"] else "",
        family_threshold="--family_threshold " + str(config["panaroo"]["params"]["family_threshold"]) if config["panaroo"]["params"]["family_threshold"] else "",
        length_difference_cutoff="--len_dif_percent " + str(config["panaroo"]["params"]["length_difference_cutoff"]) if config["panaroo"]["params"]["length_difference_cutoff"] else "",
        merge_paralogs="--merge_paralogs " if config["panaroo"]["params"]["merge_paralogs"] == True else "",
        #Refined
        search_radius="--search_radius " + str(config["panaroo"]["params"]["search_radius"]) if config["panaroo"]["params"]["search_radius"] else "",
        refind_prop_match="--refind_prop_match " + str(config["panaroo"]["params"]["refind_prop_match"]) if config["panaroo"]["params"]["refind_prop_match"] else "",
        #Graph correction:
        min_trailing_support="--min_trailing_support " + str(config["panaroo"]["params"]["min_trailing_support"]) if config["panaroo"]["params"]["min_trailing_support"] else "",
        trailing_recursive="--trailing_recursive " + str(config["panaroo"]["params"]["trailing_recursive"]) if config["panaroo"]["params"]["trailing_recursive"] else "",
        edge_support_threshold="--edge_support_threshold " + str(config["panaroo"]["params"]["edge_support_threshold"]) if config["panaroo"]["params"]["edge_support_threshold"] else "",
        length_outlier_support_proportion="--length_outlier_support_proportion " + str(config["panaroo"]["params"]["length_outlier_support_proportion"]) if config["panaroo"]["params"]["length_outlier_support_proportion"] else "",
        high_var_flag="--high_var_flag " + str(config["panaroo"]["params"]["high_var_flag"]) if config["panaroo"]["params"]["high_var_flag"] else "",
        min_edge_support_sv="--min_edge_support_sv " + str(config["panaroo"]["params"]["min_edge_support_sv"]) if config["panaroo"]["params"]["min_edge_support_sv"] else "",
        remove_by_consensus="--remove_by_consensus " if config["panaroo"]["params"]["remove_by_consensus"] == True else "",
        all_seq_in_graph="--all_seq_in_graph " if config["panaroo"]["params"]["all_seq_in_graph"] == True else "",
        no_clean_edges="--no_clean_edges " if config["panaroo"]["params"]["no_clean_edges"] == True else "",
        #Gene alignment:
        alignment_output="--alignment " + str(config["panaroo"]["params"]["alignment_output"]) if config["panaroo"]["params"]["alignment_output"] else "",
        core_threshold="--core_threshold " + str(config["panaroo"]["params"]["core_threshold"]) if config["panaroo"]["params"]["core_threshold"] else "",
        aligner="--aligner " + str(config["panaroo"]["params"]["aligner"]) if config["panaroo"]["params"]["aligner"] else "",
    shell:
        """
        bash {bin_dir}fixRoaryOutDirError.sh {params.dir} {params.tmp_dir} #remove old results
        mkdir -p {params.dir} #create to save the log
        panaroo -i {input} \
        -o {params.dir} \
        --clean-mode {params.clean_mode} \
        --threads {threads} \
        {params.remove_invalid_genes} \
        {params.sequence_identity_threshold} \
        {params.family_threshold} \
        {params.length_difference_cutoff} \
        {params.merge_paralogs} \
        {params.search_radius} \
        {params.refind_prop_match} \
        {params.min_trailing_support} \
        {params.trailing_recursive} \
        {params.edge_support_threshold} \
        {params.length_outlier_support_proportion} \
        {params.remove_by_consensus} \
        {params.high_var_flag} \
        {params.min_edge_support_sv} \
        {params.all_seq_in_graph} \
        {params.no_clean_edges} \
        {params.alignment_output} \
        {params.aligner} \
        {params.core_threshold} \
         2>&1 | tee {log} >/dev/null
        """


rule create_accessory_binary_genes_from_panaroo:
    input:
        rules.panaroo.output[4],
    output:
        results_all_samples_dir + pangenome_dir_name + "/accessory_binary_genes.fa",
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        cat {input} \
        | csvtk replace -t -f 2-$(expr $(csvtk ncol -t  {input})) -p 1 -r A \
        | csvtk replace -t -f 2-$(expr $(csvtk ncol -t  {input})) -p 0 -r C \
        | csvtk transpose -t \
        | csvtk del-header  -t \
        | sed -e 's/\t/,/1' \
        | sed -e 's/\t//g' \
        | csvtk csv2tab \
        | seqkit tab2fx > {output}
        """


rule create_accessory_binary_genes_newick_from_panaroo:
    input:
        rules.create_accessory_binary_genes_from_panaroo.output,
    output:
        results_all_samples_dir + pangenome_dir_name + "/accessory_binary_genes.fa.unrooted.newick",
    conda:
        envs_folder + "phylogeny.yaml"
    shell:
        """
        fasttree -fastest -nt {input} > {output}
        """


use rule midpoint_tree_rooting as midpoint_tree_rooting_accessory with:
    input:
        phylogeny_tree = rules.create_accessory_binary_genes_newick_from_panaroo.output[0],
    output:
        tre=os.path.join(
            results_all_samples_dir, 
            pangenome_dir_name, 
            "accessory_binary_genes.fa.newick"
        ),
        version=os.path.join(results_all_samples_dir + pangenome_dir_name, "versions/Treebender_version.txt"),


rule pangenome_plots:
    input:
        results_all_samples_dir + pangenome_dir_name + "/accessory_binary_genes.fa.newick",
        results_all_samples_dir + pangenome_dir_name + "/gene_presence_absence_roary.csv" if config["Pangenome_software"] == "panaroo" else results_all_samples_dir + pangenome_dir_name + "/gene_presence_absence.csv",
    output:
        Roary_dir=results_all_samples_dir + pangenome_dir_name + "/pangenome_frequency.png",
        Roary_pie=results_all_samples_dir + pangenome_dir_name + "/pangenome_pie.png",
    conda:
        envs_folder + "roary_plots.yaml"
    params:
        #options=config["roary_params"],
        Roary_dir=results_all_samples_dir + pangenome_dir_name + "",
    shell:
        " python {bin_dir}pangenome_plots.py {input[0]} {input[1]}"
        " && mv -t {params.Roary_dir} pangenome_frequency.png pangenome_matrix.png pangenome_pie.png"


# source ~/.bash_profile SVG.pm
def pangenome_svg_input():
    if config["Pangenome_software"] == "roary":
        return results_all_samples_dir + pangenome_dir_name + "/gene_presence_absence.csv"
    elif config["Pangenome_software"] == "panaroo":
        return results_all_samples_dir + pangenome_dir_name + "/gene_presence_absence_roary.csv"
    else:  # default prokka
        return results_all_samples_dir + pangenome_dir_name + "/gene_presence_absence.csv"


rule pangenome_svg:  #run roary
    input:
        pangenome_svg_input(),
    output:
        pan_svg=results_all_samples_dir + pangenome_dir_name + "/pan.svg",
    conda:
        envs_folder + "perl.yaml"
    shell:
        "{bin_dir}roary2svg.pl {input} > {output.pan_svg}"


rule pangenome_accessory_tree_plot:  #run roary
    input:
        results_all_samples_dir + pangenome_dir_name + "/accessory_binary_genes.fa.newick",
    output:
        results_all_samples_dir + pangenome_dir_name + "/acc.svg",
    conda:
        envs_folder + "perl.yaml"
    shell:
        " nw_display -S -s -w 1024 -l 'font-size:12;font-family:sans-serif;' -i 'opacity:0' -b 'opacity:0' -v 16 {input} > {output}"


rule add_header_summary:
    input: results_all_samples_dir + pangenome_dir_name + "/summary_statistics.txt",
    output: results_all_samples_dir + pangenome_dir_name + "/summary_statistics_header.txt",
    conda: envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk add-header -tH -n Group,Description,Total {input} -o {output}
        """

# rule write_report_script_pangenome: 
#     input:
#         input=PANGENOME(),
#         pangstats= results_all_samples_dir + pangenome_dir_name + "/summary_statistics_header.txt",
#     output:
#         report = temp(reports_dir + "pangenome_script.Rmd"),
#     threads: 2
#     params:
#         results_dir=results_all_samples_dir,
#         reports_dir=reports_dir,
#         Annotation_software=config["Annotation_software"],
#         Pangenome_software=config["Pangenome_software"],
#         pangstats= working_dir + results_all_samples_dir + pangenome_dir_name + "/summary_statistics_header.txt",
#         Pangenome_pie= working_dir + results_all_samples_dir + pangenome_dir_name + "/pangenome_pie.png" if config["plot_with_roary_plots"] == True else "",
#         acc_distrib= working_dir + results_all_samples_dir + pangenome_dir_name + "/pangenome_frequency.png" if config["plot_with_roary_plots"] == True else "",
#         pan_svg= working_dir + results_all_samples_dir + pangenome_dir_name + "/pan.svg" if config["plot_with_roary2svg"] == True else "",
#         acc_svg= working_dir + results_all_samples_dir + pangenome_dir_name + "/acc.svg" if config["plot_with_roary2svg"] == True else "",
#     script:
#         bin_dir + "write_report_script_pangenome.sh"
