"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


rule get_resfinder_db:
    output:
        db = temp(directory(os.path.join(temporary_todelete, "resfinder_db"))),
    threads: 2
    conda:
        envs_folder + "resfinder.yaml"
    log:
        os.path.join(results_all_samples_dir, all_resistance_dir_name, "versions/resfinder_database.log"),
    shell:
        r"""
        git clone https://bitbucket.org/genomicepidemiology/resfinder_db/ {output.db}/resfinder_db  &>  {log}
        git clone https://bitbucket.org/genomicepidemiology/pointfinder_db/ {output.db}/pointfinder_db &>>  {log}
        git clone https://bitbucket.org/genomicepidemiology/disinfinder_db/ {output.db}/disinfinder_db &>>  {log}
        """


rule resfinder_version:
    input:
        db = rules.get_resfinder_db.output.db,
        dir = os.path.join(temporary_todelete, "resfinder_db"),
    output:
        versions = os.path.join(results_all_samples_dir, all_resistance_dir_name, "versions/resfinder.txt"),
    threads: 2
    conda:
        envs_folder + "resfinder.yaml"
    shell:
        r"""
        python -m resfinder -v  | sed 's/^/resfinder /' &>  {output.versions}
        cat {input.dir}/resfinder_db/VERSION | sed 's/^/resfinder_db /' &>>  {output.versions}
        cat {input.dir}/pointfinder_db/VERSION | sed 's/^/pointfinder_db /' &>>  {output.versions}
        cat {input.dir}/disinfinder_db/VERSION | sed 's/^/disinfinder_db /' &>>  {output.versions}
        """


rule run_resfinder:
    input:
        contig = assembly_all + "{sample}.fasta",
        db = rules.get_resfinder_db.output.db,
    output:
        # json = os.path.join(results_per_sample_dir, "{sample}/resistance/resfinder/{sample}.json"),
        tab = os.path.join(results_per_sample_dir, "{sample}/resistance/resfinder/ResFinder_results_tab.txt"),
        pheno = os.path.join(results_per_sample_dir, "{sample}/resistance/resfinder/pheno_table.txt"),
        Others = [
            temp(directory(os.path.join(results_per_sample_dir, "{sample}/resistance/resfinder/resfinder_blast"))),
            temp(directory(os.path.join(results_per_sample_dir, "{sample}/resistance/resfinder/pointfinder_blast"))),
            temp(directory(os.path.join(results_per_sample_dir, "{sample}/resistance/resfinder/disinfinder_blast"))),
        ]
    threads: 8
    conda:
        envs_folder + "resfinder.yaml"
    log:
        os.path.join(results_per_sample_dir, "{sample}/resistance/resfinder/resfinder.log"),
    params:
        min_identity = lambda config: f"-t {val}" if (val := config.get("resfinder", {}).get("params", {}).get("min_identity")) else "",
        min_coverage = lambda config: f"-l {val}" if (val := config.get("resfinder", {}).get("params", {}).get("min_coverage")) else "",
        species = config.get("resfinder", {}).get("params", {}).get("species") or "Other",  
        args = config.get("resfinder", {}).get("params", {}).get("extraparams") or "",
        prefix = os.path.join(results_per_sample_dir, "{sample}/resistance/resfinder")
    shell:
        r"""
        python -m  resfinder -ifa {input.contig} --outputPath {params.prefix} \
        -db_res {input.db}/resfinder_db  -db_disinf  {input.db}/disinfinder_db -db_point {input.db}/pointfinder_db \
        -s {params.species} {params.args} {params.min_identity} {params.min_coverage}  --point --acquired  &> {log}
        """


rule add_sample_name_resfinder:
    input:
        res = rules.run_resfinder.output.tab, 
    output:
        tsv = os.path.join(results_per_sample_dir, "{sample}/resistance/resfinder/{sample}_resfinder.tsv"),
    conda:
        envs_folder + "data_parse.yaml"
    params:
        headers = '"Sample","Resistance gene","Identity","Alignment Length/Gene Length","Coverage","Position in reference","Contig","Position in contig","Phenotype","Accession no."'
    shell:
        r"""
        if [[ $(csvtk nrow -tH {input.res} ) -gt 1 ]]; then
            csvtk mutate2 --quiet --at 1 -t -e " '{wildcards.sample}' " -n Sample {input.res} | 
            csvtk cut -t -f {params.headers} | csvtk uniq -t -f 1- -o {output.tsv}
        else 
            echo {params.headers} | csvtk cut -f 1- -T > {output.tsv}
        fi
        """


rule add_sample_name_resfinder_phenotype:
    input:
        res = rules.run_resfinder.output.pheno, 
    output:
        tsv = temp(os.path.join(results_per_sample_dir, "{sample}/resistance/resfinder/{sample}_phenotype.tsv")),
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk cut -tH -f 2,1,5 {input.res} | csvtk add-header -t -n Class,Antimicrobial,{wildcards.sample}  -o {output.tsv}
        """


rule collect_resfinder_phenotype:
    input:
        expand(rules.add_sample_name_resfinder_phenotype.output.tsv, sample =SAMPLES), 
        expand(rules.add_sample_name_resfinder_phenotype.output.tsv, sample =GENOMES), 
    output:
        tsv = os.path.join(results_all_samples_dir, all_resistance_dir_name,"resfinder_phenotype.tsv"),
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk join -t -f 1,2 --na - {input}  | csvtk transpose -t -o {output.tsv}
        """



rule collect_resfinder_phenotype_excel:
    input:
        rules.collect_resfinder_phenotype.output.tsv 
    output:
        excel = os.path.join(results_all_samples_dir, all_resistance_dir_name,"resfinder_phenotype.xlsx"),
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk csv2xlsx -t -f {input} -o {output.excel}
        """


rule detail_summary_resfinder:
    input:
        samples = expand(rules.add_sample_name_resfinder.output.tsv, sample=SAMPLES),
        genomes = expand(rules.add_sample_name_resfinder.output.tsv, sample=GENOMES),
    output:
        FOF = temp(temporary_todelete + "resfinder_paths.txt"),
        tsv = os.path.join(results_all_samples_dir, all_resistance_dir_name, "resfinder_detailed.tsv"),
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        : > {output.FOF} && for path in {input}; do [[ -s $path ]] && echo "$path" >> {output.FOF}; done

        [[ $(wc -l < {output.FOF}) -gt 0 ]] && 
            csvtk concat -t --infile-list {output.FOF} --out-file {output.tsv} -i -E || \
            cp {input} {output.tsv}
        """



rule resfinder_summary_temp:
    input:
        amr_results = rules.detail_summary_resfinder.output.tsv,
    output:
        temp = temp(temporary_todelete  + "resfinder_summary.temp"),
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        [[ -s {input.amr_results} ]] && 
           csvtk cut --quiet -t -f "Sample","Resistance gene","Coverage" {input.amr_results} | 
           csvtk fold --quiet -t -f 1,2 -v 3 | 
           csvtk spread --quiet -t -k 2 -v 3  --na "-"  -o {output.temp} || true
        """


rule resfinder_summary:
    input:
        res = rules.resfinder_summary_temp.output.temp,
        names = rules.isolates_list.output.isolates_list
    output:
        tsv = os.path.join(results_all_samples_dir, all_resistance_dir_name, "resfinder_summary.tsv"),
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        if [[ $(csvtk nrow -tH {input.res} ) -gt 1 ]]; then
            csvtk join -t --na - -f 1 -k  {input.names} {input.res} -o {output.tsv}
        else 
            csvtk mutate2 -t -n "n. genes" -e "0"  {input.names} -o {output.tsv} 
        fi 
        """


rule resfinder_summary_excel:
    input:
        tsv = rules.resfinder_summary.output.tsv,
    output:
        excel = os.path.join(results_all_samples_dir, all_resistance_dir_name, "resfinder_summary.xlsx"),
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        csvtk csv2xlsx -t -f {input.tsv} -o {output.excel}
        """