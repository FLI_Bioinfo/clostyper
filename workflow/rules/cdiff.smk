"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

C_DIIFICILE_CLADES=config["C_DIIFICILE_CLADES"]
C_DIFFICILE_TXOINS=config["C_DIFFICILE_TXOINS"]
C_DIFFICILE_MOBILE_ELEMENTS=config["C_DIFFICILE_MOBILE_ELEMENTS"]
C_DIFFICILE_RESISTNACE_MUTATIONS=config["C_DIFFICILE_RESISTNACE_MUTATIONS"]

def C_DIFFICILE_MODULE():
    if config["C_DIFFICILE_MODULE"] == True:
        C_DIFFICILE_MODULE = [rules.collect_cdifficile_results.output]
        return C_DIFFICILE_MODULE
    else:
        return []


if config["MLST_software"] == "mlst":

    ruleorder: collect_mlst > collect_stringMLST

elif config["MLST_software"] == "stringmlst":

    ruleorder: collect_stringMLST > collect_mlst  

else: 

    ruleorder: collect_mlst > collect_stringMLST


clostyper_database = config["clostyper_database"]
clostyper_database = add_trailing_slash(clostyper_database)
C_difficile_DB = os.path.join(clostyper_database or "", "difficile/")

res_mutations_db_dir = C_difficile_DB + "res_mutations/"
res_mutations_db_name = "file.fasta"
res_mutations_aminoacid = "protein_positions.bed"
res_mutations_nucleotide = "nucleotide_positions.bed-only_the_middle_nucleotide"

transposon_coverage = str(config["c_difficile_db"]["transposons"]["coverage"])
plasmids_coverage = str(config["c_difficile_db"]["plasmids"]["coverage"])
phages_coverage = str(config["c_difficile_db"]["phages"]["coverage"])

"""
clade designation
- based on mapping MLST results
- based on "stepwise" estimating of mash distances to representative genomes. A single mash dist cutoff is not possible for all clades (https://elifesciences.org/articles/64325)
"""


rule filter_mlst:
    input:
        mlst_results=os.path.join(results_all_samples_dir, all_mlst_dir_name, "mlst.tsv"),
        names = rules.isolates_list.output.isolates_list
    output:
        onlydiff = temp(os.path.join(temporary_todelete, "mlst_onlydiff.tsv")),
        filtered = temp(os.path.join(temporary_todelete, "mlst_filtered.tsv")),
    conda:
        envs_folder + "data_parse.yaml"
    threads: 2
#    parmas: 
    shell:
        r"""
        csvtk grep -t -p cdifficile -f Scheme {input.mlst_results} -o {output.onlydiff}
        csvtk join -tH --na none -f 1 -k  {input.names} {output.onlydiff} -o {output.filtered}
        """


rule assign_cdiffcile_clade_after_mlst:
    input:
        mlst_results= os.path.join(results_all_samples_dir, all_mlst_dir_name, "mlst.tsv") if config["MLST_software"] == "stringmlst" else rules.filter_mlst.output.filtered,
        mlst_clades=C_difficile_DB + "clades/cdiff_mlst_clades.tsv",
    output:
        mlst = temp(os.path.join(temporary_todelete, "Clades_after_mlst.tsv")),
    conda:
        envs_folder + "data_parse.yaml"
    threads: 2
    message:
        "Assigning C. difficile clades info based on MLST results"
    shell:
        r"""
        csvtk join -t -f ST {input.mlst_results} {input.mlst_clades} --keep-unmatched --na none | 
            csvtk cut -t -f Sample,ST,"Clade [mlst]" -o {output.mlst}
        """


rule assign_cdiffcile_clade_with_mash:
    input:
        contig = assembly_all + "{sample}.fasta",
        mash_database = C_difficile_DB + "clades/cdiff_clade_representatives.msh",
    output:
        results_per_sample_dir + "{sample}/" + cdiff_clade_dir_name + "/{sample}_mash.raw.txt",
        results_per_sample_dir + "{sample}/" + cdiff_clade_dir_name + "/{sample}_distance_cutoff.txt",
        results_per_sample_dir + "{sample}/" + cdiff_clade_dir_name + "/{sample}.msh",
    log:
        results_per_sample_dir + "{sample}/" + cdiff_clade_dir_name + "/{sample}_mash.log",
    params:
        directory(results_per_sample_dir + "{sample}/" + cdiff_clade_dir_name),
        sketch_size="1000",
    #        step_value='0.001'
    conda:
        envs_folder + "blast.yaml"
    threads: 2
    message:
        "Estimating MASH distance of sample: {wildcards.sample} to C. difficile representative genomes"
    shell:
        #https://unix.stackexchange.com/questions/40786/how-to-do-integer-float-calculations-in-bash-or-other-languages-frameworks
        #https://stackoverflow.com/questions/24777597/value-too-great-for-base-error-token-is-08
        r"""
        mkdir -p {params[0]}
        mash sketch -p {threads} -o {params[0]}/{wildcards.sample} -s {params.sketch_size} {input[0]} 2> {log}
        DISTANCE=0
        until [[ $DISTANCE -ge 20 ]] ||  [[ -s {output[0]} ]] ; do
            MASH_DISTANCE=$( printf %.3f\\\\n "$((${{DISTANCE#}} +   1  ))e-3"  | sed 's/,/./' )
            echo "${{MASH_DISTANCE}}" > {output[1]}
            mash dist {output[2]} {input[1]} -d ${{MASH_DISTANCE}} > {output[0]};
            let "DISTANCE=DISTANCE+1";
        done
        if [[ -s {output[0]} ]]; then
            :
        else
            echo -e "{wildcards.sample}\\tnone\\tnone\\tnone\\tnone" > {output[0]}
        fi
        """



rule collect_cdiffcile_clade_results_after_mash:
    input:
        mash_res=[expand(results_per_sample_dir + "{sample}/" + cdiff_clade_dir_name + "/{sample}_mash.raw.txt", sample=SAMPLES), expand(results_per_sample_dir + "{sample}/" + cdiff_clade_dir_name + "/{sample}_mash.raw.txt", sample=GENOMES)],
        mash_representatives=C_difficile_DB + "clades/representatives.tsv",
    output:
        temp(results_all_samples_dir + cdifficile_dir_name + "/Clades_after_mash.tsv"),
        temp(results_all_samples_dir + cdifficile_dir_name + "/mash_all.tsv"),
        temp(results_all_samples_dir + cdifficile_dir_name + "/temp_fof"),
        temp(results_all_samples_dir + cdifficile_dir_name + "/temp_mash_all.tsv"),
    conda:
        envs_folder + "data_parse.yaml"
    threads: 2
    message:
        "Collecting MASH distance and assign clades"
    shell:
        #csvtk handles FOF better than input files passed in commandline
        r"""
        echo -ne "" >  {output[2]} && 
        for file in {input.mash_res}; do echo $file >> {output[2]} ; done && 
        csvtk concat --quiet --ignore-empty-row -tH --infile-list {output[2]} -o {output[3]}  2>/dev/null && 
        csvtk add-header --quiet  -t  -n sample,reference,Mash-distance,P-value,Matching-hashes {output[3]} | 
            csvtk replace --quiet -t -f 1 -p "(.+/)" -r "" | 
            csvtk replace --quiet -t -f 2 -p "(.+/)" -r "" | 
            csvtk replace --quiet -t -f 1,2 -p ".fasta" -r "" | 
            csvtk cut --quiet -t -f 2,1,3,4,5 -o {output[1]}
        
        csvtk join --quiet {output[1]} {input.mash_representatives} --keep-unmatched --na none -t | 
            csvtk cut --quiet -t -f  sample,clade,reference,Mash-distance,P-value,Matching-hashes | 
            csvtk uniq --quiet -f sample,clade -t | 
            csvtk sort --quiet -t -k sample | 
            csvtk summary --quiet -t -f clade:collapse,Mash-distance:collapse -g sample | 
            csvtk rename --quiet -t -f sample,"clade:collapse","Mash-distance:collapse" -n Sample,"Clade [mash]","Mash dist to clade" -o {output[0]}
        """


rule collect_cdiffcile_clade_after_mash_and_mlst:
    input:
        rules.isolates_list.output[0],
        rules.assign_cdiffcile_clade_after_mlst.output[0],
        rules.collect_cdiffcile_clade_results_after_mash.output[0],
    output:
        results_all_samples_dir + cdifficile_dir_name + "/Clade_assignment.tsv",
    conda:
        envs_folder + "data_parse.yaml"
    threads: 2
    message:
        "Collecting C. difficile clades info from mash and MLST"
    shell:
        """
        csvtk join  --quiet -t -f 1 {input[0]} {input[1]} {input[2]} --keep-unmatched --na none -o {output[0]}
        """


"""
toxin profile
"""


rule cdiff_toxins_genes_blastn_detection:
    input:
        contig=assembly_all + "{sample}.fasta",
        toxin_database=C_difficile_DB + "toxins/cdiff_toxins.fasta",
    output:
        results_per_sample_dir + "{sample}/" + cdiff_toxin_dir_name+ "/{sample}_blastn_output.raw.txt",
    params:
        blastn_identity=90,
        blastn_evalue="1E-20",
        blastn_culling_limit="1",
        blastn_max_targets="10000",
    threads: 2
    message:
        "Running BLASTN to detect toxin genes in sample: {wildcards.sample}"
    conda:
        envs_folder + "blast.yaml"
    shell:
        r"""
        blastn -task blastn -dust no -perc_identity {params.blastn_identity} \
          -db {input.toxin_database} \
          -outfmt '6 qseqid qstart qend sseqid sstrand evalue pident length gaps slen stitle qseq' \
          -num_threads {threads} \
          -evalue {params.blastn_evalue} \
          -culling_limit {params.blastn_culling_limit} \
          -max_target_seqs {params.blastn_max_targets} \
          -query {input.contig}\
          -out {output[0]}
        """


rule cdiff_toxins_genes_clean_blastn_results:
    input:
        rules.cdiff_toxins_genes_blastn_detection.output[0],
    output:
        results_per_sample_dir + "{sample}/" + cdiff_toxin_dir_name+ "/{sample}_blastn_output.tsv",
    params:
        identity_threshold="90",
        coverage_threshold="70",
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        if [[ -s {input[0]} ]]; then
            cat {input} | 
                csvtk add-header --quiet -t -n contig_id,query_start,query_end,toxin_gene,strand,evalue,identity,aln_length,gaps,ref_len,description,sequence | 
                csvtk mutate2 --quiet -t -e ' (($aln_length - $gaps) / $ref_len) * 100 ' -n coverage | 
                csvtk mutate2 --quiet -t -e ' $toxin_gene + ":" +  $contig_id + "_" +  $query_start + "-" + $query_end ' -n gene_coords --numeric-as-string | 
                csvtk mutate2 --quiet -t -e " '{wildcards.sample}' " -n sample | 
                csvtk filter --quiet -t -f "coverage>{params.coverage_threshold}" | 
                csvtk filter --quiet -t -f "identity>{params.identity_threshold}" | 
                csvtk cut --quiet -t -f sample,toxin_gene,gene_coords,strand,evalue,identity,coverage,aln_length,description,sequence -o {output[0]}
        else
            touch {output[0]}
        fi
        """


rule cdiff_toxins_genes_extract_toxin_sequences:
    input:
        rules.cdiff_toxins_genes_clean_blastn_results.output[0],
    output:
        temp(results_per_sample_dir + "{sample}/" + cdiff_toxin_dir_name+ "/{sample}_tcdAB_sequences.fasta"),
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk grep --quiet -t -f toxin_gene -p TcdA -p TcdB {input} | 
            csvtk cut --quiet -t -f gene_coords,sequence | 
            csvtk del-header --quiet -t | 
            seqkit tab2fx --quiet 2>/dev/null | 
            seqkit seq --quiet --remove-gaps -o {output[0]} 2>/dev/null || touch {output[0]}
        """


rule cdiff_toxins_subtype_blastx_detection:
    input:
        toxin_seq=rules.cdiff_toxins_genes_extract_toxin_sequences.output[0],
        toxin_database=C_difficile_DB + "toxins/TcdA_and_B_subtypes.faa", 
    output:
        results_per_sample_dir + "{sample}/" + cdiff_toxin_dir_name+ "/{sample}_blastx_output.raw.txt",
    params:
        blastx_evalue="1E-20",
        blastx_culling_limit="1",
        blastx_max_targets="300",
        blastx_qcov_hsp_perc="30",
    threads: 2
    message:
        "Running BLASTX to detect the subtype of toxin A and B in sample: {wildcards.sample}"
    conda:
        envs_folder + "blast.yaml"
    shell:
        r"""
        if [[ -s {input[0]} ]]; then
            blastx -task blastx \
            -db {input.toxin_database}  \
            -outfmt '6 qseqid qstart qend sseqid evalue pident length gaps slen stitle qseq' \
            -query {input.toxin_seq} \
            -num_threads {threads} \
            -evalue {params.blastx_evalue} \
            -culling_limit {params.blastx_culling_limit} \
            -max_target_seqs {params.blastx_max_targets} \
            -subject_besthit \
            -qcov_hsp_perc {params.blastx_qcov_hsp_perc} -out {output[0]}
        else
            touch {output[0]}
        fi
        """


rule cdiff_toxins_subtype_clean_blastx_results:
    input:
        rules.cdiff_toxins_subtype_blastx_detection.output[0],
    output:
        results_per_sample_dir + "{sample}/" + cdiff_toxin_dir_name+ "/{sample}_blastx_output.tsv",
    params:
        identity_threshold="90",
        coverage_threshold="70",
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        if [[ -s {input[0]} ]]; then
            csvtk add-header -t -n toxin_gene_coords,query_start,query_end,toxin_subtype,evalue,identity,aln_length,gaps,ref_len,description,sequence {input[0]} | 
                csvtk mutate2 -t -e ' (($aln_length - $gaps) / $ref_len) * 100 ' -n coverage | 
                csvtk filter -t -f "coverage>{params.coverage_threshold}" | 
                csvtk filter -t -f "identity>{params.identity_threshold}" | 
                csvtk sep -t -f 1 -s ":" -n toxin_gene,gene_coords | 
                csvtk mutate2 -t -e " '{wildcards.sample}' " -n sample | 
                csvtk cut -t -f sample,toxin_gene,gene_coords,toxin_subtype,evalue,identity,coverage,aln_length,description,sequence -o {output[0]}
        else
            touch {output[0]}
        fi
        """


rule cdiff_toxins_gene_and_subtype_summarize:
    input:
        rules.cdiff_toxins_genes_clean_blastn_results.output[0],
        rules.cdiff_toxins_subtype_clean_blastx_results.output[0],
    output:
        results_per_sample_dir + "{sample}/" + cdiff_toxin_dir_name+ "/{sample}_toxin_profile.tsv",
        temp(results_per_sample_dir + "{sample}/" + cdiff_toxin_dir_name+ "/{sample}_toxin_genes.temp"),
        temp(results_per_sample_dir + "{sample}/" + cdiff_toxin_dir_name+ "/{sample}_toxin_subtype.temp"),
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        if [[ $(csvtk nrow -tH {input[0]} ) -gt 1 ]] ; then 
            cat {input[0]} | 
                csvtk replace -t -f toxin_gene -p TcdA -r A+ | 
                csvtk replace -t -f toxin_gene -p TcdB -r B+ | 
                csvtk sort -t -k toxin_gene | 
                csvtk summary -t -f toxin_gene:uniq -g sample | 
                csvtk unfold -t  -f "toxin_gene:uniq" -s "; " | 
                csvtk sort -t -k 2 | 
                csvtk rename -t -f 2 -n toxin_gene | 
                csvtk summary -f toxin_gene:collapse -g sample  -t | 
                csvtk rename -t -f toxin_gene:collapse -n "Toxin profile" -o {output[1]}
        else
            echo -e  "sample\\t"Toxin profile"\\n{wildcards.sample}\\tnone\\n" > {output[1]}
        fi
        if [[ $(csvtk nrow -tH {input[1]} ) -gt 1 ]]; then
            cat {input[1]} | 
                csvtk sort -t -k toxin_gene | 
                csvtk summary -t -f toxin_subtype:uniq -g sample | 
                csvtk unfold -t  -f "toxin_subtype:uniq" -s "; " | 
                csvtk sort -t -k 2 | 
                csvtk rename -t -f 2 -n toxin_subtype | 
                csvtk summary -f toxin_subtype:collapse -g sample  -t | 
                csvtk rename -t -f toxin_subtype:collapse -n "Toxin subtype" -o {output[2]}
        else
            echo -e  "sample\\t"Toxin subtype"\\n{wildcards.sample}\\tnone\\n" > {output[2]}
        fi
        csvtk join -t {output[1]} {output[2]} | csvtk rename -t -f sample -n Sample -o {output[0]}
        """


rule cdiff_toxins_gene_and_subtype_summarize_all:
    input:
        expand(rules.cdiff_toxins_gene_and_subtype_summarize.output[0], sample=SAMPLES),
        expand(rules.cdiff_toxins_gene_and_subtype_summarize.output[0], sample=GENOMES),
    output:
        results_all_samples_dir + cdifficile_dir_name + "/Toxin_profiles.tsv",
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        csvtk concat -t {input} -o {output[0]}
        """


"""
transposons
"""

rule srst2_sample_names:
    input:
        r1 = lambda wildcards: get_illumina_r1(wildcards),
        r2 = lambda wildcards: get_illumina_r2(wildcards), 
    output:
        r1=temp(os.path.join(results_per_sample_dir, "{sample}/cdiff/reads/{sample}_1.fastq.gz")),
        r2=temp(os.path.join(results_per_sample_dir, "{sample}/cdiff/reads/{sample}_2.fastq.gz")),
    params:
        tmp_dir =temp(directory(os.path.join(results_per_sample_dir, "{sample}/cdiff/reads"))),
    shell:
        r"""
        mkdir -p {params.tmp_dir}
        ln -sf {input.r1} {output.r1}
        ln -sf {input.r2} {output.r2}
        """

rule trsanposons_search_with_srst2:
    input:
        r1 = rules.srst2_sample_names.output.r1,
        r2 = rules.srst2_sample_names.output.r2,
        db = C_difficile_DB + "transposons/transposons_clustered.fasta"
    output:
        sum = results_per_sample_dir + "{sample}/" + cdiff_trsanposons_dir_name+ "/transposons.tsv",
        full_sum = results_per_sample_dir + "{sample}/" + cdiff_trsanposons_dir_name+ "/transposons_full.tsv",
        pileup = temp(results_per_sample_dir + "{sample}/" + cdiff_trsanposons_dir_name+ "/srst2__{sample}.transposons_clustered.pileup"),
        bam = temp(results_per_sample_dir + "{sample}/" + cdiff_trsanposons_dir_name+ "/srst2__{sample}.transposons_clustered.sorted.bam"),
    threads: 16
    conda:
        envs_folder + "srst2.yaml"
    params:
        cov = transposon_coverage , 
        dir=directory(results_per_sample_dir + "{sample}/" + cdiff_trsanposons_dir_name),
        srst2_raw_summary=results_per_sample_dir + "{sample}/" + cdiff_trsanposons_dir_name+ "/srst2__genes__transposons_clustered__results.txt",
        srst2_raw_summary_full=results_per_sample_dir + "{sample}/" + cdiff_trsanposons_dir_name+ "/srst2__fullgenes__transposons_clustered__results.txt",
    shell:
        # export PERL5LIB="" to avoid conflicts with PERL for bowtie and samtools
        #incase of negative results create empty file
        r"""
        export PERL5LIB=""
        srst2 --input_pe {input.r1} {input.r2} --output {params.dir}/srst2 \
            --log --gene_db {input.db} --threads {threads} \
            --min_coverage {params.cov} --stop_after 1000000 --report_new_consensus --report_all_consensus 2>/dev/null
        
        if [[ -f {params.srst2_raw_summary} ]]; then  
            mv {params.srst2_raw_summary} {output.sum}; else touch {output.sum}; 
        fi
        
        if [[ -f {params.srst2_raw_summary_full} ]]; then 
            mv {params.srst2_raw_summary_full} {output.full_sum}; else touch {output.full_sum}; 
        fi
        """


use rule trsanposons_search_with_srst2 as trsanposons_search_with_srst2_assemblies with:
    input:
        r1=results_per_sample_dir + "{genome}/simulated_reads/{genome}_1.fq",
        r2=results_per_sample_dir + "{genome}/simulated_reads/{genome}_2.fq",
        db = C_difficile_DB + "transposons/transposons_clustered.fasta"
    output:
        sum=results_per_sample_dir + "{genome}/" + cdiff_trsanposons_dir_name+ "/transposons.tsv",
        full_sum=results_per_sample_dir + "{genome}/" + cdiff_trsanposons_dir_name+ "/transposons_full.tsv",
        pileup=temp(results_per_sample_dir + "{genome}/" + cdiff_trsanposons_dir_name+ "/srst2__{genome}.transposons_clustered.pileup"),
        bam=temp(results_per_sample_dir + "{genome}/" + cdiff_trsanposons_dir_name+ "/srst2__{genome}.transposons_clustered.sorted.bam"),
    params:
        cov = transposon_coverage , 
        dir=directory(results_per_sample_dir + "{genome}/" + cdiff_trsanposons_dir_name+ "/"),
        srst2_raw_summary=results_per_sample_dir + "{genome}/" + cdiff_trsanposons_dir_name+ "/srst2__genes__transposons_clustered__results.txt",
        srst2_raw_summary_full=results_per_sample_dir + "{genome}/" + cdiff_trsanposons_dir_name+ "/srst2__fullgenes__transposons_clustered__results.txt",


"""
collect all transposons results
"""


rule compile_transposons:
    input:
        names = rules.isolates_list.output.isolates_list,
        results = [expand(results_per_sample_dir + "{sample}/" + cdiff_trsanposons_dir_name+ "/transposons.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/" + cdiff_trsanposons_dir_name+ "/transposons.tsv", genome=GENOMES),]
    output:
        all_sum=results_all_samples_dir + cdifficile_dir_name + "/Transposons_summary.tsv",
    threads: 16
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        csvtk join -k -t --na - -k {input.names} {input.results} -o {output.all_sum}
        """

rule compile_transposons_2:
    input:
        expand(results_per_sample_dir + "{sample}/" + cdiff_trsanposons_dir_name+ "/transposons_full.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/" + cdiff_trsanposons_dir_name+ "/transposons_full.tsv", genome=GENOMES),
    output:
        FOF=temp(results_all_samples_dir + cdifficile_dir_name + "/all_transposons_full.tsv"),
        res=results_all_samples_dir + cdifficile_dir_name + "/Transposons_results.tsv",
    conda:
        envs_folder + "srst2.yaml"
    shell:
        r"""
        echo -ne "" > {output.FOF} &&
        for path in {input}; do
            if [[ -s $path ]]; then
                echo -ne "$path\n" >> {output.FOF};
            fi
        done
        if [[ $(wc -l < {output.FOF} ) -gt 1 ]]; then
            csvtk concat -t --infile-list {output.FOF} --out-file {output.res} -T -i -E  -I
        else 
            echo -e "Sample\tDB\tgene\tallele\tcoverage\tdepth\tdiffs\tuncertainty\tdivergence\tlength\tmaxMAF\tclusterid\tseqid\tannotation\n" > {output.res}
        fi 
        """


rule define_transposon_profile_per_sample:
    input:
        results_per_sample_dir + "{sample}/" + cdiff_trsanposons_dir_name+ "/transposons.tsv",
    output:
        results_per_sample_dir + "{sample}/" + cdiff_trsanposons_dir_name+ "/transposons_profile.tsv",
    conda:
        envs_folder + "data_parse.yaml"
    threads: 2
    shell:
        r"""
        if [[ $(csvtk ncol -t {input[0]}) -ge 2 ]]; then
            cat {input[0]} | 
                csvtk replace -t -f -1 -p '[.\?]' -r '' | 
                csvtk replace -t -f -1 -p '[.*]' -r '' | 
                sed -e 's/\t/,/1' | 
                sed -e 's/\t/; /g' | 
                csvtk csv2tab | csvtk del-header -t -o {output[0]}
        else
            echo -e "{wildcards.sample}\\tnone" > {output[0]}
        fi
        """


rule define_transposon_profile_all_samples:
    input:
        expand(rules.define_transposon_profile_per_sample.output[0], sample=SAMPLES),
        expand(rules.define_transposon_profile_per_sample.output[0], sample=GENOMES),
    output:
        results_all_samples_dir + cdifficile_dir_name + "/Transposons_profiles_per_sample.tsv",
        results_all_samples_dir + cdifficile_dir_name + "/Transposons_profiles_all.tsv",
    conda:
        envs_folder + "data_parse.yaml"
    threads: 2
    params:
        mge_name="Transposon profile",
    shell:
        r"""
        csvtk add-header -t -n Sample,"{params.mge_name}"  {input} -o {output[0]}

        csvtk cut -t -f "{params.mge_name}",Sample {output[0]} | 
            csvtk summary -t -f Sample:count,Sample:collapse -g "{params.mge_name}" | 
            csvtk rename -t -f "Sample:count","Sample:collapse" -n "Number of isolates","Name of isolates" -o {output[1]}
        """


"""
plasmids
"""


use rule trsanposons_search_with_srst2 as plasmids_search_with_srst2 with:
    input:
        r1 = rules.srst2_sample_names.output.r1,
        r2 = rules.srst2_sample_names.output.r2,
        db = C_difficile_DB + "plasmids/plasmids_clustered.fasta",
    output:
        sum=results_per_sample_dir + "{sample}/" + cdiff_plasmids_dir_name + "/plasmids.tsv",
        full_sum=results_per_sample_dir + "{sample}/" + cdiff_plasmids_dir_name + "/plasmids_full.tsv",
        pileup=temp(results_per_sample_dir + "{sample}/" + cdiff_plasmids_dir_name + "/srst2__{sample}.plasmids_clustered.pileup"),
        bam=temp(results_per_sample_dir + "{sample}/" + cdiff_plasmids_dir_name + "/srst2__{sample}.plasmids_clustered.sorted.bam"),
    threads: 16
    conda:
        envs_folder + "srst2.yaml"
    params:
        cov = plasmids_coverage, 
        dir=directory(results_per_sample_dir + "{sample}/" + cdiff_plasmids_dir_name),
        srst2_raw_summary=results_per_sample_dir + "{sample}/" + cdiff_plasmids_dir_name + "/srst2__genes__plasmids_clustered__results.txt",
        srst2_raw_summary_full=results_per_sample_dir + "{sample}/" + cdiff_plasmids_dir_name + "/srst2__fullgenes__plasmids_clustered__results.txt",


use rule plasmids_search_with_srst2 as plasmids_search_with_srst2_assemblies with:
    input:
        r1=results_per_sample_dir + "{genome}/simulated_reads/{genome}_1.fq",
        r2=results_per_sample_dir + "{genome}/simulated_reads/{genome}_2.fq",
        db = C_difficile_DB + "plasmids/plasmids_clustered.fasta"
    output:
        sum=results_per_sample_dir + "{genome}/" + cdiff_plasmids_dir_name + "/plasmids.tsv",
        full_sum=results_per_sample_dir + "{genome}/" + cdiff_plasmids_dir_name + "/plasmids_full.tsv",
        pileup=temp(results_per_sample_dir + "{genome}/" + cdiff_plasmids_dir_name + "/srst2__{genome}.plasmids_clustered.pileup"),
        bam=temp(results_per_sample_dir + "{genome}/" + cdiff_plasmids_dir_name + "/srst2__{genome}.plasmids_clustered.sorted.bam"),
    threads: 16
    conda:
        envs_folder + "srst2.yaml"
    params:
        cov = plasmids_coverage, 
        dir=directory(results_per_sample_dir + "{genome}/" + cdiff_plasmids_dir_name + "/"),        srst2_raw_summary=results_per_sample_dir + "{genome}/" + cdiff_plasmids_dir_name + "/srst2__genes__plasmids_clustered__results.txt",
        srst2_raw_summary_full=results_per_sample_dir + "{genome}/" + cdiff_plasmids_dir_name + "/srst2__fullgenes__plasmids_clustered__results.txt",


"""
collect all plasmids results
"""


use rule compile_transposons as compile_plasmids with:
    input:
        names = rules.isolates_list.output.isolates_list,
        results = [expand(results_per_sample_dir + "{sample}/" + cdiff_plasmids_dir_name + "/plasmids.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/" + cdiff_plasmids_dir_name + "/plasmids.tsv", genome=GENOMES),]
    output:
        all_sum=results_all_samples_dir + cdifficile_dir_name + "/Plasmids_summary.tsv",


use rule compile_transposons_2 as compile_plasmids_2 with:
    input:
        expand(results_per_sample_dir + "{sample}/" + cdiff_plasmids_dir_name + "/plasmids_full.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/" + cdiff_plasmids_dir_name + "/plasmids_full.tsv", genome=GENOMES),
    output:
        FOF=temp(results_all_samples_dir + cdifficile_dir_name + "/all_plasmids_full.tsv"),
        res=results_all_samples_dir + cdifficile_dir_name + "/Plasmids_results.tsv",
    conda:
        envs_folder + "srst2.yaml"


use rule define_transposon_profile_per_sample as define_plasmid_profile_per_sample with:
    input:
        results_per_sample_dir + "{sample}/" + cdiff_plasmids_dir_name + "/plasmids.tsv",
    output:
        results_per_sample_dir + "{sample}/" + cdiff_plasmids_dir_name + "/plasmid_profile.tsv",


use rule define_transposon_profile_all_samples as define_plasmid_profile_all_samples with:
    input:
        expand(rules.define_plasmid_profile_per_sample.output[0], sample=SAMPLES),
        expand(rules.define_plasmid_profile_per_sample.output[0], sample=GENOMES),
    output:
        results_all_samples_dir + cdifficile_dir_name + "/Plasmid_profiles_per_sample.tsv",
        results_all_samples_dir + cdifficile_dir_name + "/Plasmid_profiles_all.tsv",
    params:
        mge_name="Plasmid profile",


"""
phages
"""


use rule trsanposons_search_with_srst2 as phages_search_with_srst2 with:
    input:
        r1 = rules.srst2_sample_names.output.r1,
        r2 = rules.srst2_sample_names.output.r2,
        db = C_difficile_DB + "phages/phages_clustered.fasta"
    output:
        sum=results_per_sample_dir + "{sample}/" + cdiff_phages_dir_name + "/phages.tsv",
        full_sum=results_per_sample_dir + "{sample}/" + cdiff_phages_dir_name + "/phages_full.tsv",
        pileup=temp(results_per_sample_dir + "{sample}/" + cdiff_phages_dir_name + "/srst2__{sample}.phages_clustered.pileup"),
        bam=temp(results_per_sample_dir + "{sample}/" + cdiff_phages_dir_name + "/srst2__{sample}.phages_clustered.sorted.bam"),
    threads: 16
    conda:
        envs_folder + "srst2.yaml"
    params:
        cov = phages_coverage , 
        dir=directory(results_per_sample_dir + "{sample}/" + cdiff_phages_dir_name),
        srst2_raw_summary=results_per_sample_dir + "{sample}/" + cdiff_phages_dir_name + "/srst2__genes__phages_clustered__results.txt",
        srst2_raw_summary_full=results_per_sample_dir + "{sample}/" + cdiff_phages_dir_name + "/srst2__fullgenes__phages_clustered__results.txt",


use rule phages_search_with_srst2 as phages_search_with_srst2_assemblies with:
    input:
        r1=results_per_sample_dir + "{genome}/simulated_reads/{genome}_1.fq",
        r2=results_per_sample_dir + "{genome}/simulated_reads/{genome}_2.fq",
        db = C_difficile_DB + "phages/phages_clustered.fasta"
    output:
        sum=results_per_sample_dir + "{genome}/" + cdiff_phages_dir_name + "/phages.tsv",
        full_sum=results_per_sample_dir + "{genome}/" + cdiff_phages_dir_name + "/phages_full.tsv",
        pileup=temp(results_per_sample_dir + "{genome}/" + cdiff_phages_dir_name + "/srst2__{genome}.phages_clustered.pileup"),
        bam=temp(results_per_sample_dir + "{genome}/" + cdiff_phages_dir_name + "/srst2__{genome}.phages_clustered.sorted.bam"),
    threads: 16
    conda:
        envs_folder + "srst2.yaml"
    params:
        cov = phages_coverage , 
        dir=directory(results_per_sample_dir + "{genome}/" + cdiff_phages_dir_name + "/"),        srst2_raw_summary=results_per_sample_dir + "{genome}/" + cdiff_phages_dir_name + "/srst2__genes__phages_clustered__results.txt",
        srst2_raw_summary_full=results_per_sample_dir + "{genome}/" + cdiff_phages_dir_name + "/srst2__fullgenes__phages_clustered__results.txt",


"""
collect all phages results
"""


use rule compile_transposons as compile_phages with:
    input:
        names = rules.isolates_list.output.isolates_list,
        results = [expand(results_per_sample_dir + "{sample}/" + cdiff_phages_dir_name + "/phages.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/" + cdiff_phages_dir_name + "/phages.tsv", genome=GENOMES),]
    output:
        all_sum=results_all_samples_dir + cdifficile_dir_name + "/Phages_summary.tsv",


use rule compile_transposons_2 as compile_phages_2 with:
    input:
        expand(results_per_sample_dir + "{sample}/" + cdiff_phages_dir_name + "/phages_full.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/" + cdiff_phages_dir_name + "/phages_full.tsv", genome=GENOMES),
    output:
        FOF=temp(results_all_samples_dir + cdifficile_dir_name + "/all_phages_full.tsv"),
        res=results_all_samples_dir + cdifficile_dir_name + "/Phages_results.tsv",
    conda:
        envs_folder + "srst2.yaml"


use rule define_transposon_profile_per_sample as define_phages_profile_per_sample with:
    input:
        results_per_sample_dir + "{sample}/" + cdiff_phages_dir_name + "/phages.tsv",
    output:
        results_per_sample_dir + "{sample}/" + cdiff_phages_dir_name + "/phage_profile.tsv",


use rule define_transposon_profile_all_samples as define_phage_profile_all_samples with:
    input:
        expand(rules.define_phages_profile_per_sample.output[0], sample=SAMPLES),
        expand(rules.define_phages_profile_per_sample.output[0], sample=GENOMES),
    output:
        results_all_samples_dir + cdifficile_dir_name + "/Phage_profiles_per_sample.tsv",
        results_all_samples_dir + cdifficile_dir_name + "/Phage_profiles_all.tsv",
    params:
        mge_name="Phage profile",


"""
resistance mutations
"""


rule index_ref:
    input:
        REF=res_mutations_db_dir + res_mutations_db_name,
    output:
        res_mutations_db_dir + res_mutations_db_name + ".rev.1.bt2",
    threads: 16
    conda:
        envs_folder + "res_mutations.yaml"  #samtools 1.9, bcftools 1.9, seqkit v0.16.1, bowtie2 2.3.0, tabix 1.4-6-g10081c4
    shell:
        #make a Bowtie index file  && #index with samtools faidx
        "bowtie2-build --quiet {input} {input}"


rule resistance_mutations_positions_calling:
    input:
        r1 = lambda wildcards: get_illumina_r1(wildcards),
        r2 = lambda wildcards: get_illumina_r2(wildcards),
        bowtie2_index=res_mutations_db_dir + res_mutations_db_name + ".rev.1.bt2",
    output:
        aminoacid_change_summary=results_per_sample_dir + "{sample}/cdiff/resistance_mutations/aminoacid_at_mutated_positions.tsv",
        nucleotide_change_summary=results_per_sample_dir + "{sample}/cdiff/resistance_mutations/nucleotide_at_mutated_positions.tsv",
        sam=temp(results_per_sample_dir + "{sample}/cdiff/resistance_mutations/{sample}.sam"),
        unsortedbam=temp(results_per_sample_dir + "{sample}/cdiff/resistance_mutations/{sample}.unsorted.bam"),
        sortedbam=temp(results_per_sample_dir + "{sample}/cdiff/resistance_mutations/{sample}.sorted.bam"),
        vcf=temp(results_per_sample_dir + "{sample}/cdiff/resistance_mutations/{sample}.vcf.gz"),
        consensus=temp(results_per_sample_dir + "{sample}/cdiff/resistance_mutations/{sample}.consensus.fa"),
        consensus_aa=temp(results_per_sample_dir + "{sample}/cdiff/resistance_mutations/{sample}.consensus.faa"),
    threads: 16
    conda:
        envs_folder + "res_mutations.yaml"  #samtools 1.9, bcftools 1.9, seqkit v0.16.1, bowtie2 2.3.0, tabix 1.4-6-g10081c4
    params:
        resistance_dir=directory(results_per_sample_dir + "{sample}/cdiff/resistance_mutations/"),
        REF=res_mutations_db_dir + res_mutations_db_name,
        aminoacid_positions=res_mutations_db_dir + res_mutations_aminoacid,
        nucleotide_positions=res_mutations_db_dir + res_mutations_nucleotide,
    shell:
        #make a Bowtie index file  && #index with samtools faidx
        #"bowtie2-build --quiet {params.REF} {params.REF}"
        "samtools faidx {params.REF} "

        " && mkdir -p {params.resistance_dir}"
        " && bowtie2 --quiet -1 {input.r1} -2 {input.r2} -S {output.sam} -q --very-sensitive-local --no-unal -a -x {params.REF} --threads {threads} -u 1000000"

        " && samtools view -@ {threads} -b -O BAM -o {output.unsortedbam} -q 1 -S {output.sam}"
        " && samtools sort -@ {threads} -O BAM {output.unsortedbam} -o {output.sortedbam}"


        " && bcftools mpileup -Ou -f {params.REF} {output.sortedbam} | bcftools call -Ou --variants-only --multiallelic-caller | bcftools norm -f {params.REF} -Oz -o {output.vcf} "
        " && tabix  {output.vcf}"

        " && bcftools consensus --fasta-ref {params.REF} --haplotype 1 {output.vcf} > {output.consensus}"


        " && seqkit translate --seq-type dna --transl-table 11 {output.consensus}  --out-file {output.consensus_aa}"
        " && seqkit subseq --bed {params.aminoacid_positions} {output.consensus_aa} | seqkit fx2tab > {output.aminoacid_change_summary}"
        " && bcftools mpileup -T {params.nucleotide_positions} -Ou -f {params.REF}  {output.sortedbam} | bcftools call -Ou -vm | bcftools norm -f {params.REF} -Oz | bcftools query -f '%CHROM\\t%POS\\t%REF\\t%ALT\\n' --print-header -o {output.nucleotide_change_summary}"
        #align the short reads with mode very-sensitive-local
        #make the BAM file
        #produce a vcf file from the BAM file & index it
        #https://www.biostars.org/p/367960/
        #create a consensus sequence (ref seq seeded with sample variants)
        #get the aminoacid at the specific positions.
        #seqkit grep  -n -p gyrA consensus.fa | seqkit subseq -r 127:129 | seqkit translate --seq-type dna --transl-table 11


use rule resistance_mutations_positions_calling as resistance_mutations_positions_calling_assemblies with:
    input:
        r1=results_per_sample_dir + "{genome}/simulated_reads/{genome}_1.fq",
        r2=results_per_sample_dir + "{genome}/simulated_reads/{genome}_2.fq",
        bowtie2_index=res_mutations_db_dir + res_mutations_db_name + ".rev.1.bt2",
    output:
        aminoacid_change_summary=results_per_sample_dir + "{genome}/cdiff/resistance_mutations/aminoacid_at_mutated_positions.tsv",
        nucleotide_change_summary=results_per_sample_dir + "{genome}/cdiff/resistance_mutations/nucleotide_at_mutated_positions.tsv",
        sam=temp(results_per_sample_dir + "{genome}/cdiff/resistance_mutations/{genome}.sam"),
        unsortedbam=temp(results_per_sample_dir + "{genome}/cdiff/resistance_mutations/{genome}.unsorted.bam"),
        sortedbam=temp(results_per_sample_dir + "{genome}/cdiff/resistance_mutations/{genome}.sorted.bam"),
        vcf=temp(results_per_sample_dir + "{genome}/cdiff/resistance_mutations/{genome}.vcf.gz"),
        consensus=temp(results_per_sample_dir + "{genome}/cdiff/resistance_mutations/{genome}.consensus.fa"),
        consensus_aa=temp(results_per_sample_dir + "{genome}/cdiff/resistance_mutations/{genome}.consensus.faa"),
    params:
        resistance_dir=directory(results_per_sample_dir + "{genome}/cdiff/resistance_mutations/"),
        REF=res_mutations_db_dir + res_mutations_db_name,
        aminoacid_positions=res_mutations_db_dir + res_mutations_aminoacid,
        nucleotide_positions=res_mutations_db_dir + res_mutations_nucleotide,


rule summarize_resistance_mutations_per_sample:
    input:
        aminoacid_change_summary=results_per_sample_dir + "{sample}/cdiff/resistance_mutations/aminoacid_at_mutated_positions.tsv",
    output:
        aa_positions=results_per_sample_dir + "{sample}/cdiff/resistance_mutations/aminoacid_at_mutated_positions_2.tsv",
        aa_positions_3=results_per_sample_dir + "{sample}/cdiff/resistance_mutations/aminoacid_at_mutated_positions_3.tsv",
    threads: 16
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk cut --quiet -tH -f 1,2 {input} |
            csvtk sep --quiet -tH -f 1 -s "-" -n f1,f2 |
            csvtk cut --quiet -tH -f 3,2 | csvtk add-header -t -n Gene_position,Sample -o {output.aa_positions}

        csvtk join -f 1 -t  {res_mutations_db_dir}/protein_ref.txt {output.aa_positions} |
            csvtk sep -t -f 1 -s "_" -n f1,f2 |
            csvtk mutate2 -t -e ' $f1 + "_" + $ref + "" + $f2 + "" + $Sample  ' -n mutation_position |
            csvtk rename -t -f Sample -n  {wildcards.sample} |
            csvtk cut -t -f mutation_position,{wildcards.sample}  -o {output.aa_positions_3}
        """
#             csvtk sep -t -f 1 -s "_" -n f1,f2 |
#            csvtk mutate2 -t -e ' $f1 + "_" + $ref + "" + $f2 + "" + $Sample  ' -n Sample |


rule summarize_resistance_mutations_all_samples:
    input:
        expand(results_per_sample_dir + "{sample}/cdiff/resistance_mutations/aminoacid_at_mutated_positions_3.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/cdiff/resistance_mutations/aminoacid_at_mutated_positions_3.tsv", genome=GENOMES),
    output:
        tsv=results_all_samples_dir + all_resistance_dir_name + "/resistance_mutations_summary.tsv",
        tsv_binary=results_all_samples_dir + all_resistance_dir_name + "/resistance_mutations_summary_binary.tsv",
        xlsx=results_all_samples_dir + all_resistance_dir_name + "/resistance_mutations_summary.xlsx",
    threads: 16
    conda:
        envs_folder + "data_parse.yaml"  
    shell:
        r"""
        csvtk join -f mutation_position -t {res_mutations_db_dir}/resistance_mutations.txt {input} -L --na "-" | 
            csvtk transpose -t | 
            csvtk rename -t -f 1 -n "Sample" | 
            csvtk sort -t -k "Sample" -o {output.tsv}
        
        cat {output.tsv} | 
            csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv})) -p '^-$'   -r @ | 
            csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv})) -p '[^@]+' -r 1 | 
            csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv})) -p '^@$'   -r 0 -o {output.tsv_binary}
        
        csvtk csv2xlsx -t {output.tsv} -o {output.xlsx}
        """


"""
collect all c. difficile tasks in a summary table
"""


rule collect_cdifficile_results:
    input:
        rules.isolates_list.output[0],
        rules.write_refseq_masher.output.filtered,
        rules.collect_cdiffcile_clade_after_mash_and_mlst.output[0] if C_DIIFICILE_CLADES == True else [],
        rules.cdiff_toxins_gene_and_subtype_summarize_all.output[0] if C_DIFFICILE_TXOINS == True else [],
        trans = rules.define_transposon_profile_all_samples.output[0] if C_DIFFICILE_MOBILE_ELEMENTS == True else [],
        plasmids = rules.define_plasmid_profile_all_samples.output[0] if C_DIFFICILE_MOBILE_ELEMENTS == True else [],
        phages = rules.define_phage_profile_all_samples.output[0] if C_DIFFICILE_MOBILE_ELEMENTS == True else [],
        res = rules.summarize_resistance_mutations_all_samples.output[0] if C_DIFFICILE_RESISTNACE_MUTATIONS == True else [],
    output:
        results_all_samples_dir + cdifficile_dir_name + "/summary_results_cdifficile.tsv",
        results_all_samples_dir + cdifficile_dir_name + "/summary_results_cdifficile.xlsx",
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk join -t {input}  -k --na none -o {output[0]}
        csvtk csv2xlsx -t {output[0]} -o {output[1]}
        """


rule cleanup_pileup:
    input:
        rules.collect_cdifficile_results.output
    output: 
        tmp = temp(results_per_sample_dir + "{sample}/cdiff/temp.txt")
    params: 
        trans_dir = os.path.join(results_per_sample_dir + "{sample}/cdiff/trsanposons"),
        plas_dir = os.path.join(results_per_sample_dir + "{sample}/cdiff/plasmids"),
        phages_dir = os.path.join(results_per_sample_dir + "{sample}/cdiff/phages")
    shell:
        """
        rm -f {params.trans_dir}/*.srst2__{wildcards.sample}.transposons_clustered.pileup
        rm -f {params.plas_dir}/*.srst2__{wildcards.sample}.plasmids_clustered.pileup
        rm -f {params.phages_dir}/*.srst2__{wildcards.sample}.phages_clustered.pileup
        touch {output.tmp}
        """


# """
# write report
# """


# rule write_report_script_cdiff_mge:
#     input:
#         input=C_DIFFICILE_MODULE(),
#         summary=rules.collect_cdifficile_results.output[0],
#         clean = [expand(rules.cleanup_pileup.output.tmp, sample = SAMPLES), expand(rules.cleanup_pileup.output.tmp, sample = GENOMES)]
#     output:
#         report = temp(reports_dir + "cdiff_mge_script.Rmd"),
#     threads: 2
#     params:
#         results_dir=results_all_samples_dir,
#         reports_dir=reports_dir,
#         C_DIIFICILE_CLADES=C_DIIFICILE_CLADES,
#         C_DIFFICILE_TXOINS=C_DIFFICILE_TXOINS,
#         C_DIFFICILE_MOBILE_ELEMENTS=C_DIFFICILE_MOBILE_ELEMENTS,
#         C_DIFFICILE_RESISTNACE_MUTATIONS=C_DIFFICILE_RESISTNACE_MUTATIONS,
#     script:
#         bin_dir + "write_report_script_cdifficile.sh"
