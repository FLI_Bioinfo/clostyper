"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# FIXME: report of AMRfinderplus summary in cases all analysed samples were AMR-negative
# FIXME: in case of only one sample, error will occur. CARD 


def RESISTANCE():
    if config["RESISTANCE"] == True :
        RESISTANCE = []
        # expected output files according to the configuration
        if config["run_abricate_resistance"] == True:
            RESISTANCE += [] #rules.abricate_resistance_call.output[0]
            if config["abricate_resistance"]["params"]["databases"] == "all":
                RESISTANCE += [
                    expand(rules.abricate_resistance_ncbi.output[0], sample=SAMPLES), 
                    expand(rules.abricate_resistance_ncbi.output[0], sample=GENOMES)
                    ]
                RESISTANCE += [rules.abricate_summary_res_ncbi.output]
                RESISTANCE += [rules.detail_summary_abricate_ncbi.output.tsv]
                RESISTANCE += [
                    expand(rules.abricate_resistance_card.output[0], sample=SAMPLES), 
                    expand(rules.abricate_resistance_card.output[0], sample=GENOMES)
                    ]
                RESISTANCE += [rules.abricate_summary_res_card.output]
                RESISTANCE += [rules.detail_summary_abricate_card.output.tsv]
                RESISTANCE += [
                    expand(rules.abricate_resistance_argannot.output[0], sample=SAMPLES), 
                    expand(rules.abricate_resistance_argannot.output[0], sample=GENOMES)
                    ]
                RESISTANCE += [rules.abricate_summary_res_argannot.output]
                RESISTANCE += [rules.detail_summary_abricate_argannot.output.tsv]
                RESISTANCE += [
                    expand(rules.abricate_resistance_resfinder.output[0], sample=SAMPLES), 
                    expand(rules.abricate_resistance_resfinder.output[0], sample=GENOMES)
                    ]
                RESISTANCE += [rules.abricate_summary_res_resfinder.output]
                RESISTANCE += [rules.detail_summary_abricate_resfinder.output.tsv]
                RESISTANCE += [
                    expand(rules.abricate_resistance_megares.output[0], sample=SAMPLES), 
                    expand(rules.abricate_resistance_megares.output[0], sample=GENOMES)
                    ]
                RESISTANCE += [rules.abricate_summary_res_megares.output]
                RESISTANCE += [rules.detail_summary_abricate_megares.output.tsv]
            elif config["abricate_resistance"]["params"]["databases"] == "ncbi":
                RESISTANCE += [
                    expand(rules.abricate_resistance_ncbi.output[0], sample=SAMPLES), 
                    expand(rules.abricate_resistance_ncbi.output[0], sample=GENOMES)
                    ]
                RESISTANCE += [rules.abricate_summary_res_ncbi.output]
                RESISTANCE += [rules.detail_summary_abricate_ncbi.output.tsv]
            elif config["abricate_resistance"]["params"]["databases"] == "card":
                RESISTANCE += [
                    expand(rules.abricate_resistance_card.output[0], sample=SAMPLES), 
                    expand(rules.abricate_resistance_card.output[0], sample=GENOMES)
                    ]
                RESISTANCE += [rules.abricate_summary_res_card.output]
                RESISTANCE += [rules.detail_summary_abricate_card.output.tsv]
            elif config["abricate_resistance"]["params"]["databases"] == "argannot":
                RESISTANCE += [
                    expand(rules.abricate_resistance_argannot.output[0], sample=SAMPLES), 
                    expand(rules.abricate_resistance_argannot.output[0], sample=GENOMES)
                    ]
                RESISTANCE += [rules.abricate_summary_res_argannot.output]
                RESISTANCE += [rules.detail_summary_abricate_argannot.output.tsv]
            elif config["abricate_resistance"]["params"]["databases"] == "resfinder":
                RESISTANCE += [
                    expand(rules.abricate_resistance_resfinder.output[0], sample=SAMPLES), 
                    expand(rules.abricate_resistance_resfinder.output[0], sample=GENOMES)
                    ]
                RESISTANCE += [rules.abricate_summary_res_resfinder.output]
                RESISTANCE += [rules.detail_summary_abricate_resfinder.output.tsv]
            elif config["abricate_resistance"]["params"]["databases"] == "megares":
                RESISTANCE += [
                    expand(rules.abricate_resistance_megares.output[0], sample=SAMPLES), 
                    expand(rules.abricate_resistance_megares.output[0], sample=GENOMES)
                    ]
                RESISTANCE += [rules.abricate_summary_res_megares.output]
                RESISTANCE += [rules.detail_summary_abricate_megares.output.tsv]
        if config["run_amrfinderplus"] == True:
            RESISTANCE += [
                expand(rules.run_amrfinderplus.output.tsv, sample=SAMPLES), 
                expand(rules.run_amrfinderplus.output.tsv, sample=GENOMES)
                ]
            RESISTANCE += [rules.amrfinderplus_version.output[0]]
            RESISTANCE += [
                expand(rules.summarize_amrfinderplus.output.profile, sample=SAMPLES), 
                expand(rules.summarize_amrfinderplus.output.profile, sample=GENOMES)
                ]
            RESISTANCE += [rules.collect_resistance_profiles.output.all_profiles]
            RESISTANCE += [rules.collect_all_amrfinderplus_summaries.output.tsv]
            RESISTANCE += [rules.detail_summary_amrfinderplus.output.tsv]
        if config["run_card_rgi"] == True:
            RESISTANCE += [
                expand(rules.add_sample_name_card_rgi.output.tsv, sample=SAMPLES), 
                expand(rules.add_sample_name_card_rgi.output.tsv, sample=GENOMES)
                ]
            RESISTANCE += [rules.rgi_version.output]
            RESISTANCE += [rules.detail_summary_card_rgi.output.tsv]
            RESISTANCE += [rules.crad_rgi_summary.output.tsv]
            RESISTANCE += [rules.crad_rgi_summary_excel.output.excel]

        resfinder_enabled = config.get("run_resfinder", False)
        if resfinder_enabled:
            RESISTANCE += [
                expand(rules.add_sample_name_resfinder.output.tsv, sample=SAMPLES), 
                expand(rules.add_sample_name_resfinder.output.tsv, sample=GENOMES)
                ]
            RESISTANCE += [rules.resfinder_version.output]
            RESISTANCE += [rules.detail_summary_resfinder.output.tsv]
            RESISTANCE += [rules.resfinder_summary.output.tsv]
            RESISTANCE += [rules.resfinder_summary_excel.output.excel]
            RESISTANCE += [rules.collect_resfinder_phenotype.output.tsv]
            RESISTANCE += [rules.collect_resfinder_phenotype_excel.output.excel]

        return RESISTANCE
    else:
        return []



# rule write_report_script_resistance:
#     input:
#         input=RESISTANCE(),
#         resistance_profile_per_isolate=rules.collect_resistance_profiles.output[0] if config["run_amrfinderplus"] == True else [],
#         resistance_profile_all_isolates=rules.collect_resistance_profiles.output[1] if config["run_amrfinderplus"] == True else [],
#         sum_amrfinder=rules.collect_all_amrfinderplus_summaries.output[0] if config["run_amrfinderplus"] == True else [],
#         amrfinder_binary=rules.collect_all_amrfinderplus_summaries.output[1] if config["run_amrfinderplus"] == True else [],
#         detail_amrfinder=rules.detail_summary_amrfinderplus.output[1] if config["run_amrfinderplus"] == True else [],

#         resistance_ncbi_raw=rules.abricate_summary_res_ncbi.output[0] if config["run_abricate_resistance"] == True else [],
#         resistance_ncbi_binary=rules.abricate_summary_res_ncbi.output[1] if config["run_abricate_resistance"] == True else [],
#         detailed_ncbi=rules.detail_summary_abricate_ncbi.output[1] if config["run_abricate_resistance"] == True else [],
#         resistance_card_raw=rules.abricate_summary_res_card.output[0] if config["run_abricate_resistance"] == True else [],
#         resistance_card_binary=rules.abricate_summary_res_card.output[1] if config["run_abricate_resistance"] == True else [],
#         detailed_card=rules.detail_summary_abricate_card.output[1] if config["run_abricate_resistance"] == True else [],
#         resistance_resfinder_raw=rules.abricate_summary_res_resfinder.output[0] if config["run_abricate_resistance"] == True else [],
#         resistance_resfinder_binary=rules.abricate_summary_res_resfinder.output[1] if config["run_abricate_resistance"] == True else [],
#         detailed_resfinder=rules.detail_summary_abricate_resfinder.output[1] if config["run_abricate_resistance"] == True else [],
#         resistance_megares_raw=rules.abricate_summary_res_megares.output[0] if config["run_abricate_resistance"] == True else [],
#         resistance_megares_binary=rules.abricate_summary_res_megares.output[1] if config["run_abricate_resistance"] == True else [],
#         detailed_megares=rules.detail_summary_abricate_megares.output[1] if config["run_abricate_resistance"] == True else [],
#         resistance_argannot_raw=rules.abricate_summary_res_argannot.output[0] if config["run_abricate_resistance"] == True else [],
#         resistance_argannot_binary=rules.abricate_summary_res_argannot.output[1] if config["run_abricate_resistance"] == True else [],
#         detailed_argannot=rules.detail_summary_abricate_argannot.output[1] if config["run_abricate_resistance"] == True else [],
#     #        cdiff_mutations = report_scripts + "resistance_custom_mutations.Rmd" if config["C_DIFFICILE_MODULE"] == True else [],
#     output:
#         report = temp(reports_dir + "resistance_script.Rmd"),
#     threads: 2
#     params:
#         results_dir=results_all_samples_dir,
#         reports_dir=reports_dir,
#     script:
#         bin_dir + "write_report_script_resistance_analysis.sh"