"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


def SAMPLENAMES():
    return rules.isolates_list.output


"""
isolates.txt
"""
names = (expand("{sample}", sample=SAMPLES),)
names_genomes = (expand("{sample}", sample=GENOMES),)
isolates = [names, names_genomes]


def count_items(names, names_genomes):
    count_samples = len(names)
    count_genomes = len(names_genomes)
    return count_samples, count_genomes


count_samples, count_genomes = count_items(names[0], names_genomes[0]) ## Access the list inside the tuple
count_isolates = count_samples + count_genomes


rule isolates_list:
    output:
        isolates_list=results_all_samples_dir + "sample_names.txt",
    run:
        all_samples = list(names[0]) + list(names_genomes[0]) # Flatten tuple of lists into a single list

        # Write to file with one sample per line
        with open(output.isolates_list, "w") as f:
            f.write("Sample\n")  # Add header
            f.write("\n".join(all_samples) + "\n")

