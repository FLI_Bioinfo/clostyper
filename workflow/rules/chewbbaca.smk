"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# TODO add cgMLST workflow
# TODO add captions to the tables
# TODO what kind of tree in the report MST or NJ tree from GrapeTree or clustering tree from hclust
# TODO 7.4 Pairwise cgMLST allelic distances -> add caption about using grape tree to calcluate the number of differences


# clostyper_database = "/mnt/c/Data/work/projects/clostyper_testdata/cgmlst/ClostyperDB"
clostyper_database = config["clostyper_database"]
clostyper_database = add_trailing_slash(clostyper_database)

species=config["species"] if config["species"] else "none"


def get_scheme_and_threshold(species):
    """
    Retrieve the cgMLST scheme, clustering thresholds, and database references based on the provided species.
    """
    # Defaults from the configuration
    scheme = config.get("cgmlst_scheme")
    clustering_threshold_hclust = config.get("clustering_cgmlst_hclust", {}).get("clustering_threshold", 6)
    clustering_threshold_phiercc = config.get("clustering_cgmlst_phiercc", {}).get("clustering_threshold", 6)
    
    # Initialize references
    metadata_ref = []
    reference_database = []
    phiercc_npz = []

    # Species-specific 
    base_path = clostyper_database
    if species == "cdifficile":
        scheme = scheme or f"{base_path}difficile/cgmlst/chewbbaca3/"
        clustering_threshold_hclust = clustering_threshold_hclust or 6
        clustering_threshold_phiercc = clustering_threshold_phiercc or 6
        reference_database = f"{base_path}difficile/cgmlst/genome_allele_DB/cgMLST.tsv"
        metadata_ref = f"{base_path}difficile/cgmlst/genome_allele_DB/Enterobase_metadata.tsv.gz"
        phiercc_npz = f"{base_path}difficile/cgmlst/genome_allele_DB/phiercc.npz"
    elif species == "cperfringens":
        scheme = scheme or f"{base_path}perfringens/cgmlst/chewbbaca3/"
        clustering_threshold_hclust = clustering_threshold_hclust or 7
        clustering_threshold_phiercc = clustering_threshold_phiercc or 7
        metadata_ref = f"{base_path}perfringens/cgmlst/genome_allele_DB/reference_metadata.tsv.gz"
    elif species == "cchauvoei":
        scheme = scheme or f"{base_path}chauvoei/cgmlst/chewbbaca3/"
        clustering_threshold_hclust = clustering_threshold_hclust or 6
    else:
        # For unrecognized species, use default settings
        scheme = scheme or "none"
        clustering_threshold_hclust = clustering_threshold_hclust or 6
        clustering_threshold_phiercc = clustering_threshold_phiercc or 6

    # Validate the scheme configuration
    if (scheme == "none" or not os.path.isdir(scheme)) and config.get("COREGENOME_MLST", False):
        sys.exit(
            f"{RED}\nError: cgMLST analysis was set to True but you have not specified a valid cgMLST scheme.\n"
            f"To solve this:\n"
            f"(1) Provide a valid species name and make sure Clostyper database is accessible OR\n"
            f"(2) Provide a valid cgMLST scheme in the config.yaml file\n{NOFORMAT}"
        )

    return scheme, clustering_threshold_hclust, clustering_threshold_phiercc, metadata_ref, reference_database, phiercc_npz

scheme, clustering_threshold_hclust, clustering_threshold_phiercc, metadata_ref, reference_database, phiercc_npz = get_scheme_and_threshold(species)

def COREGENOME_MLST():
    if config["COREGENOME_MLST"] == True:
        COREGENOME_MLST = []
        COREGENOME_MLST.append(rules.chewbbaca_allele_call.output)
        COREGENOME_MLST.append(rules.chewbbaca_version.output.version)
        COREGENOME_MLST.append(rules.chewbbaca_extract_cgmlst.output)
        COREGENOME_MLST.append(rules.chewbbaca_calling_ratio.output.stats)
        COREGENOME_MLST.append(rules.write_distance_matrix_cgmlst.output[0])
        COREGENOME_MLST.append(rules.chewbbaca_extract_cgmlst.output)
        if len(samplesheet["sample"]) > 1:
            COREGENOME_MLST.append(rules.grapetree_for_cgmlst.output)
            COREGENOME_MLST.append(rules.phylogeny_image_cgmlst.output.svg)
            if config["clustering_with_hclust_cgmlst"] == True:
                COREGENOME_MLST.append(rules.clustering_with_hclust_for_cgmlst.output)
                COREGENOME_MLST.append(rules.overview_of_cgmlst_clusters_hclust.output[0])

            if config["clustering_with_phiercc"] == True:
                COREGENOME_MLST.append(rules.extract_HierCC_info_for_samples.output.pHierCC)
        
            # figures
            COREGENOME_MLST.append(rules.distribution_cgmlst_allelic_distances.output)
            COREGENOME_MLST.append(rules.boxplot_missing_percentage_genomes.output)
        
        COREGENOME_MLST.append(rules.combine_all_statistics.output[0])
        
        if species == "cdifficile":
            COREGENOME_MLST.append(rules.join_clusters_and_metadata.output)
        return COREGENOME_MLST
    else:
        return []



rule correct_sample_names_for_chewbbaca:
    """
    chewbbaca define samples IDs based on first "." - This causes inconsistencies in sample names. 
    https://github.com/B-UMMI/chewBBACA/issues/155, https://github.com/B-UMMI/chewBBACA/issues/129, https://github.com/B-UMMI/chewBBACA/issues/108
    We will add the prefix sample_ to avoid errors such as 'prefixes are interpreted by BLAST as chain PDB IDs'
    """
    input: 
        rules.isolates_list.output[0],
    output: 
        names = temporary_todelete + "temp_sample_names.tsv",
    threads: 1
    shell:
        r"""
        cat {input} | csvtk cut -f 1,1 | 
            csvtk replace -f 1 -p "\\." -r "_" -T | 
            csvtk replace -t -f 1 -p "^" -r "sample_" |
            csvtk rename -t -f 1 -n File -o {output.names} 
        """


rule prepare_sample_sheet:
    input: 
        expand(assembly_all + "{sample}.fasta", sample=SAMPLES),
        expand(assembly_all + "{genome}.fasta", genome=GENOMES),
        csv_file = temporary_todelete + "temp_sample_names.tsv"
    output: 
        sample_file = temp(os.path.join(results_all_samples_dir, cgmlst_dir_name, "samples.tsv")),
    params:
    #    fast_dir = temp(directory(os.path.join(temporary_todelete, "input_fasta/"))), 
       fast_dir = temp(directory(os.path.realpath(os.path.join(temporary_todelete, "input_fasta/")))),
    run:
        if not os.path.exists(params.fast_dir):
            os.makedirs(params.fast_dir)
        with open(input.csv_file, "r") as file:
            reader = csv.reader(file, delimiter="\t")
            next(reader)  # Skip the header row if present

            # Open the output file in append mode
            with open(output.sample_file, "a") as outfile:
                # Iterate over each row in the CSV file
                for row in reader:
                    original_filename = f"{row[1]}.fasta"
                    new_filename = f"{row[0]}.fasta"

                    # Construct full real paths for original and new files
                    original_path = os.path.realpath(os.path.join(assembly_all, original_filename))
                    new_path = os.path.realpath(os.path.join(params.fast_dir, new_filename))

                    # Remove existing symbolic link if present
                    if os.path.islink(new_path):
                        os.unlink(new_path)
                    
                    # Create new symbolic link
                    os.symlink(original_path, new_path)

                    # Write the full real path of the new file to the output file
                    outfile.write(new_path + "\n")


rule chewbbaca_allele_call:
    input:
        samples = rules.prepare_sample_sheet.output.sample_file,
        scheme = scheme, 
    output:
        alleles = os.path.join(results_all_samples_dir, cgmlst_dir_name, allele_calling_dir_name, "results_alleles.tsv"),
        stats = os.path.join(results_all_samples_dir, cgmlst_dir_name, allele_calling_dir_name, "results_statistics.tsv"),
        allele_calling_dir=directory(os.path.join(results_all_samples_dir, cgmlst_dir_name, allele_calling_dir_name)),
    threads: workflow.cores * 0.5
    conda:
        envs_folder + "chewbbaca.yaml"
    log:
        os.path.join(results_all_samples_dir, cgmlst_dir_name, "log/allele_calling.log"),
    resources:
        wget_limit=1,
        res=1,
    params:
        args=config.get("chewbbaca_allele_call", {}).get("params") or "",
    shell:
        r"""
        bash {bin_dir}fixRoaryOutDirError.sh {output.allele_calling_dir} {temporary_todelete}

        chewBBACA.py AlleleCall \
            -i {input.samples} \
            -g {input.scheme} \
            -o {output.allele_calling_dir} \
            --cpu {threads} \
            {params.args} > {log}
        """


rule chewbbaca_version:
    output:
        version = os.path.join(results_all_samples_dir, cgmlst_dir_name, "versions/chewbbaca_version.txt"),
    conda:
        envs_folder + "chewbbaca.yaml"
    shell:
        """      
        chewBBACA.py  -v  > {output.version}
        """


rule restore_sample_names_after_chewbbaca:
    input: 
        names = rules.correct_sample_names_for_chewbbaca.output.names,
        alleles = rules.chewbbaca_allele_call.output.alleles,
        stats = rules.chewbbaca_allele_call.output.stats,
    output: 
        allele = os.path.join(results_all_samples_dir, cgmlst_dir_name, allele_calling_dir_name, "results_alleles_corr.tsv"),
        stats = os.path.join(results_all_samples_dir, cgmlst_dir_name, allele_calling_dir_name, "results_statistics_corr.tsv"),
    threads: 1
    shell:
        r"""
        csvtk join -t {input.names} {input.alleles} -f 1 | csvtk cut -f -1 -t -o {output.allele} 
        csvtk join -t {input.names} {input.stats} -f 1 | csvtk cut -f -1 -t -o {output.stats} 
        """


rule chewbbaca_extract_cgmlst:
    input:
        allele_out = rules.restore_sample_names_after_chewbbaca.output.allele,
    output:
        alleles = os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_profiles_dir_name, "cgMLST.tsv"),
        mdata_stats = os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_profiles_dir_name, "missing_loci_stats.tsv"),
    threads: workflow.cores * 0.2
    conda:
        envs_folder + "chewbbaca.yaml"
    log:
        os.path.join(results_all_samples_dir, cgmlst_dir_name, "log/extract_cgmlst.log"),
    resources:
        wget_limit=1,
        res=1,
    params:
        cgmlst_profiles_dir=directory(os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_profiles_dir_name)),
        args=config.get("chewbbaca_extract_cgmlst", {}).get("params") or "--t 0",
    shell:
        r"""
        chewBBACA.py ExtractCgMLST \
            -i {input.allele_out} \
            -o {params.cgmlst_profiles_dir} \
            {params.args} > {log}
        
        mv {params.cgmlst_profiles_dir}/cgMLST[0-9].tsv {params.cgmlst_profiles_dir}/cgMLST.tsv
        """


rule chewbbaca_calling_ratio:
    input:
        mdata_stats = rules.chewbbaca_extract_cgmlst.output.mdata_stats,
    output:
        stats = os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_profiles_dir_name, "allele_calling_stats_per_genome.tsv"),
    threads: workflow.cores * 0.2
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk mutate2 -t -e ' (1 -  $3) * 100 ' -n Allele_call_ratio  {input.mdata_stats} | 
            csvtk mutate2 -t -e ' $3 * 100 ' -n missing_allele_ratio | 
            csvtk cut -t -f 1,4,5 -o {output.stats}
        """


rule calculate_distance_matrix_w_grapetree:
    input:
        rules.chewbbaca_extract_cgmlst.output.alleles,
    output:
        temp_matrix = os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_distance_matrix_dir_name, "grapetree_matrix.txt"),
        version = os.path.join(results_all_samples_dir, cgmlst_dir_name, "versions/GrapeTree_version.txt"),
    threads: 4
    log:
        os.path.join(results_all_samples_dir, cgmlst_dir_name, "log/distance_matrix_grapetree.log"),
    message:
        "Calculating allelic distance of cgMLST results with GrapeTree"
    conda:
        envs_folder + "grapetree.yaml"
    params:
        grapetree_dir=directory(os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_distance_matrix_dir_name)),
        method="distance",
        missing="3",
    shell:
        r"""
        mkdir -p {params.grapetree_dir}
        grapetree \
            --profile {input} \
            --method {params.method} \
            --missing {params.missing} \
            --n_proc {threads}  > {output.temp_matrix}
        echo "GrapeTree version 2.1" > {output.version}
        """


rule write_distance_matrix_cgmlst:
    input:
        temp_matrix = rules.calculate_distance_matrix_w_grapetree.output.temp_matrix,
    output:
        tsv_matrix = ensure(os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_distance_matrix_dir_name, "distance_matrix.tsv"), non_empty=True),
        xlsx = ensure(os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_distance_matrix_dir_name, "distance_matrix.xlsx"), non_empty=True),
        temp = temp(os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_distance_matrix_dir_name, "distance_matrix.temp")),
    threads: 4
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        tail -n +2 {input.temp_matrix} | csvtk space2tab -H |  csvtk round -f -1 -n 0 -tH >  {output.temp} 

        cat {output.temp}  | 
        csvtk add-header -t -n $(cat {output.temp} | csvtk transpose -t -H | csvtk head -n 1 -tH | sed -e 's/^/Sample\t/' | csvtk tab2csv) > {output.tsv_matrix}

        csvtk csv2xlsx -f -t {output.tsv_matrix} -o {output.xlsx}
        """


rule grapetree_for_cgmlst:
    input:
        rules.chewbbaca_extract_cgmlst.output.alleles,
    output:
        nwk = ensure(os.path.join(results_all_samples_dir, cgmlst_dir_name, trees_from_cgmlst_results_dir_name, "grapetree_cgmlst.nwk"), non_empty=True),
    threads: 4
    log:
        os.path.join(results_all_samples_dir, cgmlst_dir_name, "log/grapetree.log"),
    conda:
        envs_folder + "grapetree.yaml"
    message:
        "Making distance based tree from cgMLST results with GrapeTree"
    params:
        method =  config.get("grapetree_for_cgmlst", {}).get("method") or "MSTreeV2",
        args = config.get("grapetree_for_cgmlst", {}).get("params") or "--missing 0",
    shell:
        r"""
        grapetree \
            --profile {input} \
            --method {params.method} \
            {params.args} \
            --n_proc {threads} > {output.nwk}
        """


use rule midpoint_tree_rooting as midpoint_tree_rooting_cgmlst with:
    input:
        phylogeny_tree = rules.grapetree_for_cgmlst.output[0],
    output:
        tre=os.path.join(
            results_all_samples_dir, 
            cgmlst_dir_name, 
            trees_from_cgmlst_results_dir_name, 
            "grapetree_cgmlst_midpoint_root.nwk"
        ),
        version=os.path.join(results_all_samples_dir, cgmlst_dir_name, "versions/Treebender_version.txt"),


use rule phylogeny_image as phylogeny_image_cgmlst with:
    input:
        rules.midpoint_tree_rooting_cgmlst.output.tre,
    output:
        svg = os.path.join(
            results_all_samples_dir,
            cgmlst_dir_name, 
            trees_from_cgmlst_results_dir_name, 
            "grapetree_cgmlst.svg"
            ),
        version=os.path.join(results_all_samples_dir, cgmlst_dir_name, "versions/Gotree_version.txt"),

threshold_range_cgmlst_hclust = config["clustering_cgmlst_hclust"]["clustering_threshold_range"] 
clustering_threshold_range_cgmlst = ensure_threshold_in_range(threshold_range_cgmlst_hclust, clustering_threshold_hclust)


use rule clustering_with_hclust as clustering_with_hclust_for_cgmlst with:
    input:
        rules.write_distance_matrix_cgmlst.output.tsv_matrix,
    output:
        clusters_tsv=os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_clustering_dir_name, "Hierarchical_Clusters_of_cgMLST.tsv"),
        clusters_xlsx=os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_clustering_dir_name, "Hierarchical_Clusters_of_cgMLST.xlsx"),
        cluster_summary_tsv=os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_clustering_dir_name, "Hierarchical_Clusters_summary_of_cgMLST.tsv"),
        plot=os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_clustering_dir_name, "Hierarchical_Clustering_of_cgMLST_tree.pdf"),
        version=os.path.join(results_all_samples_dir, cgmlst_dir_name, "versions/hclust_version.txt"),
    params:
        clustering_threshold_range = clustering_threshold_range_cgmlst,
        clustering_method = config.get("clustering_cgmlst_hclust", {}).get("clustering_method", "single"),
        distance_threshold = clustering_threshold_hclust


rule distribution_cgmlst_allelic_distances:
    input:
        rules.write_distance_matrix_cgmlst.output.tsv_matrix,
    output:
        results_all_samples_dir + cgmlst_dir_name + "/" + cgmlst_distance_matrix_dir_name + "/Distribution_of_cgMLST_allelic_distances.tif",
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        #TODO correct the bins to be max allelic distabce
        #cat /home/mostafa.abdel/aProjects/git/clostyper/test_data/out_test/3_results_all_samples/7_core_genome_MLST/distance_matrix_and_clustering/distance_matrix.tsv | csvtk gather -k item -v value -f -1 -t | csvtk sort -t -k 3:nr | csvtk head -n 1 | csvtk cut -f 3 -t
        r"""
        csvtk gather -k Sample2 -v cgMLST_distance -f -1 -t  {input[0]} |
        csvtk plot hist -f 3 -t \
            --title "Distribution of cgMLST allelic distances" \
            --ylab "Number of genome pairs" \
            --xlab "cgMLST allelic distance" \
            --bins 800 \
            --percentiles \
            --format tif \
            -o {output[0]}
        """


rule boxplot_missing_percentage_genomes:
    input:
        rules.chewbbaca_calling_ratio.output.stats,
    output:
        tif=
        report(results_all_samples_dir + cgmlst_dir_name + "/" + cgmlst_profiles_dir_name + "/boxplot_missing_percentage.tif",
            category="cgMLST analysis",
            subcategory="Plots",
            labels={
              "figure": "boxplot_missing_percentage"
              }),

    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk plot box -t -f "missing_allele_ratio" {input[0]} \
            --width 4 \
            --format tif \
            --title "Boxplot of %missing alleles per genome" \
            --ylab "%missing alleles" \
            --xlab " " \
            --box-width 50 \
            -o {output[0]}
        """


rule combine_all_statistics:
    input:
        rules.restore_sample_names_after_chewbbaca.output.stats,
        rules.chewbbaca_calling_ratio.output.stats,
    output:
        results_all_samples_dir + cgmlst_dir_name + "/cgmlst_allele_calling_stats.tsv",
        # results_all_samples_dir + cgmlst_dir_name + "/cgmlst_allele_calling_stats.xlsx",
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk join  -t  {input[1]} {input[0]} -f 1 | 
        csvtk rename -t \
            -f Allele_call_ratio,missing_allele_ratio,EXC,INF,PLOT3,PLOT5,LOTSC,NIPH,NIPHEM,ALM,ASM,PAMA,LNF \
            -n "% allele calling","% missing alleles","Exact allele[EXC]","Inferred new allele[INF]","Possible locus on 3'tip[PLOT3]","Possible locus on 5'tip[PLOT5]","Short query contig[LOTSC]","Non-informative paralog[NIPH]","Non-informative paralog & exact match[NIPHEM]","Alleles 20% larger than length mode[ALM]","Alleles 20% smaller than length mode[ASM]","Paralog match[PAMA]","Locus not found[LNF]" -o {output[0]}
        """
        # csvtk csv2xlsx -t {output[0]} -o {output[1]}


rule overview_of_cgmlst_clusters_hclust:
    input:
        clusters_tsv = rules.clustering_with_hclust_for_cgmlst.output.clusters_tsv,
    output:
        overview = os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_clustering_dir_name, "clusters_overview.tsv"),
        # results_all_samples_dir + cgmlst_dir_name + "/" + cgmlst_clustering_dir_name + "/clusters_overview.xlsx",
    conda:
        envs_folder + "data_parse.yaml"
    params:
        distance_threshold = clustering_threshold_hclust,
    shell:
        r"""
        cat {input.clusters_tsv} | 
            csvtk rename2 -t  -p "Threshold = " -r "HC" -f "Threshold*" -F |
            csvtk cut -t -f "HC{params.distance_threshold}",Sample | 
            csvtk summary -f Sample:count,Sample:collapse -i -g "HC{params.distance_threshold}" -t | 
            csvtk rename -t -f 1,2,3  -n  "Cluster [threshold {params.distance_threshold}]","Number of isolates","Name of isolates" | 
            csvtk  sort -k 1:n -t -o {output.overview}
        """
        #  csvtk csv2xlsx -t {output[0]} -o {output[1]}


"""
COREGENOME_MLST in light of reference database
"""


rule chewbbaca_join_allele_profiles:
    input:
       reference_database, 
       rules.chewbbaca_extract_cgmlst.output.alleles, #new samples
    output:
        temp(os.path.join(results_all_samples_dir, cgmlst_dir_name, phiecc_clustering_dir_name, "joined_allele_profiles.tsv")),
        # temp(temporary_todelete + "joined_allele_profiles.tsv"),
    threads: workflow.cores * 0.2
    conda:
        envs_folder + "chewbbaca.yaml"
    log:
        results_all_samples_dir + cgmlst_dir_name + "/log/joined_allele_profiles.log",
    resources:
        wget_limit=1,
        res=1,
    shell:
        r"""
        chewBBACA.py JoinProfiles  \
            -p {input} \
            -o {output[0]} > {log}
        """

"""
Hierarchical clustering of cgMLST (https://github.com/zheminzhou/pHierCC). 
This script (from Enterobase) can handle a large number of profiles better than the approach described here for hclust. 
"""
rule run_pHierCC_on_cgmlst_profiles: 
    input:
       rules.chewbbaca_join_allele_profiles.output[0] if reference_database else rules.chewbbaca_extract_cgmlst.output.alleles, #new samples
       phiercc_npz if phiercc_npz else [],
    output:
        temp(os.path.join(results_all_samples_dir, cgmlst_dir_name, phiecc_clustering_dir_name, "cgmlst_profiles.HierCC.gz")),
        temp(os.path.join(results_all_samples_dir, cgmlst_dir_name, phiecc_clustering_dir_name, "cgmlst_profiles.npz")),
    threads: workflow.cores * 0.75
    conda:
        envs_folder + "phiercc.yaml"
    log:
        os.path.join(results_all_samples_dir, cgmlst_dir_name, "log/pHierCC.log"),
    params: 
        prefix = os.path.join(results_all_samples_dir, cgmlst_dir_name, phiecc_clustering_dir_name,  "cgmlst_profiles"),
        production_mode = "-a " + phiercc_npz if  phiercc_npz else "", 
    shell:
        r"""
        {bin_dir}pHierCC/pHierCC -n {threads} -p {input[0]} {params.production_mode} -o {params.prefix} > {log}
        """


rule extract_HierCC_info_for_samples: 
    """
    From the results of new and old samples, we will extract only the HCC of newly added samples 
    """
    input:
       rules.run_pHierCC_on_cgmlst_profiles.output[0], #results of all samples
       rules.isolates_list.output[0] #new sample names
    output:
        pHierCC = os.path.join(results_all_samples_dir, cgmlst_dir_name, cgmlst_clustering_dir_name, "pHierCC.tsv"),
        ref = temp(os.path.join(temporary_todelete, "pHierCC_reference_samples.tsv.gz")),
        cluster =temp(os.path.join(temporary_todelete, "pHierCC_new_samples_cluster_ids.txt")),    
    threads: 4
    params:
        threshold = clustering_threshold_phiercc 
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk join -C "$" -t -L {input[1]}  {input[0]}  -o {output[0]} 
        csvtk cut -t -f HC{params.threshold} {output[0]} | csvtk uniq | csvtk del-header -o {output[2]} 

        #reference samples
        csvtk grep -C "$" -t -v -P {input[1]}  {input[0]}  | csvtk rename -t -C "$" -f 1 -n "Sample" -o {output[1]}
        """

rule summarize_cgmlst_clusters_based_on_threshold: 
    """
    based on the defined threshold, newly added samples will be collpased in groups
    """
    input:
        rules.extract_HierCC_info_for_samples.output.pHierCC, #results of new samples
    output:
        # temp(results_all_samples_dir + cgmlst_dir_name + "/" + phiecc_clustering_dir_name + "/summary_clusters_new_samples.tsv"),
        temp(temporary_todelete + "summary_clusters_new_samples.tsv"),
    threads: 4
    conda:
        envs_folder + "data_parse.yaml"
    params:
        threshold = clustering_threshold_phiercc,
    shell:
        r"""
        csvtk summary -t -g HC{params.threshold} -f Sample:count,Sample:collapse {input[0]} | 
        csvtk rename -t -f "Sample:count"  -n "Number of samples" |
        csvtk rename -t -f "Sample:collapse"  -n Samples -o {output[0]}
        """


rule combine_and_filter_reference_samples_with_metadata: 
    input:
        ref = rules.extract_HierCC_info_for_samples.output.ref,
        metadata_ref = metadata_ref #"/data/AGr150/clostyperDB/C_difficile_cgmlst/genome_allele_DB/Enterobase_metadata.tsv.gz"
    output:
        profiles = temp(results_all_samples_dir + cgmlst_dir_name + "/" + phiecc_clustering_dir_name + "/Filtered_profiles.tsv.gz"),
        meta = temp(results_all_samples_dir + cgmlst_dir_name + "/" + phiecc_clustering_dir_name + "/reference_genomes_with_metadata.tsv.gz")
    conda:
        envs_folder + "data_parse.yaml"
    threads: 4
    params:
        threshold = clustering_threshold_phiercc,
    shell:
        r"""
        csvtk rename -t -f 1 -n Sample -C "$" {input.ref} | csvtk cut -t -C "$" -f Sample,HC{params.threshold}  -o {output.profiles}
        csvtk join -L -t {output.profiles} {input.metadata_ref} -o {output.meta}
        """

rule select_matching_cgmlst_clusters_from_ref_database: 
    input:
        cluster = rules.extract_HierCC_info_for_samples.output.cluster,
        meta = rules.combine_and_filter_reference_samples_with_metadata.output.meta,
    output:
        tsv = results_all_samples_dir + cgmlst_dir_name + "/" + phiecc_clustering_dir_name + "/matching_cgmlst_clusters_from_ref_database.tsv",
        xlsx = results_all_samples_dir + cgmlst_dir_name + "/" + phiecc_clustering_dir_name + "/matching_cgmlst_clusters_from_ref_database.xlsx",
    params:
        threshold = clustering_threshold_phiercc,
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk grep -t -P {input.cluster} -f HC{params.threshold} {input.meta} -o {output.tsv}
        csvtk csv2xlsx -t -f {output.tsv} -o {output.xlsx}
        """

rule summarize_country_matching_cgmlst_clusters: 
    input:
        rules.select_matching_cgmlst_clusters_from_ref_database.output.tsv,
    output:
        temp(results_all_samples_dir + cgmlst_dir_name + "/" + phiecc_clustering_dir_name + "/summary_country_cgmlst_clusters_ref_database.tsv"),
    params:
        threshold = clustering_threshold_phiercc,
        metadata="Country",
        metadata_col_name ="Country of isolation"
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk summary -t -g {params.metadata},HC{params.threshold} -f Sample:count,Sample:collapse {input[0]} | 
            csvtk mutate2 -t  -n "{params.metadata}"_numbers -e ' ${params.metadata} + " (" + ${{Sample:count}} + ")" ' | 
            csvtk rename -t  -f "Sample:count" -n Sample_count | 
            csvtk rename -t  -f "Sample:collapse" -n Sample_collapse | 
            csvtk summary -t -g HC{params.threshold} -f Sample_count:sum,"{params.metadata}"_numbers:collapse,Sample_collapse:collapse | 
            csvtk round -t -f 2 -n 0 | 
            csvtk rename -t  -f "Sample_count:sum" -n "number of matching samples" |
            csvtk rename -t  -f "Sample_collapse:collapse" -n "matching samples" |
            csvtk rename -t  -f "{params.metadata}_numbers:collapse" -n "{params.metadata_col_name} (n. samples)" -o {output[0]}
        """


use rule summarize_country_matching_cgmlst_clusters as summarize_year_of_matching_cgmlst_clusters with:
    input:
        rules.select_matching_cgmlst_clusters_from_ref_database.output.tsv,
    output:
        temp(results_all_samples_dir + cgmlst_dir_name + "/" + phiecc_clustering_dir_name + "/summary_year_cgmlst_clusters_ref_database.tsv"),
    params:
        threshold = clustering_threshold_phiercc,
        metadata="Collection_year",
        metadata_col_name ="Year of isolation"


use rule summarize_country_matching_cgmlst_clusters as summarize_host_of_matching_cgmlst_clusters with:
    input:
        rules.select_matching_cgmlst_clusters_from_ref_database.output.tsv,
    output:
        temp(results_all_samples_dir + cgmlst_dir_name + "/" + phiecc_clustering_dir_name + "/summary_host_cgmlst_clusters_ref_database.tsv"),
    params:
        threshold = clustering_threshold_phiercc,
        metadata="Source_niche",
        metadata_col_name ="Source of isolation"


rule join_clusters_and_metadata: 
    input:
        rules.summarize_cgmlst_clusters_based_on_threshold.output[0],
        rules.summarize_country_matching_cgmlst_clusters.output[0],
        rules.summarize_year_of_matching_cgmlst_clusters.output[0],
        rules.summarize_host_of_matching_cgmlst_clusters.output[0],
    output:
        tsv = results_all_samples_dir + cgmlst_dir_name + "/" + phiecc_clustering_dir_name + "/summary_matching_cgmlst_and_metadata.tsv",
        xlsx = results_all_samples_dir + cgmlst_dir_name + "/" + phiecc_clustering_dir_name + "/summary_matching_cgmlst_and_metadata.xlsx",
    params:
        threshold = clustering_threshold_phiercc,
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        if [[ $(wc -l < {input[1]} ) -gt 1 || $(wc -l < {input[2]} ) -gt 1 || $(wc -l < {input[3]} ) -gt 1 ]]; then
            csvtk join -L -t {input} | 
            csvtk cut -t -f 1,2,3,4,5,6,9,12 | 
            csvtk rename -t -f 1 -n "pHierCC Cluster [threshold {params.threshold}]" -o {output.tsv}
        else 
            csvtk join -L -t {input} | 
            csvtk mutate2 -t -n "Matching isolates" -e " 'none' " | 
            csvtk rename -t -f 1 -n "pHierCC Cluster [threshold {params.threshold}]" -o {output.tsv}            
        fi 
       
        csvtk csv2xlsx -t {output.tsv} -o {output.xlsx}
        """


# rule write_report_script_cgmlst_analysis:
#     input:
#         input=COREGENOME_MLST(),
#         working_dir = working_dir,
#         statistics=rules.combine_all_statistics.output[0],
#         pairwise_distances=rules.write_distance_matrix_cgmlst.output[0],
#         phylogeny_image=rules.phylogeny_image_cgmlst.output[0] if len(samplesheet["sample"]) > 1 else [],  
#         clusters_tsv=rules.clustering_with_hclust_for_cgmlst.output[0] if len(samplesheet["sample"]) > 1 and  config["clustering_with_hclust_cgmlst"] == True else [],
#         overview_of_cgmlst_clusters=rules.overview_of_cgmlst_clusters_hclust.output[0] if len(samplesheet["sample"]) > 1 and  config["clustering_with_hclust_cgmlst"] == True else [],
#         cluster_summary_tsv=rules.clustering_with_hclust_for_cgmlst.output[2]  if len(samplesheet["sample"]) > 1 and  config["clustering_with_hclust_cgmlst"] == True else [],
#     output:
#         report = temp(reports_dir + "cgmlst_script.Rmd"),
#     threads: 2
#     params:
#         results_dir=results_all_samples_dir,
#         reports_dir=reports_dir,
#     script:
#         bin_dir + "write_report_script_cgmlst_analysis.sh"

