"""
MIT License

Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

NOFORMAT = "\033[0m"
BOLD = "\033[1m"
UNDERLINE = "\033[4m"
RED = "\033[31m"
BOLDRED = "\033[1;31m"
BOLDGREEN ="\033[1;32m"
GREEN = "\033[32m"
YELLOW = "\033[93m"

# DIRS: 2_results_per_sample #NO starting slash
fastp_dir_name = "qc/fastp"
confinder_dir_name = "qc/confindr"
kraken_dir_name = "qc/kraken"
referenceseeker_dir_name = "taxonomy/referenceseeker"

cdiff_clade_dir_name= "cdiff/clade"
cdiff_toxin_dir_name = "cdiff/toxin_profile"
cdiff_trsanposons_dir_name = "cdiff/trsanposons"
cdiff_plasmids_dir_name ="cdiff/plasmids"
cdiff_phages_dir_name = "cdiff/phages"

# DIRS: 3_results_all_samples
all_raw_reads_dir_name = "1_Raw_reads"
all_genomes_dir_name = "2_genomic_stats"
all_resistance_dir_name = "3_resistance"
all_virulence_dir_name = "4_virulence"
all_mlst_dir_name = "5_MLST"
all_snp_analysis_dir_name = "6_core_genome_SNPs"
variant_calling_dir_name = "Core_genome_SNPs"
recombination_filter_dir_name = "Recombination_filtering"
homoplasyFinder_dir_name = "HomoplasyFinder"
pairwise_distance_dir_name = "Pairwise_SNP_distances"
phylogeny_dir_name = "Phylogeny"
cluster_dir_name = "Clustering"
cgmlst_dir_name = "7_core_genome_MLST"


allele_calling_dir_name = "1_allele_calling"
cgmlst_profiles_dir_name = "2_cgmlst_profiles"
test_genome_quality_dir_name = "genome_quality"
cgmlst_clustering_dir_name = "4_cgmlst_clustering"
cgmlst_distance_matrix_dir_name = "3_distance_matrix"
trees_from_cgmlst_results_dir_name = "5_trees_from_cgmlst"
phiecc_clustering_dir_name = "6_matching_strains"


pangenome_dir_name = "8_Pangenome"
reference_dir_name = "reference"
# species directories
cdifficile_dir_name = "C_difficile_results"


# warn on new versions on git
try:  # https://github.com/bhattlab/lathe - MIT License (c) 2019 Moss
    import git

    repo_dir = os.path.dirname(workflow.snakefile) + "/../"
    repo = git.Repo(repo_dir)
    assert not repo.bare
    repo_git = repo.git
    stat = repo_git.diff("origin/master")
    if stat != "":
        print(YELLOW + "\nWARNING: " + NOFORMAT + "This " + program_name + " version (" + __version__ + ") is different from the latest version on git. \n         Please reset changes and/or git pull " + repo_url + "\n\n")
except:
    #    print("make sure you run the latest version on the git repo")
    pass

def remove_temp_directory(directory):
    try:
        shutil.rmtree(directory)
        print(f"Successfully removed temporary directory: {directory}")
    except FileNotFoundError:
        pass 



@contextmanager
def suppress_stdout():
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:
            yield
        finally:
            sys.stdout = old_stdout


            
onstart:
    print(r"""

    _____   _                 _
   / ____| | |               | |
  | |      | |   ___    ___  | |_   _   _   _ __     ___   _ __
  | |      | |  / _ \  / __| | __| | | | | | '_ \   / _ \ | '__|
  | |____  | | | (_) | \__ \ | |_  | |_| | | |_) | |  __/ | |
   \_____| |_|  \___/  |___/  \__|  \__, | | .__/   \___| |_|
                                     __/ | | |
                                    |___/  | | """ + YELLOW + """O       o O       o """ + NOFORMAT + """
                                           | | """ + YELLOW + """| O   o | | O   o | """ + NOFORMAT + """
                                           | | """ + YELLOW + """| | O | | | | O | | """ + NOFORMAT + """
                                           | | """ + YELLOW + """| o   O | | o   O | """ + NOFORMAT + """
                                           |_| """ + YELLOW + """o       O o       O """ + NOFORMAT + """

        Clostridia characterization and typing pipeline
    (available at: """ + repo_url + """)
                 """ + YELLOW + """ version: """ + __version__ + NOFORMAT + """
    """)
    sys.stdout.flush()  # output is flushed immediately



onsuccess:
    remove_temp_directory(temporary_todelete)
    print(BOLDGREEN + "\nPipeline finished successfully \U0001F600 \n" + NOFORMAT)
    sys.stdout.flush()  # ou  tput is flushed immediately

onerror:
    remove_temp_directory(temporary_todelete)
    print(BOLDRED + "\nPipeline finished with error \U0001F612 \n" + NOFORMAT)
    sys.stdout.flush()  # output is flushed immediately

