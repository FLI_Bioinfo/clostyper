"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


def VIRULENCE():
    if config["VIRULENCE"] == True:

        VIRULENCE = []

        databases = config["abricate_virulence"]["params"]["databases"]

        if databases in ["all", "vfdb"]:
            VIRULENCE.append(rules.get_latest_vfdb_database.output.version)
            VIRULENCE.append(expand(rules.abricate_virulence_vfdb.output.tsv, sample =SAMPLES))
            VIRULENCE.append(expand(rules.abricate_virulence_vfdb.output.tsv, sample =GENOMES))
            VIRULENCE.append(rules.abricate_summary_vir_vfdb.output.tsv)
            VIRULENCE.append(rules.detail_summary_abricate_vfdb.output.tsv)

        if databases in ["all", "victors"]:
            VIRULENCE.append(rules.get_latest_victors_database.output.version)
            VIRULENCE.append(expand(rules.abricate_virulence_victors.output.tsv, sample =SAMPLES))
            VIRULENCE.append(expand(rules.abricate_virulence_victors.output.tsv, sample =GENOMES))
            VIRULENCE.append(rules.abricate_summary_vir_victors.output.tsv)
            VIRULENCE.append(rules.detail_summary_abricate_victors.output.tsv)

        return VIRULENCE
    
    else: 
        
        return []


"""
abricate
"""


rule get_latest_vfdb_database:
    output:
        abricate_databases=directory(temporary_todelete + "abricate_db_vir"),
        vfdb=directory(temporary_todelete + "abricate_db_vir/vfdb"),
        version=os.path.join(results_all_samples_dir, all_virulence_dir_name, "versions/abricate_vfdb_version.txt"),
    conda:
        envs_folder + "abricate.yaml"
    threads: 2
    log:
        os.path.join(results_all_samples_dir, all_virulence_dir_name, "versions/abricate_vfdb_database.log"),
    params:
        abricate_databases=config["abricate_databases"] if config["abricate_databases"] else "__no_name__",
        database_name="vfdb"
    shell:
        r"""
        mkdir -p {output.abricate_databases}
        abricate --version > {output.version}
        date > {log}
        if [[ -d {params.abricate_databases}/{params.database_name} ]] && [[ "$(ls -A {params.abricate_databases}/{params.database_name}/sequences )" ]]; then
            cp -r -L {params.abricate_databases}/{params.database_name}/ {output.abricate_databases}/
            echo "Found ABRicate database: {params.database_name} "
            echo -ne "\\n\\nWill use the database {params.database_name} from the folder: {params.abricate_databases}\\n" >> {output.version}
        elif
            abricate-get_db --db {params.database_name}  --force --dbdir {output.abricate_databases} --debug &> {log}; then
            echo -ne "\\n\\nABRicate database {params.database_name}  update on: `date`  \\n" >> {output.version}
            sed -n "/Setting up '{params.database_name}/,+2p" {log} | grep "Downloading: \|Cloning " | sed 's/Downloading: /Download link: /g; ' >> {output.version}
        else
            abricate_path=$(dirname `which abricate`)
            cp -r "$abricate_path/../db/{params.database_name}" {output.abricate_databases}
            echo -ne "\\n\\nFailed to update DB: {params.database_name} - will use the database from abricate installation: $abricate_path\\n" >> {output.version}
        fi
        """


use rule get_latest_vfdb_database as get_latest_victors_database with:
    output:
        abricate_databases=directory(temporary_todelete + "abricate_db_vir"),
        victors=directory(temporary_todelete + "abricate_db_vir/victors"),
        version=os.path.join(results_all_samples_dir, all_virulence_dir_name, "/abricate_victors_version.txt"),
    conda:
        envs_folder + "abricate.yaml"
    log:
        os.path.join(results_all_samples_dir, all_virulence_dir_name, "/abricate_victors_database.log"),
    params:
        abricate_databases=config["abricate_databases"] if config["abricate_databases"] else "__no_name__",
        database_name="victors"


# vfdb
rule abricate_virulence_vfdb:
    input:
        contig=assembly_all + "{sample}.fasta",
        db_dir=rules.get_latest_vfdb_database.output.abricate_databases,
        vfdb=rules.get_latest_vfdb_database.output.vfdb,
    output:
        tsv = os.path.join(results_per_sample_dir, "{sample}/virulence/{sample}_abricate_vfdb.tsv"),
    threads: 8
    log:
        os.path.join(results_per_sample_dir, "{sample}/virulence/{sample}_abricate_vfdb.log"),
    conda:
        envs_folder + "abricate.yaml"
    params:
        abricate_database="vfdb",
        min_identity="--minid " + str(config["abricate_virulence"]["params"]["min_identity"]) if config["abricate_virulence"]["params"]["min_identity"] else "",
        min_coverage="--mincov " + str(config["abricate_virulence"]["params"]["min_coverage"]) if config["abricate_virulence"]["params"]["min_coverage"] else "",
        abricate_extraparams=config["abricate_virulence"]["params"]["abricate_extraparams"] if config["abricate_virulence"]["params"]["abricate_extraparams"] else "",
    shell:
        r"""
        abricate --nopath --datadir {input.db_dir} \
            --db {params.abricate_database} --threads {threads}\
            {params.min_identity} {params.min_coverage} {params.abricate_extraparams} {input.contig} > {output.tsv} 2> {log}
        """


rule abricate_summary_vir_vfdb:
    input:
        expand(results_per_sample_dir + "{sample}/virulence/{sample}_abricate_vfdb.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/virulence/{genome}_abricate_vfdb.tsv", genome=GENOMES),
    output:
        tsv=os.path.join(results_all_samples_dir, all_virulence_dir_name, "abricate_vfdb_summary.tsv"),
        tsv_binary=os.path.join(results_all_samples_dir, all_virulence_dir_name, "abricate_vfdb_summary_binary.tsv"),
    conda:
        envs_folder + "abricate.yaml"
    params:
        suffix="_abricate_vfdb.tsv",
    shell:
        r"""
        abricate --nopath --summary {input} |
            csvtk replace -t -C "$" -f 1 -p "{params.suffix}" -r "" |
            csvtk rename  -t -C "$" -f "#FILE" -n "Sample" |
            csvtk rename  -t -C "$" -f "NUM_FOUND" -n "No. of genes" > {output.tsv}

        if [[ $(csvtk ncol -t {output.tsv} ) -gt 2 ]]; then
            cat {output.tsv} | 
            csvtk cut -t -f -2 | 
            csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv}) - 1 ) -p '^.$'   -r @ | 
            csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv}) - 1 ) -p '[^@]+' -r 1 | 
            csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv}) - 1 ) -p '^@$'   -r 0 > {output.tsv_binary}
        else
            cat {output.tsv}  > {output.tsv_binary};
        fi
        """


rule detail_summary_abricate_vfdb:
    input:
        expand(results_per_sample_dir + "{sample}/virulence/{sample}_abricate_vfdb.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/virulence/{genome}_abricate_vfdb.tsv", genome=GENOMES),
    output:
        FOF=temp(os.path.join(results_all_samples_dir, all_virulence_dir_name, "samples_path_vfdb.tsv")),
        tsv=os.path.join(results_all_samples_dir, all_virulence_dir_name, "abricate_vfdb_detailed.tsv"),
        xlsx=os.path.join(results_all_samples_dir, all_virulence_dir_name, "abricate_vfdb_detailed.xlsx"),
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        echo -ne "" > {output.FOF} &&
        for path in {input}; do
            if [[ -s $path ]]; then
                echo -ne "$path\n" >> {output.FOF};
            fi
        done
        csvtk concat -t --infile-list {output.FOF} -C "$" -T -i -E  -I | 
            csvtk replace -C "$" -p ".fasta" -r '$1 ' |
            csvtk rename  -t -C "$" -f "#FILE" -n "Sample" --out-file {output.tsv}

        csvtk csv2xlsx -t {output.tsv} -o {output.xlsx}
        """


# victors
use rule abricate_virulence_vfdb as abricate_virulence_victors with:
    input:
        contig=assembly_all + "{sample}.fasta",
        db_dir=rules.get_latest_victors_database.output[0],
        victors=rules.get_latest_victors_database.output[1],
    output:
        tsv = results_per_sample_dir + "{sample}/virulence/{sample}_abricate_victors.tsv",
    log:
        results_per_sample_dir + "{sample}/virulence/{sample}_abricate_victors.log",
    params:
        abricate_database="victors",
        min_identity="--minid " + str(config["abricate_virulence"]["params"]["min_identity"]) if config["abricate_virulence"]["params"]["min_identity"] else "",
        min_coverage="--mincov " + str(config["abricate_virulence"]["params"]["min_coverage"]) if config["abricate_virulence"]["params"]["min_coverage"] else "",
        abricate_extraparams=config["abricate_virulence"]["params"]["abricate_extraparams"] if config["abricate_virulence"]["params"]["abricate_extraparams"] else "",


use rule abricate_summary_vir_vfdb as abricate_summary_vir_victors with:
    input:
        expand(results_per_sample_dir + "{sample}/virulence/{sample}_abricate_victors.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/virulence/{genome}_abricate_victors.tsv", genome=GENOMES),
    output:
        tsv=results_all_samples_dir + all_virulence_dir_name + "/abricate_victors_summary.tsv",
        tsv_binary=results_all_samples_dir + all_virulence_dir_name + "/abricate_victors_summary_binary.tsv",
    params:
        suffix="_abricate_victors.tsv",


use rule detail_summary_abricate_vfdb as detail_summary_abricate_victors with:
    input:
        expand(results_per_sample_dir + "{sample}/virulence/{sample}_abricate_victors.tsv", sample=SAMPLES),
        expand(results_per_sample_dir + "{genome}/virulence/{genome}_abricate_victors.tsv", genome=GENOMES),
    output:
        FOF=temp(results_all_samples_dir + all_virulence_dir_name + "/samples_path_victors.tsv"),
        tsv=results_all_samples_dir + all_virulence_dir_name + "/abricate_victors_detailed.tsv",
        xlsx=results_all_samples_dir + all_virulence_dir_name + "/abricate_victors_detailed.xlsx",



# report
# rule write_report_script_virulence:
#     input:
#         input=VIRULENCE(),
#         header=report_scripts + "virulence_header.Rmd",
#         abricate_vfdb=report_scripts + "virulence_vfdb.Rmd" if config["abricate_virulence"]["params"]["databases"] == "vfdb" or config["abricate_virulence"]["params"]["databases"] == "all" else [],
#         abricate_victors=report_scripts + "virulence_victors.Rmd" if config["abricate_virulence"]["params"]["databases"] == "victors" or config["abricate_virulence"]["params"]["databases"] == "all" else [],
#     output:
#         report = temp(reports_dir + "virulence_script.Rmd"),
#     threads: 2
#     params:
#         results_dir=results_all_samples_dir,
#         reports_dir=reports_dir,
#     shell:
#         """
#         cat {input.header} {input.abricate_vfdb} {input.abricate_victors} > {output}
#         """

