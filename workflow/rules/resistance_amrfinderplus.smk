"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# FIXME: report of AMRfinderplus summary in cases all analysed samples were AMR-negative


"""
AMRfinderplus
"""


# amrfinderplus is the core of this ISO-certified workflow: https://www.nature.com/articles/s41467-022-35713-4
rule get_amrfinderplus_db:
    output:
        amrfinderplus_db=directory(temporary_todelete + "amrfinderplus_db"),
        amrfinderplus_db_latest=directory(temporary_todelete + "amrfinderplus_db/latest"),
    threads: 8
    conda:
        envs_folder + "amrfinderplus.yaml"
    log:
        results_all_samples_dir + all_resistance_dir_name + "/amrfinderplus_database.log",
    shell:
        r"""
        amrfinder_update --database {output.amrfinderplus_db} &> {log} || amrfinder_update --force_update --database {output.amrfinderplus_db} &> {log}
        """


def amrfinderplus_database():
    amrfinderplus_db = None
    provided_amrfinderplus_db=config["amrfinderplus_database"]
    # Check if amrfinderplus_db is present
    if provided_amrfinderplus_db and os.path.exists(provided_amrfinderplus_db) and os.path.isdir(provided_amrfinderplus_db):
        # Check if the required files are present in the directory
        required_files = ['AMR.LIB', 'AMR_CDS', 'AMRProt']
        files_in_directory = os.listdir(provided_amrfinderplus_db)
        if all(file in files_in_directory for file in required_files):
            amrfinderplus_db = provided_amrfinderplus_db
            # print("found AMRfinderplus database:" + amrfinderplus_db )

    if amrfinderplus_db is None: 
        amrfinderplus_db = temporary_todelete + "amrfinderplus_db/latest"
        # print("Cannot find the AMRfinderplus database - will download the latest version here: " + amrfinderplus_db )
 
    return amrfinderplus_db


rule amrfinderplus_version:
    input: amrfinderplus_database()
    output:
        version=results_all_samples_dir + all_resistance_dir_name + "/versions/amrfinderplus.txt",
    threads: 1
    conda:
        envs_folder + "amrfinderplus.yaml"
    shell:
        r"""
        amrfinder -V -d {input}  &>>  {output.version}
        """


rule run_amrfinderplus:
    input:
        contig=assembly_all + "{sample}.fasta",
        # amrfinderplus_db=temporary_todelete + "amrfinderplus_db",
        amrfinderplus_db= amrfinderplus_database(),
        gff=gff_files(),
        protein=results_per_sample_dir + "{sample}/annotation/{sample}.faa",
    output:
        tsv=results_per_sample_dir + "{sample}/resistance/{sample}_amrfinder_raw.tsv",
    threads: 8
    conda:
        envs_folder + "amrfinderplus.yaml"
    log:
        results_per_sample_dir + "{sample}/resistance/amrfinderplus.log",
    params:
        organism="-O " + config["amrfinderplus"]["params"]["organism"] if config["amrfinderplus"]["params"]["organism"] else "",
        annotation_program="-a " + config["Annotation_software"] if config["Annotation_software"] else "",
        min_identity="-i " + str(config["amrfinderplus"]["params"]["min_identity"]) if config["amrfinderplus"]["params"]["min_identity"] else "",
        min_coverage="-c " + str(config["amrfinderplus"]["params"]["min_coverage"]) if config["amrfinderplus"]["params"]["min_coverage"] else "",
        amrfinderplus_extraparams=config["amrfinderplus"]["params"]["amrfinderplus_extraparams"] if config["amrfinderplus"]["params"]["amrfinderplus_extraparams"] else "",
    shell:
        r"""
        amrfinder -p {input.protein} -n {input.contig} {params.organism} \
            -d {input.amrfinderplus_db} \
            -g {input.gff} \
            {params.annotation_program} \
            {params.min_identity} \
            {params.min_coverage} \
            {params.amrfinderplus_extraparams} -o {output.tsv} 2>> {log}
        """


rule add_taxonomy_of_contigs_with_resistance_amrfinderplus:
    input:
        results_per_sample_dir + "{sample}/resistance/{sample}_amrfinder_raw.tsv",
        kraken=results_per_sample_dir + "{sample}/" + kraken_dir_name + "/{sample}_kraken_contigs_raw.txt",
    output:
        results_per_sample_dir + "{sample}/resistance/{sample}_amrfinderplus_w_taxonomy.tsv",
    threads: 4
    params:
        taxdump_database=config["taxdump_database"] if config["taxdump_database"] else "",
    conda:
        envs_folder + "kraken2.yaml"
    shell:
        r"""
         if [[ $(csvtk nrow -tH {input[0]} ) -gt 1 ]]; then
            paste <( cat {input[0]} ) <( tail -n +2 {input[0]} | 
                csvtk cut -f 2 -t | 
                    while read contig ; do 
                csvtk grep -f 2 -p $contig  -tH  {input[1]} | 
                csvtk cut -f 3 -t | 
                taxonkit lineage --threads 4 --data-dir {params.taxdump_database} -n | 
                csvtk -Ht cut -f 1,2,3 ;
            done | csvtk add-header -n "Contig taxonid","Contig taxonomic group","Scientific name" -t ) --delimiters '\t' >  {output}
        else
            touch {output}
        fi
         """


rule add_sample_name_amrfinderplus:
    input:
        rules.add_taxonomy_of_contigs_with_resistance_amrfinderplus.output[0] if config["add_taxonomy_for_contigs_with_resistance"] == True else rules.run_amrfinderplus.output[0],  # results_per_sample_dir + "{sample}/resistance/{sample}_amrfinder_raw.tsv"
    output:
        tsv=results_per_sample_dir + "{sample}/resistance/{sample}_amrfinder.tsv",
        # xlsx=results_per_sample_dir + "{sample}/resistance/{sample}_amrfinder.xlsx",
    conda:
        envs_folder + "data_parse.yaml"
    params:
        additional_cols=',"Contig taxonid","Contig taxonomic group","Scientific name"' if config["add_taxonomy_for_contigs_with_resistance"] == True else "",
        additional_cols2="\tContig taxonid\tContig taxonomic group\tScientific name" if config["add_taxonomy_for_contigs_with_resistance"] == True else "",
    shell:
        r"""
        if [[ $(csvtk nrow -tH {input[0]} ) -gt 1 ]]; then
            csvtk mutate2 -t -e " '{wildcards.sample}' " -n Sample {input} |
            csvtk cut -t -f "Sample","Protein identifier","Contig id","Start","Stop","Strand","Gene symbol","Sequence name","Scope","Element type","Element subtype","Class","Subclass","Method","Target length","Reference sequence length","% Coverage of reference sequence","% Identity to reference sequence","Alignment length","Accession of closest sequence","Name of closest sequence","HMM id","HMM description"{params.additional_cols} -o {output.tsv}
        else 
             echo -ne "Sample\tProtein identifier\tContig id\tStart\tStop\tStrand\tGene symbol\tSequence name\tScope\tElement type\tElement subtype\tClass\tSubclass\tMethod\tTarget length\tReference sequence length\t% Coverage of reference sequence\t% Identity to reference sequence\tAlignment length\tAccession of closest sequence\tName of closest sequence\tHMM id\tHMM description{params.additional_cols2}\n" > {output.tsv}
        fi
        """
            # csvtk csv2xlsx -t {output.tsv} -o {output.xlsx}

rule detail_summary_amrfinderplus:
    input:
        samples=expand(results_per_sample_dir + "{sample}/resistance/{sample}_amrfinder.tsv", sample=SAMPLES),
        genomes=expand(results_per_sample_dir + "{genome}/resistance/{genome}_amrfinder.tsv", genome=GENOMES),
    output:
        FOF=temp(results_all_samples_dir + all_resistance_dir_name + "/samples_path_amrfinder.tsv"),
        tsv=results_all_samples_dir + all_resistance_dir_name + "/amrfinder_detailed.tsv",
        # xlsx=results_all_samples_dir + all_resistance_dir_name + "/amrfinder_detailed.xlsx",
    conda:
        envs_folder + "amrfinderplus.yaml"
    shell:
        """
        echo -ne "" > {output.FOF} &&
        for path in {input}; do
            if [[ -s $path ]]; then
                echo -ne "$path\n" >> {output.FOF};
            fi
        done
        csvtk concat -t --infile-list {output.FOF} --out-file {output.tsv} -T -i -E  -I
        """


rule detail_summary_amrfinderplus_in_excel:
    input:
        rules.detail_summary_amrfinderplus.output.tsv,
    output:
        xlsx=results_all_samples_dir + all_resistance_dir_name + "/amrfinder_detailed.xlsx",
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        csvtk csv2xlsx -f -t {output.tsv} -o {output.xlsx}
        """


"""
using spread from csvtk - TODO to be tested  
"""
# rule amrfinderplus_summary:
#     input:
#         amr_results=rules.detail_summary_amrfinderplus.output[1],
#         names=rules.isolates_list.output[0]
#     output:
#         tsv=results_all_samples_dir + all_resistance_dir_name + "/amrfinder_summary.tsv",
#         tsv_binary=results_all_samples_dir + all_resistance_dir_name + "/amrfinder_summary_binary.tsv",
#         xlsx=results_all_samples_dir + all_resistance_dir_name + "/amrfinder_summary.xlsx",
#         temp=temp(results_all_samples_dir + all_resistance_dir_name + "/amrfinder_summary.temp"),
#     conda:
#         envs_folder + "amrfinderplus.yaml"
#     shell:
#         """
#         csvtk cut -t -f "Sample","Gene symbol","% Coverage of reference sequence" {input.amr_results} | 
#            csvtk fold -t -f 1,2 -v 3 | #collapse coverage values of each gene 
#            csvtk spread -t -k 2 -v 3  --na "-"  -o {output.temp}
#         if [[ $(csvtk nrow -tH {output.temp} ) -gt 1 ]]; then
#             csvtk join -t --na - -f 1 -k  {input.names} {output.temp} -o {output.tsv}
#             csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv})) -p '^.$'   -r @  {output.tsv} | 
#                 csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv})) -p '[^@]+' -r 1 | 
#                 csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv})) -p '^@$'   -r 0 > {output.tsv_binary}
#         else 
#             csvtk mutate2 -t -n "n. genes" -e "0"  {input.names} -o {output.tsv} 
#             csvtk mutate2 -t -n "n. genes" -e "0"  {input.names} -o {output.tsv_binary} 
#         fi 
#         #convert to excel
#         csvtk csv2xlsx -t {output.tsv} -o {output.xlsx}
#         """

rule summarize_amrfinderplus:
    input:
        rules.run_amrfinderplus.output[0],
    output:
        reduced=results_per_sample_dir + "{sample}/resistance/{sample}_amrfinder_reduced.tsv",
        profile=results_per_sample_dir + "{sample}/resistance/{sample}_amrfinder_resistance_profile.tsv",
    conda:
        envs_folder + "amrfinderplus.yaml"
    params:
        sample_name="{sample}",
    shell:
        r"""
        csvtk cut -t -f "Gene symbol","% Coverage of reference sequence" {input} -E -I \
        | csvtk fold -t -f "Gene symbol" -v "% Coverage of reference sequence" -s ";" \
        | csvtk rename -t -f 2 -n {params.sample_name} -o {output.reduced} 

        if [[ $(csvtk nrow -t {output.reduced} ) == 0 ]]; then
            echo -ne "Sample\tNumber of genes\tResistance profile\tAMR (sub)class\n{params.sample_name}\tNone\tNone\tNone\n" > {output.profile}
        else
            csvtk cut -t -f "Gene symbol","% Coverage of reference sequence","Subclass" {input} -E -I \
            | csvtk fold -t -f "Gene symbol","Subclass" -v "% Coverage of reference sequence" -s ";" \
            | csvtk cut -t -f 1,3,2 \
            | csvtk rename -t -f 2 -n {params.sample_name} \
            | csvtk sort -k 1 \
            | csvtk replace -t -f 2 -p "(.+)" -r  {params.sample_name} \
            | csvtk summary -f "Gene symbol":count,"Gene symbol":collapse,Subclass:uniq -i -g {params.sample_name} -t \
            | csvtk rename -t -f 1,2,3,4  -n  "Sample","Number of genes","Resistance profile","AMR (sub)class" -o {output.profile}
        fi
        """


rule collect_resistance_profiles:
    input:
        expand(rules.summarize_amrfinderplus.output[1], sample=SAMPLES),
        expand(rules.summarize_amrfinderplus.output[1], sample=GENOMES),
    output:
        profile = results_all_samples_dir + all_resistance_dir_name + "/resistance_profiles_per_isolate_amrfinderplus.tsv",
        all_profiles = results_all_samples_dir + all_resistance_dir_name + "/resistance_profiles_all_isolates_amrfinderplus.tsv",
        # results_all_samples_dir + all_resistance_dir_name + "/resistance_profiles_per_isolate_amrfinderplus.xlsx",
        # results_all_samples_dir + all_resistance_dir_name + "/resistance_profiles_all_isolates_amrfinderplus.xlsx",
        temp=temp(results_all_samples_dir + all_resistance_dir_name + "/resistance_profiles_all_isolates_amrfinderplus.temp"),
    conda:
        envs_folder + "amrfinderplus.yaml"
    shell:
        """
        echo -ne "" >  {output.temp} &&  
        for file in {input} ; do echo $file >> {output.temp} ; done 
                 
        csvtk concat -t --infile-list {output.temp} -o {output.profile}

        cat {output.profile} \
        | csvtk summary -f "Sample":count,"Sample":collapse,"AMR (sub)class":uniq -i -g "Resistance profile" -t \
        | csvtk unfold -t  -f "AMR (sub)class:uniq" -s "; " \
        | csvtk sort -t  -k 4 \
        | csvtk uniq  -t -f 1,2,3,4 | csvtk summary -f 4:collapse -g 1,2,3  -t \
        | csvtk rename -t -f 1,2,3,4  -n  "Resistance profile","Number of isolates","Name of isolates","AMR (sub)classes" \
        | csvtk sort -t  -k 2:nr -o {output.all_profiles}

        """

        #prepare excels
        # csvtk csv2xlsx -t {output[0]} -o {output[2]}
        # csvtk csv2xlsx -t {output[1]} -o {output[3]}

rule collect_all_amrfinderplus_summaries:
    input:
        samples=expand(results_per_sample_dir + "{sample}/resistance/{sample}_amrfinder_reduced.tsv", sample=SAMPLES),
        genomes=expand(results_per_sample_dir + "{genome}/resistance/{genome}_amrfinder_reduced.tsv", genome=GENOMES),
        names=rules.isolates_list.output[0]
    output:
        tsv=results_all_samples_dir + all_resistance_dir_name + "/amrfinder_summary.tsv",
        # tsv_binary=results_all_samples_dir + all_resistance_dir_name + "/amrfinder_summary_binary.tsv",
        # xlsx=results_all_samples_dir + all_resistance_dir_name + "/amrfinder_summary.xlsx",
        temp=temp(results_all_samples_dir + all_resistance_dir_name + "/amrfinder_summary.temp"),
        temp2=temp(results_all_samples_dir + all_resistance_dir_name + "/amrfinder_summary.temp2"),
    conda:
        envs_folder + "amrfinderplus.yaml"
    shell:
        """
        echo -ne "" >  {output.temp2} && 
        for file in {input.samples} {input.genomes} ; do echo $file >> {output.temp2} ; done &&  
        csvtk join -f 1 --infile-list {output.temp2} -O --na "-"  -t | 
            csvtk sort -t -k 1 | csvtk transpose -t -o {output.temp}
        if [[ $(csvtk nrow -tH {output.temp} ) -gt 1 ]]; then
            csvtk join -t --na - -f 1 -k  {input.names} {output.temp} -o {output.tsv}
        else 
            csvtk mutate2 -t -n "n. genes" -e "0"  {input.names} -o {output.tsv} 
        fi 
        """

# """
# echo -ne "" >  {output.temp2} && 
# for file in {input.samples} {input.genomes} ; do echo $file >> {output.temp2} ; done &&  
# csvtk join -f 1 --infile-list {output.temp2} -O --na "-"  -t | 
#     csvtk sort -t -k 1 | csvtk transpose -t -o {output.temp}
# if [[ $(csvtk nrow -tH {output.temp} ) -gt 1 ]]; then
#     csvtk join -t --na - -f 1 -k  {input.names} {output.temp} -o {output.tsv}
#     csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv})) -p '^.$'   -r @  {output.tsv} | 
#         csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv})) -p '[^@]+' -r 1 | 
#         csvtk replace -t -f 2-$(expr $(csvtk ncol -t {output.tsv})) -p '^@$'   -r 0 > {output.tsv_binary}
# else 
#     csvtk mutate2 -t -n "n. genes" -e "0"  {input.names} -o {output.tsv} 
#     csvtk mutate2 -t -n "n. genes" -e "0"  {input.names} -o {output.tsv_binary} 
# fi 
# """
        #convert to excel
        # csvtk csv2xlsx -t {output.tsv} -o {output.xlsx}
