"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

C_PERFRINGENS_MODULE=config["C_PERFRINGENS_MODULE"]
C_PERFRINGENS_TOXIN_TYPING=config["C_PERFRINGENS_TOXIN_TYPING"]
C_PERFRINGENS_VIRULENCE_GENES=config["C_PERFRINGENS_VIRULENCE_GENES"]
C_PERFRINGENS_PLASMID_TYPING=config["C_PERFRINGENS_PLASMID_TYPING"]
C_PERFRINGENS_INSERTION_SEQUENCES=config["C_PERFRINGENS_INSERTION_SEQUENCES"]
C_PERFRINGENS_IRON_SYSTEMS=config["C_PERFRINGENS_IRON_SYSTEMS"]
 

def C_PERFRINGENS_MODULE():
    if config["C_PERFRINGENS_MODULE"] == True:

        C_PERFRINGENS_MODULE = [rules.collect_cperfringens_results.output.tsv]
        if C_PERFRINGENS_TOXIN_TYPING == True: 
            C_PERFRINGENS_MODULE.append([
                expand(rules.clean_blastn_cperf_toxins.output.blast_res, sample=GENOMES),
                expand(rules.clean_blastn_cperf_toxins.output.blast_res, sample=SAMPLES),
                expand(rules.call_toxintype.output.type, sample=GENOMES),
                expand(rules.call_toxintype.output.type, sample=SAMPLES),
                rules.collect_toxintype.output.type,
                ])
        if C_PERFRINGENS_VIRULENCE_GENES == True: 
            C_PERFRINGENS_MODULE.append([
                rules.join_names_with_vf.output.summary,
                ])
        if C_PERFRINGENS_PLASMID_TYPING == True: 
            C_PERFRINGENS_MODULE.append([
                expand(rules.clean_blastn_cperf_pcp_plasmid.output.blast_res, sample=GENOMES),
                expand(rules.clean_blastn_cperf_pcp_plasmid.output.blast_res, sample=SAMPLES),
                expand(rules.clean_blastn_cperf_tcp_plasmid.output.blast_res, sample=GENOMES),
                expand(rules.clean_blastn_cperf_tcp_plasmid.output.blast_res, sample=SAMPLES),
                rules.join_names_with_pcp_plasmid_genes.output.summary, 
                rules.join_names_with_tcp_plasmid_genes.output.summary,
                ])

        return C_PERFRINGENS_MODULE
    else:
        return []


clostyper_database = config["clostyper_database"]
clostyper_database = add_trailing_slash(clostyper_database)
C_perfringens_DB = os.path.join(clostyper_database or "", "perfringens")
plasmid_dir = os.path.join(C_perfringens_DB, "plasmids/")
virulence_dir = os.path.join(C_perfringens_DB, "virulence/")
insertion_seq_dir = os.path.join(C_perfringens_DB, "IS/")

cperf_dir_name = "C_perfringens_results"


"""
phylogroup designation
"""

"""
toxin profile
"""

rule run_blastn_cperf_toxins:
    input:
        contig=assembly_all + "{sample}.fasta",
        database=virulence_dir + "vfs/sequences",
    output:
        blast_res = results_per_sample_dir + "{sample}/cperf/toxins/{sample}_blastn_output.raw.txt",
    params:
        blastn_identity=str(config["cperf_toxins"]["blast_identity_threshold"]),
        blastn_evalue="1E-20",
        blastn_culling_limit="1",
        blastn_max_targets="10000",
    threads: 2
    message:
        "Running BLASTN to detect toxin genes in sample: {wildcards.sample}"
    conda:
        envs_folder + "blast.yaml"
    shell:
        """
        blastn -task blastn -dust no -perc_identity {params.blastn_identity} \
          -db {input.database} \
          -outfmt '6 qseqid qstart qend sseqid sstrand evalue pident length gaps slen stitle qseq' \
          -num_threads {threads} \
          -evalue {params.blastn_evalue} \
          -culling_limit {params.blastn_culling_limit} \
          -max_target_seqs {params.blastn_max_targets} \
          -query {input.contig}\
          -out {output.blast_res}
        """


rule clean_blastn_cperf_toxins:
    input:
        rules.run_blastn_cperf_toxins.output.blast_res,
    output:
        blast_res = results_per_sample_dir + "{sample}/cperf/toxins/{sample}_blastn_output.tsv",
    params:
        identity_threshold=str(config["cperf_toxins"]["blast_identity_threshold"]),
        coverage_threshold=str(config["cperf_toxins"]["blast_coverage_threshold"]),
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        if [[ -s {input[0]} ]]; then
            cat {input} | 
                csvtk add-header -t -n contig_id,query_start,query_end,gene,strand,evalue,identity,aln_length,gaps,ref_len,description,sequence | 
                csvtk mutate2 -t -e ' (($aln_length - $gaps) / $ref_len) * 100 ' -n coverage | 
                # csvtk mutate2 -t -e ' $gene + ":" +  $contig_id + "_" +  $query_start + "-" + $query_end ' -n gene_coords --numeric-as-string | 
                csvtk mutate2 -t -e " '{wildcards.sample}' " -n sample | 
                csvtk filter -t -f "coverage>{params.coverage_threshold}" | 
                csvtk filter -t -f "identity>{params.identity_threshold}" | 
                csvtk cut -t -f sample,contig_id,query_start,query_end,gene,strand,evalue,identity,coverage,aln_length,description,sequence -o {output.blast_res}
        else
            touch {output.blast_res}
        fi
        """


rule call_toxintype:
    input:
        rules.clean_blastn_cperf_toxins.output.blast_res,
    output:
        type = results_per_sample_dir + "{sample}/cperf/toxins/{sample}_toxintype.txt",
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        toxin_content=$(csvtk cut -t -f gene {input} |
            csvtk filter2 -t -f '$gene in ("plc", "cpb", "etx", "iap", "ibp", "cpe", "netB")' | 
            csvtk uniq -f 1 | csvtk sort -k 1 | csvtk del-header | csvtk transpose)

        if [ "$toxin_content" == "netB,plc" ]; then 
            echo -e "{wildcards.sample}\\tType G: $toxin_content" > {output[0]}
        elif [ "$toxin_content" == "cpe,plc" ]; then 
            echo -e "{wildcards.sample}\\tType F: $toxin_content" > {output[0]}
        elif [[ "$toxin_content" =~ ^(cpe,iap,ibp|cpe,iap|cpe,ibp|iap,ibp|iap|ibp),plc$ ]]; then 
            echo -e "{wildcards.sample}\\tType E: $toxin_content" > {output[0]}
        elif [[ "$toxin_content" =~ ^(etx|cpe,etx),plc$ ]]; then 
            echo -e "{wildcards.sample}\\tType D: $toxin_content" > {output[0]}
        elif [[ "$toxin_content" =~ ^(cpb|cpb,cpe),plc$ ]]; then 
            echo -e "{wildcards.sample}\\tType C: $toxin_content" > {output[0]}
        elif [[ "$toxin_content" =~ ^(cpb,etx|cpb,cpe,etx),plc$ ]]; then 
            echo -e "{wildcards.sample}\\tType B: $toxin_content" > {output[0]}
        elif [ "$toxin_content" == "plc" ]; then 
            echo -e "{wildcards.sample}\\tType A: $toxin_content" > {output[0]}
        else 
            echo -e "{wildcards.sample}\\tUnknown Type: $toxin_content" > {output[0]}
        fi
        """


rule collect_toxintype:
    input:
        expand(rules.call_toxintype.output.type, sample=SAMPLES),
        expand(rules.call_toxintype.output.type, sample=GENOMES),
    output:
        type = results_all_samples_dir + cperf_dir_name + "/Toxin_type.tsv",
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        csvtk concat --quiet -tH {input} | csvtk add-header -t -n Sample,Toxin-Type -o {output[0]}
        """

rule summarize_virulence_genes:
    input:
        expand(rules.clean_blastn_cperf_toxins.output.blast_res, sample=SAMPLES), 
        expand(rules.clean_blastn_cperf_toxins.output.blast_res, sample=GENOMES), 
    output:
        sum = temp(results_all_samples_dir + cperf_dir_name + "/temp_virulence_genes.tsv"),
    threads: 2
    params:
        spread_value= "coverage" #or identity
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        csvtk concat --quiet -t {input} | 
            csvtk cut -t -f sample,gene,{params.spread_value} | 
            csvtk spread -t -k 1 -v 3 --na - | csvtk sort -t -k 1 | csvtk transpose -t | csvtk sort -t -k 1 -o {output.sum}
        """

rule calculate_vf_per_sample:
    input: 
        summary = rules.summarize_virulence_genes.output.sum,
    output:
        tsv = temp(os.path.join(results_all_samples_dir, cperf_dir_name, "temp_n_vir_genes.tsv")),
    threads: 2
    run: 
        # Check if the input file exists and is not empty
        if not os.path.exists(input.summary) or os.stat(input.summary).st_size == 0:
            # Create an empty file with headers to avoid failure
            pd.DataFrame(columns=["gene", "Virulence_genes"]).to_csv(output.tsv, sep="\t", index=False)
        else:
            # Load input file
            df = pd.read_csv(input.summary, sep="\t")

            # Identify gene columns dynamically, excluding the first column (assumed to be 'gene')
            gene_columns = [col for col in df.columns if col != "gene"]

            # If no gene columns exist, write an empty file and exit
            if not gene_columns:
                pd.DataFrame(columns=["gene", "Virulence_genes"]).to_csv(output.tsv, sep="\t", index=False)
            else:
                # Function to handle semicolon-separated values and convert to numeric
                def process_value(val):
                    if val == "-":
                        return val
                    elif isinstance(val, str) and ";" in val:
                        try:
                            return float(val.split(";")[0])  # Use first value before semicolon
                        except ValueError:
                            return None
                    try:
                        return float(val)
                    except ValueError:
                        return None

                # Apply the processing function to each gene column
                df[gene_columns] = df[gene_columns].apply(lambda col: col.apply(process_value))

                # Convert all values in gene columns to numeric (forcing errors to NaN for non-numeric values)
                df[gene_columns] = df[gene_columns].apply(pd.to_numeric, errors='coerce')

                # Calculate the number of positive genes (genes > 0) for each sample
                positive_genes = df[gene_columns].gt(0).sum(axis=1)

                # Calculate the total number of genes 
                total_genes = len(gene_columns)

                # Report as "positive_genes / total_genes"
                df["Virulence_genes"] = positive_genes.astype(str) + " / " + str(total_genes)

                # Select only the Sample and the new 'Virulence_genes' column
                df_result = df[["gene", "Virulence_genes"]]

                # Save the processed table
                df_result.to_csv(output.tsv, sep="\t", index=False)



rule join_names_with_vf:
    input:
        rules.isolates_list.output.isolates_list,
        rules.calculate_vf_per_sample.output.tsv,
        rules.summarize_virulence_genes.output.sum, 
    output:
        summary = results_all_samples_dir + cperf_dir_name + "/virulence_genes.tsv",
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        csvtk join -t -f 1 {input} -k --na - -o {output.summary}
        """


"""
Pcp plasmid detection
"""
use rule run_blastn_cperf_toxins as run_blastn_cperf_pcp_plasmid with:
    input:
        contig=assembly_all + "{sample}.fasta",
        database=plasmid_dir + "pcp/sequences",
    output:
        blast_res = results_per_sample_dir + "{sample}/cperf/plasmids/{sample}_pcp_blastn_output.raw.txt",
    params:
        blastn_identity=str(config["cperf_plasmid"]["blast_identity_threshold"]),
        blastn_evalue="1E-20",
        blastn_culling_limit="1",
        blastn_max_targets="10000",
    threads: 2
    message:
        "Running BLASTN to detect Pcp plasmid genes in sample: {wildcards.sample}"
    conda:
        envs_folder + "blast.yaml"


use rule clean_blastn_cperf_toxins as clean_blastn_cperf_pcp_plasmid with:
    input:
        rules.run_blastn_cperf_pcp_plasmid.output.blast_res,
    output:
        blast_res = results_per_sample_dir + "{sample}/cperf/plasmids/{sample}_pcp_blastn_output.tsv",
    params:
        identity_threshold=str(config["cperf_plasmid"]["blast_identity_threshold"]),
        coverage_threshold=str(config["cperf_plasmid"]["blast_coverage_threshold"]),
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"


use rule summarize_virulence_genes as summarize_pcp_plasmid_genes with:
    input:
        expand(rules.clean_blastn_cperf_pcp_plasmid.output.blast_res, sample=SAMPLES), 
        expand(rules.clean_blastn_cperf_pcp_plasmid.output.blast_res, sample=GENOMES), 
    output:
        sum = temp(results_all_samples_dir + cperf_dir_name + "/temp_pcp1.tsv"),
    threads: 2
    params:
        spread_value= "coverage" #or identity
    conda:
        envs_folder + "data_parse.yaml"


rule summary_pcp:
    input:
        summary = rules.summarize_pcp_plasmid_genes.output.sum
    output:
        tsv = temp(results_all_samples_dir + cperf_dir_name + "/temp_pcp2.tsv"),
    params:
        prefix="pcp"
    run:
        # Check if file is empty before reading
        if not os.path.exists(input.summary) or os.stat(input.summary).st_size == 0:
            # Create an empty file with headers to avoid failure
            pd.DataFrame(columns=["gene", f"{params.prefix.capitalize()} Locus"]).to_csv(output.tsv, sep="\t", index=False)
        else:
            # Load input file
            df = pd.read_csv(input.summary, sep="\t")

            # Identify gene columns dynamically based on prefix
            gene_columns = [col for col in df.columns if col.startswith(params.prefix)]

            # If no gene columns are found, create an empty file with headers
            if not gene_columns:
                pd.DataFrame(columns=["gene", f"{params.prefix.capitalize()} Locus"]).to_csv(output.tsv, sep="\t", index=False)
            else:
                # Replace all empty values, None, NaN, and empty strings with "-"
                df = df.replace({"": "-", None: "-", pd.NA: "-", "NaN": "-"})

                # Function to handle semicolon-separated values and convert to numeric
                def process_value(val):
                    if val == "-":
                        return val
                    elif isinstance(val, str) and ";" in val:
                        try:
                            return float(val.split(";")[0])  # Use first value before semicolon
                        except ValueError:
                            return None
                    try:
                        return float(val)
                    except ValueError:
                        return None

                # Apply the processing function to each gene column
                df[gene_columns] = df[gene_columns].apply(lambda col: col.apply(process_value))

                # Create summary column for gene presence
                summary_col = f"{params.prefix.capitalize()} Locus"  
                df[summary_col] = df[gene_columns].apply(lambda col: col.apply(lambda x: x if isinstance(x, (int, float)) and x >= 0 else None)).gt(0).sum(axis=1)

                # Format the summary column
                total_genes = len(gene_columns)
                df[summary_col] = df.apply(
                    lambda row: "-" if row[summary_col] == 0 else f"{int(row[summary_col])} / {total_genes}",
                    axis=1
                )

                # Select only the Sample and the summary column (Pcp or Tcp)
                df = df[["gene", summary_col]]

                # Save the processed table
                df.to_csv(output.tsv, sep="\t", index=False)



use rule join_names_with_vf as join_names_with_pcp_plasmid_genes with:
    input:
        rules.isolates_list.output.isolates_list,
        rules.summarize_pcp_plasmid_genes.output.sum,
        rules.summary_pcp.output.tsv
    output:
        summary = os.path.join(results_all_samples_dir, cperf_dir_name, "Pcp_plasmid_genes.tsv")
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"


"""
Tcp plasmid detection
"""
use rule run_blastn_cperf_toxins as run_blastn_cperf_tcp_plasmid with:
    input:
        contig=assembly_all + "{sample}.fasta",
        database=plasmid_dir + "tcp/sequences",
    output:
        blast_res = results_per_sample_dir + "{sample}/cperf/plasmids/{sample}_tcp_blastn_output.raw.txt",
    params:
        blastn_identity=str(config["cperf_plasmid"]["blast_identity_threshold"]),
        blastn_evalue="1E-20",
        blastn_culling_limit="1",
        blastn_max_targets="10000",
    threads: 2
    message:
        "Running BLASTN to detect Tcp plasmid genes in sample: {wildcards.sample}"
    conda:
        envs_folder + "blast.yaml"


use rule clean_blastn_cperf_toxins as clean_blastn_cperf_tcp_plasmid with:
    input:
        rules.run_blastn_cperf_tcp_plasmid.output.blast_res,
    output:
        blast_res = results_per_sample_dir + "{sample}/cperf/plasmids/{sample}_tcp_blastn_output.tsv",
    params:
        identity_threshold=str(config["cperf_plasmid"]["blast_identity_threshold"]),
        coverage_threshold=str(config["cperf_plasmid"]["blast_coverage_threshold"]),
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"


use rule summarize_virulence_genes as summarize_tcp_plasmid_genes with:
    input:
        expand(rules.clean_blastn_cperf_tcp_plasmid.output.blast_res, sample=SAMPLES), 
        expand(rules.clean_blastn_cperf_tcp_plasmid.output.blast_res, sample=GENOMES), 
    output:
        sum = temp(results_all_samples_dir + cperf_dir_name + "/temp_tcp1.tsv"),
    threads: 2
    params:
        spread_value= "coverage" #or identity
    conda:
        envs_folder + "data_parse.yaml"


use rule summary_pcp as summary_tcp with:
    input:
        summary=rules.summarize_tcp_plasmid_genes.output.sum
    output:
        tsv = temp(os.path.join(results_all_samples_dir, cperf_dir_name, "temp_tcp2.tsv")),
    params:
        prefix="tcp"


use rule join_names_with_vf as join_names_with_tcp_plasmid_genes with:
    input:
        rules.isolates_list.output.isolates_list,
        rules.summarize_tcp_plasmid_genes.output.sum,
        rules.summary_tcp.output.tsv,
    output:
        summary = os.path.join(results_all_samples_dir, cperf_dir_name,"Tcp_plasmid_genes.tsv"),
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"


"""
Insertion Sequences
"""
use rule run_blastn_cperf_toxins as run_blastn_cperf_IS with:
    input:
        contig=assembly_all + "{sample}.fasta",
        database=os.path.join(insertion_seq_dir, "insertion_seq"),
    output:
        blast_res = results_per_sample_dir + "{sample}/cperf/IS/{sample}_IS_blastn_output.raw.txt",
    params:
        blastn_identity=str(config["cperf_insertion_seq"]["blast_identity_threshold"]),
        blastn_evalue="1E-20",
        blastn_culling_limit="1",
        blastn_max_targets="10000",
    threads: 2
    conda:
        envs_folder + "blast.yaml"


use rule clean_blastn_cperf_toxins as clean_blastn_cperf_IS with:
    input:
        rules.run_blastn_cperf_IS.output.blast_res,
    output:
        blast_res = results_per_sample_dir + "{sample}/cperf/IS/{sample}_IS_blastn_output.tsv",
    params:
        identity_threshold=str(config["cperf_insertion_seq"]["blast_identity_threshold"]),
        coverage_threshold=str(config["cperf_insertion_seq"]["blast_coverage_threshold"]),
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"


use rule summarize_virulence_genes as summarize_insertion_seq with:
    input:
        expand(rules.clean_blastn_cperf_IS.output.blast_res, sample=SAMPLES), 
        expand(rules.clean_blastn_cperf_IS.output.blast_res, sample=GENOMES), 
    output:
        sum = temp(results_all_samples_dir + cperf_dir_name + "/temp_IS1.tsv"),
    threads: 2
    params:
        spread_value= "coverage" #or identity
    conda:
        envs_folder + "data_parse.yaml"


use rule join_names_with_vf as join_names_with_insertion_seq with:
    input:
        rules.isolates_list.output.isolates_list,
        rules.summarize_insertion_seq.output.sum,
    output:
        summary = os.path.join(results_all_samples_dir, cperf_dir_name,"Insertion_sequences.tsv"),
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"



"""
Iron systems
"""
use rule run_blastn_cperf_toxins as run_blastn_cperf_iron with:
    input:
        contig=assembly_all + "{sample}.fasta",
        database=os.path.join(virulence_dir, "iron/sequences"),
    output:
        blast_res = results_per_sample_dir + "{sample}/cperf/Iron_systems/{sample}_iron_blastn_output.raw.txt",
    params:
        blastn_identity=str(config["cperf_toxins"]["blast_identity_threshold"]),
        blastn_evalue="1E-20",
        blastn_culling_limit="1",
        blastn_max_targets="10000",
    threads: 2
    conda:
        envs_folder + "blast.yaml"


use rule clean_blastn_cperf_toxins as clean_blastn_cperf_iron with:
    input:
        rules.run_blastn_cperf_iron.output.blast_res,
    output:
        blast_res = results_per_sample_dir + "{sample}/cperf/Iron_systems/{sample}_iron_blastn_output.tsv",
    params:
        identity_threshold=str(config["cperf_toxins"]["blast_identity_threshold"]),
        coverage_threshold=str(config["cperf_toxins"]["blast_coverage_threshold"]),
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"


use rule summarize_virulence_genes as summarize_iron_systems with:
    input:
        expand(rules.clean_blastn_cperf_iron.output.blast_res, sample=SAMPLES), 
        expand(rules.clean_blastn_cperf_iron.output.blast_res, sample=GENOMES), 
    output:
        sum = temp(results_all_samples_dir + cperf_dir_name + "/temp_iron1.tsv"),
    threads: 2
    params:
        spread_value= "coverage" #or identity
    conda:
        envs_folder + "data_parse.yaml"


use rule join_names_with_vf as join_names_with_iron_systems with:
    input:
        rules.isolates_list.output.isolates_list,
        rules.summarize_iron_systems.output.sum,
    output:
        summary = os.path.join(results_all_samples_dir, cperf_dir_name,"Iron_systems.tsv"),
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"



"""
collect all c. perfringens tasks in a summary table
"""


rule collect_cperfringens_results:
    input:
        rules.isolates_list.output[0],
        rules.write_refseq_masher.output.filtered,
        toxin = rules.collect_toxintype.output.type if C_PERFRINGENS_TOXIN_TYPING == True else [],
        vir = rules.join_names_with_vf.output.summary if C_PERFRINGENS_VIRULENCE_GENES == True else [],
        pcp = rules.join_names_with_pcp_plasmid_genes.output.summary if C_PERFRINGENS_PLASMID_TYPING == True else [],
        tcp = rules.join_names_with_tcp_plasmid_genes.output.summary if C_PERFRINGENS_PLASMID_TYPING == True else [],
        is_seq = rules.join_names_with_insertion_seq.output.summary if C_PERFRINGENS_INSERTION_SEQUENCES == True else [],
        iron = rules.join_names_with_iron_systems.output.summary if C_PERFRINGENS_IRON_SYSTEMS == True else [],
    output:
        tsv_imp = temp(results_all_samples_dir + cperf_dir_name + "/summary_results_cperfringens_imp.tsv"),
        tsv_rest = temp(results_all_samples_dir + cperf_dir_name + "/summary_results_cperfringens_rest.tsv"),
        tsv = results_all_samples_dir + cperf_dir_name + "/summary_results_cperfringens.tsv",
        excel = results_all_samples_dir + cperf_dir_name + "/summary_results_cperfringens.xlsx",
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk join -t {input}  -k --na none | csvtk cut -t -m -f "Sample",taxonomic_species,distance,"Toxin-Type","Virulence_genes","Tcp Locus","Pcp Locus"  -o {output[0]}
        csvtk join -t {input}  -k | csvtk cut -t -m -f -"taxonomic_species",-"distance",-"Toxin-Type",-"Virulence_genes",-"Tcp Locus",-"Pcp Locus",-"rep" -o {output[1]}
        csvtk join -t {output[0]} {output[1]}  -k -o {output[2]}
        csvtk csv2xlsx -t {output[2]} -o {output[3]}
        """