"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


rule get_card_db:
    output:
        db = temp(directory(working_dir + "localDB")),
    threads: 4
    conda:
        envs_folder + "rgi.yaml"
    log:
        os.path.join(results_all_samples_dir, all_resistance_dir_name, "versions/card_database.log"),
    shell:
        r"""
        if [ -s "{config[card_database]}/card.json" ]; then
            echo "Card database provided: {config[card_database]}"
            ln -s {config[card_database]} {output.db}
        else
            echo "No card database provided, will install to local DB: {output.db}"
            rgi auto_load --local  --debug --clean &>> {log}
        fi
        """


rule rgi_version:
    input:
        db = rules.get_card_db.output.db,
    output:
        rgi = os.path.join(results_all_samples_dir, all_resistance_dir_name, "versions/rgi.txt"),
        card = os.path.join(results_all_samples_dir, all_resistance_dir_name, "versions/card_db.txt"),
    threads: 2
    conda:
        envs_folder + "rgi.yaml"
    shell:
        r"""
        rgi main -v  &>>  {output.rgi}
        rgi database --local -v  &>>  {output.card}
        """


rule run_card_rgi:
    input:
        contig = assembly_all + "{sample}.fasta",
        db = rules.get_card_db.output.db,
    output:
        txt = os.path.join(results_per_sample_dir, "{sample}/resistance/{sample}_card_rgi.txt"),
        json = os.path.join(results_per_sample_dir, "{sample}/resistance/{sample}_card_rgi.json"),
    threads: 8
    conda:
        envs_folder + "rgi.yaml"
    log:
        os.path.join(results_per_sample_dir, "{sample}/resistance/card.log"),
    params:
        prefix = results_per_sample_dir + "{sample}/resistance/{sample}_card_rgi",
        args = config["card_rgi"]["params"] if config["card_rgi"]["params"] else "",
    shell:
        r"""
        rgi main --debug -i {input.contig} \
            -n {threads} --local {params.args} \
            -o {params.prefix} 2>> {log}
        """


rule add_sample_name_card_rgi:
    input:
        res = rules.run_card_rgi.output.txt, 
    output:
        tsv = os.path.join(results_per_sample_dir, "{sample}/resistance/{sample}_card_rgi.tsv"),
    conda:
        envs_folder + "data_parse.yaml"
    params:
        headers = 'Sample,Contig,Start,Stop,Orientation,Cut_Off,Model_type,SNPs_in_Best_Hit_ARO,Other_SNPs,Antibiotic,"Drug Class","Resistance Mechanism","AMR Gene Family",Best_Identities,"Percentage Length of Reference Sequence",ID,Model_ID,Hit_Start,Hit_End,Pass_Bitscore,Best_Hit_Bitscore,Best_Hit_ARO,ARO,Nudged,Note'
    shell:
        r"""
        if [[ $(csvtk nrow -tH {input.res} ) -gt 1 ]]; then
            csvtk mutate2 --quiet --at 1 -t -e " '{wildcards.sample}' " -n Sample {input.res} | 
            csvtk cut -t -f {params.headers} | csvtk uniq -t -f 1- -o {output.tsv}
        else 
            echo {params.headers} | csvtk cut -f 1- -T > {output.tsv}
        fi
        """



rule detail_summary_card_rgi:
    input:
        samples = expand(rules.add_sample_name_card_rgi.output.tsv, sample=SAMPLES),
        genomes = expand(rules.add_sample_name_card_rgi.output.tsv, sample=GENOMES),
    output:
        FOF = temp(temporary_todelete + "card_rgi_paths.txt"),
        tsv = os.path.join(results_all_samples_dir, all_resistance_dir_name, "card_rgi_detailed.tsv"),
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        : > {output.FOF} && for path in {input}; do [[ -s $path ]] && echo "$path" >> {output.FOF}; done

        [[ $(wc -l < {output.FOF}) -gt 0 ]] && 
            csvtk concat -t --infile-list {output.FOF} --out-file {output.tsv} -i -E || \
            cp {input} {output.tsv}
        """



rule crad_rgi_summary_temp:
    input:
        amr_results = rules.detail_summary_card_rgi.output.tsv,
    output:
        temp = temp(temporary_todelete  + "card_rgi_summary.temp"),
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        [[ -s {input.amr_results} ]] && 
           csvtk cut --quiet -t -f "Sample","Best_Hit_ARO","Percentage Length of Reference Sequence" {input.amr_results} | 
           csvtk fold --quiet -t -f 1,2 -v 3 | 
           csvtk spread --quiet -t -k 2 -v 3  --na "-"  -o {output.temp} || true
        """


rule crad_rgi_summary:
    input:
        res = rules.crad_rgi_summary_temp.output.temp,
        names = rules.isolates_list.output[0]
    output:
        tsv = os.path.join(results_all_samples_dir, all_resistance_dir_name, "card_rgi_summary.tsv"),
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        if [[ $(csvtk nrow -tH {input.res} ) -gt 1 ]]; then
            csvtk join -t --na - -f 1 -k  {input.names} {input.res} -o {output.tsv}
        else 
            csvtk mutate2 -t -n "n. genes" -e "0"  {input.names} -o {output.tsv} 
        fi 
        """


rule crad_rgi_summary_excel:
    input:
        tsv = rules.crad_rgi_summary.output.tsv,
    output:
        excel = os.path.join(results_all_samples_dir, all_resistance_dir_name, "card_rgi_summary.xlsx"),
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        """
        csvtk csv2xlsx -f -t {input.tsv} -o {output.excel}
        """