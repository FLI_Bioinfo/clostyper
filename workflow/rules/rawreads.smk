"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

def has_pairedend_samples(samples_file):
    # Load the samplesheet
    samplesheet = pd.read_csv(samples_file, sep=',')

    # Check if there are any paired-end samples
    pairedend_samples = samplesheet[samplesheet['data_type'] == 'pairedend']

    # Return True if there are paired-end samples, otherwise False
    return not pairedend_samples.empty


def RAWREADS_QUALITY():

    # Check if the RAWREADS_QUALITY module is enabled
    rawreads_quality_enabled = config.get("RAWREADS_QUALITY", False)

    # Check if fastp, kraken2, or confindr is enabled
    fastp_enabled = config.get("use_fastp", False)
    kraken2_enabled = config.get("use_kraken2", False)
    confindr_enabled = config.get("use_confindr", False)

    # Check if paired-end samples exist in the sample sheet
    pairedend_samples_exist = has_pairedend_samples(config["samples"])

    if rawreads_quality_enabled and (fastp_enabled or kraken2_enabled or confindr_enabled) and pairedend_samples_exist:
        RAWREADS_QUALITY = [rules.collect_all_raw_reads_results.output]

        # Add Fastp if enabled
        if fastp_enabled:
            RAWREADS_QUALITY.append([
                expand(rules.run_fastp.output.fastp_html, sample=SAMPLES),
                expand(rules.convert_fastp_json_to_csv.output, sample=SAMPLES),
                rules.collect_fastp.output.tsv
            ])

        # Add Kraken2 if enabled and the database path exists
        kraken2_database = config.get("kraken2_database", "")
        kraken2_database_exists = kraken2_database and os.path.isdir(kraken2_database)

        if kraken2_enabled and kraken2_database_exists:
            RAWREADS_QUALITY.append([
                expand(rules.run_kraken.output, sample=SAMPLES),
                expand(rules.convert_kraken_report_to_csv.output, sample=SAMPLES),
                rules.collect_kraken_reads.output.tsv
            ])
        else:
            print(YELLOW +"Kraken2 skipped: run_kraken2 =", kraken2_enabled, ", Database exists =", kraken2_database_exists, NOFORMAT)

        # Add ConFindr if enabled and the database path exists
        confindr_database = config.get("confindr_database", "")
        confindr_database_exists = confindr_database and os.path.isdir(confindr_database)

        if confindr_enabled and confindr_database_exists:
            RAWREADS_QUALITY.append([
                expand(rules.run_confindr.output.confindr, sample=SAMPLES),
                rules.collect_confindr.output.tsv
            ])
        else:
            print(YELLOW + "ConFindr skipped: run_confindr =", confindr_enabled, ", Database exists =", confindr_database_exists, NOFORMAT)

        return RAWREADS_QUALITY
    else:
        # If no paired-end samples exist, print a message and return an empty list
        if not pairedend_samples_exist and config["RAWREADS_QUALITY"] == True:
            sys.exit( RED + "\n"
                "Error: Raw Read QC analysis was set to True but no paired-end samples were found in the sample sheet.\n"
                "       Please set the Raw Read QC analysis: 'RAWREADS_QUALITY' to 'False'"
                "\n" + NOFORMAT )
        return []



def RAWREADS_QUALITY_noprintout():
    with suppress_stdout():
        return RAWREADS_QUALITY()



"""
fastp
"""


rule run_fastp:
    input:
        r1 = lambda wildcards: get_illumina_r1(wildcards),
        r2 = lambda wildcards: get_illumina_r2(wildcards), 
        ref_fasta=rules.reference.output[0],
    output:
        fastp_html=results_per_sample_dir + "{sample}/" + fastp_dir_name + "/{sample}_fastp.html",
        fastp_json=results_per_sample_dir + "{sample}/" + fastp_dir_name + "/{sample}_fastp.json",
    threads: 3
    log:
        results_per_sample_dir + "{sample}/" + fastp_dir_name + "/{sample}_fastp.log",
    conda:
        envs_folder + "fastp.yaml"
    params:
        report_title="{sample}",
        params = config.get("fastp_params", ""),

    shell:
        r"""
        fastp {params.params} --verbose \
            -i {input.r1} \
            -I {input.r2} \
            --json {output.fastp_json} \
            --html {output.fastp_html} \
            --thread {threads} \
            --report_title {params.report_title} 2> {log}
        """


rule convert_fastp_json_to_csv:
    input:
        data=rules.run_fastp.output[1],
        ref_size=rules.reference.output[1],
    output:
        results_per_sample_dir + "{sample}/" + fastp_dir_name + "/{sample}_fastp.tsv",
    conda:
        envs_folder + "data_parse.yaml"
    threads: 2
    params:
        total_bases="summary.before_filtering.total_bases",
        total_reads="summary.before_filtering.total_reads",
        gc_content="summary.before_filtering.gc_content",
        q30_rate="summary.before_filtering.q30_rate",
        q20_rate="summary.before_filtering.q20_rate",
        read1_mean_length="summary.before_filtering.read1_mean_length",
        read2_mean_length="summary.before_filtering.read2_mean_length",
        duplication_rate="duplication.rate",
    shell:
        #logic: json to csv with miller -> add sample name col -> select cols of interest -> add ref size col and  aclculate depth (bases / ref_size) | edit GC% column & Q30 & Q20 & Duplication, each * 100 | rename the rest of column. and select the columns for the final table
        r"""
        REFERENCE_GENOME_SIZE=`cat {input.ref_size}`
        mlr --ijson --ocsv cat {input.data} | 
            csvtk mutate2 -e " '{wildcards.sample}' " -n Sample | 
            csvtk cut -f "Sample",{params.total_bases},{params.total_reads},{params.gc_content},{params.q30_rate},{params.q20_rate},{params.read1_mean_length},{params.read2_mean_length},{params.duplication_rate}  | 
            csvtk mutate2 -n Reference -e " $REFERENCE_GENOME_SIZE " | 
            csvtk mutate2 -n Depth -e ' $2 / $10 ' | 
            csvtk mutate2 -n "GC content" -e ' $4 * 100 ' | 
            csvtk mutate2 -n "%Q30" -e ' $5 * 100 ' | 
            csvtk mutate2 -n "%Q20" -e ' $6 * 100 ' | 
            csvtk mutate2 -n "%Duplication" -e ' $9 * 100 ' | 
            csvtk rename -f {params.total_bases},{params.total_reads},{params.read1_mean_length},{params.read2_mean_length} \
                -n "Total bases","Total reads","R1 mean length","R2 mean length" | 
            csvtk cut -f "Sample","Total bases","Total reads","GC content","%Q30","%Q20","R1 mean length","R2 mean length","%Duplication","Depth" -T > {output}
        """


rule collect_fastp:
    input:
        expand(rules.convert_fastp_json_to_csv.output, sample=SAMPLES),
    output:
        FOF=temp(results_all_samples_dir + all_raw_reads_dir_name + "/samples_path_fastp.tsv"),
        tsv=results_all_samples_dir + all_raw_reads_dir_name + "/fastp_results.tsv",
        xlsx=results_all_samples_dir + all_raw_reads_dir_name + "/fastp_results.xlsx",
    conda:
        envs_folder + "data_parse.yaml"
    threads: 2
    params:
        sep="-t ",
        outsep="-T ",
    shell:
        r"""
        echo -ne "" > {output.FOF} &&
        for path in {input}; do
            if [[ -s $path ]]; then
                echo -ne "$path\\n" >> {output.FOF};
            fi
        done
        csvtk concat {params.sep} --infile-list {output.FOF} {params.outsep} -i -E  -I --out-file {output.tsv}
        csvtk csv2xlsx {params.sep} {output.tsv} -o {output.xlsx}
        """


"""
kraken
"""


rule run_kraken:
    input:
        r1 = lambda wildcards: get_illumina_r1(wildcards),
        r2 = lambda wildcards: get_illumina_r2(wildcards), 
        db=config["kraken2_database"] if config["kraken2_database"] else [], #TODO
    output:
        results_per_sample_dir + "{sample}/" + kraken_dir_name + "/{sample}_kraken_reads.tsv",
    log:
        results_per_sample_dir + "{sample}/" + kraken_dir_name + "/{sample}_kraken_reads.log",
    threads: 2 #workflow.cores * 0.1
 #   resources:
 #       mem_mb=lambda wildcards, threads: 100 * threads #https://stackoverflow.com/questions/48542333/snakemake-memory-limiting
    conda:
        envs_folder + "kraken2.yaml"
    params:
        params=config.get("kraken2_params", ""),
        memorymapping = (
            "--memory-mapping " if config.get("kraken2_memory-mapping") is True else ""
        ),
    shell:
        r"""
        kraken2 {params.params} --threads {threads} \
            --db {input.db} \
            --gzip-compressed \
            --output - \
            --report {output} \
            {params.memorymapping} {input.r1} {input.r2} 2> {log}
        """


rule convert_kraken_report_to_csv:
    input:
        results_per_sample_dir + "{sample}/" + kraken_dir_name + "/{sample}_kraken_reads.tsv",
    output:
        results_per_sample_dir + "{sample}/" + kraken_dir_name + "/{sample}_kraken_reads_table.tsv",
    conda:
        envs_folder + "data_parse.yaml"
    threads: 2
    shell:
        r"""
        header=$(echo -ne "Kraken best match\\t\\n%Kraken best match\\t\\n2nd Kraken best match\\t\\n%2nd Kraken best match\\t\\n3rd Kraken best match\\t\\n%3rd Kraken best match\\t\\n")
        results=$(cat {input} |
            csvtk filter2 -tH  -f '$4=="S"' |
            csvtk replace -tH  -f 1 -p " " -r ""  |
            csvtk replace -tH  -f 6 -p "^ +"  -r "" | 
            csvtk sort -k 1:nr -tH |
            csvtk mutate2 -n tax  -e ' $6 + ";" + $1 ' -tH  |
            csvtk cut -f 7 -t  |
            head -n  3 | csvtk unfold -f 1 -H -s ";" )
        paste <(echo "$header") <(echo "$results") --delimiters '' | 
            csvtk add-header -n Sample -n {wildcards.sample} -t | 
            csvtk transpose -t  >  {output}
        """


use rule collect_fastp as collect_kraken_reads with:
    input:
        expand(rules.convert_kraken_report_to_csv.output[0], sample=SAMPLES),
    output:
        FOF=temp(results_all_samples_dir + all_raw_reads_dir_name + "/samples_path_kraken_reads.tsv"),
        tsv=results_all_samples_dir + all_raw_reads_dir_name + "/kraken_reads_results.tsv",
        xlsx=results_all_samples_dir + all_raw_reads_dir_name + "/kraken_reads_results.xlsx",
    params:
        sep="-t ",
        outsep="-T ",


"""
confindr
"""

def confindr_approach():
    # Initialize confindr_approach variable
    confindr_approach = ""

    # Handling use_rmlst
    if config["confindr_params"]["confindr_approach"] == "rmlst":
        confindr_approach = "--rmlst "

    # Handling use_cgmlst
    elif config["confindr_params"]["confindr_approach"] == "cgmlst":
        cgmlst_path = config["confindr_params"]["cgmlst_database_path"]
        if cgmlst_path.startswith("/"):
            confindr_approach = "--cgmlst " + cgmlst_path
        else:
            sys.exit("Error: Since confindr_approach = cgmlst, the full path to 'cgmlst_database_path' must be provided.")

    else:
        sys.exit("Error: 'confindr_approach' must be either 'rmlst' or 'cgmlst'.")

    return confindr_approach


rule run_confindr:  #donot break the piepline if this fails
    input:
        r1 = lambda wildcards: get_illumina_r1(wildcards),
        r2 = lambda wildcards: get_illumina_r2(wildcards), 
        databases_folder = (config.get("confindr_database") or []),
    output:
        confindr=results_per_sample_dir + "{sample}/" + confinder_dir_name + "/confindr_report.csv",
        confindr_dir=directory(results_per_sample_dir + "{sample}/" + confinder_dir_name),
        confindr_temp_dir=temp(directory(results_per_sample_dir + "{sample}/temp_dir")),
    params:
        confindr_approach=confindr_approach(),
        forward_id="_1.fastq.gz",
        reverse_id="_2.fastq.gz",
        extraparams=config["confindr_params"]["extraparams"] if config["confindr_params"]["extraparams"] else "",
    conda:
        envs_folder + "confindr.yaml"
    message:
        "Running confindr with sample: {wildcards.sample}"
    log:
        results_per_sample_dir + "{sample}/" + confinder_dir_name + "/{sample}_confindr.log",
    threads: workflow.cores * 0.5 if workflow.cores > 8 else 4 # 16
    priority: 10
    resources:
        mem_mb=4000,
        time=4
    shell:
        r"""
        mkdir -p {output.confindr_temp_dir}
        ln -sf {input.r1} {output.confindr_temp_dir}/{wildcards.sample}_1.fastq.gz
        ln -sf {input.r2} {output.confindr_temp_dir}/{wildcards.sample}_2.fastq.gz

        confindr -t {threads} {params.confindr_approach} {params.extraparams} \
            -i {output.confindr_temp_dir} \
            -d {input.databases_folder} \
            -o {output.confindr_dir} \
            --forward_id "{params.forward_id}" \
            --reverse_id "{params.reverse_id}" 2> {log}
        """


use rule collect_fastp as collect_confindr with:
    input:
        expand(rules.run_confindr.output.confindr, sample=SAMPLES),
    output:
        FOF=temp(results_all_samples_dir + all_raw_reads_dir_name + "/samples_path_confindr.tsv"),
        tsv=results_all_samples_dir + all_raw_reads_dir_name + "/confindr_results.tsv",
        xlsx=results_all_samples_dir + all_raw_reads_dir_name + "/confindr_results.xlsx",
    params:
        sep="",
        outsep="-T ",


rule collect_all_raw_reads_results:
    input:
        rules.isolates_list.output[0],
        *[
            rules.collect_fastp.output.tsv
            for run_fastp in [config["use_fastp"]]
            if run_fastp and rules.collect_fastp.output.tsv is not None
        ],
        *[
            rules.collect_kraken_reads.output.tsv
            for run_kraken in [config["use_kraken2"]]
            if run_kraken and config["kraken2_database"] is not None 
            and os.path.exists(config["kraken2_database"]) 
            and rules.collect_kraken_reads.output.tsv is not None
        ],
        *[
            rules.collect_confindr.output.tsv
            for run_confindr in [config["use_confindr"]]
            if run_confindr and config["confindr_database"] is not None 
            and os.path.exists(config["confindr_database"]) 
            and rules.collect_confindr.output.tsv is not None
        ],
    output:
        tsv=results_all_samples_dir + all_raw_reads_dir_name + "/summary_results.tsv",
        xlsx=results_all_samples_dir + all_raw_reads_dir_name + "/summary_results.xlsx",
    conda:
        envs_folder + "data_parse.yaml"
    threads: 2
    shell:
        r"""
        csvtk join  -t  -k -f 1 --na "-" {input} -o {output[0]}
        csvtk csv2xlsx -t {output[0]} -o {output[1]}
        """

# rule write_report_script_raw_reads_quality:
#     input:
#         input=RAWREADS_QUALITY_noprintout(),
#         summary=rules.collect_all_raw_reads_results.output[0],
#     output:
#         workflow = temp(os.path.join(results_all_samples_dir, all_raw_reads_dir_name, "raw_reads_workflow.Rmd")),
#         report = temp(os.path.join(reports_dir, "raw_reads_quality_script.Rmd")),
#     threads: 2
#     params:
#         results_dir=results_all_samples_dir,
#         fastp=config["use_fastp"],
#         confindr=config["use_confindr"],
#         kraken=config["use_kraken2"],
#     script:
#         bin_dir + "write_report_script_raw_reads.sh"

