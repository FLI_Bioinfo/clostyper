"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# TODO: incase of ANI <80%, no results for fastANI

def GENOME_ASSESSMENT():
    # Check if the genome assessment is enabled in the config
    genome_assessment_enabled = config.get("GENOME_ASSESSMENT", False)

    if genome_assessment_enabled:
        # Initialize the QC list with initial outputs
        QC = [rules.fasta_stats.output[0], rules.collect_all_genome_assessment_results.output[0]]

        # Check for Kraken2 assemblies
        run_kraken2_assemblies = config.get("kraken2_assemblies", {}).get("run_kraken2_assemblies", False)
        kraken2_database = config.get("kraken2_database", "")
        
        # Check if the Kraken2 database path is a valid directory
        kraken2_database_exists = kraken2_database and os.path.isdir(kraken2_database)

        if run_kraken2_assemblies and kraken2_database_exists:
            QC.append([
                expand(rules.kraken_assemblies.output, sample=SAMPLES), 
                expand(rules.kraken_assemblies.output, sample=GENOMES), 
                expand(rules.convert_kraken_report_contigs_to_csv.output, sample=SAMPLES), 
                rules.collect_kraken_contigs.output[1]
            ])
        else:
            print("Kraken2 assemblies not added: run_kraken2_assemblies =", run_kraken2_assemblies, 
                  ", Database exists =", kraken2_database_exists)

        # Check for CheckM
        checkM_enabled = config.get("checkM", {}).get("run_checkM", False)
        checkM_database = config.get("checkM_database", "")
        
        # Check if the CheckM database path is a valid directory
        checkM_database_exists = checkM_database and os.path.isdir(checkM_database)

        if checkM_enabled and checkM_database_exists:
            QC.append([rules.run_checkM.output[1]])

        # Check for Reference Seeker
        reference_seeker_enabled = config.get("referenceseeker", {}).get("run_referenceseeker", False)
        reference_seeker_database = config.get("referenceseeker_database", "")
        
        # Check if the Reference Seeker database path is a valid directory
        reference_seeker_database_exists = reference_seeker_database and os.path.isdir(reference_seeker_database)

        if reference_seeker_enabled and reference_seeker_database_exists:
            QC.append([
                expand(rules.run_referenceseeker.output, sample=SAMPLES), 
                expand(rules.run_referenceseeker.output, sample=GENOMES), 
                rules.collect_referenceseeker.output[1]
            ])

        # Check for FastANI
        fastANI_enabled = config.get("fastANI", {}).get("run_fastANI", False)

        if fastANI_enabled:
            QC.append([
                rules.run_fastANI.output[1], 
                rules.write_fastani_nicer.output[0]
            ])

        return QC
    else:
        return []


# def GENOME_ASSESSMENT_noprintout():
#     with suppress_stdout():
#         return GENOME_ASSESSMENT()


"""
kraken2 assemblies
"""


rule kraken_assemblies:
    input:
        assemblies=assembly_all + "{sample}.fasta",
        db=config["kraken2_database"] if config["kraken2_database"] else [], #TODO:   
    output:
        results_per_sample_dir + "{sample}/" + kraken_dir_name + "/{sample}_kraken_contigs.tsv",
        results_per_sample_dir + "{sample}/" + kraken_dir_name + "/{sample}_kraken_contigs_raw.txt",
    log:
        results_per_sample_dir + "{sample}/" + kraken_dir_name + "/{sample}_kraken_contigs.log",
    threads: 2 #workflow.cores * 0.1
    conda:
        envs_folder + "kraken2.yaml"
    singularity: "docker://quay.io/biocontainers/kraken2:2.1.2--pl5262h7d875b9_0"
    params:
        extraparams=config["kraken2_assemblies"]["params"]["kraken2_extra_params"] if config["kraken2_assemblies"]["params"]["kraken2_extra_params"] else "",
        memorymapping= "--memory-mapping " if config["kraken2_assemblies"]["kraken2_memory-mapping"] == True else "",
    shell:
        r"""
        kraken2 {params.extraparams} --threads {threads} \
            --db {input.db} \
            {input.assemblies} \
            --output {output[1]} \
            --report {output[0]} \
            {params.memorymapping}  2> {log}
        """


use rule convert_kraken_report_to_csv as convert_kraken_report_contigs_to_csv with:
    input:
        results_per_sample_dir + "{sample}/" + kraken_dir_name + "/{sample}_kraken_contigs.tsv",
    output:
        results_per_sample_dir + "{sample}/" + kraken_dir_name + "/{sample}_kraken_contigs_table.tsv",


use rule collect_fastp as collect_kraken_contigs with:
    input:
        expand(rules.convert_kraken_report_contigs_to_csv.output[0], sample=SAMPLES),
        expand(rules.convert_kraken_report_contigs_to_csv.output[0], sample=GENOMES),
    output:
        FOF=temp(temporary_todelete + "samples_path_kraken_contigs.tsv"),
        tsv=results_all_samples_dir + all_genomes_dir_name + "/kraken_genomes_results.tsv",
        xlsx=results_all_samples_dir + all_genomes_dir_name + "/kraken_genomes_results.xlsx",
    params:
        sep="-t ",
        outsep="-T ",


"""
RefSeq masher
"""


rule run_refseq_masher:
    input:
        fasta=expand(assembly_all + "{sample}.fasta", sample=SAMPLES),
        fasta_assembly=expand(assembly_all + "{sample}.fasta", sample=GENOMES),
    output:
        version=results_all_samples_dir + all_genomes_dir_name + "/versions/refseq_masher_version.txt",
        res=results_all_samples_dir + all_genomes_dir_name + "/refseq_masher.tsv",
        temp_dir = temp(directory(temporary_todelete + "refseq_masher"))
    threads: workflow.cores * 0.25 if workflow.cores > 8 else 4
    log:
        results_all_samples_dir + all_genomes_dir_name + "/versions/refseq_masher.log",
    conda:
        envs_folder + "refseq_masher.yaml"
    shell:
        r"""
        refseq_masher --version  >  {output.version}
        mkdir -p {output.temp_dir}
        refseq_masher matches --tmp-dir {output.temp_dir} --output-type tab --top-n-results 1 {input} --output {output.res}  2>> {log}
        """


rule write_refseq_masher:
    input:
        res=results_all_samples_dir + all_genomes_dir_name + "/refseq_masher.tsv",
    output:
        filtered = temp(os.path.join(temporary_todelete, "refseq_masher_filtered.tsv")),
    conda:
        envs_folder + "data_parse.yaml"
    threads: 2
    shell:
        r"""
        cat {input.res} | 
        csvtk cut -t -f sample,taxonomic_species,distance  > {output.filtered}
        """



"""
fastANI
"""


rule fastANI_input:
    input:
        fasta=expand(assembly_all + "{sample}.fasta", sample=SAMPLES),
        fasta_assembly=expand(assembly_all + "{sample}.fasta", sample=GENOMES),
    output:
        FOF=temp(temporary_todelete + "fastani_query_samples.txt"),
    shell:
        r"""
        echo -ne "" > {output.FOF} &&
        for path in {input.fasta} {input.fasta_assembly}; do
            echo -ne "$path\\n" >> {output.FOF};
        done
        """


rule run_fastANI:
    input:
        fof=rules.fastANI_input.output.FOF,
        ref=rules.reference.output[0],
    output:
        version=results_all_samples_dir + all_genomes_dir_name + "/versions/fastani_version.txt",
        raw=results_all_samples_dir + all_genomes_dir_name + "/fastani_results_raw.tsv",
    threads: workflow.cores * 0.5
    log:
        results_all_samples_dir + all_genomes_dir_name + "/versions/fastani.log",
    conda:
        envs_folder + "fastani.yaml"
    params:
        extraparams=config["fastANI"]["params"]["extra_params"] if config["fastANI"]["params"]["extra_params"] else "",
    shell:
        r"""
        fastANI -v 2>&1 | sed -E 's/version ([0-9]+\.[0-9]+).*/\1/'  >  {output.version}
        fastANI --ref  {input.ref} --queryList {input.fof} {params.extraparams} -t {threads} -o {output.raw} 2>> {log}
        """


rule write_fastani_nicer:
    input:
        fastani_res=results_all_samples_dir + all_genomes_dir_name + "/fastani_results_raw.tsv",
        ref=rules.run_fastANI.input.ref,
    output:
        tsv=results_all_samples_dir + all_genomes_dir_name + "/fastani_results.tsv",
        excel=results_all_samples_dir + all_genomes_dir_name + "/fastani_results.xlsx",
    conda:
        envs_folder + "data_parse.yaml"
    threads: 2
    shell:
        r"""
        cat {input.fastani_res} | 
            csvtk replace -p "{assembly_all}" -r "" -tH -f 1 | 
            csvtk replace -p ".fasta" -r "" -tH -f 1 | 
            csvtk cut -t -f 1,3 | 
            csvtk add-header -t -n Sample -n "ANI (%) to the user-provided reference"  > {output.tsv}
        csvtk csv2xlsx --tabs {output.tsv} -o {output.excel}
        """


"""
checkM
"""


rule run_checkM:
    input:
        fasta=expand(assembly_all + "{sample}.fasta", sample=SAMPLES),
        fasta_assembly=expand(assembly_all + "{sample}.fasta", sample=GENOMES),
    output:
        tsv=results_all_samples_dir + all_genomes_dir_name + "/checkm_genome_results.tsv",
        excel=results_all_samples_dir + all_genomes_dir_name + "/checkm_genome_results.xlsx",
        temp_dir=temp(directory(temporary_todelete + "temp_checkm_dir")),
        version= results_all_samples_dir + all_genomes_dir_name + "/versions/checkm_version.txt",
    threads: workflow.cores * 0.5 if workflow.cores > 16 else 8
    log:
        results_all_samples_dir + all_genomes_dir_name + "/versions/checkm.log",
    conda:
        envs_folder + "checkm.yaml"
    params:
        database=config["checkM_database"],
        fasta_suffix="-x .fasta ",
        extraparams=config["checkM"]["params"]["extra_params"] if config["checkM"]["params"]["extra_params"] else "",
    shell:
        r"""
        checkm -h | grep "CheckM v" | sed -E 's/.*CheckM v([0-9.]+).*/\1/' > {output.version}
        
        checkm data setRoot {params.database} &> {log}
        checkm lineage_wf -t {threads} {params.fasta_suffix} {assembly_all} {output.temp_dir} --tab_table --file {output.tsv} &>> {log}
        csvtk csv2xlsx --tabs {output.tsv} -o {output.excel}
        """


rule summarize_checkM:
    input:
        tsv=results_all_samples_dir + all_genomes_dir_name + "/checkm_genome_results.tsv",
    output:
        temp(temporary_todelete + "checkm_genome_results_summary.tsv"),
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk cut -t -f "Bin Id","Marker lineage","Completeness","Contamination","Strain heterogeneity" {input.tsv} -o {output}
        """


"""
referenceseeker
"""


rule run_referenceseeker:
    input:
        genome=assembly_all + "{sample}.fasta",
        database=config["referenceseeker_database"] if config["referenceseeker_database"] else [],
    output:
        raw=results_per_sample_dir + "{sample}/" + referenceseeker_dir_name + "/{sample}_referenceseeker.tsv",
        best_hit=results_per_sample_dir + "{sample}/" + referenceseeker_dir_name + "/{sample}_referenceseeker_best_hit.tsv",
    threads: 4 #workflow.cores * 0.1
    conda:
        envs_folder + "referenceseeker.yaml"
    params:
        max_candidate_reference=" --crg " + str(config["referenceseeker"]["params"]["max_candidate_reference"]) if config["referenceseeker"]["params"]["max_candidate_reference"] else "",
        extraparams=config["referenceseeker"]["params"]["extra_params"] if config["referenceseeker"]["params"]["extra_params"] else "",
    shell:
        r"""
        referenceseeker --threads {threads}  {input.database}\
            {input.genome} {params.max_candidate_reference}  {params.extraparams} > {output.raw}
        
        head -n 2 {output.raw} | 
            csvtk rename -t -C "$" -f "#ID" -n "Closest Reference" | 
            csvtk mutate2 -t -e " '{wildcards.sample}' " -n Sample | 
            csvtk cut -t -f "Sample","Closest Reference","Mash Distance","ANI","Con. DNA","Taxonomy ID","Assembly Status","Organism" -o {output.best_hit}
        """


use rule collect_fastp as collect_referenceseeker with:
    input:
        expand(rules.run_referenceseeker.output.best_hit, sample=SAMPLES),
        expand(rules.run_referenceseeker.output.best_hit, sample=GENOMES),
    output:
        FOF=temp(temporary_todelete + "samples_path_referenceseeker.tsv"),
        tsv=results_all_samples_dir + all_genomes_dir_name + "/referenceseeker_results.tsv",
        xlsx=results_all_samples_dir + all_genomes_dir_name + "/referenceseeker_results.xlsx",
    params:
        sep="-t ",
        outsep="-T ",


use rule collect_all_raw_reads_results as collect_all_genome_assessment_results with:
    input:
        rules.isolates_list.output[0],
        rules.fasta_stats.output[0],
        *[
            rules.collect_kraken_contigs.output[1]
            for run_kraken_assemblies in [config["kraken2_assemblies"]["run_kraken2_assemblies"]]
            if run_kraken_assemblies and config["kraken2_database"] is not None 
            and os.path.exists(config["kraken2_database"]) 
            and rules.collect_kraken_contigs.output[1] is not None
        ],
        *[
            rules.write_fastani_nicer.output[0]
            for run_fastANI in [config["fastANI"]["run_fastANI"]]
            if run_fastANI and rules.write_fastani_nicer.output[0] is not None
        ],
        *[
            rules.summary_gbk.output[0] if config["ANNOTATION"] == True else  []
        ],
        *[
            rules.summarize_checkM.output[0]
            for run_checkM in [config["checkM"]["run_checkM"]]
            if run_checkM and config["checkM_database"] is not None 
            and os.path.exists(config["checkM_database"]) 
            and rules.summarize_checkM.output[0] is not None
        ],
        *[
            rules.collect_referenceseeker.output[1]
            for run_referenceseeker in [config["referenceseeker"]["run_referenceseeker"]]
            if run_referenceseeker and config["referenceseeker_database"] is not None 
            and os.path.exists(config["referenceseeker_database"]) 
            and rules.collect_referenceseeker.output[1] is not None
        ],
    output:
        tsv=results_all_samples_dir + all_genomes_dir_name + "/summary_results.tsv",
        xlsx=results_all_samples_dir + all_genomes_dir_name + "/summary_results.xlsx",



# rule write_report_script_genome_quality:
#     input:
#         input=GENOME_ASSESSMENT_noprintout(),
#         summary=rules.collect_all_genome_assessment_results.output[0],
#     output:
#         workflow = temp(results_all_samples_dir + all_genomes_dir_name + "/genome_quality_workflow.Rmd"),
#         report = temp(reports_dir + "genome_quality_script.Rmd"),
#     threads: 2
#     params:
#         results_dir=results_all_samples_dir,
#         reports_dir=reports_dir,
#         Assembly_software=config["Assembly_software"],
#         Annotation=config["ANNOTATION"],
#         Annotation_software=config["Annotation_software"],
#         kraken2_assemblies=config["kraken2_assemblies"]["run_kraken2_assemblies"],
#         checkM=config["checkM"]["run_checkM"],
#         referenceseeker=config["referenceseeker"]["run_referenceseeker"],
#         fastANI=config["fastANI"]["run_fastANI"],
#     script:
#         bin_dir + "write_report_script_assembly.sh"
