"""
Copyright (c) 2025, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

c_chauvoei_module_enabled = config.get("C_CHAUVOEI_MODULE", False)
c_chauvoei_virulence_genes = config.get("c_chauvoei_virulence_genes", False)
c_chauvoei_plasmid = config.get("c_chauvoei_plasmid", False)
c_chauvoei_insertion_sequences = config.get("c_chauvoei_insertion_sequences", False)

def C_CHAUVOEI_MODULE():
    if c_chauvoei_module_enabled:
        chau_module_results = [rules.collect_cchauvoei_results.output.tsv]
        
        if c_chauvoei_virulence_genes:
            chau_module_results.append(rules.summarize_cchau_vir2.output.summary)

        if c_chauvoei_plasmid:
            chau_module_results.append(rules.summarize_cchau_plas2.output.summary)

        if c_chauvoei_insertion_sequences:
            chau_module_results.append(rules.summarize_insertion_seq_chau2.output.summary)

        return chau_module_results
    return []



clostyper_database = config["clostyper_database"]
clostyper_database = add_trailing_slash(clostyper_database)
C_chauvoei_DB = os.path.join(clostyper_database or "", "chauvoei")
chau_vir_dir = os.path.join(C_chauvoei_DB, "virulence/")
chau_plas_dir = os.path.join(C_chauvoei_DB, "plasmid/")


cchau_dir_name = "C_chauvoei_results"


"""
virulence
"""

use rule run_blastn_cperf_toxins as run_blastn_chau_vf with:
    input:
        contig=assembly_all + "{sample}.fasta",
        database=os.path.join(chau_vir_dir, "vf.fasta"),
    output:
        blast_res = results_per_sample_dir + "{sample}/cchau/virulence/{sample}_vf_blastn_output.raw.txt",
    params:
        blastn_identity = str(config.get("c_chauvoei_virulence", {}).get("blast_identity_threshold", "90")),
        blastn_evalue="1E-20",
        blastn_culling_limit="1",
        blastn_max_targets="10000",
    threads: 2
    conda:
        envs_folder + "blast.yaml"


use rule clean_blastn_cperf_toxins as clean_blastn_chau_vf with:
    input:
        rules.run_blastn_chau_vf.output.blast_res,
    output:
        blast_res = results_per_sample_dir + "{sample}/cchau/virulence/{sample}_vf_blastn_output.tsv",
    params:
        identity_threshold = str(config.get("c_chauvoei_virulence", {}).get("blast_identity_threshold", "90")),
        coverage_threshold = str(config.get("c_chauvoei_virulence", {}).get("blast_coverage_threshold", "50"))
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"


use rule summarize_virulence_genes as summarize_cchau_vir with:
    input:
        expand(rules.clean_blastn_chau_vf.output.blast_res, sample=SAMPLES), 
        expand(rules.clean_blastn_chau_vf.output.blast_res, sample=GENOMES), 
    output:
        sum = temp(results_all_samples_dir + cchau_dir_name + "/temp_vf1.tsv"),
    threads: 2
    params:
        spread_value= "coverage" #or identity
    conda:
        envs_folder + "data_parse.yaml"


use rule join_names_with_vf as summarize_cchau_vir2 with:
    input:
        rules.isolates_list.output.isolates_list,
        rules.summarize_cchau_vir.output.sum,
    output:
        summary = os.path.join(results_all_samples_dir, cchau_dir_name,"Virulence_genes.tsv"),
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"

"""
Plasmids
"""

use rule run_blastn_cperf_toxins as run_blastn_chau_plas with:
    input:
        contig=assembly_all + "{sample}.fasta",
        database=os.path.join(chau_plas_dir, "Plasmid.fasta"),
    output:
        blast_res = results_per_sample_dir + "{sample}/cchau/plasmids/{sample}_plasmid_blastn_output.raw.txt",
    params:
        blastn_identity = str(config.get("c_chauvoei_plasmid", {}).get("blast_identity_threshold", "90")),
        blastn_evalue="1E-20",
        blastn_culling_limit="1",
        blastn_max_targets="10000",
    threads: 2
    conda:
        envs_folder + "blast.yaml"


use rule clean_blastn_cperf_toxins as clean_blastn_chau_plas with:
    input:
        rules.run_blastn_chau_plas.output.blast_res,
    output:
        blast_res = results_per_sample_dir + "{sample}/cchau/plasmid/{sample}_plas_blastn_output.tsv",
    params:
        identity_threshold = str(config.get("c_chauvoei_plasmid", {}).get("blast_identity_threshold", "90")),
        coverage_threshold = str(config.get("c_chauvoei_plasmid", {}).get("blast_coverage_threshold", "50"))
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"


use rule summarize_virulence_genes as summarize_cchau_plas with:
    input:
        expand(rules.clean_blastn_chau_plas.output.blast_res, sample=SAMPLES), 
        expand(rules.clean_blastn_chau_plas.output.blast_res, sample=GENOMES), 
    output:
        sum = temp(results_all_samples_dir + cchau_dir_name + "/temp_plas1.tsv"),
    threads: 2
    params:
        spread_value= "coverage" #or identity
    conda:
        envs_folder + "data_parse.yaml"


use rule join_names_with_vf as summarize_cchau_plas2 with:
    input:
        rules.isolates_list.output.isolates_list,
        rules.summarize_cchau_plas.output.sum,
    output:
        summary = os.path.join(results_all_samples_dir, cchau_dir_name,"Plasmid.tsv"),
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"




"""
Insertion Sequences
"""
use rule run_blastn_cperf_toxins as run_blastn_cchau_IS with:
    input:
        contig=assembly_all + "{sample}.fasta",
        database=os.path.join(insertion_seq_dir, "insertion_seq"), #as perf
    output:
        blast_res = results_per_sample_dir + "{sample}/cchau/IS/{sample}_IS_blastn_output.raw.txt",
    params:
        blastn_identity = str(config.get("c_chauvoei_insertion_seq", {}).get("blast_identity_threshold", "90")),
        blastn_evalue="1E-20",
        blastn_culling_limit="1",
        blastn_max_targets="10000",
    threads: 2
    conda:
        envs_folder + "blast.yaml"


use rule clean_blastn_cperf_toxins as clean_blastn_cchau_IS with:
    input:
        rules.run_blastn_cchau_IS.output.blast_res,
    output:
        blast_res = results_per_sample_dir + "{sample}/cchau/IS/{sample}_IS_blastn_output.tsv",
    params:
        identity_threshold = str(config.get("c_chauvoei_insertion_seq", {}).get("blast_identity_threshold", "90")),
        coverage_threshold = str(config.get("c_chauvoei_insertion_seq", {}).get("blast_coverage_threshold", "50"))
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"


use rule summarize_virulence_genes as summarize_insertion_seq_chau with:
    input:
        expand(rules.clean_blastn_cchau_IS.output.blast_res, sample=SAMPLES), 
        expand(rules.clean_blastn_cchau_IS.output.blast_res, sample=GENOMES), 
    output:
        sum = temp(results_all_samples_dir + cchau_dir_name + "/temp_IS1.tsv"),
    threads: 2
    params:
        spread_value= "coverage" #or identity
    conda:
        envs_folder + "data_parse.yaml"


use rule join_names_with_vf as summarize_insertion_seq_chau2 with:
    input:
        rules.isolates_list.output.isolates_list,
        rules.summarize_insertion_seq_chau.output.sum,
    output:
        summary = os.path.join(results_all_samples_dir, cchau_dir_name,"Insertion_sequences.tsv"),
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"



"""
collect all c. chauvoei tasks in a summary table
"""


rule collect_cchauvoei_results:
    input:
        rules.isolates_list.output[0],
        rules.write_refseq_masher.output.filtered,
        plas = rules.summarize_cchau_plas2.output.summary if c_chauvoei_plasmid else [],
        vir = rules.summarize_cchau_vir2.output.summary if c_chauvoei_virulence_genes else [],
        is_seq = rules.summarize_insertion_seq_chau2.output.summary if c_chauvoei_insertion_sequences else [],
    output:
        tsv = results_all_samples_dir + cchau_dir_name + "/summary_results_cchauvoei.tsv",
        excel = results_all_samples_dir + cchau_dir_name + "/summary_results_cchauvoei.xlsx",
    threads: 2
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        csvtk join -t {input} -k -o {output.tsv}
        csvtk csv2xlsx -t -f {output.tsv} -o {output.excel}
        """