"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


def REFERENCE():
    return rules.reference.output.ref_fasta


"""
reference
"""


rule reference:
    output:
        ref_fasta=results_all_samples_dir + reference_dir_name + "/ref.fa",
        ref_size=results_all_samples_dir + reference_dir_name + "/ref_size.txt",
        ref_name=results_all_samples_dir + reference_dir_name + "/ref_name.txt",
    conda:
        envs_folder + "reference.yaml"
    params:
        ref_dir=directory(results_all_samples_dir + reference_dir_name + "/"),
    shell:
        """
        mkdir -p {params.ref_dir}
        any2fasta -q -u {reference} > {output.ref_fasta}
        samtools faidx {output.ref_fasta}
        grep -v '^>' {output.ref_fasta} | tr -d '\n'  | wc  -c  > {output.ref_size}
        grep '>' {output.ref_fasta} | sed 's/>//g' > {output.ref_name}
        """


rule detect_reptitive_regions_in_ref:
    input:
        rules.reference.output.ref_fasta,
    output:
        results_all_samples_dir + reference_dir_name + "/ref_repeats.tsv",
        results_all_samples_dir + reference_dir_name + "/ref.delta.coords.tsv",
    conda:
        envs_folder + "reference.yaml"
    log:
        results_all_samples_dir + reference_dir_name + "/nucmer.log",
    params:
        ref_dir=directory(results_all_samples_dir + reference_dir_name + "/"),
        minimum_alignment_length = (
            "-L " + str(config.get("nucmer", {}).get("params", {}).get("minimum_alignment_length"))
            if config.get("nucmer", {}).get("params", {}).get("minimum_alignment_length")
            else ""
        ),
        
        minimum_percent_identity = (
            str(config.get("nucmer", {}).get("params", {}).get("minimum_percent_identity"))
            if config.get("nucmer", {}).get("params", {}).get("minimum_percent_identity")
            else ""
        ),
    shell:
        #after running nucmer remove all coordinates that are identical between query and ref (sequence aligned to itself)
        """
        nucmer --version  &> {log}

        nucmer --maxmatch --nosimplify --prefix={params.ref_dir}ref {input} {input} &>> {log}
        show-coords  -r {params.ref_dir}ref.delta -T {params.minimum_alignment_length} -I {params.minimum_percent_identity} -H > {output[1]}
        
        csvtk filter2 -H -t -f '$1 != $3 && $2 != $4' {output[1]} | 
            csvtk cut -tH -f 8,1,2 | 
            csvtk mutate2 -tH -e " 'repeat sequence based on nucmer ({params.minimum_alignment_length} -I {params.minimum_percent_identity})' " | 
            csvtk uniq > {output[0]}
        """
