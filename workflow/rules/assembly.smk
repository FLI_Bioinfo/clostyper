"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


def ASSEMBLY():
    if config["ASSEMBLY"] == True:
        ASSEMBLY = [
            expand(assembly_all + "{sample}.fasta", sample=SAMPLES), 
            expand(assembly_all + "{genome}.fasta", genome=GENOMES), 
            results_all_samples_dir + all_genomes_dir_name + "/versions/assembler_version.txt",
            ]
        return ASSEMBLY
    else:
        return []


# choose assembler
if config["Assembly_software"] == "shovill":

    ruleorder: assembly_shovill > assembly_skesa  > assembly_spades
    ruleorder: shovill_version > skesa_version  > spades_version

elif config["Assembly_software"] == "skesa":

    ruleorder: assembly_skesa > assembly_shovill  >  assembly_spades > collect_assemblies
    ruleorder: skesa_version  > spades_version >  shovill_version 

elif config["Assembly_software"] == "spades":

    ruleorder: assembly_spades > assembly_skesa > assembly_shovill  >  collect_assemblies
    ruleorder: spades_version > skesa_version  >  shovill_version 

# Define valid assembly software options

valid_software = {"shovill", "skesa", "spades"}

# Get the selected software from config
assembly_software = config.get("Assembly_software", "").strip().lower()

# Validate input
if assembly_software not in valid_software:
    sys.exit(
        f"{RED}\nError: Assembly software must be one of the following: shovill, skesa, or spades\n{NOFORMAT}"
    )


if "skesa_options" in config:
    skesa_options = config["skesa_options"]
else:
    skesa_options = ""



"""
collect assemblies
"""


rule collect_assemblies:
    input:
        assemblies= lambda wildcards: get_genome(wildcards),
    output:
        contig=assembly_all + "{genome}.fasta",
    conda:
        envs_folder + "data_parse.yaml" 
    shell:
        r"""
        seqkit replace -p .+ -r "contig_{{nr}}" --nr-width 4  -i {input.assemblies} -o {output.contig}
        """


"""
assembly
"""


rule assembly_shovill:
    input:
        r1 = lambda wildcards: get_illumina_r1(wildcards),
        r2 = lambda wildcards: get_illumina_r2(wildcards), 
    output:
        #        contig = results_dir + "{sample}/contigs.fa",
        contigs_fasta=assembly_all + "{sample}.fasta",
    threads: 8  #increasing threads produces errors with spades
    conda:
        envs_folder + "shovill.yaml"  #spades, sickle
    priority: 100
    resources: #https://snakemake-wrappers.readthedocs.io/en/stable/wrappers/spades/metaspades.html
        mem_mem=250000,
        time=60 * 24,
    log:
        results_per_sample_dir + "{sample}/assembly/shovill.log",
    message:
        "Running shovill on {wildcards.sample}"
    params:
        shovill_options=config["shovill_params"],
        assembly_dir=directory(results_per_sample_dir + "{sample}"),
    shell:
        r"""
        shovill --cpus {threads} --force \
            --R1 {input.r1} \
            --R2 {input.r2} \
            --outdir {params.assembly_dir}/assembly \
            {params.shovill_options}  2>&1 | tee {log} >/dev/null

        cp {params.assembly_dir}/assembly/contigs.fa {output.contigs_fasta}
        """

rule shovill_version:
    output:
        version=results_all_samples_dir + all_genomes_dir_name + "/versions/assembler_version.txt",
    threads: 16
    conda:
        envs_folder + "shovill.yaml"
    shell:
        r""" 
        shovill --version >  {output.version}
        """


rule assembly_skesa:
    input:
        r1 = lambda wildcards: get_illumina_r1(wildcards),
        r2 = lambda wildcards: get_illumina_r2(wildcards), 
    output:
        contigs_fasta=assembly_all + "{sample}.fasta",
    threads: 16
    conda:
        envs_folder + "skesa.yaml"
    log:
        results_per_sample_dir + "{sample}/assembly/skesa.log",
    message:
        "Running SKESA on {wildcards.sample}"
    params:
        assembly_dir=directory(results_per_sample_dir + "{sample}/assembly"),
        skesa_options=config["sksea_params"],
    shell:
        r"""
        mkdir -p {params.assembly_dir}
        echo -ne "" > {log} 
        skesa {skesa_options} {params.skesa_options} --use_paired_ends \
            --reads {input.r1},{input.r2} \
            --cores {threads} \
            --contigs_out {params.assembly_dir}/{wildcards.sample}_skesa.fasta  2> {log}
        
        cp {params.assembly_dir}/{wildcards.sample}_skesa.fasta {output.contigs_fasta}
        """

rule skesa_version:
    output:
        version=results_all_samples_dir + all_genomes_dir_name + "/versions/assembler_version.txt",
    threads: 16
    conda:
        envs_folder + "skesa.yaml"
    shell:
        r""" 
        skesa --version &>  {output.version}
        """


rule assembly_spades:
    input:
        r1 = lambda wildcards: get_illumina_r1(wildcards),
        r2 = lambda wildcards: get_illumina_r2(wildcards), 
    output:
        contigs_fasta=assembly_all + "{sample}.fasta",
    threads: 16  #increasing threads produces errors with spades
    conda:
        envs_folder + "spades.yaml"
    log:
        results_per_sample_dir + "{sample}/assembly/spades.log",
    message:
        "Running spades on {wildcards.sample}"
    params:
        tmp_dir=directory(temporary_todelete),
        spades_params=config.get("spades_params", ""),
        assembly_dir=directory(results_per_sample_dir + "{sample}/assembly"),
    shell:
        r""" 
        bash {bin_dir}fixRoaryOutDirError.sh {params.assembly_dir} {params.tmp_dir}
        mkdir -p {params.assembly_dir} {assembly_all}
        spades.py -t {threads} -1 {input.r1}  -2 {input.r2} -o {params.assembly_dir} {params.spades_params} > {log} 2>&1
        cp {params.assembly_dir}/contigs.fasta {output.contigs_fasta}
        """

rule spades_version:
    output:
        version=results_all_samples_dir + all_genomes_dir_name + "/versions/assembler_version.txt",
    threads: 16
    conda:
        envs_folder + "spades.yaml"
    shell:
        r""" 
        spades.py --version >  {output.version}
        """



"""
fasta statistics
"""

rule fasta_stats:
    input:
        fasta=expand(assembly_all + "{sample}.fasta", sample=SAMPLES),
        fasta_assembly=expand(assembly_all + "{sample}.fasta", sample=GENOMES),
        ref_size=rules.reference.output[1],
    output:
        tsv=results_all_samples_dir + all_genomes_dir_name + "/genome_stats.tsv",
        excel=results_all_samples_dir + all_genomes_dir_name + "/genome_stats.xlsx",
        temp=temp(results_all_samples_dir + all_genomes_dir_name + "/FOF.temp"),
    conda:
        envs_folder + "data_parse.yaml"
    params:
        gap_characters = "B b D d E e F f H h I i J j K k L l M m N n O o P p Q q R r S s U u V v W w X x Y y Z z - ." #"N - ."
    shell:
        r"""
        REFERENCE_GENOME_SIZE=`cat {input.ref_size}`
        echo -ne "" >  {output.temp} && 
        for file in {input.fasta} {input.fasta_assembly} ; do echo $file >> {output.temp} ; done && 
        seqkit stats -a -T -b -G "{params.gap_characters}" -e  --infile-list {output.temp} | 
            csvtk replace -p ".fasta" -r '$1' |
            csvtk mutate2  -t -n Reference -e " $REFERENCE_GENOME_SIZE " | 
            csvtk mutate2 -n genome_fraction -t -e ' $5 / $17 * 100 ' | 
            csvtk cut -t -f file,num_seqs,sum_len,min_len,avg_len,max_len,sum_gap,N50,"GC(%)",genome_fraction  | 
            csvtk rename -f file,num_seqs,sum_len,min_len,avg_len,max_len,sum_gap,genome_fraction \
                -n "Sample","n. contigs","Total length (bp)","min. contig length","average contig length","max. contig length","Ambiguous bases","Length relative to Ref. (%)" -t > {output.tsv}
        csvtk csv2xlsx --tabs {output.tsv} -o {output.excel}
        """
