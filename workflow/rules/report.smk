"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


genomes_threshold = 300 # config["genomes_threshold"]


def REPORT():
    if config["REPORT"] == True:
        count_samples, count_genomes = count_items(names[0], names_genomes[0]) ## Access the list inside the tuple
        total_count = count_samples + count_genomes

        if total_count > genomes_threshold: #ToDo: read the number from config
            print("The number of samples is greater than " + str(genomes_threshold) + ". " + program_name + " will create individual reports")
            REPORT = rules.make_multi_report.output.html_reports
        else:
            REPORT = rules.make_report.output.html_report
        return REPORT
    else:
        return []

"""
create report
"""

# Define module-to-Rmd mapping outside the rule so it's accessible in both `output` and `run`
module_to_rmd = {
    "RAWREADS_QUALITY": "raw_reads.Rmd",
    "GENOME_ASSESSMENT": "genome.Rmd",
    "RESISTANCE": "res.Rmd",
    "VIRULENCE": "vir.Rmd",
    "MLST": "mlst.Rmd",
    "SNP_ANALYSIS": "cgsnp.Rmd",
    "COREGENOME_MLST": "cgmlst.Rmd",
    "PANGENOME": "pan.Rmd",
    "C_DIFFICILE_MODULE": "diff.Rmd",
    "C_PERFRINGENS_MODULE": "perf.Rmd",
    "C_CHAUVOEI_MODULE": "chau.Rmd",
}


def report_input():
    with suppress_stdout():
        return SAMPLENAMES(), RAWREADS_QUALITY(), ASSEMBLY(), GENOME_ASSESSMENT(), RESISTANCE(), VIRULENCE(), MLST(), SNPANALYSIS(), COREGENOME_MLST(), ANNOTATION(), PANGENOME(), C_DIFFICILE_MODULE(), C_PERFRINGENS_MODULE(), C_CHAUVOEI_MODULE(),


rule generate_report_rmd:
    input: report_input()
    output:
        report_rmd = temp(temporary_todelete + "generate_report.Rmd"),
    run:
        header_rmd = bin_dir + "report_scripts/header.Rmd"

        enabled_modules = {module: config.get(module, False) for module in module_to_rmd.keys()}

        with open(output.report_rmd, "w") as f:
            with open(header_rmd) as header_file:
                f.write(header_file.read())

            for module, enabled in enabled_modules.items():
                if enabled:
                    module_rmd_path = bin_dir + f"report_scripts/{module_to_rmd[module]}"
                    
                    if not os.path.exists(module_rmd_path):
                        print(f"Warning: {module_rmd_path} not found. Skipping...")
                        continue

                    with open(module_rmd_path) as module_file:
                        f.write(module_file.read())

        print(f"Generated combined Rmd file: {output.report_rmd}")



rule generate_module_report_rmd:
    input:
        header = bin_dir + "report_scripts/header.Rmd",
    output:
        temp(expand((temporary_todelete + "{module}_report.Rmd"), 
               module=[module_to_rmd[module].replace(".Rmd", "").lower() 
                       for module in module_to_rmd.keys() if config.get(module, False)])),
    run:
        import os

        enabled_modules = {module: config.get(module, False) for module in module_to_rmd.keys()}

        for module, enabled in enabled_modules.items():
            if enabled:
                module_rmd_path = os.path.join(bin_dir, "report_scripts", module_to_rmd[module])
                output_rmd_path = os.path.join(temporary_todelete, f"{module_to_rmd[module].replace('.Rmd', '').lower()}_report.Rmd")

                with open(output_rmd_path, "w") as out_rmd:
                    with open(input.header) as header_file:
                        out_rmd.write(header_file.read())

                    with open(module_rmd_path) as module_file:
                        out_rmd.write(module_file.read())

                print(f"Generated separate Rmd file: {output_rmd_path}")




rule make_report:
    input: 
        script = rules.generate_report_rmd.output.report_rmd 
    output:
        html_report = reports_dir + "clostyper_report.html",
    params:
        working_dir=working_dir,
        version=__version__,
    threads: 16
    conda:
        envs_folder + "report_env.yaml"
    script:
        temporary_todelete + "generate_report.Rmd"


rule make_multi_report:
    input: 
        report_input(),
        scripts = rules.generate_module_report_rmd.output  # Takes all generated Rmd scripts
    output:
        html_reports = expand(reports_dir + "{basename}.html", 
                              basename=[os.path.basename(script).replace(".Rmd", "") for script in rules.generate_module_report_rmd.output]),
    params:
        working_dir=working_dir,
        version=__version__,
    threads: 16
    conda:
        envs_folder + "report_env.yaml"
    script:
        bin_dir + "report_scripts/render_reports.R"
