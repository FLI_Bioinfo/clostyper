"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


def ANNOTATION():
    if config["ANNOTATION"] == True:
        ANNOTATION = []
        ANNOTATION.append([
            expand(results_all_samples_dir + "0_genbank_annotation/{sample}.gbk", sample=SAMPLES), 
            expand(results_all_samples_dir + "0_genbank_annotation/{genome}.gbk", genome=GENOMES), 
            rules.summary_gbk.output.res
            ])
        return ANNOTATION
    else:
        return []

def check_bakta_db():
    """
    Validates the configuration for annotation using Bakta.
    """
    if config.get("ANNOTATION", False) is True and config.get("Annotation_software") == "bakta":
        if not config.get("bakta_database"):
            sys.exit(
                f"{RED}\nError: The Bakta database must be provided when using Bakta for annotation.\n{NOFORMAT}"
            )

# choose annotator
if config["Annotation_software"] == "prokka":

    ruleorder: prokka > bakta

elif config["Annotation_software"] == "bakta":

    ruleorder: bakta > prokka
    check_bakta_db()

else:  # default prokka

    ruleorder: prokka > bakta


"""
Prokka
"""


rule prokka:
    input:
        contig=assembly_all + "{sample}.fasta",
    output:
        prokka_gff=results_per_sample_dir + "{sample}/annotation/{sample}.gff",
        prokka_gbk=results_per_sample_dir + "{sample}/annotation/{sample}.gbk",
        prokka_faa=results_per_sample_dir + "{sample}/annotation/{sample}.faa",
        sqn=temp(results_per_sample_dir + "{sample}/annotation/{sample}.sqn"),
        fna=temp(results_per_sample_dir + "{sample}/annotation/{sample}.fna"),
        fsa=temp(results_per_sample_dir + "{sample}/annotation/{sample}.fsa"),
        tbl=temp(results_per_sample_dir + "{sample}/annotation/{sample}.tbl"),
    threads: 4
    conda:
        envs_folder + "prokka.yaml"
    message:
        "Running Prokka on {wildcards.sample}"
    params:
        prokka_outdir=results_per_sample_dir + "{sample}/annotation",
        prefix="{sample}",
        prokka_params=config["prokka_params"],
    shell:
        r"""
        prokka --cpus {threads} {params.prokka_params} --force \
            --kingdom Bacteria \
            --outdir {params.prokka_outdir}/ \
            --prefix {params.prefix} \
            --gcode 11 \
            --locustag {params.prefix} \
            --strain {params.prefix}  {input.contig} &> /dev/null
        """


"""
Bakta
"""


rule bakta:
    input:
        contig=assembly_all + "{sample}.fasta",
        db=lambda wildcards: config.get("bakta_database", []),
    output:
        gff=results_per_sample_dir + "{sample}/annotation/{sample}.gff3",
        gbk=results_per_sample_dir + "{sample}/annotation/{sample}.gbff",
        faa=results_per_sample_dir + "{sample}/annotation/{sample}.faa",
        fna=temp(results_per_sample_dir + "{sample}/annotation/{sample}.fna"),
        embl=temp(results_per_sample_dir + "{sample}/annotation/{sample}.embl"),
        temp_dir=temp(directory(results_per_sample_dir + "{sample}/annotation/temporary")),
    threads: workflow.cores * 0.2
    log:
        results_per_sample_dir + "{sample}/annotation/bakta.log",
    conda:
        envs_folder + "bakta.yaml"
    message:
        "Running Bakta on {wildcards.sample}"
    params:
        outdir=results_per_sample_dir + "{sample}/annotation",
        prefix="{sample}",
        extraparams=config["bakta_params"] if config["bakta_params"] else "",
    shell:
        """
        mkdir -p {output.temp_dir}
        bakta --force --db {input.db} \
            --keep-contig-headers \
            --prefix {params.prefix} \
            --strain {params.prefix} \
            --output {params.outdir}  \
            --tmp-dir {output.temp_dir} \
            --threads {threads} \
            {params.extraparams} \
            {input.contig} 2>&1 | tee {log} >/dev/null
        """



def genebank_files():
    if config["Annotation_software"] == "prokka":
        return rules.prokka.output[1]
    elif config["Annotation_software"] == "bakta":
        return rules.bakta.output[1]
    else:  # default prokka
        return rules.prokka.output[1]


def gff_files():
    if config["Annotation_software"] == "prokka":
        return rules.prokka.output[0]
    elif config["Annotation_software"] == "bakta":
        return rules.bakta.output[0]
    else:  # default prokka
        return rules.prokka.output[0]


rule annotation_copy:
    input:
        genebank_files(),
    output:
        gbk=results_all_samples_dir + "0_genbank_annotation/{sample}.gbk",
    shell:
        "cp {input} {output.gbk}"


rule summary_gbk:
    input:
        expand(results_all_samples_dir + "0_genbank_annotation/{sample}.gbk", sample=SAMPLES), 
        expand(results_all_samples_dir + "0_genbank_annotation/{genome}.gbk", genome=GENOMES)
    output:
        res=results_all_samples_dir + all_genomes_dir_name + "/Annotation_summary.tsv",
    conda:
        envs_folder + "parse_gbk.yaml"
    shell:
        r"""
        python {bin_dir}/parse_gbk.py -o {output.res} -i {input}
        """
