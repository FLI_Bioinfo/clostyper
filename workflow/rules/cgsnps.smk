"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


def SNPANALYSIS():
    if config.get("SNP_ANALYSIS", False):
        SNPANALYSIS = []
        
        #snippy
        SNPANALYSIS.append([
            expand(rules.snippy.output.res, sample=SAMPLES), 
            expand(rules.snippy_assemblies.output.res, genome=GENOMES), 
            ])
        
        #snippy-core
        if count_isolates > 2:
            SNPANALYSIS.append(rules.snippy_core.output)

            #ref excluded
            if config["exclude_reference_from_alignment"] == True:

                SNPANALYSIS.append(rules.cgsnps_noref.output)

            #final cgsnps

            SNPANALYSIS.append(rules.cgsnps.output.cgsnps)

            SNPANALYSIS.append(rules.snpdists.output.snpdists)

            if config["recombination_filter"] == True:

                SNPANALYSIS.append(rules.run_gubbins.output.fasta)
                SNPANALYSIS.append(rules.run_gubbins_version.output.version)
                SNPANALYSIS.append(rules.mask_gubbins.output.masked)

            if config["homoplasies_filter"] == True:

                SNPANALYSIS.append(rules.run_homoplasyfinder.output.res)

            if config["prepare_full_SNP_table"] == True:
            # if config["annotate_snps"] == True:

                SNPANALYSIS.append(rules.prepare_snp_table.output.tbl)
                SNPANALYSIS.append(rules.annotate_snps.output.cons)

            if config["clustering_with_hclust"] == True:

                SNPANALYSIS.append(rules.clustering_with_hclust.output.clusters_tsv)
        else:

            print ("No of isolates are less than 2 - Complete cgSNP analysis is not possible")

        # Write the workflow
        
        SNPANALYSIS.append(check_alignment_status)

        return SNPANALYSIS

    else:

        return []


"""
snippy for snp calling
"""


rule snippy:
    input:
        r1 = lambda wildcards: get_illumina_r1(wildcards),
        r2 = lambda wildcards: get_illumina_r2(wildcards),
        reference=rules.reference.output[0],
    output:
        res= multiext(
            results_per_sample_dir + "{sample}/snippy/{sample}/snps", 
            ".tab", ".aligned.fa", ".vcf", ".log"
            ),
        dir = temp(directory(results_per_sample_dir + "{sample}/snippy/{sample}/reference")),
    threads: workflow.cores * 0.25 if workflow.cores > 8 else 8
    conda:
        envs_folder + "cgsnps.yaml"
    log:
        results_per_sample_dir + "{sample}/snippy/{sample}_snippy.log",
    message:
        "Calling variants with snippy on {wildcards.sample}"
    params:
        snippy_outdir=results_per_sample_dir + "{sample}/snippy/{sample}",
        params=config.get("snippy", {}).get("params", ""),
    shell:
        r"""
        export PERL5LIB=""
        snippy \
        --force  \
        --cpus {threads} \
        --ram {threads} \
        --outdir {params.snippy_outdir} \
        --ref {input.reference} \
        --R1 {input.r1} \
        --R2 {input.r2} \
        {params.params} &> {log}
        """


rule snippy_assemblies:
    input:
        contig= lambda wildcards: get_genome(wildcards),
        reference=rules.reference.output[0],
    output:
        res= multiext(
            results_per_sample_dir + "{genome}/snippy/{genome}/snps", 
            ".tab", ".aligned.fa", ".vcf", ".log"
            ),
        dir = temp(directory(results_per_sample_dir + "{genome}/snippy/{genome}/reference")),
    threads: workflow.cores * 0.25 if workflow.cores > 8 else 8
    conda:
        envs_folder + "cgsnps.yaml"
    log:
        results_per_sample_dir + "{genome}/snippy/{genome}_snippy.log",
    message:
        "Calling variants with snippy on {wildcards.genome}"
    params:
        snippy_outdir=results_per_sample_dir + "{genome}/snippy/{genome}",
        params=config.get("snippy", {}).get("params", ""),
    shell:
        r"""
        export PERL5LIB=""
        snippy \
        --force  \
        --cpus {threads} \
        --ram {threads} \
        --outdir {params.snippy_outdir} \
        --ref {input.reference} \
        --ctgs {input.contig} \
        {params.params} &> {log}
        """


def snippy_folders(wildcards):
    return expand(results_per_sample_dir + "{sample}/snippy/{sample}", sample=SAMPLES)


def snippy_folders_assemblies(wildcards):
    return expand(results_per_sample_dir + "{genome}/snippy/{genome}", genome=GENOMES)


rule collect_masked_regions:
    input:
        rules.detect_reptitive_regions_in_ref.output[0] if config["mask_repitive_sequence_regions"] == True else [],
        config["snippy-core"]["params"]["masked_regions"] if config["snippy-core"]["params"]["masked_regions"] else [],
    output:
        mask = os.path.join(
            results_all_samples_dir, 
            all_snp_analysis_dir_name, 
            variant_calling_dir_name, 
            "masked_regions.tsv"
            ),
    message:
        "collect all masked sequence regions for snippy"
    params:
        "__no_name__",
    shell:
        r"""
        echo -ne "" > {output.mask}
        for file in {input} {params}; do
            if [[ -f $file ]]; then 
              cat $file >> {output.mask}
            fi
        done
        """


rule snippy_core: 
    """
    snippy-core will abort if no core genome snps were found 
    This is in case of identical samples to the ref or extremely outlier samples to the ref. 
    """
    input:
        expand(rules.snippy.output.res, sample=SAMPLES), 
        expand(rules.snippy_assemblies.output.res, genome=GENOMES),
        rules.collect_masked_regions.output.mask if config["snippy-core"]["params"]["masked_regions"] or config["mask_repitive_sequence_regions"] == True else [],
        reference=rules.reference.output[0],
    output:
        aln = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, variant_calling_dir_name, "snippy_core/core.aln"),
        full_aln = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, variant_calling_dir_name, "snippy_core/core.full.aln"),
        res = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, variant_calling_dir_name, "snippy_core/core.txt"), 
        tab = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, variant_calling_dir_name, "snippy_core/core.tab"), 
        version=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/snippy_version.txt"),
    conda:
        envs_folder + "cgsnps.yaml"
    log:
        os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, variant_calling_dir_name, "snippy_core/snippycore.log"),
    message:
        "Identifying core SNPs with snippy-core"
    params:
        prefix = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, variant_calling_dir_name, "snippy_core/core"),
        snippy_folders = snippy_folders,
        snippy_folders_assemblies = snippy_folders_assemblies,
        masked_regions = (
            "--mask=" + rules.collect_masked_regions.output[0] 
            if config.get("snippy-core", {}).get("params", {}).get("masked_regions", False) or config.get("mask_repitive_sequence_regions", False) 
            else ""
        ),
        Gap_or_deletion_character = (
            "--gap-char=" + config.get("snippy-core", {}).get("params", {}).get("Gap_or_deletion_character")  or ""
            if config.get("snippy-core", {}).get("params", {}).get("Gap_or_deletion_character") 
            else ""
        ),

        Masking_character = (
            "--mask-char=" + config.get("snippy-core", {}).get("params", {}).get("Masking_character")  or ""
            if config.get("snippy-core", {}).get("params", {}).get("Masking_character") 
            else ""
        ),
    shell:
        r"""
        export PERL5LIB=""
        snippy-core  \
            --ref {input.reference} \
            {params.snippy_folders} \
            {params.snippy_folders_assemblies}  \
            {params.masked_regions} \
            {params.Gap_or_deletion_character} \
            {params.Masking_character} \
            --prefix {params.prefix}  &> {log}
        
        snippy --version > {output.version}
        """



rule clean_snippy_alignment:
    """
    snippy-clean_full_aln keeps two characters "N -" for missing nuc. - ok for gubbins, ?? for other tools. 
    seqkit is similarly fast, keep only "-" or "N" for missing nuc. Seqkit is also good for this Rule inheritance
    """
    input:
        rules.snippy_core.output.full_aln,
    output:
        res = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, variant_calling_dir_name, "core_genome_full.aln"),
        version=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/seqkit_version.txt"),
    conda:
        envs_folder + "data_parse.yaml"
    params:
        missing_chr=config["clean_snippy_alignment"]["params"]["missing_data_character"] if config["clean_snippy_alignment"]["params"]["missing_data_character"] else "-",
    shell:
        r"""
        cat {input} | seqkit replace  -p " |-|x|X|n|N|\?" -s -r "{params.missing_chr}" | seqkit seq -u  > {output.res}
        seqkit version > {output.version}
        """

        #cat {output} | grep -v ">" | grep -o . | sort | uniq -c #count the frequency of all characters in the sequence


rule cgsnps_noref:
    input:
        rules.clean_snippy_alignment.output.res,
    output:
        snippycorenoRef = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, variant_calling_dir_name, "coregenome_NoRef.aln"),
    conda:
        envs_folder + "cgsnps.yaml"
    params: 
        core_percent = config.get("coresnpfilter", {}).get("core_snp_threshold", 0.98) 
    shell:
        r"""
        seqkit grep --quiet -v -p Reference {input} -o {output.snippycorenoRef} 
        """


rule cgsnps:
    input:
        aln = rules.cgsnps_noref.output.snippycorenoRef if config.get("exclude_reference_from_alignment", False) else rules.clean_snippy_alignment.output.res,
    output:
        cgsnps = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, variant_calling_dir_name, "coregenome_snps.aln"),
        version = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/coresnpfilter_version.txt"),
    params: 
        core_percent = config.get("coresnpfilter", {}).get("core_snp_threshold", 0.98) 
    log: 
        results_all_samples_dir + all_snp_analysis_dir_name + "/" + variant_calling_dir_name + "/coresnpfilter.log"
    shell:
        r"""
        {bin_dir}coresnpfilter -e -c {params.core_percent} {input.aln} > {output.cgsnps} 2> {log}
        {bin_dir}coresnpfilter -V >  {output.version}
        """



rule prepare_snp_table:  #can be used with the script parseSNPtable3.py & SNPPar (https://github.com/d-j-e/SNPPar)
    input:
        rules.clean_snippy_alignment.output.res,
    output:
        tbl = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, variant_calling_dir_name, "full_snp_table.csv"),
    conda:
        envs_folder + "cgsnps.yaml"
    shadow:
        "minimal"
    threads: 2
    shell:
        #create vcf file for all snps with gubbins (no recombination filter therefore we will use gubbins not run_gubbins)
        r"""
        gubbins {input}
        cat core_genome_full.aln.vcf | 
            sed -e '1,3d' | 
            csvtk cut -t --quiet -m -f -1 -C "$" | 
            csvtk cut -t  --quiet -m -f 1,9- | 
            csvtk mutate2 -t  --quiet -n Ref -e ' $Reference '  --at 2 | 
            csvtk cut -t  --quiet -f -"Reference" | 
            csvtk rename  --quiet -f POS -n Pos -t | 
            csvtk tab2csv > {output.tbl}
        """


rule annotate_snps: # TODO: test this
    input:
        tbl = rules.prepare_snp_table.output.tbl,
        ref = config["reference"] # must be in Genbank format
    output:
        cons = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, variant_calling_dir_name, "SNP_annotation/full_snp_table_var.csv"),
    conda:
        envs_folder + "srst2.yaml" #same as for cdiff module
    threads: 2
    params: 
        dir = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, variant_calling_dir_name, "SNP_annotation"), 
        core_percent = config.get("coresnpfilter", {}).get("core_snp_threshold", 0.98),
        params=config.get("parseSNPtable", {}).get("params", "")
    log: 
        os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, variant_calling_dir_name, "SNP_annotation/parseSNPtable.log"), 
    shell:
        r"""
        mkdir -p {params.dir}
        python {bin_dir}parseSNPtable.py -d {params.dir} -s {input.tbl} -r {input.ref} -c {params.core_percent} -m cons,coding {params.params}  &> {log}
        """


"""
Recombination filtering
"""


rule run_gubbins:  #will filter and mask
    input:
        rules.clean_snippy_alignment.output.res,
    output:
        fasta = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, recombination_filter_dir_name, "gubbins.filtered_polymorphic_sites.fasta"), 
        tree = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, recombination_filter_dir_name, "gubbins.final_tree.tre"), 
        gff = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, recombination_filter_dir_name, "gubbins.recombination_predictions.gff"), 
        others = multiext(
            os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, recombination_filter_dir_name, "gubbins"), 
            ".node_labelled.final_tree.tre", ".recombination_predictions.embl", ".summary_of_snp_distribution.vcf", 
            ".branch_base_reconstruction.embl", ".filtered_polymorphic_sites.phylip", ".per_branch_statistics.csv", 
        ),
    conda:
        envs_folder + "gubbins.yaml"
    threads: workflow.cores * 0.5 if workflow.cores > 8 else 8
    message:
        "Filtering recombination sites with gubbins"
    shadow:
        "minimal"  #Gubbins temporary_outdir: #local_SSD due to permissions to erite to /data
    log:
        os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, recombination_filter_dir_name, "run_gubbins.log")
    params:
        recomb_dir=directory(os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, recombination_filter_dir_name)),  
        prefix = "gubbins",
        params = config.get("gubbins", {}).get("params") or "",
    shell:
        r"""
        mkdir -p {params.recomb_dir}

        run_gubbins.py --verbose --threads {threads} \
        -p {params.recomb_dir}/{params.prefix} \
        {params.params} \
        {input} 2>&1 | tee {log} >/dev/null
        """



rule mask_gubbins:
    input:
        fasta = rules.clean_snippy_alignment.output.res,
        gff = rules.run_gubbins.output.gff,
    output:
        masked =  os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, recombination_filter_dir_name, "recombination_masked_aln.fasta"),
    conda:
        envs_folder + "gubbins.yaml"
    threads: workflow.cores * 0.5 if workflow.cores > 8 else 8
    params:
        masking_char = config.get("gubbins", {}).get("masking_char", "N")
    shell:
        r"""
        mask_gubbins_aln.py \
        --aln {input.fasta} \
        --gff {input.gff} \
        --out {output.masked} \
        --out-fmt fasta \
        --missing-char {params.masking_char}
        """


rule run_gubbins_version:  #will filter and mask
    input:
        rules.run_gubbins.output,
    output:
        version = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/gubbins_version.txt"),
    conda:
        envs_folder + "gubbins.yaml"
    shell:
        r"""
        run_gubbins.py --version >  {output.version}
        """


"""
homoplasies filtering
"""


rule run_homoplasyfinder:
    input:
        fasta = rules.run_gubbins.output.fasta,
        tree = rules.run_gubbins.output.tree,
    output:
        res = results_all_samples_dir + all_snp_analysis_dir_name + "/" + homoplasyFinder_dir_name + "/sequences_noInconsistentSites.fasta",
    conda:
        envs_folder + "cgsnps.yaml"
    message:
        "Detecting and filtering homoplasies in the recombination filtered alignment"
    params:
        homoplasy_dir=directory(results_all_samples_dir + all_snp_analysis_dir_name + "/" + homoplasyFinder_dir_name + "/"),
        extraparams = config.get("HomoplasyFinder", {}).get("params") or ""
    shell:
        r"""
        mkdir -p {params.homoplasy_dir} &&  cd {params.homoplasy_dir}

        time_stamp=$(date +"%d-%m-%y")

        java -jar {bin_dir}HomoplasyFinder.jar \
        --fasta {input.fasta} \
        --tree {input.tree} \
        --createFasta \
        {params.extraparams} \
        --createAnnotatedTree

        tail -n +2 sequences_noInconsistentSites_"$time_stamp".fasta > {output.res}
        """


"""
Core genome phylogeny
"""


def phylogeny_input():
    if config.get("phylogeny_input") == "snippy":
        return rules.cgsnps.output.cgsnps
    elif config.get("phylogeny_input") == "gubbins":
        return rules.run_gubbins.output.fasta
    elif config.get("phylogeny_input") == "HomoplasyFinder":
        return rules.run_homoplasyfinder.output.res
    else:
        return rules.cgsnps.output.cgsnps



rule stats_of_selected_alignment:
    input:
        phylogeny_input(),
    output:
        stats = temp(os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "temp_alignment_stats.txt")),
        version=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/amas_version.txt"),
    threads: 2
    conda:
        envs_folder + "cgsnps.yaml"
    shell:
        r""" 
        AMAS.py summary --in-format fasta -c {threads} -d dna -i {input} -o {output.stats} 2>/dev/null
        echo "AMAS version 1.0" > {output.version}
        """
        
        
rule collect_stats_of_selected_alignment:
    input:
        rules.stats_of_selected_alignment.output.stats,
    output:
        stats = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "alignment_stats.txt"),
    threads: 2
    shell:
        r""" 
        cat {input} | csvtk transpose -t | csvtk head -tH -n 10 | csvtk pretty -tH -o {output.stats} 
        """


rule visualize_alignment:
    input:
        phylogeny_input(),
    output:
        aln = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "alignment.html"),
        version=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/goalign_version.txt"),
    threads: 2
    conda:
        envs_folder + "cgsnps.yaml"
    shell:
        r""" 
        goalign draw biojs -i {input} -o {output.aln}      
        goalign version > {output.version}
        """


# phylogeny algorithm & software [now only maximum liklihood && neighbor joiing ]
if config["phylogeny_software"] == "FastTree":

    ruleorder: FastTree > raxml > raxmlng > fastphylo > grapetree

elif config["phylogeny_software"] == "RAxML":

    ruleorder: raxml > FastTree > raxmlng > fastphylo > grapetree

elif config["phylogeny_software"] == "RAxML-NG":

    ruleorder: raxmlng > raxml > FastTree > fastphylo > grapetree

elif config["phylogeny_software"] == "Fastphylo":

    ruleorder: fastphylo > raxml > FastTree > raxmlng > grapetree

elif config["phylogeny_software"] == "GrapeTree":

    ruleorder: grapetree > fastphylo > raxml > FastTree > raxmlng

else:  # default: FastTree

    ruleorder: FastTree > raxml > raxmlng > fastphylo > grapetree


# FastTree

rule FastTree:
    input:
        phylogeny_input(),
    output:
        nwk = ensure(
            os.path.join(
                results_all_samples_dir,
                all_snp_analysis_dir_name, 
                phylogeny_dir_name,  
                "coregenome_phylogeny.newick"
                ), 
            non_empty=True
            ),
        version=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/FastTree_version.txt"),
    threads: workflow.cores * 0.3 if workflow.cores > 9 else 4
    log:
        os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, phylogeny_dir_name, "FastTree.log"),
    message:
        "Making ML phylogeny with FastTree"
    conda:
        envs_folder + "phylogeny.yaml"
    params:
        args = config.get("fasttree", {}).get("params") or ""
    shell:
        r"""
        FastTree -nt \
            {params.args} \
            {input} > {output} 2> {log}
        echo "FastTree version 2.1.10" > {output.version}
        """


# raxml

rule raxml:
    input:
        phylogeny_input(),
    output:
        nwk = ensure(
            os.path.join(
                results_all_samples_dir,
                all_snp_analysis_dir_name, 
                phylogeny_dir_name,  
                "coregenome_phylogeny.newick"
                ), 
            non_empty=True
            ),
        others = multiext(
            os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, phylogeny_dir_name, "RAxML_"), 
            "bestTree.raxml", "bipartitionsBranchLabels.raxml", "bipartitions.raxml", "bootstrap.raxml", "info.raxml"
            ),
        version=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/RaxML_version.txt"),
    threads: workflow.cores * 0.3 if workflow.cores > 9 else 4
    log:
        results_all_samples_dir + all_snp_analysis_dir_name + "/" + phylogeny_dir_name + "/raxml.log",
    message:
        "Making ML phylogeny with RAxML"
    conda:
        envs_folder + "phylogeny.yaml"
    params:
        phylo_dir=directory(results_all_samples_dir + all_snp_analysis_dir_name + "/" + phylogeny_dir_name),
        suffix="raxml",
        params = config.get("raxml", {}).get("params") or ""
    shell:
        r"""
        mkdir -p {params.phylo_dir}
        raxmlHPC-PTHREADS \
            -s {input} \
            -n {params.suffix} \
            -T {threads} \
            -w {params.phylo_dir} {params.params}  &> {log}

        cp {output[2]} {output[0]}
        
        raxmlHPC-PTHREADS -v | grep version > {output.version}
        """

# raxml-ng 

rule raxmlng:
    input:
        phylogeny_input(),
    output:
        nwk = ensure(
            os.path.join(
                results_all_samples_dir,
                all_snp_analysis_dir_name, 
                phylogeny_dir_name,  
                "coregenome_phylogeny.newick"
                ), 
            non_empty=True
            ),
        others = multiext(
            os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, phylogeny_dir_name, "RAxML-NG"), 
            ".raxml.support", ".raxml.bestTree", ".raxml.mlTrees", ".raxml.bestModel", ".raxml.bootstraps"
            ),
        version=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/RaxML-NG_version.txt"),
    threads: 4  #more threads is not always good (https://github.com/amkozlov/raxml-ng/wiki/Parallelization#so-how-many-threadscores-should-i-use)
    log:
        results_all_samples_dir + all_snp_analysis_dir_name + "/" + phylogeny_dir_name + "/raxml-ng.log",
    message:
        "Making ML phylogeny with RAxML"
    conda:
        envs_folder + "phylogeny.yaml"
    params:
        prefix=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, phylogeny_dir_name, "RAxML-NG"),
        args= config.get("raxml-ng", {}).get("params") or "", 
    shell:
        r"""
        raxml-ng \
            --all \
            --msa {input} \
            --threads auto \
            --prefix {params.prefix} \
            --data-type DNA \
            {params.args} --redo  &> {log}

        cp {output[1]} {output[0]}

        raxml-ng -v | grep "RAxML-NG v." > {output.version}
        """

# distance based phylogenies

rule fastphylo:  #requires input in phylip format or XML
    input:
        phylogeny_input(),
    output:
        nwk = ensure(
            os.path.join(
                results_all_samples_dir,
                all_snp_analysis_dir_name, 
                phylogeny_dir_name,  
                "coregenome_phylogeny.newick"
                ), 
            non_empty=True
            ),
        phy = results_all_samples_dir + all_snp_analysis_dir_name + "/" + phylogeny_dir_name + "/fastphylo_distance_matrix.phy",
        version=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/Fastphylo_version.txt"),
    threads: 4
    log:
        results_all_samples_dir + all_snp_analysis_dir_name + "/" + phylogeny_dir_name + "/fastphylo.log",
    message:
        "Making Fast Neighbor Joining tree with fastphylo"
    conda:
        envs_folder + "phylogeny_dist.yaml"
    params:
        fastdist_params = config.get("fastphylo", {}).get("fastdist_params") or "",
        fnj_params = config.get("fastphylo", {}).get("fnj_params") or ""
    shell:
        r"""
        fastdist \
            -I fasta \
            -O phylip \
            {params.fastdist_params} \
            -o {output.phy} {input} &> {log}

        fnj \
            -I phylip \
            -O newick \
            {params.fnj_params} \
            -o {output[0]} {output[1]}  &>> {log}
        
        fastdist -V > {output.version} && fnj -V >> {output.version}
        """


rule grapetree:
    input:
        phylogeny_input(),
    output:
        nwk = ensure(
            os.path.join(
                results_all_samples_dir,
                all_snp_analysis_dir_name, 
                phylogeny_dir_name,  
                "coregenome_phylogeny.newick"
                ), 
            non_empty=True
            ),
        version=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/GrapeTree_version.txt"),
    threads: 4
    log:
        results_all_samples_dir + all_snp_analysis_dir_name + "/" + phylogeny_dir_name + "/grapetree.log",
    message:
        "Making distance based tree with GrapeTree"
    conda:
        envs_folder + "grapetree.yaml"
    params:
        args = config.get("grapetree", {}).get("params") or "",
    shell:
        r"""
        grapetree \
            --profile {input} \
            {params.args} \
            --n_proc {threads} > {output.nwk}

        echo "GrapeTree version 2.1" > {output.version}
        """


# reroot tree

rule midpoint_tree_rooting:
    input:
        phylogeny_tree = os.path.join(
            results_all_samples_dir, 
            all_snp_analysis_dir_name, 
            phylogeny_dir_name, 
            "coregenome_phylogeny.newick",
        )
    output:
        tre=os.path.join(
            results_all_samples_dir, 
            all_snp_analysis_dir_name, 
            phylogeny_dir_name, 
            "coregenome_phylogeny_midpoint_root.newick"
            ),
        version=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/Treebender_version.txt"),
    message:
        "Midpoint rooting the phylogeny"
    conda:
        envs_folder + "phylogeny.yaml"
    shell:
        r"""
        treebender --mid_point_root < {input.phylogeny_tree} >  {output.tre}
        treebender -h | grep -oP 'Treebender.{{5}}' > {output.version}
        """


# make phylogeny image TODO read params from config e.g. bootstrap cutoff

rule phylogeny_image:
    input:
        rules.midpoint_tree_rooting.output.tre,
    output:
        svg = os.path.join(
            results_all_samples_dir,
            all_snp_analysis_dir_name, 
            phylogeny_dir_name, 
            "coregenome_phylogeny.svg"
            ),
        version=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/Gotree_version.txt"),
    conda:
        envs_folder + "phylogeny.yaml"
    shell:
        r"""
        if [ $(cat {input} | gotree labels | wc -l) -gt 500 ]; then
            cat {input} | 
            gotree rotate sort | 
            gotree draw svg -o {output[0]} --width 900 --height 900 --with-branch-support --no-tip-labels > {output}
        else
            cat {input} | 
            gotree rotate sort | 
            gotree draw svg -o {output[0]} --width 900 --height 900 --with-branch-support > {output}
        fi

        gotree version  > {output.version}
        """


"""
SNP distance matrix
"""


rule snpdists:
    input:
        phylogeny_input(),
    output:
        snpdists = os.path.join(
            results_all_samples_dir, 
            all_snp_analysis_dir_name, 
            pairwise_distance_dir_name, 
            "cgSNP_PairwiseDistances.tsv"
        ),
        version=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/snp-dists_version.txt"),
    message:
        "Calculating SNP distances between each pair of genomes with snp-dists"
    conda:
        envs_folder + "cgsnps.yaml"
    shell:
        r"""
        snp-dists -q -b {input} | csvtk rename -t -f 1 -n Sample > {output.snpdists}
        snp-dists -v > {output.version}
        """


           
"""
Clustering
"""
threshold_range=config["clustering_snps_hclust"]["clustering_threshold_range"] 
distance_threshold = int(config["clustering_snps_hclust"]["distance_threshold"])  

# Validate and convert the threshold_range string to a list of integers.
# Exits the program if an invalid item is found.

def validate_threshold_range(threshold_range: str) -> list:
    items = threshold_range.split(",")
    thresholds = []

    # Validate each item to ensure it's a number
    for item in items:
        if item.strip().isdigit():
            thresholds.append(int(item.strip()))
        else:
            sys.exit(RED + f"Error: Invalid item found in threshold range: {item}" + NOFORMAT)

    return thresholds



# Ensures the distance is part of the threshold range and returns the updated range as a string.

def ensure_threshold_in_range(threshold_range: str, distance: int) -> str:

    # Validate and convert threshold range into a list of integers
    clustering_thresholds = validate_threshold_range(threshold_range)

    # Add the distance to the list if it's not already present
    if distance not in clustering_thresholds:
        clustering_thresholds.append(distance)

    # Sort the list to maintain order
    clustering_thresholds.sort()

    # Convert the list back to a comma-separated string and return
    return ",".join(map(str, clustering_thresholds))

# The distance_threshold is included in the range
clustering_threshold_range = ensure_threshold_in_range(threshold_range, distance_threshold)



rule clustering_with_hclust:
    input:
        rules.snpdists.output.snpdists,
    output:
        clusters_tsv=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, cluster_dir_name, "Hierarchical_Clusters_of_PairwiseDistances.tsv"),
        clusters_xlsx=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, cluster_dir_name, "Hierarchical_Clusters_of_PairwiseDistances.xlsx"),
        cluster_summary_tsv= os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, cluster_dir_name, "Hierarchical_Clusters_summary.tsv"),
        plot=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, cluster_dir_name, "Hierarchical_Clustering_tree.pdf"),
        version=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/hclust_version.txt"),
    conda:
        envs_folder + "report_env.yaml"
    params:
        clustering_threshold_range = clustering_threshold_range, 
        distance_threshold = distance_threshold,
        clustering_method = config.get("clustering_snps_hclust", {}).get("clustering_method", "single"),
    script:
        bin_dir + "clustering_hclust.R"


rule clustering_with_fastbaps:
    input:
        tree= rules.midpoint_tree_rooting.output.tre, 
        alignment=phylogeny_input(),
    output:
        fastbaps_tsv = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, cluster_dir_name, "FastBAPS_clusters.tsv"),
        fastbaps_xlsx = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, cluster_dir_name, "FastBAPS_clusters.xlsx"),
        plot = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, cluster_dir_name, "FastBAPS_clusters.pdf"),
        version=os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "versions/FastBAPS_version.txt"),
    conda:
        envs_folder + "fastbaps.yaml"
    threads: 8
    params: 
        prior = config.get("clustering_fastbaps", {}).get("params", {}).get("prior", "symmetric"),
        levels = config.get("clustering_fastbaps", {}).get("params", {}).get("number_hierarchical_levels", "6")
    script:
        bin_dir + "clustering_fastbaps.R"




"""
Conditional apply cgsnp rules 
Skip: phylogeny, alignment stats fastbaps  
- if the core genome SNP file is not empty 
"""



checkpoint check_alignment:
    input:
        cgsnp = rules.cgsnps.output.cgsnps,
    output:
        out = os.path.join(
                results_all_samples_dir, 
                all_snp_analysis_dir_name, 
                phylogeny_dir_name, 
                "alignment_check.txt"
                ),
    run:
        alignment_file = input[0]
        # Check if there are any sequences in the alignment file
        with open(alignment_file) as f:
            sequences = [line for line in f if not line.startswith(">") and line.strip()]
        
        # Create an output file with the result of the check
        with open(output[0], "w") as out:
            if len(sequences) > 0:
                out.write("has_sequences\n")
            else:
                out.write("no_sequences\n")




def check_alignment_status(wildcards):
# reads the content of the checkpoint output file 
# if sequences exist and the number of isolates are more than 4, perfoem phylogeny

    checkpoint_output = checkpoints.check_alignment.get(**wildcards).output[0]
    # checkpoint_output = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, phylogeny_dir_name, "alignment_check.txt"),    

    # Open the checkpoint output file and check status
    with os.path.join(checkpoint_output).open() as f:
        status = f.read().strip()

    # Empty list to collect output files
    files = [] 

    # Proceed only if status indicates there are sequences
    if status == "has_sequences":
        # Check if there are more than 3 isolates
        if count_isolates > 3:
           
            # Append output files to the list
            if config["clustering_with_fastbaps"] == True:
                files.append(rules.clustering_with_fastbaps.output.fastbaps_tsv)  

            files.append(rules.midpoint_tree_rooting.output.tre)
            files.append(rules.phylogeny_image.output.svg)
            files.append(rules.visualize_alignment.output.aln)

            # Note here we have to use .extend to flatten the file list
            files.extend(rules.collect_stats_of_selected_alignment.output) 
    
            return files
            
        else:
            print("The number of isolates is less than 4. Skipping phylogeny rules.")
            return []
    else:
        print("No cgSNPs sequences found. Skipping phylogeny rules.")
        return []


# def aggregate_inputs_after_aln(wildcards):
#     aln_status = None

#     # checkpoint_output = os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, phylogeny_dir_name, "alignment_check.txt"),
#     with checkpoints.check_alignment.get().output[0].open() as f:
#         aln_status = f.read().strip()
    
#     if aln_status == "has_sequences":
#         inputs = [rules.snippy_core.output.res]

#         if count_isolates > 2:
#             dist = rules.snpdists.output.snpdists
#             inputs.append(dist)
            
#             if config.get("clustering_with_hclust"):
#                 clusters_tsv = rules.clustering_with_hclust.output.clusters_tsv
#                 inputs.append(clusters_tsv)
            
#             if config.get("clustering_with_fastbaps"):
#                 fastbaps = rules.clustering_with_fastbaps.output.fastbaps_tsv
#                 inputs.append(fastbaps)
        
#         if count_isolates > 3:
#             final_tree = rules.midpoint_tree_rooting.output.tre
#             phylogeny_image = rules.phylogeny_image.output.svg
            
#             inputs.extend([final_tree, phylogeny_image])

#         return inputs
    
#     return []


# """
# write SNP workflow
# """


# rule write_snp_workflow:  #this rule collects SNP info from config file
#     input:
#         aggregate_inputs_after_aln,
#         reference=reference,
#         isolates_list=rules.isolates_list.output.isolates_list,
#         snippycore=rules.snippy_core.output.aln,
#         coresnpfilter=rules.cgsnps.output.cgsnps,
#         gubbins=rules.run_gubbins.output if config["recombination_filter"] == True and count_isolates > 2 else [],
#         homoplasyfinder=rules.run_homoplasyfinder.output[0] if config["homoplasies_filter"] == True and count_isolates > 2 else [],
#         phylogeny_input=phylogeny_input(),
#     output:
#         summary = temp(os.path.join(results_all_samples_dir, all_snp_analysis_dir_name, "temp_coregenome_SNPs_workflow.txt")),
#     conda:
#         envs_folder + "cgsnps.yaml"
#     params:
#         all_snp_analysis_dir_name=all_snp_analysis_dir_name,
#         mask_user_sites=config["snippy-core"]["params"]["masked_regions"],
#         mask_reptitive_regions=config["mask_repitive_sequence_regions"],
#         minimum_alignment_length="-L " + str(config["nucmer"]["params"]["minimum_alignment_length"]) if config["nucmer"]["params"]["minimum_alignment_length"] else "",
#         minimum_percent_identity=" -I " + str(config["nucmer"]["params"]["minimum_percent_identity"]) if config["nucmer"]["params"]["minimum_percent_identity"] else "",
#         coresnpfilter_thr=config["coresnpfilter"]["core_snp_threshold"],

#         #recombination
#         recombination_filter=config["recombination_filter"],
#         homoplasies_filter=config["homoplasies_filter"],

#         #remove reference
#         exclude_reference_from_alignment=config["exclude_reference_from_alignment"],

#         #phylogeny
#         which_phalogeny=config["phylogeny_input"],
#         phylogeny_software=config["phylogeny_software"] if config["phylogeny_software"] else "FastTree",  #default as in the above
#         final_tree = rules.midpoint_tree_rooting.output.tre if count_isolates > 4 else "",
#         #clustering
#         snpdists=str(rules.snpdists.output.snpdists) if count_isolates > 2 else "",
#         hclust= rules.clustering_with_hclust.output.clusters_tsv if os.path.exists(rules.clustering_with_hclust.output.clusters_tsv) else "",
#         fastbaps=rules.clustering_with_fastbaps.output.fastbaps_tsv if os.path.exists(rules.clustering_with_fastbaps.output.fastbaps_tsv) else "",
#     script:
#         bin_dir + "write_snp_workflow.sh"



# rule write_report_script_snp_analysis:
#     input:
#         aggregate_inputs_after_aln,
#         workflow = rules.write_snp_workflow.output.summary,
#     output:
#         report = temp(reports_dir + "snps_and_phylogeny_script.Rmd"),
#     threads: 2
#     params:
#         working_dir=working_dir,
#         core_genome_text = rules.snippy_core.output.res,
#         pairwise_distances = rules.snpdists.output[0],
#         phylogeny_image= rules.phylogeny_image.output.svg 
#              if count_isolates > 3 else "",
#         clusters_tsv=rules.clustering_with_hclust.output[0] 
#             if config["clustering_with_hclust"] == True and count_isolates > 3 else "",
#         fastbaps=rules.clustering_with_fastbaps.output[0] 
#             if config["clustering_with_fastbaps"] == True and count_isolates > 3 else "",
#     script:
#         bin_dir + "write_report_script_snp_analysis.sh"




