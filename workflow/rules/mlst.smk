"""
Copyright (c) 2023, Mostafa Abdel-Glil (mostafa.abdelglil@fli.de); Clostyper (https://gitlab.com/FLI_Bioinfo/clostyper)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


if config["update_mlst_database"] == True:

    ruleorder: update_mlst_pubmlst > do_not_update_mlst_db

MLST_software = config.get("MLST_software", "mlst")

species=config["species"] if config["species"] else "none"

def get_mlst_scheme(species):
    """
    Retrieve the MLST scheme for mlst and stringmlst.
    """
    # Defaults from the configuration
    mlst_scheme = config.get("mlst", {}).get("scheme")
    string_mlst_scheme = config.get("stringMLST", {}).get("species")
    
    # Species-specific configuration
    if species == "cdifficile":
        mlst_scheme = mlst_scheme or "cdifficile"
        string_mlst_scheme = string_mlst_scheme or "Clostridioides difficile"
    elif species == "cperfringens":
        mlst_scheme = mlst_scheme or "cperfringens"
        string_mlst_scheme = string_mlst_scheme or "Clostridium perfringens"
    elif species == "cbotulinum":
        mlst_scheme = mlst_scheme or "cbotulinum"
        string_mlst_scheme = string_mlst_scheme or "Clostridium botulinum"
    elif species == "csepticum":
        mlst_scheme = mlst_scheme or "csepticum"
        string_mlst_scheme = string_mlst_scheme or "Clostridium septicum"
    else:
        # For unrecognized species, use default settings
        mlst_scheme = mlst_scheme or ""
        string_mlst_scheme = string_mlst_scheme or ""

    # Prefix mlst_scheme with "--scheme"
    mlst_scheme = f"--scheme {mlst_scheme}" if mlst_scheme else ""

    # Validate the scheme configuration for stringmlst
    if not string_mlst_scheme and config["MLST"] == True and config["MLST_software"] == "stringmlst":
        sys.exit(RED + f"Error: Species for stringMLST is not provided in the config file. Please add it!"+ NOFORMAT )

  
    return mlst_scheme, string_mlst_scheme

# Example usage
mlst_scheme, string_mlst_scheme = get_mlst_scheme(species)


def MLST():
    if config["MLST"] == True:
        MLST = []
        if config["MLST_software"] == "mlst":
            MLST.append(expand(rules.mlst_per_sample.output, sample =SAMPLES))
            MLST.append(expand(rules.mlst_per_sample.output, sample =GENOMES))
            MLST.append(rules.collect_mlst.output)
            MLST.append(rules.mlst_version.output.version)
        elif config["MLST_software"] == "stringmlst":
            MLST.append(rules.collect_stringMLST.output)
            MLST.append(rules.stringMLST_version.output.version)
        else:
            sys.exit(RED + f"MLST tool was not defined. Choose between 'mlst' and 'stringmlst'" + NOFORMAT)        

        return MLST
    else:
        return []



"""
mlst - seemann
"""



rule update_mlst_pubmlst:
    output:
        blast_file=temp(ensure(temporary_todelete + "mlstDB/blast/mlst.fa", non_empty=True)),
        db_dir=temp(directory(temporary_todelete + "mlstDB/pubmlst")),
    conda:
        envs_folder + "mlst.yaml"
    threads: 4 # do not increase as pubmlst will refuse so many requests
    message:
        "Downloading latest pubmlst database for MLST typing"
    params:
        blast_dir=temp(directory(temporary_todelete + "mlstDB/blast")),
        db_dir=temp(temporary_todelete + "mlstDB"),
    log:
        results_all_samples_dir + all_mlst_dir_name + "/mlst.log",
    script:
        bin_dir + "update_mlst_seemann.sh"




rule do_not_update_mlst_db:
    output:
        blast_file=temp(ensure(temporary_todelete + "mlstDB/blast/mlst.fa", non_empty=True)),
        db_dir=temp(directory(temporary_todelete + "mlstDB/pubmlst")),
    conda:
        envs_folder + "mlst.yaml"
    threads: 4 
    params:
        db_dir=temp(temporary_todelete + "mlstDB"),
    log:
        results_all_samples_dir + all_mlst_dir_name + "/mlst.log",
    shell:
        r"""
        mkdir -p {output.db_dir}
        mlst_path=$(dirname `which mlst`)
        cp -a "$mlst_path"/../db/. {params.db_dir}
        """


rule mlst_per_sample:
    input:
        contig = assembly_all + "{sample}.fasta",
        blast_file = rules.update_mlst_pubmlst.output.blast_file if config ["update_mlst_database"] == True else rules.do_not_update_mlst_db.output.blast_file,
        db_dir = rules.update_mlst_pubmlst.output.db_dir if config ["update_mlst_database"] == True else rules.do_not_update_mlst_db.output.db_dir,
    output:
        results_per_sample_dir + "{sample}/MLST/{sample}_mlst.tsv",
    conda:
        envs_folder + "mlst.yaml"
    log:
        results_per_sample_dir + "{sample}/MLST/{sample}_mlst.log",
    message:
        "Running mlst on {wildcards.sample}"
    params:
        # sample_name = "{sample}",
        novel = "--novel " + results_per_sample_dir + "{sample}/MLST/{sample}_novel.fasta",
        scheme = mlst_scheme,
        mlst_params = config.get("mlst", {}).get("params", {}).get("args")  or "",
    shell:
        r"""
        mlst {input.contig} \
            --datadir {input.db_dir} \
            --blastdb {input.blast_file} \
            --label {wildcards.sample} \
            {params.scheme} \
            {params.mlst_params} \
            {params.novel} > {output} 2> {log}
        """


rule collect_mlst_files:
    input:
        expand(rules.mlst_per_sample.output, sample=SAMPLES), 
        expand(rules.mlst_per_sample.output, sample=GENOMES),
    output:
        FOF=temp(results_all_samples_dir + all_mlst_dir_name + "/samples_path_mlst.tsv"),
        tmp=temp(results_all_samples_dir + all_mlst_dir_name + "/tmp_mlst.tsv"),
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        echo -ne "" > {output.FOF} &&
        for path in {input}; do
            if [[ -s $path ]]; then echo -ne "$path\n" >> {output.FOF}; fi
        done

        csvtk concat --quiet  -tH --infile-list {output.FOF} -C "$" -T -i -E   | 
            csvtk add-header --quiet -t  | 
            csvtk rename --quiet  -t -f 1,2,3 -n Sample,Scheme,ST -o {output.tmp} 
        """


rule collect_mlst:
    input:
        rules.collect_mlst_files.output.tmp
    output:
        tsv=results_all_samples_dir + all_mlst_dir_name + "/mlst.tsv",
        xlsx=results_all_samples_dir + all_mlst_dir_name + "/mlst.xlsx",
    conda:
        envs_folder + "data_parse.yaml"
    shell:
        r"""
        [[ $(csvtk ncol  -t {input} ) -gt 3 ]] && 
        csvtk rename2 --quiet  -t -f -1,-2,-3 -p '.+' -r Allele {input} -o {output.tsv} || cp {input} {output.tsv}

        csvtk csv2xlsx -t {output.tsv} -o {output.xlsx}
        """


rule mlst_version:
    output:
        version = os.path.join(results_all_samples_dir, all_mlst_dir_name, "versions/mlst_version.txt"),
    conda:
        envs_folder + "mlst.yaml"
    shell:
        r"""
        mlst --version > {output.version}
        """

"""
stringmlst - read-based mlst
"""

rule update_MLSTDB_w_stringMLST:
    output:
        db_dir=temp(directory(os.path.join(temporary_todelete, "stringMLST_dbs"))),
    conda:
        envs_folder + "stringmlst.yaml"
    threads: 2
    message:
        "Downloading latest PubMLST for use with stringMLST"
    log:
        os.path.join(results_all_samples_dir, all_mlst_dir_name, "stringMLST.log"),
    params:
        species = string_mlst_scheme,
        args = (
            config.get("stringMLST", {}).get("getMLST_args") or ""
        ),
    shell:
        r"""
        stringMLST.py --getMLST  --species="{params.species}" -P {output.db_dir}/kmer {params.args}  &> {log}
        stringMLST.py --buildDB  -c {output.db_dir}/kmer_config.txt -P {output.db_dir}/kmer -a {log}
        """


rule stringMLST_version:
    output:
        version = os.path.join(results_all_samples_dir, all_mlst_dir_name, "versions/stringMLST_version.txt"),
    conda:
        envs_folder + "stringmlst.yaml"
    shell:
        r"""
        stringMLST.py -v > {output.version}
        """


rule MLST_with_stringmlst:
    input:
        r1=lambda wildcards: get_illumina_r1(wildcards),
        r2=lambda wildcards: get_illumina_r2(wildcards),
        db_dir=rules.update_MLSTDB_w_stringMLST.output.db_dir,
    output:
        res = os.path.join(results_per_sample_dir, "{sample}/MLST/{sample}_stringmlst.tsv"),
        temp = temp(os.path.join(results_per_sample_dir, "{sample}/MLST/{sample}_stringmlst_temp.tsv")),
    conda:
        envs_folder + "stringmlst.yaml"
    message:
        "Running stringMLST on {wildcards.sample}"
    log:
        results_per_sample_dir + "{sample}/MLST/{sample}_stringmlst.log",
    params:
        mlst_dir = directory(os.path.join(results_per_sample_dir, "{sample}", "MLST")),
        name = "{sample}",
        args = config.get("stringMLST", {}).get("predict_args") or ""
    shell:
        r"""
        mkdir -p {params.mlst_dir}
        stringMLST.py --predict -1 {input.r1} -2 {input.r2} -p --prefix {input.db_dir}/kmer {params.args}  --output {output.temp}  2> {log}
        
        [[ -s {output.temp} ]] && cat  {output.temp} | csvtk replace -f 1 -t -p "(.+)" -r "{params.name}" > {output.res}
        """


use rule MLST_with_stringmlst as MLST_with_stringmlst_assemblies with:
    input:
        r1=rules.simulate_reads_from_assemblies.output.r1,
        r2=rules.simulate_reads_from_assemblies.output.r2,
        db_dir=rules.update_MLSTDB_w_stringMLST.output.db_dir,
    output:
        res =  os.path.join(results_per_sample_dir, "{genome}/MLST/{genome}_stringmlst.tsv"),
        temp = temp(os.path.join(results_per_sample_dir, "{genome}/MLST/{genome}_stringmlst_temp.tsv")),
    conda:
        envs_folder + "stringmlst.yaml"
    message:
        "Running stringMLST on {wildcards.genome}"
    log:
        results_per_sample_dir + "{genome}/MLST/{genome}_stringmlst.log",
    params:
        mlst_dir = directory(os.path.join(results_per_sample_dir, "{genome}", "MLST")),
        name = "{genome}",
        args = config.get("stringMLST", {}).get("predict_args") or ""


rule collect_stringMLST:
    input:
        expand(rules.MLST_with_stringmlst.output.res, sample =SAMPLES),
        expand(rules.MLST_with_stringmlst_assemblies.output.res, genome=GENOMES),
    output:
        FOF=temp(results_all_samples_dir + all_mlst_dir_name + "/samples_path_mlst.tsv"),
        tsv=results_all_samples_dir + all_mlst_dir_name + "/mlst.tsv",
        xlsx=results_all_samples_dir + all_mlst_dir_name + "/mlst.xlsx",
    conda:
        envs_folder + "stringmlst.yaml"
    shell:
        r"""
        echo -ne "" > {output.FOF} &&
        for path in {input}; do
            if [[ -s $path ]]; then echo -ne "$path\n" >> {output.FOF}; fi
        done

        csvtk concat -t --infile-list {output.FOF} -C "$" -T -i -E  -I --out-file {output.tsv}
        csvtk csv2xlsx -t {output.tsv} -o {output.xlsx}
        """


# rule write_report_script_mlst:
#     input:
#         MLST(),
#     output:
#         report  = temp(reports_dir + "mlst_script.Rmd"),
#     threads: 2
#     params:
#         results_dir=results_all_samples_dir,
#         reports_dir=reports_dir,
#         mlst_tool=config["MLST_software"],
#         mlst=results_all_samples_dir + all_mlst_dir_name + "/mlst.tsv",
#         caption_mlst="",
#     script:
#         bin_dir + "write_report_script_mlst.sh"

