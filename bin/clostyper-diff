#!/usr/bin/env bash

#Friedrich-Loeffler-Institut (https://www.fli.de/), IBIZ
#date: April, 2, 2022
#Author: Mostafa Abdel-Glil (mostafa.abdelglil@fli.de)

set -e 

# >>>>>>>>>>>>>>>>>>>>>>>> variables >>>>>>>>>>>>>>>>>>>>>>>>

# general variables
PROGNAME="clostyper diff"
QUIET=${QUIET:-false}
SAVE_LOG=${SAVE_LOG:-true}
AUTORUN=${AUTORUN:-true}

FORCE=${FORCE:-false} 
OVERWRITE=${OVERWRITE:-false} 

SNAKEMAKE_ARGUMENTS=""

# Difficile variables
DIFF_CLADE_DESIGNATION=${C_DIIFICILE_CLADES:-True}
DIFF_TOXIN_TYPES=${C_DIFFICILE_TXOINS:-True}
DIFF_MOBILE_ELEMENTS=${C_DIFFICILE_MOBILE_ELEMENTS:-False}
DIFF_RESISTANCE_MUTATIONS=${C_DIFFICILE_RESISTNACE_MUTATIONS:-False}

TRANSPOSONS_COVERAGE=80
PLASMIDS_COVERAGE=80
PHAGES_COVERAGE=80


# <<<<<<<<<<<<<<<<<<<<<<<< variables <<<<<<<<<<<<<<<<<<<<<<<<


#make a help MSG and pass arguments (arguments will overwrite the parametrs in $BASHCONFIG)
usage() {
  msg_ns ""
  msg_ns $ORANGE"Clostyper: Clostridia characterization and typing pipeline"$NOFORMAT
  msg_ns $PURPLE"Clostyper defaults are read from: ${CLOSTYPER_DEFAULTS}:"$NOFORMAT
  msg_ns ""
  msg_ns "------------------------------------------------------------------------------"
  msg_ns "Version    : $VERSION"
  msg_ns "Author     : $AUTHOR <$Email>"
  msg_ns "Source code: $URL"
  msg_ns "------------------------------------------------------------------------------"
  msg_ns ""
  msg_ns $BOLD"USAGE:"$NOFORMAT
  msg_ns "   $PROGNAME -a sample.fa [sample2.fa sample3.fa] -o <working_dir>  # samples in fasta format"
  msg_ns "   $PROGNAME -i <sample_table.csv> -o <working_dir>                 # read samples from a table"
  msg_ns "------------------------------------------------------------------------------"
  msg_ns ""
  msg_ns $BOLD"INPUT:"$NOFORMAT
  msg_ns "   -i, --input_table XXX      Pattern: sample,data_type,illumina_read1,illumina_read2,genome"
  msg_ns "                               ${PURPLE}  TIP: use 'clostyper prepare' to prepare this table (recommended)${NOFORMAT}"
  msg_ns "   -a,  --assembly    XXX     (Alternative to -i) Genome assembly in fasta format: [ID].{fasta,fna,fa} - must be uncompressed"
  msg_ns ""
  msg_ns $BOLD"OUTPUT:"$NOFORMAT
  msg_ns "   -o, --output-directory XXX Working directory (default: output_dir_[time stamp]/)"
  msg_ns "   -w, --overwrite            Append the results of new samples to an existing directory"
  msg_ns "   -F, --force                (Experimental) Replace all the results in an existing directory"
  msg_ns ""
  msg_ns $BOLD"SOFTWARE AND MAJOR SETTINGS:"$NOFORMAT
  msg_ns "   --clade BOOL               [True|False] - assign calde based on MLST results and mash distance (Default: True)"
  msg_ns "   --toxin_types BOOL         [True|False] - report the toxins (A,B,CDT) and toxin subtypes (A,B) (Default: True)"
  # msg_ns "   --resistance  BOOL         [True|False] - report the resistance genes with AMRFinderPlus database"
  msg_ns "   --res_mutations  BOOL      [True|False] - report resistance mutations with data from clostyper database (Default: False)"
  msg_ns "   --mobile_elements  BOOL    [True|False] - report transposons, plasmids and phages with clostyper database (Default: False)"
  msg_ns "   --transposons_coverage N   Coverage cutoff for reporting transposons (Default: 80)"
  msg_ns "   --plasmids_coverage N      Coverage cutoff for reporting plasmids (Default: 80)"
  msg_ns "   --phages_coverage N        Coverage cutoff for reporting phages (Default: 80)"
  msg_ns "   --cgmlst  BOOL             [True|False] - perform cgMLST analysis and identify similar profiles in public data"
  msg_ns "${PURPLE}                                 TIP: Use 'clostyper cgmlst' to control settings of cgMLST (recommended)${NOFORMAT}"
  msg_ns "   --pcd-metro  BOOL          (Experimental) [True|False] - report if pCD-METRO [metronidazole resistance plasmid] is present "
  msg_ns ""
  msg_ns $BOLD"DATABASES:"$NOFORMAT
  msg_ns "${PURPLE}     TIP: Use 'clostyper download' to download databases (recommended)${NOFORMAT}"
  msg_ns "   -db, --clostyper_db        Path to clostyper DB (Default: '$CLOSTYPER_DATABASE')"
  msg_ns ""
  msg_ns $BOLD"GENERAL:"$NOFORMAT
  msg_ns "   -X, --do_not_run           Do not execute snakemake after configuration (default OFF) - useful for config inspection"
  msg_ns "   -t, --threads   N          Number of threads to use when executing the workflow (default: $THREADS)"
  msg_ns "   -S, --screen               Start a GNU Screen session for the workflow execution. Screen command must be callable"
  msg_ns "   -q, --quiet                Suppress $PROGNAME messages. Report only warnings, errors and the snakemake call"
  msg_ns "   -h, --help                 Show this help message and exit"
  msg_ns ""
}




POSITIONAL=()
if [ $# == 0 ]; then
  usage
  exit 0
fi

# As defaults are read from default.yaml this cause a delay of displaying help
# Split parsing the help message seprately tp avoid delay

while (( $# > 0 )); do
  case "${1}" in
        "" | -h | --help)
          usage
          exit 0
          ;;
          
        --version)
          echo -e "$VERSION"
          exit 0
          ;;

        --citation)
          echo -e "$CITATION"
          exit 0
          ;;
    *) 
    POSITIONAL+=("${1}")
    shift
    ;;
  esac
done


set -- "${POSITIONAL[@]}" # restore positional params


parse_params() {
  POSITIONAL=()
  while [[ $# -gt 0 ]]; do
    key="$1"
    case "${1-}" in
      "" | -h | --help | --citation | --version)
      # Do not set up trap for these flags
      ;;

    *)
      # Set up trap for other cases
      trap '[[ $? -eq 0 ]] && [[ "${ENFORCE_MASTER_MODULES}" != true ]] && cleanup' EXIT
      ;;
    esac

    case "${1-}" in
    -a | --assemblies)
      shift
          
      if [[ $# -gt 0 && ! "$1" =~ ^- ]]; then
        # Collect subsequent arguments 
        while [[ $# -gt 0 ]] && [[ ! "$1" =~ ^- ]]; do
            FILE_ARGS+=("$1")
            shift
        done
        
        FASTAS="${FILE_ARGS[*]}"

      else
        die "Please provide an argument for -a/--assemblies - exit"
      fi
      ;;


    -i | --input_table)
      if [[ "${2-}" && ! "${2-}" =~ ^- ]]; then
        INPUT_TABLE="${2-}"
        shift 2
      else
        die "Please provide an argument for -i/--input_table - exit"
      fi
      ;;


    -o | --output-directory)
      if [[ "${2-}" ]]; then
        OUTDIR=$(remove_trailing_slash "${2-}")
        shift 2
      fi
      ;;

    -w | --overwrite)
      OVERWRITE=true
      shift
      ;;

    -F | --force)
      FORCE=true
      shift
      ;;

    -q | --quiet)
      QUIET=true
      shift
      ;;

    -X | --do_not_run) 
      AUTORUN=false
      shift
      ;;

    -t | --threads)
      if [[ "${2-}" ]]; then
        THREADS="${2-}"
        if ! [[ $THREADS =~ $ISNUMBER ]]; then
          die "$THREADS is not a number.
          Please indicate the number of threads to be used with '--threads'"
        fi
        shift 2
      else
        die "Please indicate the number of threads to be used with '--threads' - exit"
      fi
      ;;

    -S | --screen)
      SCREEN=true
      shift
      ;;

     -db | --clostyper_db)
      if [[ -e "${2-}" ]]; then
        CLOSTYPER_DATABASE="${2-}"
        CLOSTYPER_DATABASE="$(realpath $CLOSTYPER_DATABASE)"
        shift 2
      else
        die "Please provide a correct path to clostyper database using '--clostyper_db' - exit"
      fi
      ;;

    --clade)
      if [[ "${2-}" ]]; then
        DIFF_CLADE_DESIGNATION=$(to_lowercase "$2")
        case "$DIFF_CLADE_DESIGNATION" in
          true)
            DIFF_CLADE_DESIGNATION="True"
            ;;
          false)
            DIFF_CLADE_DESIGNATION="False"
            ;;
          *)
            die "Invalid argument for --clade. Please provide either 'true' or 'false'."
            ;;
        esac
        shift 2
      else
        die "Missing argument for --clade. Please provide either 'true' or 'false'."
      fi
      ;;

    --toxin_types)
      if [[ "${2-}" ]]; then
        DIFF_TOXIN_TYPES=$(to_lowercase "$2")
        case "$DIFF_TOXIN_TYPES" in
          true)
            DIFF_TOXIN_TYPES="True"
            ;;
          false)
            DIFF_TOXIN_TYPES="False"
            ;;
          *)
            die "Invalid argument for --toxin_types. Please provide either 'true' or 'false'."
            ;;
        esac
        shift 2
      else
        die "Missing argument for --toxin_types. Please provide either 'true' or 'false'."
      fi
      ;;

    --mobile_elements)
      if [[ "${2-}" ]]; then
        DIFF_MOBILE_ELEMENTS=$(to_lowercase "$2")
        case "$DIFF_MOBILE_ELEMENTS" in
          true)
            DIFF_MOBILE_ELEMENTS="True"
            ;;
          false)
            DIFF_MOBILE_ELEMENTS="False"
            ;;
          *)
            die "Invalid argument for --mobile_elements. Please provide either 'true' or 'false'."
            ;;
        esac
        shift 2
      else
        die "Missing argument for --mobile_elements. Please provide either 'true' or 'false'."
      fi
      ;;



 --cgmlst)
      if [[ "${2-}" ]]; then
        CGMLST_ANALYSIS=$(to_lowercase "$2")
        case "$CGMLST_ANALYSIS" in
          true)
            CGMLST_ANALYSIS="True"
            ;;
          false)
            CGMLST_ANALYSIS="False"
            ;;
          *)
            die "Invalid argument for --cgmlst. Please provide either 'true' or 'false'."
            ;;
        esac
        shift 2
      else
        die "Missing argument for --cgmlst. Please provide either 'true' or 'false'."
      fi
      ;;

    --res_mutations)
      if [[ "${2-}" ]]; then
        DIFF_RESISTANCE_MUTATIONS=$(to_lowercase "$2")
        case "$DIFF_RESISTANCE_MUTATIONS" in
          true)
            DIFF_RESISTANCE_MUTATIONS="True"
            ;;
          false)
            DIFF_RESISTANCE_MUTATIONS="False"
            ;;
          *)
            die "Invalid argument for --res_mutations. Please provide either 'true' or 'false'."
            ;;
        esac
        shift 2
      else
        die "Missing argument for --res_mutations. Please provide either 'true' or 'false'."
      fi
      ;;

    --transposons_coverage)
      if [[ "${2-}" =~ ^[0-9]+$ ]]; then
        TRANSPOSONS_COVERAGE="$2"
        shift 2
      else
        die "Invalid argument for --transposons_coverage. Please provide a valid number."
      fi
      ;;

    --plasmids_coverage)
      if [[ "${2-}" =~ ^[0-9]+$ ]]; then
        PLASMIDS_COVERAGE="$2"
        shift 2
      else
        die "Invalid argument for --plasmids_coverage. Please provide a valid number."
      fi
      ;;

    --phages_coverage)
      if [[ "${2-}" =~ ^[0-9]+$ ]]; then
        PHAGES_COVERAGE="$2"
        shift 2
      else
        die "Invalid argument for --phages_coverage. Please provide a valid number."
      fi
      ;;

    --pcd-metro)
      if [[ "${2-}" ]]; then
        PCD_METRO=$(to_lowercase "$2")
        case "$PCD_METRO" in
          true) PCD_METRO="True" ;;
          false) PCD_METRO="False" ;;
          *) die "Invalid argument for --pcd-metro. Please provide either 'true' or 'false'." ;;
        esac
        shift 2
      else
        die "Missing argument for --pcd-metro. Please provide either 'true' or 'false'."
      fi
      ;;


    -E | --enforce_master) # Hidden argument used within the 'clostyper all' 
      ENFORCE_MASTER_MODULES=true
      SAVE_LOG=false
      shift
      ;;


    -?*) 
      die "Unknown option: '$1' 
      Please use '$PROGNAME --help' to get available options - exit" 
      ;;
    
    *) 
      POSITIONAL+=("${1}")
      break 
    ;;
    esac
  done
  set -- "${POSITIONAL[@]}" #restore positional parameters
  return 0
}



# >>>>>>>>>>>>>>>>>>>>>>>> collect arguments >>>>>>>>>>>>>>>>>>>>>>>>

# Parse arguments with new defaults
parse_params "$@"

# <<<<<<<<<<<<<<<<<<<<<<<< collect arguments <<<<<<<<<<<<<<<<<<<<<<<<



# >>>>>>>>>>>>>>>>>>>>>>>> check mandatory arguments >>>>>>>>>>>>>>>>>>>>>>>>

# Check if none of the required inputs are provided

if [[ -z "${INPUT_TABLE}" && -z "${FASTAS}" ]]; then
  die "You must provide one of the following arguments: '-i' or '-a'"
elif [[ ! -z "${INPUT_TABLE}" && ! -z "${FASTAS}" ]]; then
  die "You cannot use both '-i' and '-a' together. "
fi


# #check the fastx data
if [[ -n "${FASTAS}" ]]; then
  mkdir -p ./.clostyper/temp || die "unable to create temporary directory: './.clostyper/temp'"
  PREPARED_TABLE=$(mktemp -p ./.clostyper/temp tempfile.XXXXXXXX)
  bash $BINDIR/clostyper-prepare -a ${FASTAS} -F -o  ${PREPARED_TABLE} || die "unable to create sample file from the provided assemblies"
  INPUT_TABLE=${PREPARED_TABLE}
fi

# database

if [[ ! -d "$CLOSTYPER_DATABASE" ]];  then
  die "Please provide the path to clostyperDB"
fi 

if [[ ! -f "$CLOSTYPER_DATABASE/version.txt" ]]; then
  die "Could not parse clostyperDB version 'version.txt' in: $CLOSTYPER_DATABASE."
fi

if [ "$(head -n 1 "$CLOSTYPER_DATABASE/version.txt")" != "2025-02-10.1" ]; then
  die "incorrect version of clostyper database. It must be: 2025-02-10.1"
fi

# 
if [[ -f "${INPUT_TABLE}" ]] && [[ -s "${INPUT_TABLE}" ]] ; then
  :
else 
  die "The input sample table does not exits or is empty: '${INPUT_TABLE}'"
fi

# <<<<<<<<<<<<<<<<<<<<<<<< check mandatory arguments <<<<<<<<<<<<<<<<<<<<<<<<



# >>>>>>>>>>>>>>>>>>>>>>>> optional arguments >>>>>>>>>>>>>>>>>>>>>>>>

#check optional arguments
#output directory - set to default if $OUTDIR is not set/is empty

OUTDIR="${OUTDIR:-$OUTDIR_DEFAULT}"

[[ "$FORCE" == true ]] && SNAKEMAKE_ARGUMENTS+=" --forceall"

[[ "$QUIET" != true ]] && SNAKEMAKE_ARGUMENTS+=" --printshellcmds"


# <<<<<<<<<<<<<<<<<<<<<<<< optional arguments <<<<<<<<<<<<<<<<<<<<<<<<





# >>>>>>>>>>>>>>>>>>>>>>>> config >>>>>>>>>>>>>>>>>>>>>>>>

#=============================================================
# Writing the config file
#=============================================================

#writing config file
write_config_file(){
  if [[ -d "${OUTDIR}" ]] && [[ -f "$OUTDIR"/tmp_format_samples_table.success ]]; then
    [[ "$ENFORCE_MASTER_MODULES" != true ]] && msg "Writing the config.yaml: '$OUTDIR/config.yaml'"

    local CONFIG_MODULE_FILE=$(mktemp ${OUTDIR}/.tempfile.XXXXXX)
    local CONFIG_DATABASES_FILE=$(mktemp ${OUTDIR}/.tempfile.XXXXXX)
    local CONFIG_SETTINGS_FILE=$(mktemp ${OUTDIR}/.tempfile.XXXXXX)

    cat <<CONFIG_MODULES > "${CONFIG_MODULE_FILE}"
snakemake_folder:         ${RULESDIR}/
working_dir:              ${OUTDIR}/
reference:                ${REFERENCE:-''}
species:                  cdifficile
samples:                  ${SAMPLETABLE}

#MODULES
RAWREADS_QUALITY:        False
GENOME_ASSESSMENT:       False
RESISTANCE:              False
VIRULENCE:               False
MLST:                    False
SNP_ANALYSIS:            False
COREGENOME_MLST:         $([[ "${CGMLST_ANALYSIS}" == "True" ]] && echo "True" || echo "False")
PANGENOME:               False
REPORT:                  True
C_DIFFICILE_MODULE:      True
C_PERFRINGENS_MODULE:    False

CONFIG_MODULES

    cat <<CONFIG_DATABASES >"${CONFIG_DATABASES_FILE}"

# Database_locations
clostyper_database: ${CLOSTYPER_DATABASE}

CONFIG_DATABASES

    cat <<CONFIG_SETTINGS >"${CONFIG_SETTINGS_FILE}"

#RESISTANCE
C_DIIFICILE_CLADES:                        ${DIFF_CLADE_DESIGNATION}
C_DIFFICILE_TXOINS:                        ${DIFF_TOXIN_TYPES}
C_DIFFICILE_MOBILE_ELEMENTS:               ${DIFF_MOBILE_ELEMENTS}
C_DIFFICILE_RESISTNACE_MUTATIONS:          ${DIFF_RESISTANCE_MUTATIONS}


c_difficile_db:
 transposons:
  coverage:                                 ${TRANSPOSONS_COVERAGE}
 plasmids:
  coverage:                                 ${PLASMIDS_COVERAGE}
 phages:
  coverage:                                 ${PHAGES_COVERAGE}

CONFIG_SETTINGS

  if [[ -f "${CONFIG_MODULE_FILE}" ]] && [[ -f "${CONFIG_DATABASES_FILE}" ]] && [[ -f "${CONFIG_SETTINGS_FILE}" ]]; then
    
    if [[ ${ENFORCE_MASTER_MODULES} != true ]]; then
      echo "" > "$OUTDIR/config.yaml"
      cat "${CONFIG_MODULE_FILE}" "${CONFIG_DATABASES_FILE}" "${CONFIG_SETTINGS_FILE}" >> "$OUTDIR/config.yaml"
    else 
      cat "${CONFIG_SETTINGS_FILE}" >> "$OUTDIR/config.settings.cdiff.yaml"
    fi


    # Remove the temporary files 
    timeout 1 rm -f "${CONFIG_MODULE_FILE}" 2>/dev/null || true
    timeout 1 rm -f "${CONFIG_DATABASES_FILE}" 2>/dev/null || true
    timeout 1 rm -f "${CONFIG_SETTINGS_FILE}" 2>/dev/null || true

    [[ -s "$OUTDIR/config.yaml" ]] && touch "$OUTDIR"/tmp_create_config.success

  fi
else 
  die "Trigger file '${OUTDIR}/tmp_format_samples_table.success' does not exit"
fi
}

# <<<<<<<<<<<<<<<<<<<<<<<< config <<<<<<<<<<<<<<<<<<<<<<<<




#=============================================================
#=============================================================
#=================== Start script=============================
#=============================================================
#=============================================================

# >>>>>>>>>>>>>>>>>>>>>>>> log file  >>>>>>>>>>>>>>>>>>>>>>>>

# Set a new file for logging
if [ "$SAVE_LOG" == true ]; then
  echo -ne "" >"$LOG"
  exec > >(
    tee >(sed -u 's/\x1b\[[0-9;]*[mGKH]//g' >>"$LOG") >&2 # disable coloration when logging
  ) 2>&1
fi

# <<<<<<<<<<<<<<<<<<<<<<<< log file  <<<<<<<<<<<<<<<<<<<<<<<<




# >>>>>>>>>>>>>>>>>>>>>>>> script >>>>>>>>>>>>>>>>>>>>>>>>

# If ENFORCE_MASTER_MODULES = True then we are executing 'clostyper all', which will take care of outdir and INPUT_TABLE.

if [[ "$ENFORCE_MASTER_MODULES" != true ]]; then
  
  # Welcome
  WELCOMEMSG
  
  # Display the command used
  display_command "$@" || true
  
  # Check if deps are ok
  check_dep || die "failed to check the $PROGNAME dependencies ...."
  
  # Create output directory and process input table
  create_output_directory
  

  # Process input table 
  process_input_table
  
  #write the config file 
  write_config_file

  # Run Snakemake pipeline
  run_snakemake_pipeline


  # Display summary table if the pipeline was successful
  if [[ -f "${OUTDIR}/tmp_processes_snakefile.success" ]]; then
    SUMMARY_FILE="${OUTDIR}/3_results_all_samples/C_difficile_results/summary_results_cdifficile.tsv"
    SUMMARY_CGMLST="${OUTDIR}/3_results_all_samples/7_core_genome_MLST/6_matching_strains/summary_matching_cgmlst_and_metadata.tsv"
    
    if [[ -f "$SUMMARY_FILE" ]]; then
      echo ""
      cat "${SUMMARY_FILE}" | csvtk cut --quiet -t -m -f 1-12 |
      csvtk pretty -t -m 2- -W 25 -S simple 2>/dev/null || true
      echo ""
    fi

    if [[ -s "$SUMMARY_CGMLST" ]] && [[ "${CGMLST_ANALYSIS}" == "True" ]] ; then
      bannerColor "Found clusters    " "green" "=" 
      cat "${SUMMARY_CGMLST}" | csvtk cut --quiet -t -m  -f -"matching samples" | csvtk pretty -t -W 25 2>/dev/null || true
      echo ""
    fi

  fi
elif [[ "$ENFORCE_MASTER_MODULES" == true ]]; then 

  msg "'${PROGNAME} -E' is activated ('clostyper all' should be in use) - will write the setting file for the Read QC module"

  # Write config file
  write_config_file

  msg "Finished writing: "${OUTDIR}/config.settings.cdiff.yaml""

fi

# <<<<<<<<<<<<<<<<<<<<<<<< script <<<<<<<<<<<<<<<<<<<<<<<<
