#!/usr/bin/env bash

#Friedrich-Loeffler-Institut (https://www.fli.de/), IBIZ
#date: April, 2, 2022
#Author: Mostafa Abdel-Glil (mostafa.abdelglil@fli.de)

# Check if current directory is accessible
if ! pwd &>/dev/null; then
  echo -ne "\nError: the current directory is not accessible. Change to known directory.\nExit ...\n"
  exit 1
fi

set -e 

pushd . >/dev/null #store the current directory path
# set -Eeuo pipefail # do not continue on errors
DIR="${BASH_SOURCE[0]}" #get the actual location of the script
while [ -h "$DIR" ]; do
  cd "$(dirname "$DIR")"
  DIR="$(readlink "$(basename "$DIR")")"
done #resolve symlink
cd -P "$(dirname "$DIR")"
BINDIR="$(pwd)"
RULESDIR="$BINDIR/../workflow"
popd >/dev/null #restore the stored dir
# PROGNAME=$(basename $0)

export BINDIR RULESDIR
export PATH="$BINDIR:$PATH"

SAVE_LOG= 
QUIET=


# >>>>>>>>>>>>>>>>>>>>>>>> Source scripts >>>>>>>>>>>>>>>>>>>>>>>>

scripts=(
    "$BINDIR/../VERSION"
    "$BINDIR/misc.sh"
    "$BINDIR/deps.sh"
    "$BINDIR/var.sh"
    "$BINDIR/fa_utils.sh"
    "$BINDIR/clostyper-helper"
)


# Source scripts
source_script() {
    local script_path="$1"
    if [ -f "$script_path" ]; then
        source "$script_path"
    else
        echo "Error: Script '$script_path' does not exist." >&2
        exit 1
    fi
}

for script in "${scripts[@]}"; do
    source_script "$script"
done


# <<<<<<<<<<<<<<<<<<<<<<<< Source scripts <<<<<<<<<<<<<<<<<<<<<<<<



# >>>>>>>>>>>>>>>>>>>>>>>> variables >>>>>>>>>>>>>>>>>>>>>>>>

# General variables
PROGNAME="clostyper"
QUIET=${QUIET:-false}
SAVE_LOG=${SAVE_LOG:-true}
AUTORUN=${AUTORUN:-true}

# <<<<<<<<<<<<<<<<<<<<<<<< variables <<<<<<<<<<<<<<<<<<<<<<<<



# >>>>>>>>>>>>>>>>>>>>>>>> trap >>>>>>>>>>>>>>>>>>>>>>>>

# # Trap SIGINT (Ctrl + C) and SIGTERM (termination) for interruptions
trap 'echo ""; cleanup; exit 1' SIGINT SIGTERM

# # # Trap errors for unexpected failures
trap 'cleanup; exit 1' ERR

# <<<<<<<<<<<<<<<<<<<<<<<< trap <<<<<<<<<<<<<<<<<<<<<<<<



#activate colors
setup_colors

#make a help MSG and pass arguments (arguments will overwrite the parametrs in $BASHCONFIG)
usage() {
  msg_ns ""
  msg_ns $ORANGE"Clostyper: Clostridia characterization and typing pipeline"$NOFORMAT
  msg_ns ""
  msg_ns "------------------------------------------------------------------------------"
  msg_ns "Version    : $VERSION"
  msg_ns "Author     : $AUTHOR <$Email>"
  msg_ns "Source code: $URL"
  msg_ns "------------------------------------------------------------------------------"
  msg_ns ""
  msg_ns $BOLD"GENERAL OPTIONS:"$NOFORMAT
  msg_ns "   -h, --help         Show this help message and exit"
  msg_ns "   -v, --version      Show ${PROGNAME} version number and exit"
  msg_ns "   --citation         Show $PROGNAME citation and exit"
  msg_ns "   --check_dep        Check if ${PROGNAME}'s dependencies are installed"
  msg_ns ""
  msg_ns $BOLD"COMMANDS:"$NOFORMAT
  msg_ns "${PURPLE}--- Commands for configuration${NOFORMAT}"
  msg_ns "   defaults          ${LIGHT_GREY}Write and save databases and tool parameters as defaults for ${PROGNAME}${NOFORMAT}"
  msg_ns "   download          ${LIGHT_GREY}Download ${PROGNAME} database and other databases used by clostyper${NOFORMAT}"
  msg_ns "   prepare           ${LIGHT_GREY}Prepare the sample sheet for ${PROGNAME}${NOFORMAT}"
  msg_ns ""
  msg_ns "${PURPLE}--- Main command${NOFORMAT}"
  msg_ns "   all               ${LIGHT_GREY}Combine different analysis modules of ${PROGNAME}${NOFORMAT}"
  msg_ns ""
  msg_ns "${PURPLE}--- Commands for general analysis${NOFORMAT}"
  msg_ns "   read_qc           ${LIGHT_GREY}Evaluate the quality of the raw reads without assembly${NOFORMAT}"
  msg_ns "   genome_qc         ${LIGHT_GREY}Assemble, annotate and evaluate the quality of the genomes${NOFORMAT}"
  msg_ns "   amr               ${LIGHT_GREY}Report resistance genotype${NOFORMAT}"
  msg_ns "   vir               ${LIGHT_GREY}Report virulence genes${NOFORMAT}"
  msg_ns "   mlst              ${LIGHT_GREY}Report MLST types ${NOFORMAT}"
  msg_ns "   cgsnp             ${LIGHT_GREY}Perform core genome SNP analysis${NOFORMAT}"
  msg_ns "   cgmlst            ${LIGHT_GREY}Perform core genome MLST analysis ${NOFORMAT}"
  msg_ns "   pangenome         ${LIGHT_GREY}Perform pangenome analysis${NOFORMAT}"
  msg_ns ""
  msg_ns "${PURPLE}--- Commands for Clostridium species${NOFORMAT}"
  msg_ns "   diff              ${GREY}C. difficile analysis ${NOFORMAT}${LIGHT_GREY}-> Perform clade classification, toxintyping,"
  msg_ns "                       and search for mobile elements and specific resistance${NOFORMAT}"
  msg_ns "   perf              ${GREY}C. perfringens analysis ${NOFORMAT}${LIGHT_GREY}-> Report lineages, toxintyping [A-E], and " 
  msg_ns "                       search for important insertion elements, plasmid markers and plasmid types${NOFORMAT}"
  msg_ns "   sept              ${GREY}C. septicum analysis ${NOFORMAT}${LIGHT_GREY}-> Report C. septicum virulence factors${NOFORMAT}"
  msg_ns "   chau              ${GREY}C. chauvoei analysis ${NOFORMAT}${LIGHT_GREY}-> Report C. chauvoei virulence factors${NOFORMAT}"
  msg_ns ""
}



POSITIONAL=()
if [ $# == 0 ]; then
  usage
  exit 0
fi

# As defaults are read from default.yaml this cause a delay of displaying help
# Split parsing the help message seprately tp avoid delay

# Handle --version and --citation
if [[ "$1" == "--version" ]] || [[ "$1" == "-v" ]]; then
    echo "$VERSION"
    exit 0

elif [[ "$1" == "--check_dep" ]]; then
    msg "$PROGNAME will check if dependencies are ok..."
    SAVE_LOG=false 
    check_dep
    exit 0

elif [[ "$1" == "--citation" ]]; then
    echo -e "$CITATION"
    exit 0

elif [[ "$1" == "-h" || "$1" == "--help" ]]; then
    usage
    exit 0

fi


# >>>>>>>>>>>>>>>>>>>>>>>> read defaults >>>>>>>>>>>>>>>>>>>>>>>>

# collect defaults from default.yaml 
read_deafults

# <<<<<<<<<<<<<<<<<<<<<<<< read defaults <<<<<<<<<<<<<<<<<<<<<<<<

# while [[ $# -gt 0 ]]; do
SUBCOMMAND="${1-}"
shift 

case "$SUBCOMMAND" in 
  defaults)
    bash "$BINDIR"/clostyper-defaults "$@"
    ;;
  download)
    bash "$BINDIR"/clostyper-download "$@"
    ;;
  prepare)
    trap - SIGINT ERR TERM
    bash "$BINDIR"/clostyper-prepare "$@"
    ;;  
  all)
    bash "$BINDIR"/clostyper-all "$@"
    ;;
  read_qc)
    bash "$BINDIR"/clostyper-read_qc "$@"
    ;;
  genome_qc)
    bash "$BINDIR"/clostyper-genome_qc "$@"
    ;;
  amr)
    bash "$BINDIR"/clostyper-amr "$@"
    ;;
  vir)
    bash "$BINDIR"/clostyper-vir "$@"
    ;;
  mlst)
    bash "$BINDIR"/clostyper-mlst "$@"
    ;;
  cgsnp)
    bash "$BINDIR"/clostyper-cgsnp "$@"
    ;;
  cgmlst)
    bash "$BINDIR"/clostyper-cgmlst "$@"
    ;;
  pangenome)
    msg_ns "sorry - this module is currently under development"
    exit 0
    # bash "$BINDIR"/clostyper-pangenome "$@"
    ;;
  diff)
    bash "$BINDIR"/clostyper-diff "$@"
    ;;
  perf)
    bash "$BINDIR"/clostyper-perf "$@"
    ;;
  chau)
    bash "$BINDIR"/clostyper-chau "$@"
    ;;
  sept)
    msg_ns "sorry - this module is currently under development"
    exit 0
    # bash "$BINDIR"/clostyper-sept "$@"
    ;;
  -?*) 
    die "Unknown option: '"$SUBCOMMAND"' - Please use '$PROGNAME -h / --help' to get available options - exit"
    ;;
  *) 
    die "Unknown option: '"$SUBCOMMAND"' - Please use '$PROGNAME -h / --help' to get available options - exit"
    ;;
esac

