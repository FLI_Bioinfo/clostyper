#!/usr/bin/env bash 

#Friedrich-Loeffler-Institut (https://www.fli.de/), IBIZ
#date: April, 2, 2022
#Author: Mostafa Abdel-Glil (mostafa.abdelglil@fli.de)

set -a 

# >>>>>>>>>>>>>>>>>>>>>>>> fastx variables >>>>>>>>>>>>>>>>>>>>>>>>

FASTXFORMAT="[ID]_{1,2}.fastq{.gz} [ID]_S*_R{1,2}_001.fastq{.gz} [ID]_R{1,2}.fastq{.gz} OR [ID].{fasta,fna,fa}"


# <<<<<<<<<<<<<<<<<<<<<<<< fastx variables <<<<<<<<<<<<<<<<<<<<<<<<


compress_fastq_files() {
  local FASTXDIR="$1"
  local uncompressed_files=()

  msg "Check if we have fastq uncompressed in: '$FASTXDIR'"

  # Find uncompressed FastQ files in the directory
  while IFS= read -r -d '' file; do
    #append to the `uncompressed_files` array with the `+=` operators
    uncompressed_files+=("$file")
  done < <(find "$FASTXDIR" -maxdepth 1 -type f -name "*.fastq" -print0)

  if [[ ${#uncompressed_files[@]} -gt 0 ]]; then

    # make sure pigz is insalled 
    command -v pigz >/dev/null 2>&1 || die "pigz is not installed. Please install it!"

    #check no compressed files of the uncompressed ones
    for file in "${uncompressed_files[@]}"; do
      local compressed_file="$file.gz"
      if [[ ! -f "$compressed_file" ]]; then
        msg "----detected uncompressed FastQ files in the given input directory"
        msg "----Compressing the file: '$file' "

        # Compress the FastQ files using pigz
        pigz --processes "$THREADS" --fast --keep "$file" || die "Failed to compress the FastQ files in $FASTXDIR. Please compress the files!"
      fi
    done
  fi
}


check_fastx_samples_in_directory() { #FASTXDIR
  # check for different format of fastq and fasta files in given directories
  local FASTXDIR="$1"
  local CHECKSAMPLES
  local unique_files=()

  msg "Check for different format of fastq and fasta files in given directory/directories"
  CHECKSAMPLES=$(find "$FASTXDIR" -maxdepth 1 \( -name "*_S*_R1_001.fastq.gz" -o -name "*_R1*.fastq.gz" -o -name "*_1.fastq.gz" -o -name "*.fasta" -o -name "*.fna" -o -name "*.fa" \) 2>/dev/null | xargs -n 1 basename 2>/dev/null) || die "$PROGNAME cannot determine the samples in '$FASTXDIR'. \nSample names must match the format: $FASTXFORMAT." #\nAlternatively, please provide a sample table with the '-t' flag.\nSample Table Format:\n$SAMPLETABLEFORMAT

  while IFS= read -r file; do
    unique_files+=("$(basename "${file}")")
  done <<<"$CHECKSAMPLES"

  msg "Samples to be processed: $(echo ${CHECKSAMPLES-} | tr '\n' ' ')"

  local num_files=${#unique_files[@]}

  if [[ ${#unique_files[@]} -ge 1 ]]; then
    msg "Detected $num_files samples to process in '$FASTXDIR'"
  fi
}


# write_sample_table_from_fastq() {
#   local FASTXDIR=$1
#   local SAMPLEFILE=$2
#   local Samples=$3
#   local DELIMITER=$DELIMITER
#   local SAMPLETABLE=$SAMPLEFILE


#   for file in $Samples; do # do not use double quotes with $samples - otherwise the loop will read all samples as one
#     sample=$(basename "$file" | awk -F '_S[0-9]|_R1|_1.fastq.gz' '{print $1}')
#     msg "sample: ${sample-}"
#     msg "read 1 path: ${FASTXDIR-}/${file-}"
#     R2=$(basename "$file" | sed -e 's/_R1_001.fastq/_R2_001.fastq/' -e 's/_R1/_R2/' -e 's/_1.fastq/_2.fastq/')
#     msg "read 2 path: ${FASTXDIR-}/${R2-}"

#     if [[ -f "$FASTXDIR/$R2" ]]; then
#       # if zcat -f "$FASTXDIR/$file" | head -n1 | grep -e '@' >/dev/null 2>&1 && zcat -f "$FASTXDIR/$R2" | head -n1 | grep -e '@' >/dev/null 2>&1; then
#       if seqkit head --quiet -n 1 "$FASTXDIR/$file" 2>/dev/null | grep -m1 -q '@' >/dev/null 2>&1 && seqkit head -n 1 "$FASTXDIR/$R2" | grep -m1 -q '@' >/dev/null 2>&1; then
#         echo -e "${sample}${DELIMITER}pairedend${DELIMITER}${FASTXDIR}/${file}${DELIMITER}${FASTXDIR}/${R2}${DELIMITER}${DELIMITER}${DELIMITER}" >>"$SAMPLETABLE"
#       else
#         warn "Files of sample: '$sample' seem corrupted - will not be included"
#       fi
#     elif [[ ! -f "$FASTXDIR/$R2" ]]; then
#       warn "Reverse read of sample: '$sample' is missing - will not be included"
#     fi
#   done
# }

write_sample_table_from_fastq_w_real_path() {
  local FASTXDIR=$1
  local SAMPLEFILE=$2
  local Samples=$3
  local DELIMITER=$DELIMITER
  local SAMPLETABLE=$SAMPLEFILE
  
  for file in $Samples; do # do not use double quotes with $samples - otherwise the loop will read all samples as one
    local sample=$(basename "$file" | awk -F '_S[0-9]+|_R1|_1.fastq.gz' '{print $1}')
    msg "Processing sample: ${sample}"

    # Construct full paths for the reads
    local read1_path="${FASTXDIR}/${file}"
    local read2_file=$(basename "$file" | sed -e 's/_R1_001.fastq.gz/_R2_001.fastq.gz/' -e 's/_R1/_R2/' -e 's/_1.fastq/_2.fastq/')
    local read2_path="${FASTXDIR}/${read2_file}"

    local read1_path=$(realpath "$read1_path")
    local read2_path=$(realpath "$read2_path")

    msg "Read 1 path: ${read1_path}"
    msg "Read 2 path: ${read2_path}"

    msg "check the compression type as gz"

    check_compression_type ${read1_path} gz
    check_compression_type ${read2_path} gz

    if [[ -f "$read2_path" ]]; then
      # Use seqkit to check if the files are valid FASTQ files
      msg "Check if the files are valid FASTQ files"
      if seqkit head --quiet -n 1 "$read1_path" 2>/dev/null | grep -q '@' && seqkit head --quiet -n 1 "$read2_path" 2>/dev/null | grep -q '@'; then
        echo -e "${sample}${DELIMITER}pairedend${DELIMITER}${read1_path}${DELIMITER}${read2_path}${DELIMITER}${DELIMITER}${DELIMITER}" >> "$SAMPLETABLE"
      else
        die "Files for sample '$sample' appear corrupted."
      fi
    else
      die "Reverse read for sample '$sample' is missing."
    fi
  done
  
}

# write_sample_table_from_assemblies() {
#   local FASTXDIR=$1
#   local SAMPLEFILE=$2
#   local Samples=$3
#   local DELIMITER=$DELIMITER
#   local SAMPLETABLE=$SAMPLEFILE

#   for file in $Samples; do
#     local sample=$(echo $file | xargs -n1 basename 2>/dev/null | sed 's/\(.*\)\.gz/\1/' | sed 's/\(.*\)\..*/\1/')
#     msg "sample: ${sample-}"
#     msg "genome path: ${FASTXDIR-}/${file-}"
#     # if zcat -f "$FASTXDIR/$file" | head -n1 | grep -e '>' >/dev/null 2>&1; then
#     if seqkit head --quiet -n 1 "$FASTXDIR/$file" 2>/dev/null | grep -m1 -q '>' >/dev/null 2>&1; then
#       echo -e "${sample}${DELIMITER}genome${DELIMITER}${DELIMITER}${DELIMITER}${FASTXDIR}/${file}${DELIMITER}${DELIMITER}" >>"$SAMPLETABLE"
#     else
#       die "Files of sample: '$sample' seem corrupted"
#     fi
#   done
# }

write_sample_table_from_assemblies_w_real_path() {
  local FASTXDIR=$1
  local SAMPLEFILE=$2
  local Samples=$3
  local DELIMITER=$DELIMITER
  local SAMPLETABLE=$SAMPLEFILE
  local full_file_path

  for file in $Samples; do
    # Extract sample name by removing extensions (.gz, .fasta, .fa, etc.)
    local sample=$(basename "$file" | sed 's/\(.*\)\.gz/\1/' | sed 's/\(.*\)\..*/\1/')
    msg "sample: ${sample}"
    
    local full_file_path="${FASTXDIR}/${file}"
    full_file_path=$(realpath "$full_file_path")

    msg "check the compression type"
    check_compression_type $full_file_path plain

    msg "genome path: ${full_file_path}"

    # Check if the file is a valid genome file (FASTA format, containing '>' for headers)
    msg "Check if the files are valid FASTA files"

    if seqkit head --quiet -n 1 "$full_file_path" 2>/dev/null | grep -m1 -q '>'; then
      echo -e "${sample}${DELIMITER}genome${DELIMITER}${DELIMITER}${DELIMITER}${full_file_path}${DELIMITER}${DELIMITER}" >>"$SAMPLETABLE"
    else
      die "The genome file of sample: '$sample' seems corrupted"
    fi
  done
}


verify_fastx_file_format() {
  local file="$1"
  local expected_format="$2"

  # Ensure the file exists
  if [[ ! -f "$file" ]]; then
    die "File does not exist: $file"
  fi

  # Check if the file is gzipped
  if [[ "$file" == *".gz" ]]; then
    local first_char=$(seqkit head -n 1 "$file" | head -c 1)
  else
    local first_char=$(head -c 1 "$file")
  fi

  # Determine the file format based on the first character
  case "$first_char" in
  ">")
    local detected_format="Fasta"
    ;;
  "@")
    local detected_format="Fastq"
    ;;
  *)
    local detected_format="Unknown"
    ;;
  esac

  # Check if the detected format matches the expected format
  if [[ "$detected_format" == "$expected_format" ]]; then
    msg "File format: $detected_format"
  else
    die "File seems corrupt or not in the correct format. Please check the sample table! \n\nAffected sample: $file"
  fi
}

