#!/usr/bin/env bash 

#Friedrich-Loeffler-Institut (https://www.fli.de/), IBIZ
#date: April, 2, 2022
#Author: Mostafa Abdel-Glil (mostafa.abdelglil@fli.de)

set -a

# >>>>>>>>>>>>>>>>>>>>>>>> Global variables >>>>>>>>>>>>>>>>>>>>>>>>

#define Global variables
CITATION="${AUTHOR} (${YEAR}). Clostyper: Clostridia characterization and typing pipeline (version ${VERSION}). Source code: ${URL}."

ISNUMBER="^[0-9]+$"
TIME_FULL="$(date '+%F %T %Z')"

SAVE_LOG=true
SCREEN=false
QUIET=false
NO_COLOR=false
DELIMITER="," #DONOT change it for now



THREADS=$(nproc)
THREADS=$((THREADS * 75 / 100))
THREADS=$((THREADS > 4 ? THREADS : 4)) # Ternary Expression # https://www.baeldung.com/linux/bash-ternary-conditional-operator

REFERENCE=

# <<<<<<<<<<<<<<<<<<<<<<<< Global variables <<<<<<<<<<<<<<<<<<<<<<<<



# >>>>>>>>>>>>>>>>>>>>>>>> Sample table Variables >>>>>>>>>>>>>>>>>>>>>>>>

REFFORMAT="[ID].{gbk,fasta,gff,embl}"
SAMPLETABLEFORMAT="sample${DELIMITER}data_type${DELIMITER}illumina_read1${DELIMITER}illumina_read2${DELIMITER}genome\nID${DELIMITER}pairedend${DELIMITER}/path/2/read1${DELIMITER}/path/2/read2${DELIMITER}\nID${DELIMITER}genome${DELIMITER}${DELIMITER}${DELIMITER}/path/2/genome" #TODO

SAMPLETABLEFORMAT=$(echo -e $SAMPLETABLEFORMAT | csvtk pretty -W 50 -m 1,2,3,4,5 || echo -e $SAMPLETABLEFORMAT)

# <<<<<<<<<<<<<<<<<<<<<<<< Sample table Variables <<<<<<<<<<<<<<<<<<<<<<<<


SPECIES_READY="cdifficile cperfringens cchauvoei" #cbotulinum.

# >>>>>>>>>>>>>>>>>>>>>>>> Snakemake variable >>>>>>>>>>>>>>>>>>>>>>>>

SNAKEMAKE_CONFIG=
DRYRUN=false

# <<<<<<<<<<<<<<<<<<<<<<<< Snakemake variable <<<<<<<<<<<<<<<<<<<<<<<<

SPECIES=
OUTDIR=

N_SAMPLES=

PLOT_WITH_ROARY2SVG="True"
PLOT_WITH_ROARY_PLOTS="True"


