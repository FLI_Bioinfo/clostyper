#!/usr/bin/env bash 

#Friedrich-Loeffler-Institut (https://www.fli.de/), IBIZ
#date: April, 2, 2022
#Author: Mostafa Abdel-Glil (mostafa.abdelglil@fli.de)

# Check if current directory is accessible
# if ! pwd &>/dev/null; then
#   echo -ne "\nError: the current directory is not accessible. Change to known directory.\nExit ...\n"
#   exit 1
# fi

set -e 

# >>>>>>>>>>>>>>>>>>>>>>>> variables >>>>>>>>>>>>>>>>>>>>>>>>

# general variables
PROGNAME="clostyper defaults"
QUIET=true
SAVE_LOG=false

CLOSTYPER_DEFAULTS="${HOME}/.clostyper/config/defaults.yaml"
NEW_CLOSTYPER_DEFAULTS=$(mktemp)
OLD_CLOSTYPER_DEFAULTS="${HOME}/.clostyper/config/defaults-old.yaml"


PRINT_CURRENT_DEFAULTS=false
REPLACE_DEFAULTS=false
VALIDATE_DEFAULTS=false

# <<<<<<<<<<<<<<<<<<<<<<<< variables <<<<<<<<<<<<<<<<<<<<<<<<



#make a help MSG and pass arguments (arguments will overwrite the parametrs in $BASHCONFIG)
usage() {
  msg_ns ""
  msg_ns $ORANGE"Clostyper: Clostridia characterization and typing pipeline"$NOFORMAT
  msg_ns $PURPLE"Clostyper defaults are read from: ${CLOSTYPER_DEFAULTS}:"$NOFORMAT
  msg_ns ""
  msg_ns $BLUE"This script prints, validate and/or overwrite the default settings for Clostyper"$NOFORMAT
  msg_ns ""
  msg_ns "------------------------------------------------------------------------------"
  msg_ns "Version    : $VERSION"
  msg_ns "Author     : $AUTHOR <$Email>"
  msg_ns "Source code: $URL"
  msg_ns "------------------------------------------------------------------------------"
  msg_ns ""
  msg_ns $BOLD"USAGE:"$NOFORMAT
  msg_ns "   $PROGNAME --replace --conda_prefix <installation_path> --kraken_database <kraken_database> [...]  "
  msg_ns $BLUE"  👉 Important: $PROGNAME prints output to STDOUT"$NOFORMAT
  msg_ns $BLUE"       You must use the flag '--replace' to overwrite Clostyper defaults"$NOFORMAT
  msg_ns "------------------------------------------------------------------------------"
  msg_ns ""
  msg_ns $BOLD"Options:"$NOFORMAT
  msg_ns "   --print                 Print the current default setting of Clostyper (Default: OFF)"
  msg_ns "   --replace               Insert changes directly into the file: ${CLOSTYPER_DEFAULTS} (Default: OFF; STDOUT)"
  msg_ns "   --validate              Ensure this file follows YAML syntax: ${CLOSTYPER_DEFAULTS} (Default: OFF)"
  msg_ns "   -h, --help              Show this help message and exit"  
  msg_ns ""
  msg_ns $BOLD"Installation & Execution:"$NOFORMAT
  msg_ns "   --conda_prefix XX       Directory to install conda envs for snakemake (default: ${CONDAENVS})"
  msg_ns "   --threads_default       Number of threads to use when executing the workflow (default: ${THREADS})"
  msg_ns ""
  msg_ns $BOLD"Paths to databases:"$NOFORMAT
  msg_ns "${PURPLE}         TIP: Use 'clostyper download' to download MOST of these databases (recommended)${NOFORMAT}"
  msg_ns "   --kraken_database       Path to (Mini)kraken2 DB (Default: '$KRAKEN_DATABASE')"
  msg_ns "   --amrfinderplus_db      Path to amrfinderplus DB (Default: '$AMRFINDERPLUS_DATABASE') #Leave empty for auto-update the database"
  msg_ns "   --abricate_databases    Path to ABRicate DB (Default: '$ABRICATE_DATABASES') #Leave empty for auto-update the database"
  msg_ns "   --confindr_database     Path to confinder DB (Default: '$CONFINDR_DATABASE') "
  msg_ns "   --checkM_database       Path to checkM DB (Default: '$CHECKM_DATABASE') "
  msg_ns "   --referenceseeker_db    Path to referenceseeker DB (Default: '$REFERENCESEEKER_DATABASE') "
  msg_ns "   --bakta_database        Path to Bakta DB (Default: '$BAKTA_DATABASE')."
  msg_ns "   --taxdump_database      Path to taxdump DB (Default: '$TAXDUMP_DATABASE') "
  msg_ns "   --card_database         Path to CARD database scheme (Default: '$CARD_DATABASE')"
  msg_ns "   --resfinder_database    Path to Resfinder database (Default: '$RESFINDER_DATABASE')"
  msg_ns "   --clostyper_db          Path to clostyper DB (Default: '$CLOSTYPER_DATABASE')"
  msg_ns ""

}

POSITIONAL=()
if [ $# == 0 ]; then
  usage
  exit 0
fi

# As defaults are read from default.yaml this cause a delay of displaying help
# Split parsing the help message seprately tp avoid delay

while (( $# > 0 )); do
  case "${1}" in
        "" | -h | --help)
          usage
          exit 0
          ;;
          
        --help_all)
          usage_extended
          exit 0
          ;;
          
        --version)
          echo -e "$VERSION"
          exit 0
          ;;

        --citation)
          echo -e "$CITATION"
          exit 0
          ;;
    *) 
    POSITIONAL+=("${1}")
    shift
    ;;
  esac
done

set -- "${POSITIONAL[@]}" # restore positional params


parse_params() {

  POSITIONAL=()
  while [[ $# -gt 0 ]]; do
    key="$1"
    case "${1-}" in
      "" | -h | --help | --help_all | --citation | --version)
      # Do not set up trap for these flags
      ;;

    *)
      # Set up trap for other cases
      trap '[[ $? -eq 0 ]] && cleanup' EXIT
      ;;
    esac

    case "${1-}" in

    --print)
      PRINT_CURRENT_DEFAULTS=true
      shift
      ;;

    --replace)
      REPLACE_DEFAULTS=true
      shift
      ;;

    --validate) 
      VALIDATE_DEFAULTS=true
      shift
      ;;


    --threads_default)
      if [[ "${2-}" ]]; then
        THREADS="${2-}"
        if ! [[ $THREADS =~ $ISNUMBER ]]; then
          die "$THREADS is not a number. Please indicate the number of threads to be used with '--threads_default'"
        fi
        shift 2
      else
        die "Please indicate the number of threads to be used with '--threads_default' - exit"
      fi
      ;;


    --conda_prefix)
      if [[ -n "${2-}" ]]; then
        CONDAENVS="${2-}"
        # Check if the directory exists; if not, create it
        if [[ ! -d "${CONDAENVS}" ]]; then
          mkdir -p "${CONDAENVS}" || die "Failed to create directory '${CONDAENVS}'. Please provide a valid path using '--conda_prefix' - exit"
        fi
        # Check if the directory is writable
        if [[ ! -w "$CONDAENVS" ]]; then
          die "The directory '$CONDAENVS' is not writable. Please provide a writable path using '--conda_prefix' - exit"
        fi
        CONDAENVS=$(realpath "$CONDAENVS")
        shift 2
      else
        die "Please provide a valid path for conda environments using '--conda_prefix' - exit"
      fi
      ;;

    --kraken_database)
      if [[ -e "${2-}" ]]; then
        KRAKEN_DATABASE=$(realpath "$2")
        
        if [[ -d "$KRAKEN_DATABASE" &&
              -f "$KRAKEN_DATABASE/hash.k2d" &&
              -f "$KRAKEN_DATABASE/opts.k2d" &&
              -f "$KRAKEN_DATABASE/taxo.k2d" ]]; then
          :
        else
          die "The Kraken2 database at '$KRAKEN_DATABASE' is either unavailable or corrupted."
        fi
        
        shift 2
      else
        die "Invalid or missing Kraken2 database path. Use '--kraken2db' to specify a valid path."
      fi
      ;;

    --amrfinderplus_db)
      if [[ -e "${2-}" ]]; then
        AMRFINDERPLUS_DATABASE="${2-}"
        AMRFINDERPLUS_DATABASE=$(realpath "$AMRFINDERPLUS_DATABASE")
        shift 2
      else
        die "Please provide a correct path to AMRFINDERPLUS database using '--amrfinderplus_database' - exit"
      fi
      ;;

    --abricate_databases)
      if [[ -e "${2-}" ]]; then
        ABRICATE_DATABASES="${2-}"
        ABRICATE_DATABASES=$(realpath "$ABRICATE_DATABASES")
        shift 2
      else
        die "Please provide a correct path to ABRICATE database using '--abricate_databases' - exit"
      fi
      ;;

    --confindr_database)
      if [[ -e "${2-}" ]]; then
        CONFINDR_DATABASE="${2-}"
        CONFINDR_DATABASE=$(realpath "$CONFINDR_DATABASE")
        shift 2
      else
        die "Please provide a correct path to CONFINDER database using '--confindr_database' - exit"
      fi
      ;;

    --checkM_database)
      if [[ -e "${2-}" ]]; then
        CHECKM_DATABASE="${2-}"
        CHECKM_DATABASE=$(realpath "$CHECKM_DATABASE")
        shift 2
      else
        die "Please provide a correct path to CheckM database using '--checkM_database' - exit"
      fi
      ;;

    --referenceseeker_db)
      if [[ -e "${2-}" ]]; then
        REFERENCESEEKER_DATABASE="${2-}"
        REFERENCESEEKER_DATABASE=$(realpath "$REFERENCESEEKER_DATABASE")
        shift 2
      else
        die "Please provide a correct path to referenceseeker database using '--referenceseeker_database' - exit"
      fi
      ;;

    --bakta_database)
      if [[ -e "${2-}" ]]; then
        BAKTA_DATABASE="${2-}"
        BAKTA_DATABASE=$(realpath "$BAKTA_DATABASE")
        shift 2
      else
        die "Please provide a correct path to Bakta database using '--bakta_database' - exit"
      fi
      ;;

    --taxdump_database)
      if [[ -e "${2-}" ]]; then
        TAXDUMP_DATABASE="${2-}"
        TAXDUMP_DATABASE=$(realpath "$TAXDUMP_DATABASE")
        shift 2
      else
        die "Please provide a correct path to taxdump database using '--taxdump_database' - exit"
      fi
      ;;

    --card_database)
      if [[ -e "${2-}" ]]; then
        CARD_DATABASE="${2-}"
        CARD_DATABASE=$(realpath "$CARD_DATABASE")
        shift 2
      else
        die "Please provide a correct path to CARD database using '--card_database' - exit"
      fi
      ;;


    --resfinder_database)
      if [[ -e "${2-}" ]]; then
        RESFINDER_DATABASE="${2-}"
        RESFINDER_DATABASE=$(realpath "$RESFINDER_DATABASE")
        shift 2
      else
        die "Please provide a correct path to resfinder database using '--resfinder_database' - exit"
      fi
      ;;

    --clostyper_db)
      if [[ -e "${2-}" ]]; then
        CLOSTYPER_DATABASE="${2-}"
        CLOSTYPER_DATABASE=$(realpath "$CLOSTYPER_DATABASE")
        shift 2
      else
        die "Please provide a correct path to clostyper database using '--clostyper_db' - exit"
      fi
      ;;

    -?*) die "Unknown option: $1. \nPlease use $PROGNAME --help to get available options - exit" ;;
    *) break ;;
    esac
  done
   args=("$@")
  return 0
}

parse_params "$@"


#=============================================================
# Set a new file for logging
#=============================================================


if [ "$SAVE_LOG" == true ]; then
  echo -ne "" >"$LOG"
  exec > >(
    tee >(sed -u 's/\x1b\[[0-9;]*[mGKH]//g' >>"$LOG") >&2 # disable coloration when logging
  ) 2>&1
fi

#=============================================================
# Start script
#=============================================================

#welcome
WELCOMEMSG

# display the command used
display_command "$@" || true



#=============================================================
# Check the status of the databases
#=============================================================

#check databases are correct
msg "Checking the status of databases..."
# database arguments return path
databases=(
  "$KRAKEN_DATABASE"
  "$AMRFINDERPLUS_DATABASE"
  "$ABRICATE_DATABASES"
  "$CONFINDR_DATABASE"
  "$CHECKM_DATABASE"
  "$REFERENCESEEKER_DATABASE"
  "$BAKTA_DATABASE"
  "$TAXDUMP_DATABASE"
  "$CGMLST_SCHEME"
  "$CLOSTYPER_DATABASE"
)

# Define an array of database variables along with their names
database_names=(
  "Kraken2"
  "AMRFinderPlus"
  "ABRicate"
  "ConFindr"
  "CheckM"
  "Referenceseeker"
  "Bakta"
  "Taxdump"
  "cgMLST scheme"
  "Clostyper"
)

# Iterate over the databases using a for loop
for ((i = 0; i < ${#databases[@]}; i++)); do
  database="${databases[i]}"
  name="${database_names[i]}"

  # Check if the database file exists
  if [ -e "$database" ]; then
    msg "$PROGNAME will use $name database from: '$database'"
  else
    warn_ns "The $name database '$database' is unavailable."
  fi

done

cat <<DEFAULTS >${NEW_CLOSTYPER_DEFAULTS} || true

##############################################
###### Full paths to databases and schemes ###
##############################################

KRAKEN_DATABASE:                             ${KRAKEN_DATABASE}
AMRFINDERPLUS_DATABASE:                      ${AMRFINDERPLUS_DATABASE}
ABRICATE_DATABASES:                          ${ABRICATE_DATABASES}
CONFINDR_DATABASE:                           ${CONFINDR_DATABASE}
CHECKM_DATABASE:                             ${CHECKM_DATABASE}
REFERENCESEEKER_DATABASE:                    ${REFERENCESEEKER_DATABASE}
BAKTA_DATABASE:                              ${BAKTA_DATABASE}
TAXDUMP_DATABASE:                            ${TAXDUMP_DATABASE}
CLOSTYPER_DATABASE:                          ${CLOSTYPER_DATABASE}
CARD_DATABASE:                               ${CARD_DATABASE} 
RESFINDER_DATABASE:                          ${RESFINDER_DATABASE}                              

##############################################
#### Default software and software settings ##
##############################################

#==============================
#      RAWREADS_QUALITY
#==============================

USE_FASTP:                                   ${USE_FASTP}
USE_KRAKEN2:                                 ${USE_KRAKEN2}
KRAKEN2_MEMMORY_MAPPING:                     ${KRAKEN2_MEMMORY_MAPPING}
USE_CONFINDR:                                ${USE_CONFINDR}

#==============================
#      GENOME QUALITY
#==============================

ASSEMBLY:                                    ${ASSEMBLY}
ASSEMBLE_TOOL:                               ${ASSEMBLY_SOFTWARE}
USE_KRAKEN2_ASSEMBLIES:                      ${RUN_KRAKEN2_ASSEMBLIES}
USE_CHECKM:                                  ${RUN_CHECKM}
USE_REFERENCESEEKER:                         ${RUN_REFERENCESEEKER}
USE_FASTANI:                                 ${RUN_FASTANI}
ANNOTATION:                                  ${ANNOTATION}
ANNOTATE_TOOL:                               ${ANNOTATION_SOFTWARE}

#==============================
#      RESISTANCE
#==============================


USE_ABRICATE:                                ${USE_ABRICATE}
USE_AMRFINDERPLUS:                           ${USE_AMRFINDERPLUS}
USE_CARD_RGI:                                ${USE_CARD_RGI}
USE_RESFINDER:                               ${USE_RESFINDER}


ABRICATE_DB_NAME:                            ${ABRICATE_DB_NAME}
ABRICATE_IDENTITY:                           ${ABRICATE_IDENTITY}
ABRICATE_COVERAGE:                           ${ABRICATE_COVERAGE}

ADD_CONTIG_TAXONOMY:                         ${ADD_CONTIG_TAXONOMY}

#==============================
#      VIRULENCE
#==============================

ABRICATE_VIRULENCE_DATABASES:                ${ABRICATE_VIRULENCE_DATABASES}
ABRICATE_MIN_IDENTITY_VIR:                   ${ABRICATE_MIN_IDENTITY_VIR}
ABRICATE_MIN_COVERAGE_VIR:                   ${ABRICATE_MIN_COVERAGE_VIR}

#==============================
#      MLST
#==============================

MLST_TOOL:                                  ${MLST_TOOL}

#==============================
#      cgSNP ANALYSIS
#==============================

MASK_REPITIVE_SEQUENCE_REGIONS:              ${MASK_REPITIVE_SEQUENCE_REGIONS}
RECOMBINATION_FILTER:                        ${RECOMBINATION_FILTER}
HOMOPLASIES_FILTER:                          ${HOMOPLASIES_FILTER}
PHYLOGENY_INPUT:                             ${PHYLOGENY_INPUT}
PHYLOGENY_SOFTWARE:                          ${PHYLOGENY_SOFTWARE}
MIDPOINT_ROOT_PHYLOGENY:                     ${MIDPOINT_ROOT_PHYLOGENY}
CLUSTERING:                                  ${CLUSTERING}
CLUSTERING_FASTBAPS:                         ${CLUSTERING_FASTBAPS}
CGSNPS_CLUSTERING_THRESHOLD_RANGE:           ${CGSNPS_CLUSTERING_THRESHOLD_RANGE}
CGSNPS_DISTANCE_THRESHOLD:                   ${CGSNPS_DISTANCE_THRESHOLD}

#==============================
#      cgMLST ANALYSIS
#==============================

CGMLST_CLUSTERING_THRESHOLD_RANGE:           ${CGMLST_CLUSTERING_THRESHOLD_RANGE}
CGMLST_DISTANCE_THRESHOLD:                   ${CGMLST_DISTANCE_THRESHOLD}

#==============================
#      Pangenome ANALYSIS
#==============================

PANGENOME_SOFTWARE:                          ${PANGENOME_SOFTWARE}

##############################################
############ EXECUTION #######################
##############################################

CONDAENVS:                                   ${CONDAENVS} 
THREADS:                                     ${THREADS}

DEFAULTS

if [[ $REPLACE_DEFAULTS == true ]]; then
    mv $CLOSTYPER_DEFAULTS $OLD_CLOSTYPER_DEFAULTS  || die "Can not replace $CLOSTYPER_DEFAULTS"
    mv $NEW_CLOSTYPER_DEFAULTS $CLOSTYPER_DEFAULTS  || die "Can not replace $CLOSTYPER_DEFAULTS"
else
    yq eval '.' "${NEW_CLOSTYPER_DEFAULTS}"  || true  # Print to stdout
fi
