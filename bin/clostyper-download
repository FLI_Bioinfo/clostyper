#!/usr/bin/env bash 

#Friedrich-Loeffler-Institut (https://www.fli.de/), IBIZ
#date: April, 2, 2022
#Author: Mostafa Abdel-Glil (mostafa.abdelglil@fli.de)

set -e 

# >>>>>>>>>>>>>>>>>>>>>>>> variables >>>>>>>>>>>>>>>>>>>>>>>>

# General variables

PROGNAME="clostyper download"
QUIET=false
SAVE_LOG=true
FORCE=false

# Download constants
DATA_TYPE="CLOSTYPER_DB"
DB_DIR_NAME=""

# Download variables
DOWNLOAD_DIRECTORY=
DB_DIR=

CURRENT_DBS="{clostyper_db,clostyper_test,kraken2_db,bakta_db,checkm_db,taxdump_db,referenceseeker_db}"

# <<<<<<<<<<<<<<<<<<<<<<<< variables <<<<<<<<<<<<<<<<<<<<<<<<



# # >>>>>>>>>>>>>>>>>>>>>>>> trap >>>>>>>>>>>>>>>>>>>>>>>>

cleanup() {
  local exit_code=$?

  # Move the log file to the db directory only with exist code 0

  if [[ "$exit_code" -eq 0 &&  -d "$DB_DIR" ]]; then
    mv "$LOG" "$DB_DIR"/ 2>/dev/null || true
  fi

  if [ "$SAVE_LOG" = true ]; then
    echo -e "Log info has been saved to: ${LOG}"
  fi

  unset PATH
  
  return $exit_code  # Return the original exit code

}

# Trap SIGINT (Ctrl + C) and SIGTERM (termination) for interruptions
trap 'echo ""; cleanup; exit 1' SIGINT SIGTERM

trap 'cleanup; exit 1' ERR

# # <<<<<<<<<<<<<<<<<<<<<<<< trap <<<<<<<<<<<<<<<<<<<<<<<<

#make a help MSG and pass arguments (arguments will overwrite the parametrs in $BASHCONFIG)
usage() {
  msg_ns ""
  msg_ns $ORANGE"Clostyper: Clostridia characterization and typing pipeline"$NOFORMAT
  msg_ns ""
  msg_ns "------------------------------------------------------------------------------"
  msg_ns "Version    : $VERSION"
  msg_ns "Author     : $AUTHOR <$Email>"
  msg_ns "Source code: $URL"
  msg_ns "------------------------------------------------------------------------------"
  msg_ns ""
  msg_ns $BOLD"USAGE:"$NOFORMAT
  msg_ns "   $PROGNAME --dir <directory> --db <database> "
  msg_ns "------------------------------------------------------------------------------"
  msg_ns ""
  msg_ns $BOLD"OPTIONS:"$NOFORMAT
  msg_ns "   --dir  XXX           Directory to save the downloaded data"
  msg_ns "   --db   XXX           Database to download. (default: clostyper_db)" 
  msg_ns "                         Choose between ${CURRENT_DBS}"
  msg_ns "                          - clostyper_db:   Clostyper database: https://zenodo.org/records/14703395 (144 MB)"
  msg_ns "                          - clostyper_test: FASTA files to test if Clostyper works (21 MB)"
  msg_ns "                          - kraken2_db:     Kraken2 database: https://benlangmead.github.io/aws-indexes/k2 (8 GB)"
  msg_ns "                          - bakta_db:       Bakta-light database: https://zenodo.org/records/10522951 (1.3 GB)"
  msg_ns "                          - checkm_db:      CheckM database: https://github.com/Ecogenomics/CheckM (289 MB)"
  msg_ns "                          - taxdump_db:     Taxdump database: https://bioinf.shenwei.me/taxonkit (64 M)"
  msg_ns "                          - referenceseeker_db: ReferenceSeeker database: https://github.com/oschwengers/referenceseeker (60 GB)"
  msg_ns "   -F, --force          Force writing to an existing directory/file"
  msg_ns "   -h, --help           Show this help message and exit"
#  msg_ns "bakta 1.9.4 and the light database update with amrfinder_update --force_update --database ~/dbs/bakta/db-light/amrfinderplus-db"
#  msg_ns "CARD "
  msg_ns ""
}




POSITIONAL=()
if [ $# == 0 ]; then
  usage
  exit 0
fi

# As defaults are read from default.yaml this cause a delay of displaying help
# Split parsing the help message seprately tp avoid delay

while (( $# > 0 )); do
  case "${1}" in
        "" | -h | --help)
          usage
          exit 0
          ;;
    *) 
    POSITIONAL+=("${1}")
    shift
    ;;
  esac
done

set -- "${POSITIONAL[@]}" # restore positional params


parse_params() {
  POSITIONAL=()
  while [[ $# -gt 0 ]]; do
    key="$1"
    case "${1-}" in
      "" | -h | --help) # Do not set up trap for these flags
      ;;

    *)
      # Set up trap for other cases
      trap '[[ $? -eq 0 ]] && cleanup' EXIT
      ;;
    esac

    case "${1-}" in
    --dir)
      if [[ -z "${2-}" ]]; then
        die "'--dir' requires an argument."
      elif [[ "${2-}" =~ ^- ]]; then
        die "Output directory cannot start with '-'."
      else
        DOWNLOAD_DIRECTORY=$(remove_trailing_slash "${2-}")
        shift 2
      fi
      ;;

    --db)
      if [[ "${2-}" ]]; then
        case $2 in
          clostyper_db)
            DATA_TYPE="CLOSTYPER_DB"
            ;;

          clostyper_test)
            DATA_TYPE="CLOSTYPER_TEST"
            ;;

          kraken2_db)
            DATA_TYPE="KRAKEN2_DB"
            ;;

          bakta_db)
            DATA_TYPE="BAKTA_DB"
            ;;

          checkm_db)
            DATA_TYPE="CHECKM_DB"
            ;;

          taxdump_db)
            DATA_TYPE="TAXDUMP_DB"
            ;;

          referenceseeker_db)
            DATA_TYPE="REFERENCESEEKER_DB"
            ;;

          *)
            die "'--db' assumes one of the following arguments: ${CURRENT_DBS} (default: clostyper_db)"
            ;;
        esac
        shift 2
      else
        die "'--db' requires an argument. Choose between ${CURRENT_DBS} (default: clostyper_db)"
      fi
      ;;

    -F | --force)
      FORCE=true
      shift
      ;;


    -?*) 
      die "Unknown option: $1. Please use $PROGNAME --help to get available options - exit" 
      ;;

    *) 
      POSITIONAL+=("${1}")
      break 
    ;;
    esac
  done
  set -- "${POSITIONAL[@]}" #restore positional parameters
  return 0
}



# >>>>>>>>>>>>>>>>>>>>>>>> collect arguments >>>>>>>>>>>>>>>>>>>>>>>>

# Parse arguments with new defaults
parse_params "$@"

# <<<<<<<<<<<<<<<<<<<<<<<< collect arguments <<<<<<<<<<<<<<<<<<<<<<<<



# >>>>>>>>>>>>>>>>>>>>>>>> check mandatory arguments >>>>>>>>>>>>>>>>>>>>>>>>

if [[ -z "$DOWNLOAD_DIRECTORY" ]]; then
  die "You must set the flag '--dir' with a directory where to download the data - exit."
fi

# <<<<<<<<<<<<<<<<<<<<<<<< check mandatory arguments <<<<<<<<<<<<<<<<<<<<<<<<

# >>>>>>>>>>>>>>>>>>>>>>>> argument parsing >>>>>>>>>>>>>>>>>>>>>>>>

if [[ "$DATA_TYPE" == "CLOSTYPER_DB" ]]; then
  # Clostyper database
  DB_URL="https://zenodo.org/records/14859379/files/ClostyperDB.tar.gz"
  TARBALL="ClostyperDB.tar.gz"
  DB_DIR_NAME=""
  REQUIRED_MD5="5edb16ec618700e554e644b1fc5be6e8"

elif [[ "$DATA_TYPE" == "CLOSTYPER_TEST" ]]; then
  :
  # Clostyper test data ✔😊
  # DB_URL="https://zenodo.org/records/6656045/files/test_data_clostyper.tar.gz"
  # TARBALL="test_data_clostyper.tar.gz"
  # DB_DIR_NAME=""
  # REQUIRED_MD5="59351275a97e16a061134070e50fb317"

elif [[ "$DATA_TYPE" == "KRAKEN2_DB" ]]; then
  # Kraken2 Standard-8 database 😐 MD5 is not available (calculated here)
  DB_URL="https://genome-idx.s3.amazonaws.com/kraken/k2_standard_08gb_20240605.tar.gz"
  TARBALL="k2_standard_08gb_20240605.tar.gz"
  DB_DIR_NAME="kraken2_db"
  REQUIRED_MD5="1f7a948ce35c87d9ac5fd11e983a3d77" #"standard_08gb.md5"

elif [[ "$DATA_TYPE" == "CHECKM_DB" ]]; then
  # CheckM database ✔😊
  DB_URL="https://zenodo.org/records/7401545/files/checkm_data_2015_01_16.tar.gz"
  TARBALL="checkm_data_2015_01_16.tar.gz"
  DB_DIR_NAME="checkM"
  REQUIRED_MD5="631012fa598c43fdeb88c619ad282c4d"

elif [[ "$DATA_TYPE" == "REFERENCESEEKER_DB" ]]; then
  # ReferenceSeeker database # Not tested yet
  DB_URL="https://zenodo.org/records/10488564/files/bacteria-refseq.tar.gz"
  TARBALL="bacteria-refseq.tar.gz"
  DB_DIR_NAME="referenceseeker"
  REQUIRED_MD5="61979a607532fbb589758e195e5b364a"


# elif [[ "$DATA_TYPE" == "CARD" ]]; then
#   # Not tested yet
#   DB_URL="https://card.mcmaster.ca/latest/data"
#   TARBALL="data"
#   DB_DIR_NAME="CARD_DB"
#   REQUIRED_MD5="61979a607532fbb589758e195e5b364a"

elif [[ "$DATA_TYPE" == "BAKTA_DB" ]]; then #(amrfinder version: 3.12.8, bakta version: 1.9.4) 
  # Not tested yet
  DB_URL="https://zenodo.org/record/10522951/files/db-light.tar.gz"
  AMRFINDERPLUS_DB="https://ftp.ncbi.nlm.nih.gov/pathogen/Antimicrobial_resistance/AMRFinderPlus/database/3.12/2024-07-22.1/"
  AMR_DB_VERSION="2024-07-22.1"
  TARBALL="db-light.tar.gz"
  DB_DIR_NAME="bakta_db"
  REQUIRED_MD5="31b3fbdceace50930f8607f8d664d3f4"


elif [[ "$DATA_TYPE" == "TAXDUMP_DB" ]]; then
  # Taxonkit database 😐 MD5 does not match the original one
  DB_URL="https://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz"
  TARBALL="taxdump.tar.gz"
  DB_DIR_NAME="taxdump"
  REQUIRED_MD5="b7505752a66e0673ffacc3af0dd9257e" #"7db4bc172f48b2a0dbfb2b817a58e77c"

else 
  die "'--db' must match one of the following: ${CURRENT_DBS}"
fi


declare -A DB_MESSAGES=(
    ["CLOSTYPER_DB"]="Downloading Clostyper database (144 MB)"
    ["CLOSTYPER_TEST"]="Downloading Clostyper test data (1.8 GB)"
    ["KRAKEN2_DB"]="Downloading Kraken2 Standard-8 database (8 GB)"
    ["BAKTA_DB"]="Downloading Bakta database (1.3 GB)"
    ["CHECKM_DB"]="Downloading CheckM database (289 MB)"
    ["REFERENCESEEKER_DB"]="Downloading ReferenceSeeker database (60 GB)"
    ["TAXDUMP_DB"]="Downloading Taxonkit taxonomy database (64 M)"
)


# <<<<<<<<<<<<<<<<<<<<<<<< argument parsing <<<<<<<<<<<<<<<<<<<<<<<<


# >>>>>>>>>>>>>>>>>>>>>>>>  output directory >>>>>>>>>>>>>>>>>>>>>>>>

#make output directory. if existing, overwrite or die

if [[ -d ${DOWNLOAD_DIRECTORY} ]] && [[ "$FORCE" != true ]]; then
  die "The output directory: '$DOWNLOAD_DIRECTORY' already exists. Use '--force' to replace current database"
else 
  mkdir -p "$DOWNLOAD_DIRECTORY" 
  DOWNLOAD_DIRECTORY=$(realpath "$DOWNLOAD_DIRECTORY")
fi

# <<<<<<<<<<<<<<<<<<<<<<<<  output directory <<<<<<<<<<<<<<<<<<<<<<<<

#=============================================================
#=============================================================
#=================== Start script=============================
#=============================================================
#=============================================================

# >>>>>>>>>>>>>>>>>>>>>>>> log file  >>>>>>>>>>>>>>>>>>>>>>>>

# Set a new file for logging
if [ "$SAVE_LOG" == true ]; then
  echo -ne "" >"$LOG"
  exec > >(
    tee >(sed -u 's/\x1b\[[0-9;]*[mGKH]//g' >>"$LOG") >&2 # disable coloration when logging
  ) 2>&1
fi

# <<<<<<<<<<<<<<<<<<<<<<<< log file  <<<<<<<<<<<<<<<<<<<<<<<<


# >>>>>>>>>>>>>>>>>>>>>>>> welcome >>>>>>>>>>>>>>>>>>>>>>>>

# # Welcome
WELCOMEMSG

# Display the command used
display_command "$@" || true

# <<<<<<<<<<<<<<<<<<<<<<<< welcome <<<<<<<<<<<<<<<<<<<<<<<<


# >>>>>>>>>>>>>>>>>>>>>>>> Download the database >>>>>>>>>>>>>>>>>>>>>>>>

# Ensure deps are installed

for cmd in wget gunzip tar; do
  command -v "$cmd" >/dev/null 2>&1 || die "'$cmd' is required but it's not installed. Aborting."
done

# Function to download the database
download_database() {
    DB_DIR="$DOWNLOAD_DIRECTORY/$DB_DIR_NAME" # do not use local to enable trap see $DB_DIR
    msg "Download directory = $DB_DIR"

    mkdir -p "$DB_DIR"
    local tar_path="$DB_DIR/$TARBALL"

    # Download with progress
    msg "wget --progress=dot -O ${tar_path} ${DB_URL}"
    
    wget --progress=dot --no-host-directories -O "$tar_path" "$DB_URL" 2>&1 | while read -r line; do
      # Extract the downloaded size and total size from wget's output
      if [[ $line =~ ([0-9]+)% ]]; then
        percent="${BASH_REMATCH[1]}"
    
        # Update the progress bar
        if [[ -n "${DB_MESSAGES[$DATA_TYPE]}" ]]; then
            progressBar "${DB_MESSAGES[$DATA_TYPE]}" "$percent" 100 || true
        fi

      fi
    done

    # Check if the file was downloaded completely
    if [[ ! -f "$tar_path" ]] || [[ $(stat -c%s "$tar_path") -eq 0 ]]; then
        die "Download incomplete!"
    fi

    # Check MD5 checksum
    local md5_sum

    msg "Checking MD5 to verify download"
    md5_sum=$(calc_md5_sum "$tar_path")

    if [[ "$md5_sum" == "$REQUIRED_MD5" ]]; then
        msg "Downloaded file looks OK: $md5_sum"
    else
        warn "Could not verify the download! MD5 should be '$REQUIRED_MD5' but is '$md5_sum'"
    fi

    msg "Extracting DB tarball: file=$tar_path, output=$DB_DIR"
    msg "tar -xzf ${tar_path} -C ${DB_DIR}"
    tar -xzf "$tar_path" -C "$DB_DIR" 2>/dev/null || {
        die "Could not extract $tar_path to $DB_DIR"
    }

    # Clean up
    msg "Removing tar file: $tar_path"
    timeout 2 rm -f "$tar_path" 2>/dev/null || true

    msg "Database directory: $DB_DIR" 
    msg "Database download complete" 

    if [[ "$DATA_TYPE" == "BAKTA_DB" ]]; then
      # warn_ns ${RED}"\nBAKTA DOWNLOAD WAS COMPLETE 
      # However, you will also need to update & prepare the AMRFinderPlus's internal database for bakta (amrfinder version: 3.12.8, bakta version: 1.9.4) 
      
      # Use: amrfinder_update --force_update --database ${DB_DIR}/db-light/amrfinderplus-db"${NOFORMAT}
      msg "Bakta requires AMRfinderplus database - Retrieving version 2024-07-22.1"
      wget --quiet --recursive --no-parent --no-host-directories --cut-dirs=7 -e robots=off ${AMRFINDERPLUS_DB} -P ${DB_DIR}/db-light/amrfinderplus-db/${AMR_DB_VERSION} || 
        die "could not retrieve the AMRfinderplus database" 

    fi
}

# download_database


# <<<<<<<<<<<<<<<<<<<<<<<< Download the database <<<<<<<<<<<<<<<<<<<<<<<<

# >>>>>>>>>>>>>>>>>>>>>>>> Handle downloaded datatypes >>>>>>>>>>>>>>>>>>>>>>>>

# Download C. difficile test data
download_test_data() {
    msg "Downloading C. difficile genomes..."
    mkdir -p ${DOWNLOAD_DIRECTORY}/cdiff
    wget --quiet --progress=dot -O ${DOWNLOAD_DIRECTORY}/cdiff/GCF_000027105.1.fasta.gz "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/027/105/GCF_000027105.1_ASM2710v1/GCF_000027105.1_ASM2710v1_genomic.fna.gz"
    wget --quiet --progress=dot -O ${DOWNLOAD_DIRECTORY}/cdiff/GCF_018884965.1.fasta.gz "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/018/884/965/GCF_018884965.1_ASM1888496v1/GCF_018884965.1_ASM1888496v1_genomic.fna.gz"
    wget --quiet --progress=dot -O ${DOWNLOAD_DIRECTORY}/cdiff/GCF_018885085.1.fasta.gz "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/018/885/085/GCF_018885085.1_ASM1888508v1/GCF_018885085.1_ASM1888508v1_genomic.fna.gz"
    wget --quiet --progress=dot -O ${DOWNLOAD_DIRECTORY}/cdiff/GCF_951803555.1.fasta.gz "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/951/803/555/GCF_951803555.1_L-NTCD03/GCF_951803555.1_L-NTCD03_genomic.fna.gz"
    wget --quiet --progress=dot -O ${DOWNLOAD_DIRECTORY}/cdiff/GCF_015238635.1.fasta.gz "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/015/238/635/GCF_015238635.1_ASM1523863v1/GCF_015238635.1_ASM1523863v1_genomic.fna.gz"

    msg "Downloading C. perfringens genomes..."
    mkdir -p ${DOWNLOAD_DIRECTORY}/cperf
    wget --quiet --progress=dot -O ${DOWNLOAD_DIRECTORY}/cperf/GCF_016027375.1.fasta.gz "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/016/027/375/GCF_016027375.1_ASM1602737v1/GCF_016027375.1_ASM1602737v1_genomic.fna.gz"
    wget --quiet --progress=dot -O ${DOWNLOAD_DIRECTORY}/cperf/GCF_003203455.1.fasta.gz "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/003/203/455/GCF_003203455.1_ASM320345v1/GCF_003203455.1_ASM320345v1_genomic.fna.gz"
    wget --quiet --progress=dot -O ${DOWNLOAD_DIRECTORY}/cperf/GCF_036321485.1.fasta.gz "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/036/321/485/GCF_036321485.1_ASM3632148v1/GCF_036321485.1_ASM3632148v1_genomic.fna.gz"
    wget --quiet --progress=dot -O ${DOWNLOAD_DIRECTORY}/cperf/GCF_046058955.1.fasta.gz "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/046/058/955/GCF_046058955.1_ASM4605895v1/GCF_046058955.1_ASM4605895v1_genomic.fna.gz"
    wget --quiet --progress=dot -O ${DOWNLOAD_DIRECTORY}/cperf/GCF_025946785.1.fasta.gz "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/025/946/785/GCF_025946785.1_ASM2594678v1/GCF_025946785.1_ASM2594678v1_genomic.fna.gz"   
    
    msg "Unzipping all downloaded files..."
    gunzip -f ${DOWNLOAD_DIRECTORY}/cdiff/GCF_000027105.1.fasta.gz ${DOWNLOAD_DIRECTORY}/cdiff/GCF_018884965.1.fasta.gz ${DOWNLOAD_DIRECTORY}/cdiff/GCF_018885085.1.fasta.gz ${DOWNLOAD_DIRECTORY}/cdiff/GCF_951803555.1.fasta.gz ${DOWNLOAD_DIRECTORY}/cdiff/GCF_015238635.1.fasta.gz ${DOWNLOAD_DIRECTORY}/cperf/GCF_016027375.1.fasta.gz ${DOWNLOAD_DIRECTORY}/cperf/GCF_003203455.1.fasta.gz ${DOWNLOAD_DIRECTORY}/cperf/GCF_036321485.1.fasta.gz ${DOWNLOAD_DIRECTORY}/cperf/GCF_046058955.1.fasta.gz ${DOWNLOAD_DIRECTORY}/cperf/GCF_025946785.1.fasta.gz || die "unable to decompress the genomes"
    
    msg "sample genomes downloaded and extracted."
}

# Determine which data to download
case "$DATA_TYPE" in
    CLOSTYPER_DB | KRAKEN2_DB | CHECKM_DB | REFERENCESEEKER_DB | BAKTA_DB | TAXDUMP_DB ) #| CLOSTYPER_TEST 
        download_database 
        ;;
    CLOSTYPER_TEST)
        download_test_data
        ;;
    *)
        echo "Unknown DATA_TYPE: $DATA_TYPE"
        exit 1
        ;;
esac


# <<<<<<<<<<<<<<<<<<<<<<<< Handle downloaded datatypes <<<<<<<<<<<<<<<<<<<<<<<<

