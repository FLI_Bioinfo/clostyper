#!/usr/bin/env bash 

#Friedrich-Loeffler-Institut (https://www.fli.de/), IBIZ
#date: April, 2, 2022
#Author: Mostafa Abdel-Glil (mostafa.abdelglil@fli.de)

set -a 

check_version() { #software current_version required_version status
  local software="$1"
  local current_version="$2"
  local required_version="$3"

  if [ "$(printf '%s\n' "$required_version" "$current_version" | sort --version-sort | head -n1)" = "$required_version" ]; then
    log "----will use "$software" - $(which "$software") | Version $current_version"
    status="success"
  else
    die "You have $software version '$current_version'. 
    $PROGNAME requires version >= '$required_version'"
  fi
}

check_dep() {
  #required software
  local required_software="bash script wget snakemake conda seqkit csvtk any2fasta sed awk tput fold" #pigz basic unix: sed awk tput fold
  #required versions
  local required_bash_version="4.0"
  local required_conda="24.7.1"
  # local required_pigz="2.3.4"
  local required_snakemake="7" #"8.20.0"
  local required_seqkit="2.3.0"
  local required_csvtk="0.27.0"
  local required_any2fasta="0.4.2"

  #check software are there
  for software in $required_software; do
    if ! command -v $software >/dev/null; then
      die "Some dependencies are missing\n
      Check if "$software" is installed and is in your \$PATH"
    else
      INSTALL_STATUS="success"
    fi
  done

  #collect current_versions
  current_bash=$(bash --version | head -n1 | awk '{print $4}' | cut -d'.' -f1,2)
  # current_pigz=$(pigz --version 2>&1 | cut -d' ' -f2)
  current_snakemake=$(snakemake --version 2>&1)
  current_conda=$(conda --version 2>&1 | sed 's/conda //')
  current_seqkit=$(seqkit version 2>&1 | sed 's/seqkit v//')
  current_csvtk=$(csvtk version 2>&1 | sed 's/csvtk v//')
  current_any2fasta=$(any2fasta -v 2>&1 | cut -d' ' -f2)

  #check versions are ok
  check_version "bash" "$current_bash" "$required_bash_version"
  BASH_STATUS="$status"
  # check_version pigz $current_pigz $required_pigz
  # PIGZ_STATUS="$status"
  check_version snakemake $current_snakemake $required_snakemake
  SNAKEMAKE_STATUS="$status"
  check_version conda $current_conda $required_conda
  CONDA_STATUS="$status"
  check_version seqkit $current_seqkit $required_seqkit
  SEQKIT_STATUS="$status"
  check_version csvtk $current_csvtk $required_csvtk
  CSVTK_STATUS="$status"
  check_version any2fasta $current_any2fasta $required_any2fasta
  ANY2FASTA_STATUS="$status"


  #ok message
  if [[ $INSTALL_STATUS == "success" &&
    "$BASH_STATUS" == "success" &&
    # $PIGZ_STATUS="success" &&
    $SEQKIT_STATUS="success" &&
    $SNAKEMAKE_STATUS="success" &&
    $CONDA_STATUS="success" &&
    $CSVTK_STATUS="success" && 
    $ANY2FASTA_STATUS="success" ]]; then

    msg "$PROGNAME dependencies are ok"

  fi
}


