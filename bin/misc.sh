#!/usr/bin/env bash

#Friedrich-Loeffler-Institut (https://www.fli.de/), IBIZ
#date: April, 2, 2022
#Author: Mostafa Abdel-Glil (mostafa.abdelglil@fli.de)

set -a 

# Variables
TIME="$(date '+%F_%H%M%S')"
LOG="${TIME}_${PROGRAM_NAME}".log

# Set variables, otherwise default
QUIET=${QUIET:-false}
SAVE_LOG=${SAVE_LOG:-false}


# export TIME LOG QUIET SAVE_LOG

# Calls: echo, fold, date, tput
setup_colors() {
  COLORS=$(tput colors 2>/dev/null)
  #disable colors if user specified so or if the terminal does not support this
  if [[ "$NO_COLOR" != true ]] && [[ "$COLORS" -gt 2 ]]; then
    NOFORMAT='\033[0m'
    RED='\033[0;31m'
    GREEN='\033[0;32m'
    ORANGE='\033[1;33m'
    BLUE='\033[0;34m'
    PURPLE='\033[0;35m'
    PURPLE_BOLD='\033[1;35m'
    GREEN_BOLD='\033[1;32m'
    CYAN='\033[0;36m'
    YELLOW='\033[0;33m'
    BOLD='\033[1m'
    GREY='\033[1;30m'
    LIGHT_GREY='\033[2;37m' #'\033[0;37m'
    ITALICS='\033[3m'
  else
    NOFORMAT=''
    RED=''
    GREEN=''
    ORANGE=''
    BLUE=''
    PURPLE=''
    PURPLE_BOLD=''
    GREEN_BOLD=''
    CYAN=''
    YELLOW=''
    BOLD=''
    GREY=''
    LIGHT_GREY=''
    ITALICS=''
  fi
}


display_command() {
  local timestamp=$(date '+%F %T %Z')
  local QUIET="$QUIET"

  if [[ $QUIET != true ]]; then
    echo -e "[${timestamp}] You called $PROGNAME as follow: $BINDIR/$PROGNAME $@ "
  else
    if [[ $SAVE_LOG == true ]]; then
      echo -e "[${timestamp}] You called $PROGNAME as follow: $BINDIR/$PROGNAME $@ " >>"$LOG" # Save to log file
    fi
  fi
}



msg_ns() { #no stamp
  echo -e "${1-}" 
}



msg() {
  local timestamp=$(date '+%F %T %Z')
  local message="${1-}"
  local QUIET="$QUIET"
  local SAVE_LOG="$SAVE_LOG"
  local LOG="$LOG"


  if [[ $QUIET == true && $SAVE_LOG == false ]]; then
    # Do nothing
    return
  
  elif [[ $QUIET == true && $SAVE_LOG == true ]]; then
    echo -e "[${timestamp}] ${message}" >>"$LOG"  
  
  elif [[ $QUIET != true && $SAVE_LOG == true ]]; then
    echo -e "[${timestamp}] ${message}" >&2  # Only send to stderr, tee will log it 
  
  elif [[ $QUIET != true && $SAVE_LOG == false ]]; then
    echo -e "[${timestamp}] ${message}"
  
  fi
}



# stderr to the log file
log() { #no stdout
  local message="${1-}"
  local SAVE_LOG="$SAVE_LOG"
  local LOG="$LOG"
  local timestamp=$(date '+%F %T %Z')

  if [ "$SAVE_LOG" = true ]; then
    echo -e "[${timestamp}] ${message}" >>"$LOG"
  fi
}




# Wrap long lines every n letters, wrap only on white space -s

msg_wp() { #wrap text with indentation
  local spaces="                               " # Space to indent wrapped lines
  local n_characters=120

  # Use fold to wrap lines based on the terminal width
  echo -e "${1-}" | fold -w $n_characters -s | sed "1!s/^/$spaces/"
}

msg_wp_ni() { #wrap text. no indentation
  local n_characters=120

  # Use fold to wrap lines based on the terminal width
  echo -e "${1-}" | fold -w $n_characters -s 
}


msg_wp_dyn() { #Wrap text dynamically according to the terminal width
  local terminal_width=$(tput cols) # current width of the terminal using tput
  local spaces="                               " 
  
  # Subtract a margin from the terminal width for better formatting
  local margin=2
  local n_characters=$((terminal_width - margin))

  # Ensure minimum width is sensible (to avoid negative or very small width)
  if [[ $n_characters -lt 40 ]]; then
    n_characters=40
  fi

  # Use fold to wrap lines based on the dynamic terminal width
  echo -e "${1-}" | fold -w $n_characters -s | sed "1!s/^/$spaces/"
}


die() {
  local message="${1-}"
  local code=${2-1}          # default exit status 1
  local RED="$RED"           #"\033[0;31m"
  local NOFORMAT="$NOFORMAT" #"\033[0m"

  # echo >&2 -e $RED"\nError: ${1-}\n"$NOFORMAT
  echo -e "${RED}\nError: ${message}\n${NOFORMAT}" >&2
  exit $code
}



warn() {
  local timestamp=$(date "+%F %T %Z")
  local message="${1-}"
  local YELLOW="$YELLOW"
  local QUIET="$QUIET"

  if [[ $QUIET != true ]]; then
    echo >&2 -e "[${timestamp}]${YELLOW} [WARNING]: ${message}${NOFORMAT}"
  fi
}



warn_ns() {
  local message="${1-}"
  local YELLOW="$YELLOW"

  echo >&2 -e "${YELLOW}[WARNING]: ${message}${NOFORMAT}"
}



WELCOMEMSG() {
  local PROGNAME=$PROGNAME
  local VERSION=$VERSION
  
  msg "You use ${PROGNAME} version ${VERSION}" 2>/dev/null || true 
}



to_lowercase() {
  local value="${1-}"

  if [[ "${BASH_VERSION}" =~ ^4 ]]; then
    echo "${value,,}"
  else
    echo "$value" | tr '[:upper:]' '[:lower:]'
  fi
}



bannerColor() { #MIT License https://github.com/yousefvand/shellman
  # Usage: bannerColor "my title" "black" "#" 
  case ${2} in
    black) color=0
    ;;
    red) color=1
    ;;
    green) color=2
    ;;
    yellow) color=3
    ;;
    blue) color=4
    ;;
    magenta) color=5
    ;;
    cyan) color=6
    ;;
    white) color=7
    ;;
    *) echo "color is not set"; exit 1
    ;;
  esac

  local msg="${3} ${1} ${3}"
  local edge
  edge=${msg//?/$3}
  tput setaf ${color}
  tput bold
  echo "${edge}"
  echo "${msg}"
  echo "${edge}"
  tput sgr 0
  echo
}



progressBar() { # Modified from https://github.com/yousefvand/shellman # MIT License 
  # Usage: progressBar "message" currentStep totalSteps

  local bar='████████████████████'
  local space='....................'
  local wheel=('\' '|' '/' '-')

  local msg="${1}"
  local current=${2}
  local total=${3}
  local wheelIndex=$((current % 4))
  local position=$((100 * current / total))
  local barPosition=$((position / 5))
  local fullBarLength=20  # Length of the progress bar
  local barPosition=$((position / 5))

  # Ensure barPosition does not exceed fullBarLength
  if [[ $barPosition -gt $fullBarLength ]]; then
    barPosition=$fullBarLength
  fi

  # Ensure that fullBarLength - barPosition does not go negative
  local spaceLength=$((fullBarLength - barPosition))
  if [[ $spaceLength -lt 0 ]]; then
    spaceLength=0
  fi

  if [[ $position -ge 100 ]]; then
    # Print progress bar and move to the next line
    echo -e "\r|${bar:0:$fullBarLength}$(tput dim)${space:0:$((fullBarLength - barPosition))}$(tput sgr0)| ${wheel[wheelIndex]} 100% [${msg}]"
  else
    # Print progress bar as usual
    echo -ne "\r|${bar:0:$barPosition}$(tput dim)${space:0:$((fullBarLength - barPosition))}$(tput sgr0)| ${wheel[wheelIndex]} ${position}% [${msg}] "
  fi
}



is_number() {
  [[ "$1" =~ ^[0-9]+([.][0-9]+)?$ ]]
}



remove_trailing_slash() {
  local path=${1-}

  if [[ ${path: -1} == "/" ]]; then
    path=$(echo $path | sed 's/\(.*\)\//\1 /')
  fi
  echo $path
}



realpath() {
  local target="$1"
  local dir
  
  # Check if file exists, either as a regular file or symlink
  if [ ! -e "$target" ] && [ ! -L "$target" ]; then
      die "Unable to locate: ${target} - it does not exist." 
  fi
  
  # Check if it's a broken symlink
  if [ -L "$target" ] && [ ! -e "$target" ]; then
      die "Unable to locate: ${target} - it is a broken symlink." 
  fi

  # Check if the realpath command is available
  if command -v realpath >/dev/null 2>&1; then
    command realpath "$target"
    return $?
  fi

  # Fallback to resolving physical location using readlink -f
  if command -v readlink >/dev/null 2>&1 && readlink -f "$target" >/dev/null 2>&1; then
    readlink -f "$target"
    return $?
  fi

  # if not available, fallback to using pwd -P
  # Get the directory and target name
  if [[ -d "$target" ]]; then
    dir=$(cd "$target" && pwd -P)
  else
    dir=$(cd "$(dirname "$target")" && pwd -P)
  fi

  # Print the real path
  echo "$dir/$(basename "$target")"
}



# Check if files exist

check_files() {
  for file in "${files[@]}"; do
    if [ ! -f "$file" ]; then
      return 1
    fi
  done
  return 0
}



# Check if a given path is absolute

check_full_path() {
  local path="$1"

  if [[ "$path" == /* ]]; then
    :
  else
    die "Path $path is not absolute. Please provide full path"
  fi
}



# Check the given sample name
validate_name() {
  local name="$1" # quotes without curly brackets to maintain trailing spaces if any

  # Check if the name consists of only alphanumeric characters and underscores
  # Allowed: alphanumeric characters, underscores, dots, dashes (but not at start or end, and not consecutive)
  if [[ ! "$name" =~ ^[a-zA-Z0-9]+([-._][a-zA-Z0-9]+)*$ ]]; then
    die "Invalid sample name '$name'. 
    Allowed are alphanumeric characters and underscores."
  fi

  # Check if the name is entirely numeric
  if [[ "$name" =~ ^[0-9]+$ ]]; then
    die "Sample name cannot be entirely numeric: $name"

  fi
}



# Function to check MD5 checksum
calc_md5_sum() {
    local file="${1}"
    
    # Ensure that checksum tools are installed (md5sum)
    command -v md5sum >/dev/null 2>&1 || 
    die "md5sum is required but it's not installed. Aborting."

    # Ensure the file exists
    if [[ ! -f "$file" ]]; then
      die "File does not exist: $file"
    fi

    md5sum "$file" | awk '{ print $1 }'
}



check_compression_type() {
  #https://stackoverflow.com/questions/13044562/python-mechanism-to-identify-compressed-file-type-and-uncompress
  local filename="$1"
  local expected_type="$2"

  # Ensure the file exists
  if [[ ! -f "$filename" ]]; then
    die "File does not exist: $filename"
  fi

  # Define the magic bytes for each compression type
  declare -A magic_dict=(
    ["gz"]="1f8b08"
    ["bz2"]="425a68"
    ["zip"]="504b0304"
  )

  # Read the first few bytes from the file
  local max_len=0
  for magic_bytes in "${magic_dict[@]}"; do
    local len=${#magic_bytes}
    if ((len > max_len)); then
      max_len=$len
    fi
  done
  local file_start=$(xxd -p -l "$max_len" "$filename")

  if [[ -z "$file_start" ]]; then
    die "Failed to read file or file is empty: $filename"
  fi

  # Determine the compression type based on the magic bytes
  local compression_type="plain"
  for file_type in "${!magic_dict[@]}"; do
    local magic_bytes=${magic_dict[$file_type]}
    if [[ "${file_start:0:${#magic_bytes}}" == "$magic_bytes" ]]; then
      compression_type="$file_type"
    fi
  done

  # Check for unsupported compression types
  if [[ "$compression_type" == "bz2" ]]; then
    die "cannot use bzip2 format - use gzip instead: $filename"
  elif [[ "$compression_type" == "zip" ]]; then
    die "cannot use zip format - use gzip instead: $filename"
  fi

  # Check if the detected compression type matches the expected type
  if [[ "$compression_type" == "$expected_type" ]]; then
    msg "compression type: $compression_type"
  else
    die "Please verify the compression of the file: $filename
    - Fasta must be uncompressed
    - Fastq must be compressed in gz format"
  fi

}



# Check if symbolic links exists & points to a valid target
# This function takes an array of file paths as input and
# checks if each path corresponds to a functional symbolic link

check_symbolic_links() {
  local path=("$@")

  if [[ -L "$path" && (-e "$path" || -f "$path") ]]; then
    log "symbolic link is functional: $path"
  else
    target_path=$(readlink -f "$path")
    die "Corrupt symbolic link. Perhaps the target file does not exist. 
    Please check this sample: $target_path"
  fi

}



# Sample table in CSV format
check_duplicate_sample_names() {
  local SAMPLEFILE="$1"

  # Check if the provided argument is a file
  if [[ -f "$SAMPLEFILE" ]]; then
    # If it's a file, process it
    local duplicates
    duplicates=$(csvtk cut -f sample "$SAMPLEFILE" | csvtk del-header | sort | uniq -d) || {
      die "Failed to check for duplicates in the sample file"
    }
  else
    # If it's not a file, read from stdin
    local duplicates
    duplicates=$(csvtk cut -f sample - | csvtk del-header | sort | uniq -d) || {
      die "Failed to check for duplicates from stdin"
    }

    # Print stdin to stdout if no duplicates found
    if [[ -z "$duplicates" ]]; then
      cat -
    fi

  fi

  # Check for duplicates and report
  if [[ -n "$duplicates" ]]; then
    die "The following samples are duplicated in the sample table:\n$duplicates \nCheck if samples are formatted correctly: '$SAMPLEFILE'!"
  else
    :
  fi
}



# Check conda prefix

# check_conda_prefix() {
#   if [ -z "$CONDAENVS" ]; then
#     warn "Conda prefix parameter is not set - 
#     will install conda envs here: ${DEFAULT_CONDAENVS}"
    
#     CONDAENVS="$DEFAULT_CONDAENVS"
#   fi
# }



# Function to check if a directory exists and is accessible
# check_directory_accessibility() {
#     local DIR="$1"  # Get the directory path from the first argument

#     # Check if the directory exists
#     if [ ! -d "$DIR" ]; then
#         echo "The directory '$DIR' does not exist."
#         return 1  # Return a non-zero status indicating failure
#     fi

#     # Check if the specified directory is accessible
#     if [ -r "$DIR" ] && [ -w "$DIR" ] && [ -x "$DIR" ]; then
#         echo "The directory '$DIR' is accessible."
#     else
#         echo "The directory '$DIR' is not accessible."
#     fi
# }


# # Export all defined functions
# for func in $(declare -F | awk '{print $3}'); do
#     export -f "$func"
# done