[![stablity](https://img.shields.io/badge/Stability-Beta%20release-orange.svg?style=flat-square)](https://gitlab.com/FLI_Bioinfo/clostyper/-/releases) 
[![License](https://img.shields.io/badge/License-BSD_3--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) 
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.14859379.svg)](https://doi.org/10.5281/zenodo.14859379)
[![Snakemake](https://img.shields.io/badge/Snakemake-≥8.20-brightgreen.svg)](https://snakemake.github.io)[![release](https://gitlab.com/FLI_bioinfo/clostyper/-/badges/release.svg)](https://gitlab.com/FLI_Bioinfo/clostyper/-/releases)
[![Pipeline status](https://gitlab.com/FLI_bioinfo/clostyper/badges/master/pipeline.svg)]()
[![os](https://img.shields.io/badge/OS-Linux|WSL-brightgreen)]()
[![doc](https://img.shields.io/badge/Documentation%20-available-brightgreen)]()
[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1yCkyd_NGS6p6otyeZVQYdtnovifw3gk9)

[<img src="misc/clostyper_logo.jpg" width="1000"></p>](https://www.fli.de/en/institutes/institute-of-bacterial-infections-and-zoonoses-ibiz/labsworking-groups/working-group-clostridia/) 


<div align="center" style="font-size: 24px;">
    <strong><span style="font-size: 24px;">Quick links: </span></strong>
    <a href="https://gitlab.com/FLI_Bioinfo/clostyper/-/wikis/1_description">Description</a> •
    <a href="https://gitlab.com/FLI_Bioinfo/clostyper/-/wikis/2_installation">Installation</a> •
    <a href="https://gitlab.com/FLI_Bioinfo/clostyper/-/wikis/3-2_quick_start">Quick Start</a> •
    <a href="https://gitlab.com/FLI_Bioinfo/clostyper/-/wikis/home">Documentation</a> 
</div>

<br>

👉 **This is a work in progress (WIP)** 
<br>

[![---------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/colored.png)](#description)

## Description

**Clostyper** is a command-line tool to analyse _Clostridia_ genomes. It focuses on centain pathogenic _Clostridia_ . So far it covers _Clostridioides difficile_ and a few aspects of _Clostridium perfringens_. Currently ``Clostyper`` has modules for general characterization and genotyping as well as specific analysis (with customized databases) for the entitled species  


![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/colored.png)

## Documentation

- For more information about ``Clostyper``, installation and usage instructions, visit the 📖 [**Clostyper wiki page**](https://gitlab.com/FLI_Bioinfo/clostyper/-/wikis/home)
- To review Clostyper results, see this 📰 [**Example report**](https://mostafaabdelglil.gitlab.io/reports/report.html)

![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/colored.png)

## Getting Help

If you encounter any issues, please feel free to report them in the Clostyper [Issue page](https://gitlab.com/FLI_Bioinfo/clostyper/-/issues). Alternatively, you can send an email to clostyper@gmail.com or use this [contact form](https://www.fli.de/de/kontakt/kontakt-zu-einer-person/mostafa-abdel-glil/). 


![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/colored.png)

## License

[BSD 3-Clause license](https://opensource.org/license/bsd-3-clause/) - Copyright © 2021-Present Mostafa Abdel-Glil

